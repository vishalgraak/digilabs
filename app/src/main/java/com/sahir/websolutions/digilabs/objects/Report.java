package com.sahir.websolutions.digilabs.objects;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by SWS-PC10 on 5/25/2017.
 */

public class Report implements Serializable {
    private String video;
    private String name;
    public String member_name;
    private String user_id;
    private int flag; // 0 -empty // 1 -filled
    private String id;
    public ArrayList<String> member_list = new ArrayList<>();
    public ArrayList<String> image_list = new ArrayList<>();
    public String report_type;
    public String lab_report = "1";
    public String lab_id="";
    public String my_report = "0";
    public String getLab_id() {
        return lab_id;
    }

    public void setLab_id(String lab_id) {
        this.lab_id = lab_id;
    }



    public String getReportsList() {
        return reportsList;
    }

    public void setReportsList(String reportsList) {
        this.reportsList = reportsList;
    }

    private String reportsList;

    public String getLab_report() {
        return lab_report;
    }

    public void setLab_report(String lab_report) {
        this.lab_report = lab_report;
    }

    public String getMy_report() {
        return my_report;
    }

    public void setMy_report(String my_report) {
        this.my_report = my_report;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    private String date;
    private String isLocal;

    public String getIsLocal() {
        return isLocal;
    }

    public void setIsLocal(String isLocal) {
        this.isLocal = isLocal;
    }

    public String getIsstar() {
        return isstar;
    }

    public void setIsstar(String isstar) {
        this.isstar = isstar;
    }

    private String isstar;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    //only for local
    private String path;
    private String type;    //camera, gallery , pdf

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
