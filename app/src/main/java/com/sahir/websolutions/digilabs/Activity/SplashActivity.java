package com.sahir.websolutions.digilabs.Activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import com.sahir.websolutions.digilabs.R;

/**
 * Created by Sys9 on 6/19/2017.
 */

public class SplashActivity extends Activity {
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;
    SharedPreferences prefs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ActivityStack.activity.add(SplashActivity.this);

        prefs = getSharedPreferences(getString(R.string.user_details), MODE_PRIVATE);

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                if (prefs.getString(getString(R.string.user_id), null) != null) {
                    startActivity(new Intent(SplashActivity.this, PreviewActivity.class)
                            .putExtra("home", "yes"));
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("isFirst", "yes");
                    editor.putString("isFirstP", "yes");
                    editor.commit();
                } else {
                    startActivity(new Intent(SplashActivity.this, RegisterActivity.class)
                            .putExtra("intent", "register"));
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("isFirst", "yes");
                    editor.putString("isFirstP", "yes");
                    editor.commit();
                }
                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
