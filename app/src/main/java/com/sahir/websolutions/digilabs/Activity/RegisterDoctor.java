package com.sahir.websolutions.digilabs.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sahir.websolutions.digilabs.HelperClass.Helper;
import com.sahir.websolutions.digilabs.R;
import com.sahir.websolutions.digilabs.objects.GpsTracker;
import com.sahir.websolutions.digilabs.objects.StatesModel;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by safal bhatia on 7/19/2017.
 */

public class RegisterDoctor extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    Toolbar toolbar;
    LinearLayout facebookSignInButton, googleSignInButton;
    EditText editTextFirstName, editTextLastName, editTextEmail, editTextPhone, editTextPin,
            edtCity, editTextAddress, editTextName;
    private static final int RC_SIGN_IN = 9001;
    GoogleApiClient mGoogleApiClient;
    public static String phoneNumber;
    ImageView ivBubble;
    TextView txtSelectedGender, txtSelectedDob, textViewOR, txt_anim1, textViewGoogle, textViewFb,
            txt_terms, txt_privacy, txtSelectedState, txt_doctor, mTitle, txt_salutation;
    LinearLayout btnDob, btnGender, ll_or, btnState;
    Button buttonRegister, buttonLogin;
    SharedPreferences pref;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    CallbackManager callbackManager;
    RelativeLayout rl_bubble;
    LinearLayout llAnimation, linearLayoutAOI;
    String firstName = "", lastName = "", email = "", dob = "", userId = "", gender = "", clinicId = "",
            pin, role = "3", address = "", city = "", lat = "", lng = "", clinicName = "", state,
            title = "Dr.", device_token = "";
    int state_id = 0;
    ArrayList<StatesModel> statesModelArrayList = new ArrayList<>();
    String[] arrayStates;
    CheckBox checkbox, checkbox_doc;
    ProgressDialog progressDialog;
    GpsTracker gpsTracker;
    Activity sActivity;
    String Keyhash;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());

        setContentView(R.layout.activity_register_doctor);
        sActivity = RegisterDoctor.this;
        init();

        onclick();

        animation();

        showHashKey();

        setFont();

        getData();

        if (!Utils.isNetworkConnected(RegisterDoctor.this)) {
            Utils.conDialog(RegisterDoctor.this);
        } else {
            getStates();
        }

        if (Build.VERSION.SDK_INT >= 23) {
            AllowPermissions();
        } else {
            gpsTracker = new GpsTracker(RegisterDoctor.this);
            if (gpsTracker.canGetLocation()) {
                lat = String.valueOf(gpsTracker.latitude);
                lng = String.valueOf(gpsTracker.longitude);
            } else {
                showGPSDisabledAlertToUser();
            }
        }
    }

    public void getData() {
        if (getIntent().getStringExtra("intent") != null) {
            if (getIntent().getStringExtra("intent").equals("update")) {
                txt_doctor.setVisibility(View.GONE);
                userId = pref.getString(getString(R.string.user_id), null);
                clinicId = pref.getString("clinic_id", null);
                firstName = pref.getString("fname", null);
                lastName = pref.getString("lname", null);
                email = pref.getString("email", null);
                phoneNumber = pref.getString("phone", null);
                pin = pref.getString("pin", null);
                gender = pref.getString("gender", null);
                dob = pref.getString("dob", null);
                address = pref.getString("address", null);
                clinicName = pref.getString("clinic", null);
                state = pref.getString("state", null);
//                state_id = Integer.parseInt(pref.getString("state_id", null));
                city = pref.getString("city", null);
                toolbar.setBackgroundColor(Color.parseColor("#06B4BF"));
                mTitle.setText("PROFILE");
                mTitle.setTypeface(Utils.setTypeface(this));
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
                ll_or.setVisibility(View.GONE);
                buttonLogin.setVisibility(View.GONE);
                facebookSignInButton.setVisibility(View.GONE);
                linearLayoutAOI.setVisibility(View.VISIBLE);
                buttonRegister.setText("Update");
                buttonRegister.setBackgroundResource(R.drawable.square_solid_blue_border);
                editTextPhone.setEnabled(false);
                editTextPin.setEnabled(false);
                editTextPhone.setTextColor(Color.parseColor("#757575"));
                editTextPin.setTextColor(Color.parseColor("#757575"));
                editTextFirstName.setText(firstName);
                editTextLastName.setText(lastName);
                editTextEmail.setText(email);
                editTextPhone.setText(phoneNumber);
                editTextPin.setText(pin);
                getProfile();
                if (!gender.equals("")) {
                    txtSelectedGender.setText(gender);
                }
                if (!dob.equals("")) {
                    txtSelectedDob.setText(dob);
                }
                editTextName.setText(clinicName);
                editTextAddress.setText(address);
                edtCity.setText(city);
                if (state != null) {
                    txtSelectedState.setText(state);
                }
                checkbox.setChecked(true);
            } else {
                mTitle.setText("SIGNUP");
                mTitle.setTypeface(Utils.setTypeface(this));
            }
        }
    }

    public void onclick() {

        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });

        txt_doctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Helper.setCheck("normal",sActivity);
                finish();
            }
        });

        txt_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterDoctor.this, TermsActivity.class)
                        .putExtra("type", "terms"));
            }
        });

        txt_privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterDoctor.this, TermsActivity.class)
                        .putExtra("type", "privacy"));
            }
        });


        btnState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showStatesList(arrayStates);
            }
        });

        btnDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
                DatePickerDialog dialog = new DatePickerDialog(RegisterDoctor.this, RegisterDoctor.this,
                        calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));
                dialog.show();
            }
        });


        btnGender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectGender();
            }
        });


        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Helper.setCheck("doctor",sActivity);
                email = editTextEmail.getText().toString().trim();
                if (editTextFirstName.length() == 0) {
                    editTextFirstName.setError("FirstName empty!");
                    editTextFirstName.requestFocus();
                } else if (editTextEmail.length() == 0) {
                    editTextEmail.setError("Email empty!");
                    editTextEmail.requestFocus();
                } else if (!email.matches(emailPattern)) {
                    editTextEmail.setError("Incorrect email format!");
                    editTextEmail.requestFocus();
                } else if (editTextPhone.length() == 0) {
                    editTextPhone.setError("Phone number empty!");
                    editTextPhone.requestFocus();
                } else if (editTextPhone.length() != 10) {
                    editTextPhone.setError("Phone number should be of 10 digits!");
                    editTextPhone.requestFocus();
                } else if (editTextPin.length() == 0) {
                    editTextPin.setError("Pin number empty!");
                    editTextPin.requestFocus();
                } else if (editTextPin.length() < 6) {
                    editTextPin.setError("Invalid pin");
                    editTextPin.requestFocus();
                } else if (editTextAddress.length() == 0) {
                    editTextAddress.setError("Address empty!");
                    editTextAddress.requestFocus();
                } else if (txtSelectedState.getText().toString().equals("Select State")) {
                    Snackbar.make(ll_or, "Please select state.", BaseTransientBottomBar.LENGTH_SHORT).show();
                } else if (edtCity.length() == 0) {
                    edtCity.setError("City empty!");
                    edtCity.requestFocus();
                } else if (!checkbox.isChecked()) {
                    Snackbar.make(ll_or, "Please agree to terms and conditions.", BaseTransientBottomBar.LENGTH_SHORT).show();
                } else {
                    if (!Utils.isNetworkConnected(RegisterDoctor.this)) {
                        Utils.conDialog(RegisterDoctor.this);
                    } else {
                        firstName = editTextFirstName.getText().toString().trim();
                        lastName = editTextLastName.getText().toString().trim();
                        email = editTextEmail.getText().toString().trim();
                        phoneNumber = editTextPhone.getText().toString().trim();
                        pin = editTextPin.getText().toString().trim();
                        address = editTextAddress.getText().toString().trim();
                        city = edtCity.getText().toString().trim();
                        if (editTextName.length() != 0) {
                            clinicName = editTextName.getText().toString().trim();
                        }
                        if (editTextLastName.length() == 0) {
                            lastName = "";
                        }
                        progressDialog.show();
                        if (pref.getString("email", null) != null) {
                            if (pref.getString("email", null).equals(email)) {
                                if (buttonRegister.getText().toString().equals("Update")) {
                                    String url = "users/editDoctorapi/" + userId + "?";
                                    sendToServer(url);
                                }
                            } else {
                                checkEmail(email);
                            }
                        } else {
                            checkEmail(email);
                        }
                    }
                }
            }
        });

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterDoctor.this, LoginActivity.class));
                Helper.setCheck("doctor",sActivity);
            }
        });


        facebookSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Utils.isNetworkConnected(RegisterDoctor.this)) {
                    Utils.conDialog(RegisterDoctor.this);
                } else {
                    loginToFacebook();
                }
            }
        });

        googleSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.isNetworkConnected(RegisterDoctor.this)) {
                    Utils.conDialog(RegisterDoctor.this);
                } else {
                    signInWithGoogle();
                }
            }
        });
    }

    private void animation() {
        final Animation fade_in = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        final Animation fade_out = AnimationUtils.loadAnimation(this, R.anim.fade_out);
        final Animation fade_in1 = AnimationUtils.loadAnimation(this, R.anim.fade_in);

        txt_anim1.setText(getString(R.string.hi_andy));
        txt_anim1.startAnimation(fade_in);

        fade_in.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                final Handler handler = new Handler();
                new Thread(new Runnable() {
                    public void run() {
                        try {
                            Thread.sleep(2000);
                        } catch (Exception e) {
                        }
                        handler.post(new Runnable() {
                            public void run() {
                                txt_anim1.startAnimation(fade_out);
                            }
                        });
                    }
                }).start();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        fade_out.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (getIntent().getStringExtra("intent") != null) {
                    if (getIntent().getStringExtra("intent").equals("update")) {
                        txt_anim1.setText("Please Update Your Profile");
                    } else {
                        txt_anim1.setText(getString(R.string.register));
                    }
                }
                txt_anim1.startAnimation(fade_in1);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public void init() {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // ATTENTION: This "addApi(AppIndex.API)"was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                //     .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addApi(AppIndex.API).build();

        ActivityStack.activity.add(RegisterDoctor.this);
        progressDialog = new ProgressDialog(RegisterDoctor.this);
        progressDialog.setMessage("Processing...");
        btnGender = (LinearLayout) findViewById(R.id.btnGender);
        txtSelectedGender = (TextView) findViewById(R.id.btnSelectedGender);
        btnDob = (LinearLayout) findViewById(R.id.btnDob);
        btnState = (LinearLayout) findViewById(R.id.btnState);
        checkbox = (CheckBox) findViewById(R.id.checkbox);
        checkbox_doc = (CheckBox) findViewById(R.id.checkbox_doc);
        txt_anim1 = (TextView) findViewById(R.id.txt_anim1);
        txtSelectedDob = (TextView) findViewById(R.id.btnSelectedDob);
        txtSelectedState = (TextView) findViewById(R.id.txtSelectedState);
        textViewOR = (TextView) findViewById(R.id.textViewOR);
        textViewGoogle = (TextView) findViewById(R.id.textViewGoogle);
        textViewFb = (TextView) findViewById(R.id.textViewFb);
        txt_terms = (TextView) findViewById(R.id.txt_terms);
        txt_privacy = (TextView) findViewById(R.id.txt_privacy);
        txt_doctor = (TextView) findViewById(R.id.txt_doctor);
        txt_salutation = (TextView) findViewById(R.id.txt_salutation);
        editTextPin = (EditText) findViewById(R.id.editTextPin);
        linearLayoutAOI = (LinearLayout) findViewById(R.id.linearLayoutAOI);
        ll_or = (LinearLayout) findViewById(R.id.ll_or);
        editTextFirstName = (EditText) findViewById(R.id.editTextFirstName);
        editTextLastName = (EditText) findViewById(R.id.editTextLastName);
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPhone = (EditText) findViewById(R.id.editTextPhone);
        edtCity = (EditText) findViewById(R.id.edtCity);
        editTextAddress = (EditText) findViewById(R.id.editTextAddress);
        editTextName = (EditText) findViewById(R.id.editTextName);
        facebookSignInButton = (LinearLayout) findViewById(R.id.facebook_sign_in_button);
        buttonRegister = (Button) findViewById(R.id.buttonRegister);
        buttonLogin = (Button) findViewById(R.id.buttonLogin);

        setTitle("");
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("SIGNUP");
        callbackManager = CallbackManager.Factory.create();
        pref = getSharedPreferences(getString(R.string.user_details), MODE_PRIVATE);

        SharedPreferences token_pref = getApplicationContext().getSharedPreferences("Token", MODE_PRIVATE);
        device_token = token_pref.getString("regId", null);
        userId = pref.getString(getString(R.string.user_id), null);
        firstName = pref.getString("fname", null);
        lastName = pref.getString("lname", null);
        email = pref.getString("email", null);
        phoneNumber = pref.getString("phone", null);
        pin = pref.getString("pin", null);

        ivBubble = (ImageView) findViewById(R.id.ivBubble);
        llAnimation = (LinearLayout) findViewById(R.id.llAnimation);
        rl_bubble = (RelativeLayout) findViewById(R.id.rl_bubble);

        googleSignInButton = (LinearLayout) findViewById(R.id.google_sign_in_button);
    }


    private void setFont() {
        editTextFirstName.setTypeface(Utils.setTypeface(this));
        editTextLastName.setTypeface(Utils.setTypeface(this));
        editTextPhone.setTypeface(Utils.setTypeface(this));
        editTextEmail.setTypeface(Utils.setTypeface(this));
        editTextPin.setTypeface(Utils.setTypeface(this));
        buttonRegister.setTypeface(Utils.setTypeface(this));
        textViewOR.setTypeface(Utils.setTypeface(this));
        textViewFb.setTypeface(Utils.setTypeface(this));
        textViewGoogle.setTypeface(Utils.setTypeface(this));
        txt_anim1.setTypeface(Utils.setTypeface(this));
        txtSelectedDob.setTypeface(Utils.setTypeface(this));
        txtSelectedGender.setTypeface(Utils.setTypeface(this));
        txt_terms.setTypeface(Utils.setTypeface(this));
        txt_privacy.setTypeface(Utils.setTypeface(this));
        txtSelectedState.setTypeface(Utils.setTypeface(this));
        txt_doctor.setTypeface(Utils.setTypeface(this));
        txt_salutation.setTypeface(Utils.setTypeface(this));
        edtCity.setTypeface(Utils.setTypeface(this));
        editTextAddress.setTypeface(Utils.setTypeface(this));
        editTextName.setTypeface(Utils.setTypeface(this));
    }


    private void signInWithGoogle() {
        final Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
            /*if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                handleSignInResult(result);
//                firebaseAuthWithGoogle(account);
            } else {
                // Google Sign In failed, update UI appropriately
                // ...
            }*/
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("TAG!", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            if (acct.getDisplayName() != null) {
                firstName = acct.getGivenName();
                lastName = acct.getFamilyName();
                email = acct.getEmail();
                updateUI(firstName, lastName, email);
            }

        } else {
            // Signed out, show unauthenticated UI.
            //  updateUI(false);
            Toast.makeText(this, "Login unsuccessful!", Toast.LENGTH_SHORT).show();
        }
    }

    private void updateUI(String firstName, String lastName, String email) {

        if (lastName == null)
            lastName = "";

        editTextFirstName.setText(firstName);
        editTextLastName.setText(lastName);
        editTextEmail.setText(email);
        editTextFirstName.setError(null);
        editTextLastName.setError(null);
        editTextEmail.setError(null);
    }


    // *********************************************Facebook Login**********************************
    public void loginToFacebook() {
        if (AccessToken.getCurrentAccessToken() != null) {
            Log.i("Fb Access Token: ", "" + AccessToken.getCurrentAccessToken().getToken());
            RequestData();
        }

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException exception) {
                    }
                });

    }

    public void RequestData() {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                JSONObject json = response.getJSONObject();
                Log.i("JSON: ", "" + json);
                try {
                    if (json != null) {
                        String text = "<b>Name :</b> " + json.getString("name") + "<br><br><b>Email :</b> " + json.getString("email") + "<br><br><b>Profile link :</b> " + json.getString("link");
                        Log.i("Response", "data: " + text);
                        JSONObject jobj = json.getJSONObject("picture");
                        JSONObject data = jobj.getJSONObject("data");
                        String url = data.getString("url");
                        Log.i("Image Response", "" + url);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email,picture.type(large)");
        request.setParameters(parameters);
        request.executeAsync();
    }

    public void sendToServer(String url) {
        FetchData.getInstance(RegisterDoctor.this).getRequestQueue().add(new StringRequest
                (Request.Method.POST, Urls.URL_TO_WEB + url,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                progressDialog.dismiss();
                                final String response2 = response;
                                Log.i("Register Doctor", "" + response2);
                                try {
                                    JSONObject json = new JSONObject(response2);
                                    int status = json.getInt("status");
                                    if (status == 1) {
                                        if (json.has("clinics")) {
                                            JSONObject clinics = json.optJSONObject("clinics");
                                            clinicId = clinics.optString("id");
                                        }
                                        if (buttonRegister.getText().toString().equals("Update")) {
                                            saveUserDataSF(firstName, lastName, email, phoneNumber, pin,
                                                    gender, dob, userId, role, "yes", city, String.valueOf(state_id),
                                                    txtSelectedState.getText().toString(), address, clinicId, clinicName);
                                            Snackbar.make(buttonRegister, "Profile Updated Successfully.", BaseTransientBottomBar.LENGTH_SHORT).show();
                                            finishScreen();
                                        } else {
                                            JSONObject data = json.optJSONObject("data");
                                            if (data.has("user")) {
                                                JSONObject ob1 = data.getJSONObject("user");
                                                userId = ob1.optString("id");
                                            } else {
                                                userId = data.optString("id");
                                            }
                                            if (Build.VERSION.SDK_INT >= 23) {
                                                AllowSMSPermissions();
                                            } else {
                                                startActivity(new Intent(RegisterDoctor.this, PreviewActivity.class)
                                                        .putExtra("role", role)
                                                        .putExtra("home", "yes"));
                                                saveUserDataSF(firstName, lastName, email, phoneNumber, pin,
                                                        gender, dob, userId, role, "yes", city, String.valueOf(state_id),
                                                        txtSelectedState.getText().toString(), address, clinicId, clinicName);
                                            Intent intent = new Intent(RegisterDoctor.this, OTPActivity.class);
                                            intent.putExtra("user_id", userId);
                                            intent.putExtra("firstname", firstName);
                                            intent.putExtra("lastname", lastName);
                                            intent.putExtra("email", email);
                                            intent.putExtra("phone", phoneNumber);
                                            intent.putExtra("pin", pin);
                                            intent.putExtra("role", role);
                                            intent.putExtra("gender", gender);
                                            intent.putExtra("dob", dob);
                                            intent.putExtra("city", city);
                                            intent.putExtra("state_id", state_id);
                                            intent.putExtra("address", address);
                                            intent.putExtra("clinic", clinicName);
                                            intent.putExtra("lat", lat);
                                            intent.putExtra("lng", lng);
                                            intent.putExtra("login", "no");
                                            intent.putExtra("register", "doctor");
                                            startActivity(intent);
                                            }
                                        }
                                    } else if (status == 2) {
                                        if (buttonRegister.getText().toString().equals("Update")) {
                                            finish();
                                        } else {
                                            startActivity(new Intent(RegisterDoctor.this, LoginActivity.class));
                                        }
                                    }
                                    Log.i("valuesAre", "" + userId);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                if (buttonRegister.getText().toString().equals("Update")) {
                    params.put("clinicid", clinicId);
                }
                params.put("title", title);
                params.put("firstname", firstName);
                params.put("lastname", lastName);
                params.put("username", phoneNumber);
                params.put("email", email);
                params.put("password", "");
                params.put("role", role);
                params.put("phone", phoneNumber);
                params.put("pin", pin);
                params.put("gender", gender);
                params.put("dob", dob);
                params.put("city", city);
                params.put("state_id", String.valueOf(state_id));
                params.put("address", address);
                params.put("clinicname", clinicName);
                params.put("latitude", lat);
                params.put("longitude", lng);
                params.put("is_active", "yes");
                if (device_token != null) {
                    params.put("devicetoken", device_token);
                }
                return params;
            }
        });
    }


    public void checkUser() {
        FetchData.getInstance(RegisterDoctor.this).getRequestQueue().add(new StringRequest
                (Request.Method.GET, Urls.URL_TO_WEB + "/users/checkUserApi/" + phoneNumber,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                final String response2 = response;
                                Log.d("valRes", response2);

                                try {
                                    JSONObject json = new JSONObject(response2);
                                    int status = json.getInt("status");
                                    if (status == 1) {
                                        if (json.has("data")) {
                                            JSONArray data = json.optJSONArray("data");
                                            for (int i = 0; i < data.length(); i++) {
                                                JSONObject jsonObject = data.optJSONObject(i);
                                                userId = jsonObject.optString("id");
                                            }
                                        }
                                        progressDialog.dismiss();
                                        showDialog();
                                    } else {
                                        String url = "users/addDoctorapi/";
                                        sendToServer(url);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                    }
                }) {
        });
    }

    public void checkEmail(final String email) {
        FetchData.getInstance(RegisterDoctor.this).getRequestQueue().add(new StringRequest
                (Request.Method.GET, Urls.URL_TO_WEB + "/users/checkUserEmailApi/" + email,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                final String response2 = response;
                                Log.d("valRes", response2);
                                try {
                                    JSONObject json = new JSONObject(response2);
                                    int status = json.getInt("status");
                                    if (status == 1) {
                                        progressDialog.dismiss();
                                        editTextEmail.setError("Email ID already exists!");
                                        editTextEmail.requestFocus();
                                    } else {
                                        if (buttonRegister.getText().toString().equals("Update")) {
                                            String url = "users/editDoctorapi/" + userId + "?";
                                            sendToServer(url);
                                        } else {
                                            checkUser();
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                    }
                }) {
        });
    }

    public void showDialog() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.setCancelable(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
       /* if (height >= 800) {
            lp.height = height / 2;
        } else if (height >= 1280) {
            lp.height = height / 2;
        } else if (height >= 1920) {
            lp.height = height / 2;
        } else if (height >= 2560) {
            lp.height = height / 2;
        }*/
        dialog.getWindow().setAttributes(lp);
        dialog.show();

        TextView txt = (TextView) dialog.findViewById(R.id.txt);
        Button tvlogin = (Button) dialog.findViewById(R.id.tvlogin);
        Button tvcancel = (Button) dialog.findViewById(R.id.tvcancel);
        Button tvreg = (Button) dialog.findViewById(R.id.tvreg);
        txt.setTypeface(Utils.setTypeface(this));
        tvcancel.setTypeface(Utils.setTypeface(this));
        tvlogin.setTypeface(Utils.setTypeface(this));
        tvreg.setTypeface(Utils.setTypeface(this));
        tvreg.setVisibility(View.GONE);

        tvlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RegisterDoctor.this, LoginActivity.class));
            }
        });

        tvcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        tvreg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterDoctor.this, RegisterUserToDoctor.class)
                        .putExtra("user_id", userId));
            }
        });
    }

    public void finishScreen() {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(500);
                } catch (Exception e) {
                }
                handler.post(new Runnable() {
                    public void run() {
                        finish();
                    }
                });
            }
        }).start();
    }

    public void saveUserDataSF(String fname, String lname, String email, String phone, String pin,
                               String gender, String dob, String userId, String role, String isActive,
                               String city, String state_id, String state, String address, String clinic_id, String clinicName) {
        SharedPreferences.Editor pInfo = pref.edit();
        pInfo.putString("title", title);
        pInfo.putString("fname", fname);
        pInfo.putString("lname", lname);
        pInfo.putString("email", email);
        pInfo.putString("phone", phone);
        pInfo.putString("pin", pin);
        pInfo.putString("gender", gender);
        pInfo.putString("dob", dob);
        pInfo.putString(getString(R.string.user_id), userId);
        pInfo.putString("role", role);
        pInfo.putString("isActive", isActive);
        pInfo.putString("isFirst", "yes");
        pInfo.putString("city", city);
        pInfo.putString("state_id", state_id);
        pInfo.putString("state", state);
        pInfo.putString("address", address);
        pInfo.putString("clinic_id", clinic_id);
        pInfo.putString("clinic", clinicName);
        pInfo.apply();
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        int tempYear = year;
        int tempMonth = month + 1;
        int tempDay = dayOfMonth;
        String monthTemp;
        String dayTemp;
        if (tempMonth < 10) {
            monthTemp = "0" + tempMonth;
        } else {
            monthTemp = String.valueOf(tempMonth);
        }
        if (tempDay < 10) {
            dayTemp = "0" + tempDay;
        } else {
            dayTemp = String.valueOf(tempDay);
        }

        txtSelectedDob.setText(new StringBuilder()
                // Month is 0 based so add 1
                .append(dayTemp).append("-").append(monthTemp).append("-").append(tempYear));
        //setStartDate = tvMedicineDatePicker.getText().toString();
        dob = dayTemp + "-" + monthTemp + "-" + tempYear;
        Log.d("datePicked", txtSelectedDob.getText().toString());
        txtSelectedDob.setError(null);
    }

    public void selectGender() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.gender_dialog);
        RadioGroup rgGender = (RadioGroup) dialog.findViewById(R.id.rgGender);
        Button btnDone = (Button) dialog.findViewById(R.id.btnDone);


        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtSelectedGender.setText(gender);
                dialog.dismiss();
            }
        });

        if (gender != null) {
            if (gender.equals("Male")) {
                ((RadioButton) dialog.findViewById(R.id.rbMale)).setChecked(true);
            } else {
                ((RadioButton) dialog.findViewById(R.id.rbFemale)).setChecked(true);
            }
        }
        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rbMale) {
                    gender = "Male";
                    txtSelectedGender.setText(gender);
                    dialog.dismiss();
                } else if (checkedId == R.id.rbFemale) {
                    gender = "Female";
                    txtSelectedGender.setText(gender);
                    dialog.dismiss();
                } else {
                    Toast.makeText(RegisterDoctor.this, "Please Select Gender", Toast.LENGTH_SHORT).show();
                }
            }
        });

        dialog.show();
    }


    public void getStates() {
        FetchData.getInstance(RegisterDoctor.this).getRequestQueue().add(new StringRequest
                (Request.Method.GET, Urls.URL_TO_WEB + "countries/viewapi/101",
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                final String response2 = response;
                                Log.d("valRes", response2);
                                try {
                                    JSONObject json = new JSONObject(response2);
                                    int status = json.getInt("status");
                                    if (status == 1) {
                                        JSONArray data = json.optJSONArray("data");
                                        arrayStates = new String[data.length()];
                                        for (int i = 0; i < data.length(); i++) {
                                            JSONObject jsonObject = data.optJSONObject(i);
                                            StatesModel statesModel = new StatesModel();
                                            statesModel.state_id = jsonObject.optInt("state_id");
                                            int aa = jsonObject.optInt("state_id");
                                            statesModel.state_name = jsonObject.optString("name");
                                            statesModel.country_id = jsonObject.optInt("country_id");
                                            statesModelArrayList.add(statesModel);
                                            arrayStates[i] = jsonObject.optString("name");
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                    }
                }) {
        });
    }


    public void showStatesList(final String[] list) {
        // Initializing a new alert dialog
        final AlertDialog.Builder builder = new AlertDialog.Builder(RegisterDoctor.this);

        // Set the alert dialog title
        builder.setTitle("Choose a State.");

        // Set a single choice items list for alert dialog
        builder.setSingleChoiceItems(list, // Items list
                -1, // Index of checked item (-1 = no selection)
                new DialogInterface.OnClickListener() // Item click listener
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // Get the alert dialog selected item's text
                        String selectedItem = list[i].toString();
                        txtSelectedState.setText(selectedItem);
                        state_id = statesModelArrayList.get(i).state_id;
                    }
                });

        // Set the a;ert dialog positive button
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Just dismiss the alert dialog after selection
                // Or do something now
            }
        });

        // Create the alert dialog
        AlertDialog dialog = builder.create();

        // Finally, display the alert dialog
        dialog.show();
    }

    // **********************************************Show Hashkey***********************************
    private void showHashKey() {
        // TODO Auto-generated method stub
//        try {
//            PackageInfo info = getPackageManager().getPackageInfo("com.example.sws_pc10.digilabs", PackageManager.GET_SIGNATURES); // Your
//            // here
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                Log.i("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//                System.out.println("KeyHash:: " + Base64.encodeToString(md.digest(), Base64.DEFAULT));
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        }
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Keyhash = Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.e("KeyHash:", Keyhash);
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {
        }
    }


    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Register Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        mGoogleApiClient.connect();
        AppIndex.AppIndexApi.start(mGoogleApiClient, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(mGoogleApiClient, getIndexApiAction());
        mGoogleApiClient.disconnect();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.skip, menu);

        MenuItem register = menu.findItem(R.id.next);
        register.setVisible(false);
        if (getIntent().getStringExtra("intent") != null) {
            if (getIntent().getStringExtra("intent").equals("update")) {
                MenuItem skip = menu.findItem(R.id.skip);
                skip.setVisible(false);
            } else {
            }
        }

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.skip:
                Helper.setSkip("login",sActivity);
                startActivity(new Intent(RegisterDoctor.this, PreviewActivity.class)
                        .putExtra("home", "yes")
                        .putExtra("role", "3"));
                return true;
        }
        return true;
    }

    private void AllowPermissions() {
        int hasLocationPermission = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION);
        int hasSMSPermission = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> permissions = new ArrayList<String>();
        if (hasLocationPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }

        if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }

        if (!permissions.isEmpty()) {
            ActivityCompat.requestPermissions(RegisterDoctor.this, permissions.toArray(new String[permissions.size()]), 101);
        } else {
            gpsTracker = new GpsTracker(RegisterDoctor.this);
            if (gpsTracker.canGetLocation()) {
                lat = String.valueOf(gpsTracker.latitude);
                lng = String.valueOf(gpsTracker.longitude);
            } else {
                showGPSDisabledAlertToUser();
            }
        }
    }


    private void AllowSMSPermissions() {
        int hasSMSPermission = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_SMS);
        int hasSMSReceive = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.RECEIVE_SMS);
        int hasSMSSend = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.SEND_SMS);

        List<String> permissions = new ArrayList<String>();

        if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.READ_SMS);
        }

        if (hasSMSReceive != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.RECEIVE_SMS);
        }

        if (hasSMSSend != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.SEND_SMS);
        }

        if (!permissions.isEmpty()) {
            ActivityCompat.requestPermissions(RegisterDoctor.this, permissions.toArray(new String[permissions.size()]), 100);
        } else {
            startActivity(new Intent(RegisterDoctor.this, PreviewActivity.class)
                    .putExtra("role", role)
                    .putExtra("home", "yes"));
            saveUserDataSF(firstName, lastName, email, phoneNumber, pin,
                    gender, dob, userId, role, "yes", city, String.valueOf(state_id),
                    txtSelectedState.getText().toString(), address, clinicId, clinicName);
            Intent intent = new Intent(RegisterDoctor.this, OTPActivity.class);
            intent.putExtra("firstname", firstName);
            intent.putExtra("lastname", lastName);
            intent.putExtra("email", email);
            intent.putExtra("phone", phoneNumber);
            intent.putExtra("pin", pin);
            intent.putExtra("role", role);
            intent.putExtra("gender", gender);
            intent.putExtra("dob", dob);
            intent.putExtra("city", city);
            intent.putExtra("state_id", "0");
            intent.putExtra("address", address);
            intent.putExtra("clinic", clinicName);
            intent.putExtra("lat", lat);
            intent.putExtra("lng", lng);
            intent.putExtra("login", "no");
            intent.putExtra("register", "doctor");
            startActivity(intent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 101: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        gpsTracker = new GpsTracker(RegisterDoctor.this);
                        if (gpsTracker.canGetLocation()) {
                            lat = String.valueOf(gpsTracker.latitude);
                            lng = String.valueOf(gpsTracker.longitude);
                        } else {
                            showGPSDisabledAlertToUser();
                        }
                        return;
                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i]);
//                        finish();
                    }
                }
            }
            case 100: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        startActivity(new Intent(RegisterDoctor.this, PreviewActivity.class)
                                .putExtra("role", role)
                                .putExtra("home", "yes"));
                        saveUserDataSF(firstName, lastName, email, phoneNumber, pin,
                                gender, dob, userId, role, "yes", city, String.valueOf(state_id),
                                txtSelectedState.getText().toString(), address, clinicId, clinicName);
                        Intent intent = new Intent(RegisterDoctor.this, OTPActivity.class);
                        intent.putExtra("firstname", firstName);
                        intent.putExtra("lastname", lastName);
                        intent.putExtra("email", email);
                        intent.putExtra("phone", phoneNumber);
                        intent.putExtra("pin", pin);
                        intent.putExtra("role", role);
                        intent.putExtra("gender", gender);
                        intent.putExtra("dob", dob);
                        intent.putExtra("city", city);
                        intent.putExtra("state_id", "0");
                        intent.putExtra("address", address);
                        intent.putExtra("clinic", clinicName);
                        intent.putExtra("lat", lat);
                        intent.putExtra("lng", lng);
                        intent.putExtra("login", "no");
                        intent.putExtra("register", "doctor");
                        startActivity(intent);
                        return;
                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i]);
//                        finish();
                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    //-------------------------Check GPS LOCATION is enable or not
    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RegisterDoctor.this);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Go to Settings Page To Enable GPS",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                                finish();
                            }
                        });
        alertDialogBuilder.setNegativeButton(android.R.string.cancel,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
    public void getProfile() {
        progressDialog.show();
        FetchData.getInstance(RegisterDoctor.this).getRequestQueue().add(new StringRequest
                (Request.Method.GET, Urls.URL_TO_WEB + "users/viewApi/" + userId,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                final String response2 = response;
                                Log.d("valRes", response2);
                                try {
                                    JSONObject json = new JSONObject(response2);
                                    int status = json.getInt("status");
                                    if (status == 1) {
                                        JSONObject data = json.getJSONObject("data");
                                        String email = data.getString("email");
                                        String phone = data.getString("phone");
                                        String pin = data.getString("pin");
                                        editTextEmail.setText(email);
                                        editTextPhone.setText(phone);
                                        editTextPin.setText(pin);
                                        progressDialog.dismiss();
                                    } else {

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                    }
                }) {
        });
    }
}