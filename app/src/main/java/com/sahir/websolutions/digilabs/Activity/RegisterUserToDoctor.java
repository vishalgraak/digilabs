package com.sahir.websolutions.digilabs.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sahir.websolutions.digilabs.HelperClass.Helper;
import com.sahir.websolutions.digilabs.R;
import com.sahir.websolutions.digilabs.objects.GpsTracker;
import com.sahir.websolutions.digilabs.objects.StatesModel;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.Thing;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * Created by Sys9 on 7/25/2017.
 */

public class RegisterUserToDoctor extends AppCompatActivity {
    Toolbar toolbar;
    EditText editTextFirstName, editTextLastName, editTextEmail, editTextPhone, editTextPin,
            edtCity, editTextAddress, editTextName, editTextMessage;
    public static String phoneNumber;
    ImageView ivBubble;
    TextView txt_anim1, txt_terms, txt_privacy, txtSelectedState, mTitle, txt_salutation;
    LinearLayout btnState;
    Button buttonRegister;
    SharedPreferences pref;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    RelativeLayout rl_bubble;
    LinearLayout llAnimation;
    String firstName = "", lastName = "", email = "", dob = "", userId = "", gender = "",
            pin, role = "3", address = "", city = "", lat = "", lng = "", clinicName = "", state,
            title = "Dr.", message = "";
    int state_id = 0;
    ArrayList<StatesModel> statesModelArrayList = new ArrayList<>();
    String[] arrayStates;
    CheckBox checkbox;
    ProgressDialog progressDialog;
    GpsTracker gpsTracker;
    Activity sActivity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_usertodoctor);
        sActivity = RegisterUserToDoctor.this;
        init();

        onclick();

        animation();

        setFont();

        if (!Utils.isNetworkConnected(RegisterUserToDoctor.this)) {
            Utils.conDialog(RegisterUserToDoctor.this);
        } else {
            getStates();
        }

        if (Build.VERSION.SDK_INT >= 23) {
            AllowPermissions();
        } else {
            gpsTracker = new GpsTracker(RegisterUserToDoctor.this);
            if (gpsTracker.canGetLocation()) {
                lat = String.valueOf(gpsTracker.latitude);
                lng = String.valueOf(gpsTracker.longitude);
            } else {
                showGPSDisabledAlertToUser();
            }
        }
    }

    public void onclick() {

        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });

        txt_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterUserToDoctor.this, TermsActivity.class)
                        .putExtra("type", "terms"));
            }
        });

        txt_privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterUserToDoctor.this, TermsActivity.class)
                        .putExtra("type", "privacy"));
            }
        });


        btnState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showStatesList(arrayStates);
            }
        });


        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email = editTextEmail.getText().toString().trim();
                if (editTextFirstName.length() == 0) {
                    editTextFirstName.setError("FirstName empty!");
                    editTextFirstName.requestFocus();
                } else if (editTextEmail.length() == 0) {
                    editTextEmail.setError("Email empty!");
                    editTextEmail.requestFocus();
                } else if (!email.matches(emailPattern)) {
                    editTextEmail.setError("Incorrect email format!");
                    editTextEmail.requestFocus();
                } else if (editTextPhone.length() == 0) {
                    editTextPhone.setError("Phone number empty!");
                    editTextPhone.requestFocus();
                } else if (editTextPhone.length() != 10) {
                    editTextPhone.setError("Phone number should be of 10 digits!");
                    editTextPhone.requestFocus();
                } else if (editTextPin.length() == 0) {
                    editTextPin.setError("Pin number empty!");
                    editTextPin.requestFocus();
                } else if (editTextPin.length() < 6) {
                    editTextPin.setError("Invalid pin");
                    editTextPin.requestFocus();
                } else if (editTextAddress.length() == 0) {
                    editTextAddress.setError("Address empty!");
                    editTextAddress.requestFocus();
                } else if (txtSelectedState.getText().toString().equals("Select State")) {
                    Snackbar.make(buttonRegister, "Please select state.", BaseTransientBottomBar.LENGTH_SHORT).show();
                } else if (edtCity.length() == 0) {
                    edtCity.setError("City empty!");
                    edtCity.requestFocus();
                } else if (editTextMessage.length() == 0) {
                    editTextMessage.setError("Message empty!");
                    editTextMessage.requestFocus();
                } else if (!checkbox.isChecked()) {
                    Snackbar.make(buttonRegister, "Please agree to terms and conditions.", BaseTransientBottomBar.LENGTH_SHORT).show();
                } else {
                    if (!Utils.isNetworkConnected(RegisterUserToDoctor.this)) {
                        Utils.conDialog(RegisterUserToDoctor.this);
                    } else {
                        firstName = editTextFirstName.getText().toString().trim();
                        lastName = editTextLastName.getText().toString().trim();
                        email = editTextEmail.getText().toString().trim();
                        phoneNumber = editTextPhone.getText().toString().trim();
                        pin = editTextPin.getText().toString().trim();
                        address = editTextAddress.getText().toString().trim();
                        city = edtCity.getText().toString().trim();
                        message = editTextMessage.getText().toString().trim();
                        if (editTextName.length() != 0) {
                            clinicName = editTextName.getText().toString().trim();
                        }
                        if (editTextLastName.length() == 0) {
                            lastName = "";
                        }
                        progressDialog.show();
                        sendToServer();
                    }
                }
            }
        });
    }

    private void animation() {
        final Animation fade_in = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        final Animation fade_out = AnimationUtils.loadAnimation(this, R.anim.fade_out);
        final Animation fade_in1 = AnimationUtils.loadAnimation(this, R.anim.fade_in);

        txt_anim1.setText(getString(R.string.hi_andy));
        txt_anim1.startAnimation(fade_in);

        fade_in.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                final Handler handler = new Handler();
                new Thread(new Runnable() {
                    public void run() {
                        try {
                            Thread.sleep(2000);
                        } catch (Exception e) {
                        }
                        handler.post(new Runnable() {
                            public void run() {
                                txt_anim1.startAnimation(fade_out);
                            }
                        });
                    }
                }).start();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        fade_out.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (getIntent().getStringExtra("intent") != null) {
                    txt_anim1.setText(getString(R.string.register));
                }
                txt_anim1.startAnimation(fade_in1);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public void init() {
        ActivityStack.activity.add(RegisterUserToDoctor.this);
        progressDialog = new ProgressDialog(RegisterUserToDoctor.this);
        progressDialog.setMessage("Processing...");
        btnState = (LinearLayout) findViewById(R.id.btnState);
        checkbox = (CheckBox) findViewById(R.id.checkbox);
        txt_anim1 = (TextView) findViewById(R.id.txt_anim1);
        txtSelectedState = (TextView) findViewById(R.id.txtSelectedState);
        txt_terms = (TextView) findViewById(R.id.txt_terms);
        txt_privacy = (TextView) findViewById(R.id.txt_privacy);
        txt_salutation = (TextView) findViewById(R.id.txt_salutation);
        editTextPin = (EditText) findViewById(R.id.editTextPin);
        editTextFirstName = (EditText) findViewById(R.id.editTextFirstName);
        editTextFirstName.setText(Helper.getFirstName(sActivity));
        editTextLastName = (EditText) findViewById(R.id.editTextLastName);
        editTextLastName.setText(Helper.getLastName(sActivity));
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextEmail.setText(Helper.getEmail(sActivity));
        editTextPhone = (EditText) findViewById(R.id.editTextPhone);
        editTextPhone.setText(Helper.getPhone(sActivity));
        edtCity = (EditText) findViewById(R.id.edtCity);
        editTextAddress = (EditText) findViewById(R.id.editTextAddress);
        editTextName = (EditText) findViewById(R.id.editTextName);
        editTextMessage = (EditText) findViewById(R.id.editTextMessage);
        buttonRegister = (Button) findViewById(R.id.buttonRegister);

        setTitle("");
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("Switch from User to Doctor");
        pref = getSharedPreferences(getString(R.string.user_details), MODE_PRIVATE);

        userId = getIntent().getStringExtra("user_id");

        ivBubble = (ImageView) findViewById(R.id.ivBubble);
        llAnimation = (LinearLayout) findViewById(R.id.llAnimation);
        rl_bubble = (RelativeLayout) findViewById(R.id.rl_bubble);

    }


    private void setFont() {
        editTextFirstName.setTypeface(Utils.setTypeface(this));
        editTextLastName.setTypeface(Utils.setTypeface(this));
        editTextPhone.setTypeface(Utils.setTypeface(this));
        editTextEmail.setTypeface(Utils.setTypeface(this));
        editTextPin.setTypeface(Utils.setTypeface(this));
        buttonRegister.setTypeface(Utils.setTypeface(this));
        txt_anim1.setTypeface(Utils.setTypeface(this));
        txt_terms.setTypeface(Utils.setTypeface(this));
        txt_privacy.setTypeface(Utils.setTypeface(this));
        txtSelectedState.setTypeface(Utils.setTypeface(this));
        txt_salutation.setTypeface(Utils.setTypeface(this));
        edtCity.setTypeface(Utils.setTypeface(this));
        editTextAddress.setTypeface(Utils.setTypeface(this));
        editTextName.setTypeface(Utils.setTypeface(this));
        editTextMessage.setTypeface(Utils.setTypeface(this));
    }

    public void sendToServer() {
        FetchData.getInstance(RegisterUserToDoctor.this).getRequestQueue().add(new StringRequest
                (Request.Method.POST, Urls.URL_TO_WEB + "notifications/addApi",
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                progressDialog.dismiss();
                                final String response2 = response;
                                Log.i("valRes", "" + response2);
                                try {
                                    JSONObject json = new JSONObject(response2);
                                    int status = json.getInt("status");
                                    if (status == 1) {
                                        new AlertDialog.Builder(RegisterUserToDoctor.this)
                                                .setMessage("Request has been sent. Our team will revert you in 24 hours.")
                                                .setCancelable(false)
                                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        finish();
                                                    }
                                                })
                                                .show();
                                    } else if (status == 2) {

                                    }
                                    Log.i("valuesAre", "" + userId);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", userId);
                params.put("lab_id", "");
                params.put("message", message);
                return params;
            }
        });
    }

    public void showDialog() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.setCancelable(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        if (height >= 800) {
            lp.height = height / 2;
        } else if (height >= 1280) {
            lp.height = height / 2;
        } else if (height >= 1920) {
            lp.height = height / 2;
        } else if (height >= 2560) {
            lp.height = height / 2;
        }
        dialog.getWindow().setAttributes(lp);
        dialog.show();

        TextView txt = (TextView) dialog.findViewById(R.id.txt);
        TextView tvlogin = (TextView) dialog.findViewById(R.id.tvlogin);
        TextView tvreg = (TextView) dialog.findViewById(R.id.tvreg);
        txt.setTypeface(Utils.setTypeface(this));
        tvlogin.setTypeface(Utils.setTypeface(this));
        tvreg.setTypeface(Utils.setTypeface(this));

        tvlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RegisterUserToDoctor.this, LoginActivity.class));
            }
        });

        tvreg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    public void getStates() {
        FetchData.getInstance(RegisterUserToDoctor.this).getRequestQueue().add(new StringRequest
                (Request.Method.GET, Urls.URL_TO_WEB + "countries/viewapi/101",
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                final String response2 = response;
                                Log.d("valRes", response2);
                                try {
                                    JSONObject json = new JSONObject(response2);
                                    int status = json.getInt("status");
                                    if (status == 1) {
                                        JSONArray data = json.optJSONArray("data");
                                        arrayStates = new String[data.length()];
                                        for (int i = 0; i < data.length(); i++) {
                                            JSONObject jsonObject = data.optJSONObject(i);
                                            StatesModel statesModel = new StatesModel();
                                            statesModel.state_id = jsonObject.optInt("state_id");
                                            statesModel.state_name = jsonObject.optString("name");
                                            statesModel.country_id = jsonObject.optInt("country_id");
                                            statesModelArrayList.add(statesModel);
                                            arrayStates[i] = jsonObject.optString("name");
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                    }
                }) {
        });
    }


    public void showStatesList(final String[] list) {
        // Initializing a new alert dialog
        final AlertDialog.Builder builder = new AlertDialog.Builder(RegisterUserToDoctor.this);

        // Set the alert dialog title
        builder.setTitle("Choose a State.");

        // Set a single choice items list for alert dialog
        builder.setSingleChoiceItems(list, // Items list
                -1, // Index of checked item (-1 = no selection)
                new DialogInterface.OnClickListener() // Item click listener
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // Get the alert dialog selected item's text
                        String selectedItem = list[i].toString();
                        txtSelectedState.setText(selectedItem);
                        state_id = statesModelArrayList.get(i).state_id;
                    }
                });

        // Set the a;ert dialog positive button
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Just dismiss the alert dialog after selection
                // Or do something now
            }
        });

        // Create the alert dialog
        AlertDialog dialog = builder.create();

        // Finally, display the alert dialog
        dialog.show();
    }

    // **********************************************Show Hashkey***********************************
    private void showHashKey() {
        // TODO Auto-generated method stub
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.example.sws_pc10.digilabs", PackageManager.GET_SIGNATURES); // Your
            // here
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.i("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                System.out.println("KeyHash:: " + Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }


    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Register Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.skip, menu);

        MenuItem register = menu.findItem(R.id.next);
        register.setVisible(false);
        MenuItem skip = menu.findItem(R.id.skip);
        skip.setVisible(false);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return true;
    }

    private void AllowPermissions() {
        int hasLocationPermission = ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION);
        int hasSMSPermission = ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> permissions = new ArrayList<String>();
        if (hasLocationPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        }

        if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
        }

        if (!permissions.isEmpty()) {
            ActivityCompat.requestPermissions(RegisterUserToDoctor.this, permissions.toArray(new String[permissions.size()]), 101);
        } else {
            gpsTracker = new GpsTracker(RegisterUserToDoctor.this);
            if (gpsTracker.canGetLocation()) {
                lat = String.valueOf(gpsTracker.latitude);
                lng = String.valueOf(gpsTracker.longitude);
            } else {
                showGPSDisabledAlertToUser();
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 101: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        gpsTracker = new GpsTracker(RegisterUserToDoctor.this);
                        if (gpsTracker.canGetLocation()) {
                            lat = String.valueOf(gpsTracker.latitude);
                            lng = String.valueOf(gpsTracker.longitude);
                        } else {
                            showGPSDisabledAlertToUser();
                        }
                        return;
                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i]);
//                        finish();
                    }
                }
            }
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    //-------------------------Check GPS LOCATION is enable or not
    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RegisterUserToDoctor.this);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Go to Settings Page To Enable GPS",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                                finish();
                            }
                        });
        alertDialogBuilder.setNegativeButton(android.R.string.cancel,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
}