package com.sahir.websolutions.digilabs.objects;

import java.util.Comparator;

/**
 * Created by Sys9 on 6/30/2017.
 */

public class ComparatorRating implements Comparator<LabModel> {
    @Override
    public int compare(LabModel o1, LabModel o2) {
        return (o1.star_rating).compareTo(o2.star_rating);
    }
}
