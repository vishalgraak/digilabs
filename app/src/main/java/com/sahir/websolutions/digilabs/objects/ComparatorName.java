package com.sahir.websolutions.digilabs.objects;

import java.util.Comparator;

/**
 * Created by Sys9 on 7/14/2017.
 */

public class ComparatorName implements Comparator<Report> {
    @Override
    public int compare(Report o1, Report o2) {

        String name1 = o1.getName().toLowerCase();
        String name2 = o2.getName().toLowerCase();

        return name1.compareTo(name2);
    }
}
