package com.sahir.websolutions.digilabs.objects;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Sys9 on 6/30/2017.
 */

public class LabModel implements Parcelable{
    public String test_name;
    public String test_price;
    public String id;
    public String book_id;
    public String user_id;
    public String labname;
    public String email;
    public String address;
    public String city;
    public String state;
    public String phone;
    public String collection_distance="";
    public String collection_charges="";
    public String price;
    public String homecollection;
    public String isHomecollection;
    public String home_collection_price;
    public String home_price_fix;
    public String star_rating;
    public String price_rating;
    public String lab_image;
    public String latitude;
    public String longtitude;
    public String working_days;
    public String isprefered;
    public String opentime;
    public String closetime;
    public float distance;
    public String type;
    public String test_type;
    public String date;
    public String time;
    public String timing;
    public String price_type1;
    public String price_value1;
    public String hcCharges;
    public String hcDistance;
    public String hc_type;
    public String discount;
    public String convenience;
    public String conven_type;
    public ArrayList<LabModel> timeList = new ArrayList();
    public ArrayList<LabModel> hcList = new ArrayList();
    public ArrayList<String> tests = new ArrayList<>();

    public LabModel() {

    }

    protected LabModel(Parcel in) {
        test_name = in.readString();
        test_price = in.readString();
        id = in.readString();
        book_id = in.readString();
        user_id = in.readString();
        labname = in.readString();
        email = in.readString();
        address = in.readString();
        city = in.readString();
        state = in.readString();
        phone = in.readString();
        price = in.readString();
        homecollection = in.readString();
        isHomecollection = in.readString();
        home_collection_price = in.readString();
        home_price_fix = in.readString();
        star_rating = in.readString();
        price_rating = in.readString();
        lab_image = in.readString();
        latitude = in.readString();
        longtitude = in.readString();
        working_days = in.readString();
        isprefered = in.readString();
        opentime = in.readString();
        closetime = in.readString();
        distance = in.readFloat();
        type = in.readString();
        test_type = in.readString();
        date = in.readString();
        time = in.readString();
        timing = in.readString();
        price_type1 = in.readString();
        price_value1 = in.readString();
        hcCharges = in.readString();
        hcDistance = in.readString();
        hc_type = in.readString();
        discount = in.readString();
        conven_type = in.readString();
        convenience = in.readString();
        timeList = in.createTypedArrayList(LabModel.CREATOR);
        hcList = in.createTypedArrayList(LabModel.CREATOR);
        tests = in.createStringArrayList();
    }

    public static final Creator<LabModel> CREATOR = new Creator<LabModel>() {
        @Override
        public LabModel createFromParcel(Parcel in) {
            return new LabModel(in);
        }

        @Override
        public LabModel[] newArray(int size) {
            return new LabModel[size];
        }
    };

    public Float getDistance() {
        return this.distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(test_name);
        dest.writeString(test_price);
        dest.writeString(id);
        dest.writeString(book_id);
        dest.writeString(user_id);
        dest.writeString(labname);
        dest.writeString(email);
        dest.writeString(address);
        dest.writeString(city);
        dest.writeString(state);
        dest.writeString(phone);
        dest.writeString(price);
        dest.writeString(homecollection);
        dest.writeString(isHomecollection);
        dest.writeString(home_collection_price);
        dest.writeString(home_price_fix);
        dest.writeString(star_rating);
        dest.writeString(price_rating);
        dest.writeString(lab_image);
        dest.writeString(latitude);
        dest.writeString(longtitude);
        dest.writeString(working_days);
        dest.writeString(isprefered);
        dest.writeString(opentime);
        dest.writeString(closetime);
        dest.writeFloat(distance);
        dest.writeString(type);
        dest.writeString(test_type);
        dest.writeString(date);
        dest.writeString(time);
        dest.writeString(timing);
        dest.writeString(price_type1);
        dest.writeString(price_value1);
        dest.writeString(hcCharges);
        dest.writeString(hcDistance);
        dest.writeString(hc_type);
        dest.writeString(discount);
        dest.writeString(conven_type);
        dest.writeString(convenience);
        dest.writeTypedList(timeList);
        dest.writeTypedList(hcList);
        dest.writeStringList(tests);
    }
}
