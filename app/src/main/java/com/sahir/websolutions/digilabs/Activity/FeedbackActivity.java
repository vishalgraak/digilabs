package com.sahir.websolutions.digilabs.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sahir.websolutions.digilabs.R;

import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

/**
 * Created by Sys9 on 6/21/2017.
 */

public class FeedbackActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextView txt_feedback, txt_rate, txt_rating;
    EditText edt_feedback;
    Button btn_submit;
    String star_rating = "5", user_id, comments = "";
    SharedPreferences prefs;
    RatingBar rbRating;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_feedback);

        init();

        setToolbar();

        setFont();

        OnClick();

    }

    private void init() {
        ActivityStack.activity.add(FeedbackActivity.this);
        txt_feedback = (TextView) findViewById(R.id.txt_feedback);
        txt_rate = (TextView) findViewById(R.id.txt_rate);
        txt_rating = (TextView) findViewById(R.id.txt_rating);
        edt_feedback = (EditText) findViewById(R.id.edt_feedback);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        rbRating = (RatingBar) findViewById(R.id.rbRating);
        rbRating.setRating((float) 4.5);
        LayerDrawable stars = (LayerDrawable) rbRating.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.parseColor("#06B4BF"), PorterDuff.Mode.SRC_ATOP);
        prefs = getSharedPreferences(getString(R.string.user_details), MODE_PRIVATE);
        user_id = prefs.getString(getString(R.string.user_id), null);
        progressDialog = new ProgressDialog(FeedbackActivity.this);
        progressDialog.setMessage("Sending Feedback to server...");
    }

    public void setToolbar() {
        setTitle("");
        toolbar = (Toolbar) findViewById(R.id.toolbar_image);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("Rate App");
    }

    public void setFont() {
        txt_feedback.setTypeface(Utils.setTypeface(this));
        txt_rate.setTypeface(Utils.setTypeface(this));
        txt_rating.setTypeface(Utils.setTypeface(this));
        edt_feedback.setTypeface(Utils.setTypeface(this));
        btn_submit.setTypeface(Utils.setTypeface(this));
    }

    public void OnClick() {
        rbRating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if (rating <= 1.0 && rating >= 0.0) {
                    txt_rating.setText("Terrible");
                    star_rating = "1";
                } else if (rating <= 2.0 && rating > 1.0) {
                    txt_rating.setText("Poor");
                    star_rating = "2";
                } else if (rating <= 3.0 && rating > 2.0) {
                    txt_rating.setText("Average");
                    star_rating = "3";
                } else if (rating <= 4.0 && rating > 3.0) {
                    txt_rating.setText("Good");
                    star_rating = "4";
                } else if (rating > 4.0) {
                    txt_rating.setText("Awesome");
                    star_rating = "5";
                }
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_feedback.length() == 0) {
                    comments = "";
                } else {
                    if (!Utils.isNetworkConnected(FeedbackActivity.this)) {
                        Utils.conDialog(FeedbackActivity.this);
                    } else {
                        progressDialog.show();
                        comments = edt_feedback.getText().toString();
                        sendFeedback(user_id, star_rating, comments.replace(" ", "%20"));
                    }
                }
            }
        });
    }

    public void sendFeedback(final String userId, final String rating, final String comments) {
        FetchData.getInstance(FeedbackActivity.this).getRequestQueue().add(new StringRequest
                (Request.Method.POST, Urls.URL_TO_WEB + "appratings/addApi",
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                progressDialog.dismiss();
                                Log.i("Feedback Response: ", response);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String status = jsonObject.optString("status");
                                    if (status.equals("1")) {
                                        new AlertDialog.Builder(FeedbackActivity.this)
                                                .setMessage("Thank you for your feedback.")
                                                .setCancelable(false)
                                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        finish();
                                                    }
                                                })
                                                .show();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    progressDialog.dismiss();
                                }

                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                        progressDialog.dismiss();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", userId);
                params.put("rate", rating);
                params.put("message", comments);
                return params;
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return true;
    }
}
