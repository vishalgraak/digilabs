package com.sahir.websolutions.digilabs.IntroSlider;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.sahir.websolutions.digilabs.Activity.PreviewActivity;
import com.sahir.websolutions.digilabs.R;
import com.sahir.websolutions.digilabs.Activity.Utils;


public class MyPagerAdapter extends FragmentPagerAdapter implements ViewPager.PageTransformer {
    public final static float BIG_SCALE = 1.0f;
    public final static float SMALL_SCALE = 0.7f;
    public final static float DIFF_SCALE = BIG_SCALE - SMALL_SCALE;

    private MyLinearLayout cur = null;
    private MyLinearLayout next = null;
    private PreviewActivity context;
    private FragmentManager fm;
    private float scale;
    public static int mid_view_id = PreviewActivity.FIRST_PAGE;

    public MyPagerAdapter(PreviewActivity context, FragmentManager fm) {
        super(fm);
        this.fm = fm;
        this.context = context;
    }


    @Override
    public Fragment getItem(int position) {
        // make the first pager bigger than others
        if (position == PreviewActivity.FIRST_PAGE)
            scale = BIG_SCALE;
        else
            scale = SMALL_SCALE;

        position = position % PreviewActivity.PAGES;

        return MyFragment.newInstance(context, position, scale);
    }


    @Override
    public int getCount() {
        return PreviewActivity.PAGES;
    }


    @Override
    public void transformPage(View page, float position) {
        MyLinearLayout myLinearLayout = (MyLinearLayout) page.findViewById(R.id.root);
        float scale = BIG_SCALE;

        ImageView ivBackground = (ImageView) myLinearLayout.findViewById(R.id.ivBackground);
        // ivBackground.setBackgroundColor(Color.BLACK);

        //final TextView tvText = (TextView)  myLinearLayout.findViewById(R.id.tvText);

        final ImageView ivPerson = (ImageView) myLinearLayout.findViewById(R.id.ivPerson);
        final LinearLayout llHeader = (LinearLayout) myLinearLayout.findViewById(R.id.llHeader);

        final RelativeLayout rl_bubble = (RelativeLayout) myLinearLayout.findViewById(R.id.rl_bubble);
        TextView tvBubble = (TextView) myLinearLayout.findViewById(R.id.tvBubble);
        tvBubble.setTypeface(Utils.setTypeface(context));


        rl_bubble.setVisibility(View.INVISIBLE);
        llHeader.setVisibility(View.INVISIBLE);




        if (position > 0) {
            scale = scale - position * DIFF_SCALE;
        } else {
            scale = scale + position * DIFF_SCALE;
        }
        if (scale < 0) scale = 0;

        // ivText.animate().alpha(0.0f).setDuration(300);

        //scale range 0 to 1 to 0 (not exactly zero)
        int p = ((int) (scale * 255));
        if (position == 0) {

            mid_view_id = page.getId();
            PreviewActivity.setThumbBackground(mid_view_id);

        } else {
            ivBackground.setImageAlpha(p);
            llHeader.setVisibility(View.INVISIBLE);
        }

        myLinearLayout.setScaleBoth(scale);
    }


}
