package com.sahir.websolutions.digilabs.objects;

/**
 * Created by Sys9 on 7/17/2017.
 */

public class DoctorModel {
    public String id;
    public String firstname;
    public String lastname;
    public String username;
    public String email;
    public String pin;
    public String clinicid;
    public String clinicname;
    public String city;
    public String photo;
}
