package com.sahir.websolutions.digilabs.Activity;

import android.app.Application;
import android.os.StrictMode;

/**
 * Created by Sys9 on 6/27/2017.
 */

public class MyApplication extends Application {
public  static boolean labOrderStatus=false;
    @Override
    public void onCreate() {
        super.onCreate();
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }

}
