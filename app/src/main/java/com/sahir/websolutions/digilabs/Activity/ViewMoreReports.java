package com.sahir.websolutions.digilabs.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sahir.websolutions.digilabs.Adapter.ViewPagerAdapter;
import com.sahir.websolutions.digilabs.R;
import com.sahir.websolutions.digilabs.objects.MemberModel;
import com.sahir.websolutions.digilabs.objects.Report;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

/**
 * Created by Sys9 on 7/27/2017.
 */

public class ViewMoreReports extends AppCompatActivity {
    int offline;
    String reportName, date, url, id, type, report_type;
    ImageView img_download, img_share, arrow_left, arrow_right;
    LinearLayout ll_layout, ll_btn;
    Report report;
    Toolbar toolbar;
    ProgressBar progress_bar;
    Button btn;
    ArrayList<MemberModel> member_lists;
    SharedPreferences prefs;
    ViewPager pager;
    ViewPagerAdapter adapter;
    ArrayList<String> images_list = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_more_reports);

        init();

        setData();

        setToolbar();

        onClicks();
    }

    private void setData() {

        if (getIntent().getParcelableArrayListExtra("members_list") != null) {
            member_lists = getIntent().getParcelableArrayListExtra("members_list");
        }

        Intent intent = getIntent();
        report = (Report) intent.getSerializableExtra("Report");
        reportName = report.getName();
        url = report.getPath();
        id = String.valueOf(report.getId());  //report id
        type = report.getType();  //pdf,camera,gallery
        report_type = report.report_type;
        offline = intent.getIntExtra("offline", 2);    // //0--> online , 1--> offline
        date = report.getDate();

        if (report.image_list.size() > 0) {
            for (int i = 1; i < report.image_list.size(); i++) {
                images_list.add(report.image_list.get(i));
            }
            adapter = new ViewPagerAdapter(ViewMoreReports.this, images_list);
            pager.setAdapter(adapter);
        }

        if (report_type.equals("patients")) {
            btn.setVisibility(View.GONE);
            ll_btn.setVisibility(View.GONE);
        }

        if (report.member_name != null) {
            if (report.member_name.equals("my")) {
                btn.setText("Folder: " + prefs.getString("title", null) + prefs.getString("fname", null));
            } else {
                btn.setText("Folder: " + report.member_name);
            }
        }

        if (member_lists.size() == 0) {
            ll_btn.setVisibility(View.GONE);
        } else {
            ll_btn.setVisibility(View.VISIBLE);
        }

    }

    private void init() {
        img_download = (ImageView) findViewById(R.id.img_download);
        img_share = (ImageView) findViewById(R.id.img_share);
        arrow_left = (ImageView) findViewById(R.id.arrow_left);
        arrow_right = (ImageView) findViewById(R.id.arrow_right);
        arrow_left.setRotation(arrow_left.getRotation() + 180);
        pager = (ViewPager) findViewById(R.id.pager);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        btn = (Button) findViewById(R.id.btn);
        ll_layout = (LinearLayout) findViewById(R.id.ll_layout);
        ll_btn = (LinearLayout) findViewById(R.id.ll_btn);
        prefs = getSharedPreferences(getString(R.string.user_details), Context.MODE_PRIVATE);
    }

    private void onClicks() {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (member_lists != null) {
                    ArrayList<String> name = new ArrayList<>();
                    ArrayList<String> ids = new ArrayList<>();
                    name.add(prefs.getString("title", null) + prefs.getString("fname", null));
                    ids.add("null");
                    for (MemberModel model : member_lists) {
                        name.add(model.name);
                        ids.add(model.id);
                    }
                    String[] arr = new String[name.size()];
                    arr = name.toArray(arr);
                    String[] arrId = new String[ids.size()];
                    arrId = ids.toArray(arrId);
                    optionDialog(arr, arrId);
                }
            }
        });

        img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewMoreReports.this, ShareWithDoctor.class);
                intent.putExtra("Report", report);
                intent.putExtra("offline", offline);
                intent.putExtra("type", "more");
                startActivity(intent);
            }
        });


        img_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.isNetworkConnected(ViewMoreReports.this)) {
                    Utils.conDialog(ViewMoreReports.this);
                } else {
                    saveToFolder();
                }
            }
        });

        arrow_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pager.setCurrentItem(pager.getCurrentItem() - 1);
            }
        });

        arrow_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pager.setCurrentItem(pager.getCurrentItem() + 1);
            }
        });
    }

    protected void optionDialog(final String[] fav_arr2, final String[] arrId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ViewMoreReports.this);
        builder.setTitle("");
        builder.setItems(fav_arr2, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                if (item == 0) {
                    dialog.dismiss();
                    if (!Utils.isNetworkConnected(ViewMoreReports.this)) {
                        Utils.conDialog(ViewMoreReports.this);
                    } else {
                        btn.setText("Folder: " + fav_arr2[0]);
                        progress_bar.setVisibility(View.VISIBLE);
                        updateReport(arrId[0], id);
                    }
                } else if (item == 1) {
                    dialog.dismiss();
                    if (!Utils.isNetworkConnected(ViewMoreReports.this)) {
                        Utils.conDialog(ViewMoreReports.this);
                    } else {
                        btn.setText("Folder: " + fav_arr2[1]);
                        progress_bar.setVisibility(View.VISIBLE);
                        updateReport(arrId[1], id);
                    }
                } else if (item == 2) {
                    dialog.dismiss();
                    if (!Utils.isNetworkConnected(ViewMoreReports.this)) {
                        Utils.conDialog(ViewMoreReports.this);
                    } else {
                        btn.setText("Folder: " + fav_arr2[2]);
                        progress_bar.setVisibility(View.VISIBLE);
                        updateReport(arrId[2], id);
                    }
                } else if (item == 3) {
                    dialog.dismiss();
                    if (!Utils.isNetworkConnected(ViewMoreReports.this)) {
                        Utils.conDialog(ViewMoreReports.this);
                    } else {
                        btn.setText("Folder: " + fav_arr2[3]);
                        progress_bar.setVisibility(View.VISIBLE);
                        updateReport(arrId[3], id);
                    }
                }
            }
        });
        builder.show();
    }

    private void updateReport(final String member_id, final String reportId) {
        FetchData.getInstance(ViewMoreReports.this).getRequestQueue().add(new StringRequest
                (Request.Method.POST, Urls.URL_TO_WEB + "myreports/editApi/" + reportId,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                progress_bar.setVisibility(View.GONE);
                                Log.i("valRes", "" + response);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String status = jsonObject.optString("status");
                                    if (status.equals("1")) {
                                        Toast.makeText(getApplicationContext(), "Report updated successfully", Toast.LENGTH_SHORT).show();

                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                params.put("userfamilies_id", member_id);
                return params;
            }
        });
    }

    public void setToolbar() {
        setTitle("");
        toolbar = (Toolbar) findViewById(R.id.toolbar_image);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(reportName + " - " + date);
        mTitle.setTypeface(Utils.setTypeface(this));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return true;
    }

    public void saveToFolder() {
        DownloadFile dlfile = new DownloadFile();
        dlfile.execute();
    }


    public class DownloadFile extends AsyncTask<String, Void, String> {

        public DownloadFile() {

        }

        @Override
        protected String doInBackground(String... params) {
            URL urlTemp = null;
            try {
                for (int i = 0; i < report.image_list.size(); i++) {
                    if (report.image_list.get(i).endsWith("png")) {
                        urlTemp = new URL(report.image_list.get(i));
                        Bitmap bitmapT = getBitmapFromUrl(urlTemp);
                        saveToInternalStorage(bitmapT, "ReportImg" + i + ".png");
                    } else if (report.image_list.get(i).endsWith("jpg")) {
                        urlTemp = new URL(report.image_list.get(i));
                        Bitmap bitmapT = getBitmapFromUrl(urlTemp);
                        saveToInternalStorage(bitmapT, "ReportImg" + i + ".jpg");
                    } else if (report.image_list.get(i).endsWith("gif")) {
                        urlTemp = new URL(report.image_list.get(i));
                        Bitmap bitmapT = getBitmapFromUrl(urlTemp);
                        saveToInternalStorage(bitmapT, "ReportImg" + i + ".gif");
                    } else if (report.image_list.get(i).endsWith("pdf")) {
                        saveToInternalStoragePdf(report.image_list.get(i).replace(" ", "%20"));
                    } else {
                        saveToInternalStorageVideo(report.image_list.get(i).replace(" ", "%20"));
                    }

                }
                Snackbar.make(ll_btn, "File downloaded successfully!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public Bitmap getBitmapFromUrl(URL url) {
        try {
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    private void saveToInternalStorage(Bitmap bitmapImage, String reportName) {
        File directory = new File(Environment.getExternalStorageDirectory() + File.separator + "DigiLabs");
        if (!directory.exists()) {
            directory.mkdirs();
        }
//        String nameTemp = reportName + "." + ext;
        File pictureFile = new File(directory, reportName);
        try {
            if (pictureFile.exists()) {
                pictureFile.delete();
                pictureFile.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            FileOutputStream out = new FileOutputStream(pictureFile);
            bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveToInternalStoragePdf(String url) {
        File directory = new File(Environment.getExternalStorageDirectory() + File.separator + "DigiLabs");
        if (!directory.exists()) {
            directory.mkdirs();
        }
        String nameReportTemp = reportName + "_" + date + ".pdf";
        File file = new File(directory, nameReportTemp);
        try {
            file.createNewFile();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        getPdfFromServer(url, file);
    }


    public void getPdfFromServer(String fileURL, File directory) {
        try {
            FileOutputStream f = new FileOutputStream(directory);
            URL u = new URL(fileURL);
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();

            InputStream in = c.getInputStream();

            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ((len1 = in.read(buffer)) > 0) {
                f.write(buffer, 0, len1);
            }
            f.close();
            Snackbar.make(ll_btn, "File downloaded  successfully!", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveToInternalStorageVideo(String url) {
        File directory = new File(Environment.getExternalStorageDirectory() + File.separator + "DigiLabs");
        if (!directory.exists()) {
            directory.mkdirs();
        }
        String nameReportTemp = reportName + "_" + date + ".mp4";
        File file = new File(directory, nameReportTemp);
        try {
            file.createNewFile();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        getVideoFromServer(url, file);
    }


    public void getVideoFromServer(String fileURL, File directory) {
        try {
            FileOutputStream f = new FileOutputStream(directory);
            URL u = new URL(fileURL);
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();

            InputStream in = c.getInputStream();

            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ((len1 = in.read(buffer)) > 0) {
                f.write(buffer, 0, len1);
            }
            f.close();
            Snackbar.make(ll_btn, "File downloaded  successfully!", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


