package com.sahir.websolutions.digilabs.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sahir.websolutions.digilabs.R;
import com.sahir.websolutions.digilabs.Activity.Utils;

import java.util.ArrayList;

/**
 * Created by Sys9 on 8/10/2017.
 */

public class HealthTipsAdapter extends RecyclerView.Adapter<HealthTipsAdapter.MyViewHolder> {
    Context mContext;
    ArrayList<String> data;

    public HealthTipsAdapter(Context ctx, ArrayList<String> list) {
        this.mContext = ctx;
        this.data = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tips_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.txt_tip.setText("\"" + data.get(position) + "\"");
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_tip;

        public MyViewHolder(View view) {
            super(view);
            txt_tip = (TextView) view.findViewById(R.id.txt_tip);
            txt_tip.setTypeface(Utils.setTypeface(mContext), Typeface.ITALIC);
        }
    }

}
