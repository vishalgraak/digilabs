package com.sahir.websolutions.digilabs.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.sahir.websolutions.digilabs.HelperClass.Helper;
import com.sahir.websolutions.digilabs.Maps.VolleySingleton;
import com.sahir.websolutions.digilabs.R;
import com.sahir.websolutions.digilabs.objects.DataPart;
import com.sahir.websolutions.digilabs.objects.MemberModel;
import com.sahir.websolutions.digilabs.objects.MultipartRequest;
import com.sahir.websolutions.digilabs.objects.Report;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;
import com.nbsp.materialfilepicker.MaterialFilePicker;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Pattern;

import static com.sahir.websolutions.digilabs.Maps.Helper.MY_SOCKET_TIMEOUT_MS;


public class AddReportActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    LinearLayout llPdf, llGallery, llCamera;
    String type = "", localFilesString = "", mCurrentPhotoPath, user_id, userfamilies_id, intent_type;
    Bitmap bitmap = null;
    ArrayList<Report> arrayListLocalFile;
    Toolbar toolbar;
    LinearLayout llAnimation;
    ImageView ivBubble;
    SharedPreferences prefs;
    SharedPreferences.Editor edit;
    static final int REQUEST_TAKE_PHOTO = 1;
    private final static int SELECT_FILE = 6;
    private final static int SELECT_PDF = 7;
    TouchImageView img;
    private Uri picUri;
    ProgressBar progress_bar;
    ProgressDialog progressDialog;
    ArrayList<MemberModel> member_list;
    ArrayList<String> images_list = new ArrayList<>();
    ImageView img1, img2, img3, img4, img5, cross1, cross2, cross3, cross4, cross5;
    RelativeLayout rl1, rl2, rl3, rl4, rl5;
    HorizontalScrollView horizontalScrollView;
    Activity sActivity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_report_main);
        sActivity = AddReportActivity.this;
        init();

        setToolbar();

        setNavDrawer();

        onClicks();

        if (getIntent().getParcelableArrayListExtra("member_list") != null) {
            member_list = getIntent().getParcelableArrayListExtra("member_list");
        }
    }

    private void init() {
        ActivityStack.activity.add(AddReportActivity.this);
        progressDialog = new ProgressDialog(AddReportActivity.this);
        prefs = getSharedPreferences(getString(R.string.user_details), Context.MODE_PRIVATE);
        localFilesString = prefs.getString(getString(R.string.local_files), "");
        edit = prefs.edit();

        user_id = prefs.getString(getString(R.string.user_id), null);
        llPdf = (LinearLayout) findViewById(R.id.llPdf);
        llGallery = (LinearLayout) findViewById(R.id.llGallery);
        llCamera = (LinearLayout) findViewById(R.id.llCamera);
        arrayListLocalFile = jsonToArrayList(localFilesString);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        img = (TouchImageView) findViewById(R.id.img);
    }

    private void onClicks() {
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });

        llPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pdfIntent();
            }
        });
        llGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent_type = "gallery";
                images_list.clear();
                if (Build.VERSION.SDK_INT >= 23) {
                    AllowPermissions();
                } else {
                    galleryIntent();
                }
            }
        });
        llCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent_type = "camera";
                images_list.clear();
                if (Build.VERSION.SDK_INT >= 23) {
                    AllowPermissions();
                } else {
                    cameraIntent();
                }
            }
        });
    }

    public void openDialog(final String type, final String fileName, final String fileType) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_report_name);
        dialog.show();
        final EditText etName = (EditText) dialog.findViewById(R.id.etName);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        txt_title.setText("Enter Report Name");
        txt_title.setTypeface(Utils.setTypeface(this));
        TextView tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);
        final TextView tvNext = (TextView) dialog.findViewById(R.id.tvNext);
        tvCancel.setTypeface(Utils.setTypeface(this));
        tvNext.setTypeface(Utils.setTypeface(this));
        LinearLayout ll_belong = (LinearLayout) dialog.findViewById(R.id.ll_belong);
        final LinearLayout llImages = (LinearLayout) dialog.findViewById(R.id.llImages);
        horizontalScrollView = (HorizontalScrollView) dialog.findViewById(R.id.horizontalScrollView);
        img1 = (ImageView) dialog.findViewById(R.id.img1);
        img2 = (ImageView) dialog.findViewById(R.id.img2);
        img3 = (ImageView) dialog.findViewById(R.id.img3);
        img4 = (ImageView) dialog.findViewById(R.id.img4);
        img5 = (ImageView) dialog.findViewById(R.id.img5);
        cross1 = (ImageView) dialog.findViewById(R.id.cross1);
        cross2 = (ImageView) dialog.findViewById(R.id.cross2);
        cross3 = (ImageView) dialog.findViewById(R.id.cross3);
        cross4 = (ImageView) dialog.findViewById(R.id.cross4);
        cross5 = (ImageView) dialog.findViewById(R.id.cross5);
        rl1 = (RelativeLayout) dialog.findViewById(R.id.rl1);
        rl2 = (RelativeLayout) dialog.findViewById(R.id.rl2);
        rl3 = (RelativeLayout) dialog.findViewById(R.id.rl3);
        rl4 = (RelativeLayout) dialog.findViewById(R.id.rl4);
        rl5 = (RelativeLayout) dialog.findViewById(R.id.rl5);

        final RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.radioGroup);

        ((RadioButton) radioGroup.getChildAt(0)).setChecked(true);
        final RadioButton rbMember1 = (RadioButton) dialog.findViewById(R.id.rbMember1);
        final RadioButton rbMember2 = (RadioButton) dialog.findViewById(R.id.rbMember2);
        final RadioButton rbMember3 = (RadioButton) dialog.findViewById(R.id.rbMember3);

        if (!type.equals("pdf")) {
            refresh(llImages, images_list);
        }

        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type.equals("camera")) {
                    cameraIntent();
                } else if (type.equals("gallery")) {
                    galleryIntent();
                }
                dialog.dismiss();
            }
        });

        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type.equals("camera")) {
                    cameraIntent();
                } else if (type.equals("gallery")) {
                    galleryIntent();
                }
                dialog.dismiss();
            }
        });

        img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type.equals("camera")) {
                    cameraIntent();
                } else if (type.equals("gallery")) {
                    galleryIntent();
                }
                dialog.dismiss();
            }
        });

        img5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type.equals("camera")) {
                    cameraIntent();
                } else if (type.equals("gallery")) {
                    galleryIntent();
                }
                dialog.dismiss();
            }
        });

        cross1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (images_list.size() == 1) {
                    Snackbar.make(tvNext, "At least one image should be selected", BaseTransientBottomBar.LENGTH_SHORT).show();
                } else {
                    images_list.remove(0);
                    refresh(llImages, images_list);
                }
            }
        });

        cross2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                images_list.remove(1);
                refresh(llImages, images_list);
            }
        });

        cross3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                images_list.remove(2);
                refresh(llImages, images_list);
            }
        });

        cross4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                images_list.remove(3);
                refresh(llImages, images_list);
            }
        });

        cross5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                images_list.remove(4);
                refresh(llImages, images_list);
            }
        });

        if (member_list.size() > 0) {
            ll_belong.setVisibility(View.VISIBLE);
            for (int i = 0; i < member_list.size(); i++) {
                if (i == 0) {
                    rbMember1.setVisibility(View.VISIBLE);
                    rbMember1.setText(member_list.get(0).name);
                    rbMember1.setTag(member_list.get(0).id);
                } else if (i == 1) {
                    rbMember2.setVisibility(View.VISIBLE);
                    rbMember2.setText(member_list.get(1).name);
                    rbMember2.setTag(member_list.get(1).id);
                } else if (i == 2) {
                    rbMember3.setVisibility(View.VISIBLE);
                    rbMember3.setText(member_list.get(2).name);
                    rbMember3.setTag(member_list.get(2).id);
                }
            }
        }

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etName.length() == 0) {
                    Snackbar.make(tvNext, "Report name can not be blank", BaseTransientBottomBar.LENGTH_SHORT).show();
                } else {
                    int radioButtonId = radioGroup.getCheckedRadioButtonId();
                    switch (radioButtonId) {
                        case R.id.rbMe:
                            userfamilies_id = null;
                            break;
                        case R.id.rbMember1:
                            userfamilies_id = rbMember1.getTag().toString();
                            break;
                        case R.id.rbMember2:
                            userfamilies_id = rbMember2.getTag().toString();
                            break;
                        case R.id.rbMember3:
                            userfamilies_id = rbMember3.getTag().toString();
                            break;
                    }

                    String name = etName.getText().toString().trim();

                    Report report = new Report();
                    report.setPath(mCurrentPhotoPath);
                    report.setType(type);
                    report.setName(name);
                    Calendar c = Calendar.getInstance();
                    System.out.println("Current time => " + c.getTime());

                    SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                    String formattedDate = df.format(c.getTime());
                    report.setDate(formattedDate);

                    arrayListLocalFile.add(report);
                    arrayToJson(arrayListLocalFile);

                    Log.d("localFile", type + ":" + name + ":" + mCurrentPhotoPath);
                    dialog.dismiss();
                    progressDialog.setMessage("Uploading Report....");
                    progressDialog.show();
                    if (images_list.size() == 1) {
                        saveProfileAccount(fileName, name, fileType, userfamilies_id);
                    } else {
                        try {
                            File directory = new File(Environment.getExternalStorageDirectory() + File.separator + "DigiLabs");
                            if (!directory.exists()) {
                                directory.mkdirs();
                            }
                            Random generator = new Random();
                            int n = 10000;
                            n = generator.nextInt(n);
                            String fname = "Report" + n + ".pdf";
                            File file = new File(directory, fname);
                            file.createNewFile();
                            convertImagesToPDF(fname, name, file);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            }
        });

    }

    public void refresh(LinearLayout layout, ArrayList<String> images_list) {
        if (images_list.size() > 0) {
            layout.setVisibility(View.VISIBLE);
            rl2.setVisibility(View.GONE);
            rl3.setVisibility(View.GONE);
            rl4.setVisibility(View.GONE);
            rl5.setVisibility(View.GONE);
            cross2.setVisibility(View.GONE);
            cross3.setVisibility(View.GONE);
            cross4.setVisibility(View.GONE);
            cross5.setVisibility(View.GONE);
            img2.setImageResource(R.drawable.add_default);
            img3.setImageResource(R.drawable.add_default);
            img4.setImageResource(R.drawable.add_default);
            img5.setImageResource(R.drawable.add_default);
            img2.setEnabled(true);
            img3.setEnabled(true);
            img4.setEnabled(true);
            img5.setEnabled(true);
            for (int i = 0; i < images_list.size(); i++) {
                if (i == 0) {
                    img1.setImageURI(Uri.parse(images_list.get(0)));
                    img1.setEnabled(false);
                    cross1.setVisibility(View.VISIBLE);
                    rl2.setVisibility(View.VISIBLE);
                } else if (i == 1) {
                    img2.setImageURI(Uri.parse(images_list.get(1)));
                    img2.setEnabled(false);
                    cross2.setVisibility(View.VISIBLE);
                    rl3.setVisibility(View.VISIBLE);
                } else if (i == 2) {
                    img3.setImageURI(Uri.parse(images_list.get(2)));
                    img3.setEnabled(false);
                    cross3.setVisibility(View.VISIBLE);
                    rl4.setVisibility(View.VISIBLE);
                } else if (i == 3) {
                    img4.setImageURI(Uri.parse(images_list.get(3)));
                    img4.setEnabled(false);
                    cross4.setVisibility(View.VISIBLE);
                    rl5.setVisibility(View.VISIBLE);
                } else {
                    img5.setImageURI(Uri.parse(images_list.get(4)));
                    img5.setEnabled(false);
                    cross5.setVisibility(View.VISIBLE);
                }
            }
            horizontalScrollView.postDelayed(new Runnable() {
                public void run() {
                    horizontalScrollView.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
                }
            }, 100L);
        }
    }


    private void cameraIntent() {
        picUri = getCaptureImageOutputUri();
        // collect all camera intents
//        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        if (picUri != null) {
//            captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
//        }
//        startActivityForResult(captureIntent, REQUEST_TAKE_PHOTO);
        Intent  picIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(picIntent, REQUEST_TAKE_PHOTO);
    }

    /**
     * Get URI to image received from capture by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "profile.png"));
        }
        return outputFileUri;
    }

    //pdf
    private void pdfIntent() {
        if (Utils.checkPermissionStorage(this)) {
            new MaterialFilePicker()
                    .withActivity(AddReportActivity.this)
                    .withRequestCode(SELECT_PDF)
                    .withFilter(Pattern.compile(".*\\.pdf$"))
                    .start();
        }
    }

    //gallery
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                if (data.getData() != null) {
                    picUri = data.getData();
                    try {
//                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
//
//                        Cursor cursor = getContentResolver().query(
//                                picUri, filePathColumn, null, null, null);
//                        cursor.moveToFirst();

//                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//                        mCurrentPhotoPath = cursor.getString(columnIndex);
//                        mCurrentPhotoPath = getRealPathFromURI(getApplicationContext(),picUri);
//                        mCurrentPhotoPath = MediaStore.Images.Media.getBitmap(getContentResolver(), picUri);
//                        cursor.close();

                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), picUri);
                        bitmap = getResizedBitmap(bitmap, 600);

//                    Saving image to mobile internal memory for sometime
                        String root = getApplicationContext().getFilesDir().toString();
                        File myDir = new File(root + "/digilabs");
                        myDir.mkdirs();

                        Random generator = new Random();
                        int n = 10000;
                        n = generator.nextInt(n);
                        String fname = "Img" + n + ".jpg";

                        mCurrentPhotoPath = root + "/digilabs/" + fname;
                        File file1 = new File(myDir, fname);
                        saveFile(bitmap, file1);

                        addImagesToList(mCurrentPhotoPath);

                        File file = new File(mCurrentPhotoPath);
                        Log.i("Name", "File" + file.getName());
                        type = "gallery";
                        if (!Utils.isNetworkConnected(AddReportActivity.this)) {
                            Utils.conDialog(AddReportActivity.this);
                        } else {
                            if (getIntent().getStringExtra("type").equals("update")) {
                                progressDialog.setMessage("Updating Report....");
                                progressDialog.show();
                                updateProfileAccount(file.getName(), getIntent().getStringExtra("report_id"), "image/jpeg");
                            } else {
                                openDialog("gallery", file.getName(), "image/jpeg");
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if (requestCode == SELECT_PDF) {
                try {
                    mCurrentPhotoPath = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
                    File file = new File(mCurrentPhotoPath);
                    // Get length of file in bytes
                    long fileSizeInBytes = file.length();
                    // Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
                    long fileSizeInKB = fileSizeInBytes / 1024;
                    // Convert the KB to MegaBytes (1 MB = 1024 KBytes)
                    long fileSizeInMB = fileSizeInKB / 1024;
                    if (fileSizeInMB > 2) {
                        new AlertDialog.Builder(AddReportActivity.this)
                                .setMessage("File size exceeded 2 MB. Please select a smaller size file.")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                    }
                                })
                                .show();
                    } else {
                        type = "pdf";
                        if (!Utils.isNetworkConnected(AddReportActivity.this)) {
                            Utils.conDialog(AddReportActivity.this);
                        } else {
                            if (getIntent().getStringExtra("type").equals("update")) {
                                progressDialog.setMessage("Updating Report....");
                                progressDialog.show();
                                updateProfileAccount(file.getName(), getIntent().getStringExtra("report_id"), type);
                            } else {
                                openDialog("pdf", file.getName(), "pdf");
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == REQUEST_TAKE_PHOTO) {
                try {
                    if (data.hasExtra("data")) {
                        bitmap = (Bitmap) data.getExtras().get("data");
                    }
//                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), picUri);
                    bitmap = getResizedBitmap(bitmap, 600);
//                    bitmap = rotateImageIfRequired(bitmap, picUri);

//                    Saving image to mobile internal memory for sometime
                    String root = getApplicationContext().getFilesDir().toString();
                    File myDir = new File(root + "/digilabs");
                    myDir.mkdirs();

                    Random generator = new Random();
                    int n = 10000;
                    n = generator.nextInt(n);
                    String fname = "Img" + n + ".jpg";

                    mCurrentPhotoPath = root + "/digilabs/" + fname;
                    File file1 = new File(myDir, fname);
                    saveFile(bitmap, file1);

                    addImagesToList(mCurrentPhotoPath);

                    File file = new File(mCurrentPhotoPath);
                    type = "camera";
                    if (!Utils.isNetworkConnected(AddReportActivity.this)) {
                        Utils.conDialog(AddReportActivity.this);
                    } else {
                        if (getIntent().getStringExtra("type").equals("update")) {
                            updateProfileAccount(file.getName(), getIntent().getStringExtra("report_id"), "image/jpeg");
                        } else {
                            openDialog("camera", file.getName(), "image/jpeg");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void addImagesToList(String path) {
        if (images_list.size() == 0) {
            images_list.add(0, path);
        } else if (images_list.size() == 1) {
            images_list.add(1, path);
        } else if (images_list.size() == 2) {
            images_list.add(2, path);
        } else if (images_list.size() == 3) {
            images_list.add(3, path);
        } else {
            images_list.add(4, path);
        }
    }

    public void convertImagesToPDF(String fname, String report_name, File file) {
        try {
            Image img = Image.getInstance(images_list.get(0));
            Document document = new Document(img);
            PdfWriter.getInstance(document, new FileOutputStream(file));
            document.open();
            for (String image : images_list) {
                img = Image.getInstance(image);
                document.setPageSize(img);
                document.newPage();
                img.setAbsolutePosition(0, 0);
                document.add(img);
            }
            document.close();
            mCurrentPhotoPath = file.getPath();
            saveProfileAccount(fname, report_name, "pdf", userfamilies_id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //    Saving file to the mobile internal memory
    private void saveFile(Bitmap sourceUri, File destination) {
        if (destination.exists()) destination.delete();
        try {
            FileOutputStream out = new FileOutputStream(destination);
            sourceUri.compress(Bitmap.CompressFormat.JPEG, 60, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }


        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    private static Bitmap rotateImageIfRequired(Bitmap img, Uri selectedImage) throws IOException {

        ExifInterface ei = new ExifInterface(selectedImage.getPath());
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static ArrayList<Report> jsonToArrayList(String jsonList) {
        ArrayList<Report> arrayList = new ArrayList<>();

        if (!jsonList.equals("")) {
            Gson gson = new Gson();
            Type type = new TypeToken<List<Report>>() {
            }.getType();
            arrayList = gson.fromJson(jsonList, type);
        }
        return arrayList;
    }

    public void arrayToJson(ArrayList<Report> arrayList) {

        Gson gson = new Gson();
        Type type = new TypeToken<List<Report>>() {
        }.getType();
        String stringData = gson.toJson(arrayList, type);

        edit.putString(getString(R.string.local_files), stringData);
        edit.apply();
    }

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB_MR1)
    public void animation() {
        ivBubble = (ImageView) findViewById(R.id.ivBubble);
        llAnimation = (LinearLayout) findViewById(R.id.llAnimation);

        ivBubble.setVisibility(View.INVISIBLE);
        ivBubble.animate().alpha(0.0f).setDuration(10);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.animate_doctor_slider);

        llAnimation.startAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

                ivBubble.setVisibility(View.VISIBLE);
                ivBubble.animate().alpha(1.0f).setDuration(500);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    int hits = 0;

    private void saveProfileAccount(final String filename, final String reportname, final String type, final String member) {
        hits++;
        MultipartRequest multipartRequest = new MultipartRequest(Request.Method.POST,
                Urls.URL_TO_WEB + "myreports/addapi", new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                Snackbar.make(llCamera, "Report Added Successfullly! - " + hits, Snackbar.LENGTH_LONG).show();
                progressDialog.dismiss();
                finishScreen();
                String resultResponse = new String(response.data);
                try {
                    JSONObject result = new JSONObject(resultResponse);
                    String status = result.getString("status");
                    String message = result.getString("message");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                progressDialog.dismiss();
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Snackbar.make(llCamera, "Error" + e, Snackbar.LENGTH_LONG).show();
                    }
                }
                Log.i("Error", errorMessage);
                error.printStackTrace();
                Snackbar.make(llCamera, "Error" + errorMessage, Snackbar.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", user_id);
                params.put("name", reportname);
                params.put("userfamilies_id", member);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                params.put("photo", new DataPart(filename, readBytesFromFile(), type));
                return params;
            }
        };
        VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }


    private void updateProfileAccount(final String filename, final String reportId, final String type) {
        MultipartRequest multipartRequest = new MultipartRequest(Request.Method.POST,
                Urls.URL_TO_WEB + "myreports/editApi/" + reportId, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                Snackbar.make(llCamera, "Report Updated Successfullly!", Snackbar.LENGTH_LONG).show();
                progress_bar.setVisibility(View.GONE);
                finishScreen();
                String resultResponse = new String(response.data);
                try {
                    JSONObject result = new JSONObject(resultResponse);
                    String status = result.getString("status");
                    String message = result.getString("message");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", user_id);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                params.put("photo", new DataPart(filename, readBytesFromFile(), type));
                return params;
            }
        };
        VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }


    private byte[] readBytesFromFile() {

        //  String fileTest = String.valueOf(UploadFileActivity.this.getResources().getDrawable(R.drawable.splashscreen));
        //String imageUri = "drawable://" + R.drawable.splashscreen;
        FileInputStream fileInputStream = null;
        byte[] bytesArray = null;


        try {
            File file = new File(mCurrentPhotoPath);
            bytesArray = new byte[(int) file.length()];

            //read file into bytes[]
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bytesArray);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }

        return bytesArray;

    }

    private void AllowPermissions() {
        int hasLocationPermission = ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int hasSMSPermission = ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CAMERA);
        List<String> permissions = new ArrayList<String>();
        if (hasLocationPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(android.Manifest.permission.CAMERA);
        }

        if (!permissions.isEmpty()) {
            ActivityCompat.requestPermissions(AddReportActivity.this, permissions.toArray(new String[permissions.size()]), 101);
        } else {
            if (intent_type.equals("camera")) {
                cameraIntent();
            } else {
                galleryIntent();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 101: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        Log.d("Permissions", "Permission Granted: " + permissions[i]);
                        if (intent_type.equals("camera")) {
                            cameraIntent();
                        } else {
                            galleryIntent();
                        }
                        return;
                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i]);
                        Snackbar.make(llPdf, "You are not allowed to take image", Snackbar.LENGTH_SHORT).show();
                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    public void finishScreen() {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(500);
                } catch (Exception e) {
                }
                handler.post(new Runnable() {
                    public void run() {
                        finish();
                    }
                });
            }
        }).start();
    }

    public void setToolbar() {

        setTitle("");
        toolbar = (Toolbar) findViewById(R.id.toolbar_image);
        setSupportActionBar(toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        ImageView ivImage = (ImageView) toolbar.findViewById(R.id.ivImage);
        ivImage.setImageResource(R.drawable.ic_add_report_white);
        mTitle.setText("Add a Report");

    }

    public void setNavDrawer() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        if (Helper.getCheck(sActivity).equals("doctor")){
            navigationView.getMenu().setGroupVisible(R.id.group_item5, false);
        }else {
            navigationView.getMenu().setGroupVisible(R.id.group_item5, true);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            Intent intent = new Intent(AddReportActivity.this, PreviewActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_my_favourites) {
            Intent intent = new Intent(AddReportActivity.this, BookedTestsActivity.class);
            startActivity(intent);
            Helper.setAddClass("addNo",sActivity);
        } else if (id == R.id.nav_Profile) {
            if (prefs.getString("role", null).equals("3")) {
                startActivity(new Intent(AddReportActivity.this, RegisterDoctor.class)
                        .putExtra("intent", "update"));
            } else {
                startActivity(new Intent(AddReportActivity.this, RegisterActivity.class)
                        .putExtra("intent", "update"));
            }
        } else if (id == R.id.nav_share_app) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    "Hey check out my app at: http://bit.ly/2xv0Zi8");
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        } else if (id == R.id.nav_register_lab) {
            startActivity(new Intent(AddReportActivity.this, RegisterUserToDoctor.class)
                    .putExtra("user_id", user_id));
//            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.elvektechnologies.com/Digilabs/"));
//            startActivity(browserIntent);
        } else if (id == R.id.nav_rate_app) {
            startActivity(new Intent(AddReportActivity.this, FeedbackActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}