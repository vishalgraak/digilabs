package com.sahir.websolutions.digilabs.Adapter;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sahir.websolutions.digilabs.R;
import com.sahir.websolutions.digilabs.Activity.Utils;
import com.sahir.websolutions.digilabs.objects.TestModel;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by SWSPC8 on 5/30/2017.
 */

public class BookTestGridAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    Context mContext;
    ArrayList<TestModel> allTests = new ArrayList<>();

    public BookTestGridAdapter(Context context, ArrayList<TestModel> allTests) {
        this.mContext = context;
        this.allTests = allTests;
    }

    @Override
    public int getCount() {
        return allTests.size();
    }

    @Override
    public Object getItem(int position) {
        return allTests.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v;
        if (convertView == null) {
            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.book_test_grid_adapter, null);
        }
        TextView tvUserName = (TextView) convertView.findViewById(R.id.tvLabel);
        ImageView img_cat = (ImageView) convertView.findViewById(R.id.img_cat);
        tvUserName.setTypeface(Utils.setTypeface(mContext));

        TestModel testModel = allTests.get(position);
        tvUserName.setText(testModel.name);

        if (testModel.sub_cat.size() > 0) {
            int count = testModel.sub_cat.size();
            for (int i = 0; i < testModel.sub_cat.size(); i++) {
                TestModel testModel1 = testModel.sub_cat.get(i);
                if (!testModel1.checkSwitch) {
                    count--;
                }
            }
            if (count == testModel.sub_cat.size()) {
                img_cat.setBackgroundResource(R.drawable.ic_bg_dark);
            } else if (count > 0) {
                img_cat.setBackgroundResource(R.drawable.ic_bg_light);
            } else {
                img_cat.setBackgroundResource(R.drawable.ic_bg_default);
            }
        } else {
            if(testModel.checkSwitch){
                img_cat.setBackgroundResource(R.drawable.ic_bg_dark);
            }else {
                img_cat.setBackgroundResource(R.drawable.ic_bg_default);
            }
        }

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        int size = 0;

        if (width <= 540 && height <= 960) {
            size = 50;
        } else if (width <= 720 && height <= 1280) {
            size = 70;
        } else if (width <= 1080 && height <= 1920) {
            size = 90;
        } else {
            size = 100;
        }

        if (testModel.cat_icon.equals("")) {
            img_cat.setImageResource(R.drawable.ic_logo);
        } else {
            Picasso.with(mContext).load(testModel.cat_icon).resize(size, size).placeholder(R.drawable.ic_logo).
                    into(img_cat, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {

                        }
                    });
        }

        return convertView;
    }
}
