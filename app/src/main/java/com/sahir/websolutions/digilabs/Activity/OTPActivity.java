package com.sahir.websolutions.digilabs.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sahir.websolutions.digilabs.HelperClass.Helper;
import com.sahir.websolutions.digilabs.R;

import org.json.JSONObject;

/**
 * Created by safal bhatia on 5/24/2017.
 */

public class OTPActivity extends AppCompatActivity {
    EditText editTextOTP;
    Button buttonValidate;
    TextView tvBubble, textViewResend;
    String phone = "", user_id, firstname, lastname, email, gender, dob, pin, is_active, otp, numberReceived;
    Toolbar toolbar;
    SharedPreferences pref;
    ProgressBar progress_bar;
    ProgressDialog progressDialog;
    RequestQueue requestQueue;
    Activity sActivity;
    int a = 1;
    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB_MR1)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        sActivity = OTPActivity.this;
        requestQueue = Volley.newRequestQueue(this);
        init();

        onClicks();

        if (!Utils.isNetworkConnected(OTPActivity.this)) {
            Utils.conDialog(OTPActivity.this);
        } else {
            progressDialog.show();
            sendOTP(user_id);

        }

        SmsReceiver.bindListener(new SmsListener() {
            @Override
            public void messageReceived(String messageText) {
                numberReceived = messageText.replaceAll("[^0-9]", "");
                Log.i("Received message", "" + numberReceived);
                editTextOTP.setText(numberReceived);
                editTextOTP.setSelection(editTextOTP.getText().length());
                progressDialog.setMessage("Verifying OTP....");
                progressDialog.show();
                verifyOTP(user_id, numberReceived);
            }
        });

    }

    private void init() {
        ActivityStack.activity.add(OTPActivity.this);
        progressDialog = new ProgressDialog(OTPActivity.this);
        progressDialog.setMessage("Processing...");
        editTextOTP = (EditText) findViewById(R.id.editTextOTP);
        buttonValidate = (Button) findViewById(R.id.buttonValidate);
        tvBubble = (TextView) findViewById(R.id.tvBubble);
        textViewResend = (TextView) findViewById(R.id.textViewResend);
        tvBubble.setTypeface(Utils.setTypeface(this));
        editTextOTP.setTypeface(Utils.setTypeface(this));
        buttonValidate.setTypeface(Utils.setTypeface(this));
        textViewResend.setTypeface(Utils.setTypeface(this));
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);

        //Toolbar code
        setTitle("");
        toolbar = (Toolbar) findViewById(R.id.toolbar_image);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("One Time Password");
        mTitle.setTypeface(Utils.setTypeface(this));
        pref = getSharedPreferences(getString(R.string.user_details), MODE_PRIVATE);

        final Intent intent = getIntent();
        user_id = intent.getStringExtra("user_id");
    }

    private void onClicks() {
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });


        buttonValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setMessage("Verifying OTP....");
                progressDialog.show();
                verifyOTP(user_id, editTextOTP.getText().toString().trim());
            }
        });

        textViewResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.show();
                sendOTP(user_id);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return true;
    }
    private void sendOTP(final String userId){

        StringRequest stringRequest = new StringRequest(Request.Method.POST,Urls.URL_TO_WEB + "users/sendOtp/" + userId,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        a++;
                        Log.e("Value: - ",String.valueOf(a));
                        //If we are getting success from server
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.optString("status");
                            if (status.equals("1")) {
                                JSONObject jobj = jsonObject.optJSONObject("data");
                                user_id = jobj.optString("id");
                                firstname = jobj.optString("firstname");
                                lastname = jobj.optString("lastname");
                                email = jobj.optString("email");
                                PreviewActivity.role = jobj.optString("role");
                                is_active = jobj.optString("is_active");
                                phone = jobj.optString("phone");
                                gender = jobj.optString("gender");
                                dob = jobj.optString("dob");
                                pin = jobj.optString("pin");
                                otp = jobj.optString("otp");
                            } else {
                                progressDialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
//                        //You can handle error here if you want
//                        Toast.makeText(OTPActivity.this, "Volly Error", Toast.LENGTH_SHORT).show();
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void verifyOTP(final String userId, final String otp_value) {
        FetchData.getInstance(OTPActivity.this).getRequestQueue().add(new StringRequest
                (Request.Method.GET, Urls.URL_TO_WEB + "users/varifyOtp/" + userId + "?otp=" + otp_value,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                progressDialog.dismiss();
                                Log.d("valRes", response);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String status = jsonObject.optString("status");
                                    if (status.equals("1")) {
                                        JSONObject jobj = jsonObject.optJSONObject("data");
                                        user_id = jobj.optString("id");
                                        firstname = jobj.optString("firstname");
                                        lastname = jobj.optString("lastname");
                                        email = jobj.optString("email");
                                        PreviewActivity.role = jobj.optString("role");
                                        is_active = jobj.optString("is_active");
                                        phone = jobj.optString("phone");
                                        gender = jobj.optString("gender");
                                        dob = jobj.optString("dob");
                                        pin = jobj.optString("pin");
                                        otp = jobj.optString("otp");
                                        Helper.setFirstName(firstname,sActivity);
                                        Helper.setLastName(lastname,sActivity);
                                        Helper.setEmail(email,sActivity);
                                        Helper.setPhone(phone,sActivity);
                                        saveUserDataSF(firstname, lastname, email, phone, pin,
                                                gender, dob, user_id, PreviewActivity.role, is_active);
                                        startActivity(new Intent(OTPActivity.this, PreviewActivity.class)
                                                .putExtra("role", PreviewActivity.role)
                                                .putExtra("home", "yes"));
                                    } else {
                                        Snackbar.make(buttonValidate, "Invalid OTP!", BaseTransientBottomBar.LENGTH_SHORT).show();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                    }
                }) {
        });
    }

    public void saveUserDataSF(String fname, String lname, String email, String phone, String pin,
                               String gender, String dob, String userId, String role, String isActive) {
        SharedPreferences.Editor pInfo = pref.edit();
        pInfo.putString(getString(R.string.user_first_name), fname);
        pInfo.putString(getString(R.string.user_last_name), lname);
        pInfo.putString(getString(R.string.user_email), email);
        pInfo.putString(getString(R.string.user_phone), phone);
        pInfo.putString("pin", pin);
        pInfo.putString(getString(R.string.user_gender), gender);
        pInfo.putString(getString(R.string.user_dob), dob);
        pInfo.putString(getString(R.string.user_id), userId);
        pInfo.putString("role", role);
        pInfo.putString("isActive", isActive);
        pInfo.putString("isFirst", "yes");
        pInfo.apply();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(),RegisterActivity.class);
        startActivity(intent);
    }
}
