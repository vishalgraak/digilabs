package com.sahir.websolutions.digilabs.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vishal on 09/05/2018.
 */
@Dao
public interface AddressDao {

    @Query("SELECT * FROM user_address")
    List<AddressModel> getAll();

    @Query("SELECT * FROM user_address WHERE mBillingEmail IN (:userIds)")
    List<AddressModel> loadAllByIds(int[] userIds);

    @Query("SELECT * FROM user_address WHERE mBillingEmail LIKE :email AND "
            + "mBillingName LIKE :name LIMIT 1")
    AddressModel findByName(String email, String name);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(AddressModel... users);

    @Delete
    void delete(AddressModel user);
}

