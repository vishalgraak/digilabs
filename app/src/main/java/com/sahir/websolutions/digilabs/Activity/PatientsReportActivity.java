package com.sahir.websolutions.digilabs.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sahir.websolutions.digilabs.Adapter.ReportsListAdapter;
import com.sahir.websolutions.digilabs.HelperClass.Helper;
import com.sahir.websolutions.digilabs.R;
import com.sahir.websolutions.digilabs.objects.ComparatorDate;
import com.sahir.websolutions.digilabs.objects.ComparatorName;
import com.sahir.websolutions.digilabs.objects.Report;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import me.philio.pinentry.PinEntryView;


public class PatientsReportActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    ListView lvReport;
    Toolbar toolbar;
    EditText etSearch;
    ImageView img_sort, img_starred;
    public ArrayList<Report> arrayListReport = new ArrayList<>();
    public ArrayList<Report> arrayListStarReport = new ArrayList<>();
    public ArrayList<Report> arrayListPDFReport = new ArrayList<>();
    public ArrayList<Report> arrayListDICOMReport = new ArrayList<>();
    ReportsListAdapter reportsListAdapter;
    SharedPreferences prefs;
    FloatingActionButton Fab;
    SharedPreferences.Editor edit;
    String localFilesString;
    boolean starList = false;
    TextView txt_no_reports;
    LinearLayout ll_reports, ll_sort;
    public static String user_id;
    private Dialog dialog;
    ProgressDialog progressDialog;
    Activity sActivity;
    Context sActivity1;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.local_report_main);
        sActivity = PatientsReportActivity.this;
        sActivity1 = getApplicationContext();
        init();

        setToolbar();

        setNavDrawer();

        onClicks();

        if (!Utils.isNetworkConnected(PatientsReportActivity.this)) {
            Utils.conDialog(PatientsReportActivity.this);
        } else {
            img_starred.setImageResource(R.drawable.ic_starred);
            getMyReports();
        }

        if (prefs.getString("isFirstP", null) != null) {
            if (prefs.getString("isFirstP", null).equals("yes")) {
                showDialog(prefs.getString("role", null));
                editor = prefs.edit();


            }
        }
    }

    private void init() {
        ActivityStack.activity.add(PatientsReportActivity.this);
        progressDialog = new ProgressDialog(PatientsReportActivity.this);
        progressDialog.setMessage("Fetching Reports....");
        progressDialog.show();
        prefs = getSharedPreferences(getString(R.string.user_details), Context.MODE_PRIVATE);
        localFilesString = prefs.getString(getString(R.string.local_files), "");
        user_id = prefs.getString(getString(R.string.user_id), null);
        edit = prefs.edit();

        lvReport = (ListView) findViewById(R.id.lvReport);
        img_sort = (ImageView) findViewById(R.id.img_sort);
        img_starred = (ImageView) findViewById(R.id.img_starred);
        etSearch = (EditText) findViewById(R.id.etSearch);
        txt_no_reports = (TextView) findViewById(R.id.txt_no_reports);
        ll_reports = (LinearLayout) findViewById(R.id.ll_reports);
        ll_sort = (LinearLayout) findViewById(R.id.ll_sort);
        ll_sort.setVisibility(View.GONE);
        Fab = (FloatingActionButton) findViewById(R.id.Fab);
        Fab.setVisibility(View.GONE);
        etSearch.setTypeface(Utils.setTypeface(this));
        txt_no_reports.setTypeface(Utils.setTypeface(this));
    }
    @Override
    public void onStop() {
        super.onStop();
        progressDialog.dismiss(); // try this
    }
    private void onClicks() {
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });

        img_starred.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayStarList();
            }
        });

        img_sort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSortDialog();
            }
        });


        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (etSearch.getText().toString().equals("")) {
                    displayList(0);
                } else
                    searchList();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void openSortDialog() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_report_sort);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        if (height >= 800) {
            lp.height = height / 2;
        } else if (height >= 1280) {
            lp.height = height / 2;
        } else if (height >= 1920) {
            lp.height = height / 2;
        } else if (height >= 2560) {
            lp.height = height / 2;
        }
        dialog.getWindow().setAttributes(lp);
        dialog.show();

        TextView tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);
        TextView tvNext = (TextView) dialog.findViewById(R.id.tvNext);
        final RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.radioGroup);

        ((RadioButton) radioGroup.getChildAt(0)).setChecked(true);

        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int radioButtonId = radioGroup.getCheckedRadioButtonId();
                RadioButton radioButton = (RadioButton) dialog.findViewById(radioButtonId);
                String name = radioButton.getText().toString();
                switch (name) {
                    case "Name":
                        Collections.sort(arrayListReport, new ComparatorName());
                        reportsListAdapter = new ReportsListAdapter(PatientsReportActivity.this, arrayListReport, 0, true);
                        lvReport.setAdapter(reportsListAdapter);
                        break;
                    case "Date":
                        Collections.sort(arrayListReport, new ComparatorDate());
                        Collections.reverse(arrayListReport);
                        reportsListAdapter = new ReportsListAdapter(PatientsReportActivity.this, arrayListReport, 0, true);
                        lvReport.setAdapter(reportsListAdapter);
                        break;
                    case "File type PDF":
                        displayPDFList();
                        break;
                    case "File type DICOM":
                        displayImageList();
                        break;
                }
                dialog.dismiss();
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    public void searchList() {
        ArrayList<Report> searchArray = new ArrayList<>();
        String searchChar = etSearch.getText().toString().trim();
        for (int i = 0; i < arrayListReport.size(); i++) {
            if (arrayListReport.get(i).getName().toLowerCase().contains(searchChar.toLowerCase())) {
                searchArray.add(arrayListReport.get(i));
            }
        }
        reportsListAdapter = new ReportsListAdapter(this, searchArray, 0, true);
        lvReport.setAdapter(reportsListAdapter);
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    public void showDialog(String role) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (role.equals("3")) {
            dialog.setContentView(R.layout.dialog_doctor_pin);
            TextView txt_anim1 = (TextView) dialog.findViewById(R.id.txt_anim1);
            txt_anim1.setText("Your privacy is our primary concern. Please enter your six digit pin.");
        } else {
            dialog.setContentView(R.layout.dialog_report_pin);
        }
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
//        if (height >= 800) {
//            lp.height = height / 2;
//        } else if (height >= 1280) {
//            lp.height = height / 2;
//        } else if (height >= 1920) {
//            lp.height = height / 2;
//        } else if (height >= 2560) {
//            lp.height = height / 2;
//        }
        dialog.getWindow().setAttributes(lp);
        dialog.show();

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });

        final PinEntryView pinEntryView = (PinEntryView) dialog.findViewById(R.id.pin_entry_simple);
        pinEntryView.setOnPinEnteredListener(new PinEntryView.OnPinEnteredListener() {
            @Override
            public void onPinEntered(String pin) {
                if (prefs.getString("pin", null).equals(pin)) {
                    dialog.dismiss();
                    editor.putString("isFirstP", "no");
                    editor.commit();
                } else {
                    pinEntryView.clearText();
                    Snackbar.make(pinEntryView, "Please enter correct pin to view reports", BaseTransientBottomBar.LENGTH_SHORT).show();
                }
            }
        });

        TextView tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                finish();
            }
        });

    }

    public void displayStarList() {
        etSearch.setText("");
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        if (!starList) {
            arrayListStarReport.clear();
            for (int i = 0; i < arrayListReport.size(); i++) {
                if (arrayListReport.get(i).getIsstar().equals("yes"))
                    arrayListStarReport.add(arrayListReport.get(i));
            }
            reportsListAdapter = new ReportsListAdapter(this, arrayListStarReport, 0, true);
            lvReport.setAdapter(reportsListAdapter);
            img_starred.setImageResource(R.drawable.star_green);
            starList = true;
        } else {
            displayList(0);
            img_starred.setImageResource(R.drawable.ic_starred);
            starList = false;
        }
    }

    public void displayPDFList() {
        arrayListPDFReport.clear();
        for (int i = 0; i < arrayListReport.size(); i++) {
            if (arrayListReport.get(i).getType().equals("pdf"))
                arrayListPDFReport.add(arrayListReport.get(i));
        }
        reportsListAdapter = new ReportsListAdapter(this, arrayListPDFReport, 0, true);
        lvReport.setAdapter(reportsListAdapter);
    }

    public void displayImageList() {
        arrayListDICOMReport.clear();
        for (int i = 0; i < arrayListReport.size(); i++) {
            if (arrayListReport.get(i).getType().equals("image"))
                arrayListDICOMReport.add(arrayListReport.get(i));
        }
        reportsListAdapter = new ReportsListAdapter(this, arrayListDICOMReport, 0, true);
        lvReport.setAdapter(reportsListAdapter);
    }

    public void displayList(int type) {
        if (arrayListReport.size() == 0) {
            ll_reports.setVisibility(View.GONE);
            txt_no_reports.setVisibility(View.VISIBLE);
            txt_no_reports.setText("No Patient's Reports yet.");
        } else {
            ll_reports.setVisibility(View.VISIBLE);
            txt_no_reports.setVisibility(View.GONE);
            Collections.sort(arrayListReport, new ComparatorDate());
            Collections.reverse(arrayListReport);
            reportsListAdapter = new ReportsListAdapter(this, arrayListReport, type, false);
            lvReport.setAdapter(reportsListAdapter);
        }
    }

    private void getMyReports() {
        FetchData.getInstance(PatientsReportActivity.this).getRequestQueue().add(new StringRequest
                (Request.Method.POST, Urls.URL_TO_WEB + "reportshares/viewShareReportByDoctor/" + user_id,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                Log.d("valRes", response);
                                progressDialog.dismiss();
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String status = jsonObject.optString("status");
                                    if (status.equals("1")) {
                                        arrayListReport.clear();
                                        JSONArray data = jsonObject.optJSONArray("data");
                                        if (data != null) {
                                            for (int i = 0; i < data.length(); i++) {
                                                JSONObject jobj = data.getJSONObject(i);
                                                Report report = new Report();
                                                report.setId(jobj.optString("id"));
                                                report.setUser_id(user_id);
                                                report.setName(jobj.optString("name"));
                                                report.setPath(jobj.optString("labfile"));
                                                report.setReportsList(jobj.getJSONArray("file").toString());
                                                if (jobj.optString("photo").endsWith("pdf")) {
                                                    report.setType("pdf");
                                                } else {
                                                    report.setType("image");
                                                }
                                                report.setDate(jobj.optString("created"));
                                                report.report_type = "patients";
                                                arrayListReport.add(report);
                                            }
                                        }
                                        displayList(0);
                                    } else {
                                        ll_reports.setVisibility(View.GONE);
                                        txt_no_reports.setVisibility(View.VISIBLE);
                                        txt_no_reports.setText("No Patient's Reports yet.");
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                    }
                }) {
        });
    }

    public void setToolbar() {
        setTitle("");
        toolbar = (Toolbar) findViewById(R.id.toolbar_image);
        setSupportActionBar(toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        ImageView ivImage = (ImageView) toolbar.findViewById(R.id.ivImage);
        ivImage.setImageResource(R.drawable.ic_patients_white);
        mTitle.setText("Patient's Reports");
        Helper.setPatient("patient",sActivity1);
        mTitle.setTypeface(Utils.setTypeface(this));
    }

    public void setNavDrawer() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        TextView tvCopyright = (TextView) findViewById(R.id.tvCopyright);
        tvCopyright.setTypeface(Utils.setTypeface(this));

        Menu m = navigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);
        }
        if (Helper.getCheck(sActivity).equals("doctor")){
            navigationView.getMenu().setGroupVisible(R.id.group_item5, false);
        }else {
            navigationView.getMenu().setGroupVisible(R.id.group_item5, true);
        }
    }

    private void applyFontToMenuItem(MenuItem mi) {
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", Utils.setTypeface(this)), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        finish();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            Intent intent = new Intent(PatientsReportActivity.this, PreviewActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_my_favourites) {
            Intent intent = new Intent(PatientsReportActivity.this, BookedTestsActivity.class);
            startActivity(intent);
            Helper.setAddClass("addNo",sActivity);
        } else if (id == R.id.nav_Profile) {
            if (prefs.getString("role", null).equals("3")) {
                startActivity(new Intent(PatientsReportActivity.this, RegisterDoctor.class)
                        .putExtra("intent", "update"));
            } else {
                startActivity(new Intent(PatientsReportActivity.this, RegisterActivity.class)
                        .putExtra("intent", "update"));
            }
        } else if (id == R.id.nav_share_app) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    "Hey check out my app at: http://bit.ly/2xv0Zi8");
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        } else if (id == R.id.nav_register_lab) {
            startActivity(new Intent(PatientsReportActivity.this, RegisterUserToDoctor.class)
                    .putExtra("user_id", user_id));
//            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.elvektechnologies.com/Digilabs/"));
//            startActivity(browserIntent);
        } else if (id == R.id.nav_rate_app) {
            startActivity(new Intent(PatientsReportActivity.this, FeedbackActivity.class));
        } else if (id == R.id.nav_logout_app) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.clear();
            editor.commit();
            for (int i = 0; i < ActivityStack.activity
                    .size(); i++) {
                (ActivityStack.activity.elementAt(i))
                        .finish();
                Log.e("Activity size:" + ActivityStack.activity.size(), "finished...");
            }
            startActivity(new Intent(PatientsReportActivity.this, RegisterActivity.class).putExtra("intent", "register"));
            finish();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
