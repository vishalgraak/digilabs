package com.sahir.websolutions.digilabs.objects;

import java.util.Comparator;

/**
 * Created by Sys9 on 6/30/2017.
 */

public class ComparatorDistance implements Comparator<LabModel> {
    @Override
    public int compare(LabModel o1, LabModel o2) {
        return o1.distance < o2.distance ? -1
                : o1.distance > o2.distance ? 1
                : 0;
    }
}
