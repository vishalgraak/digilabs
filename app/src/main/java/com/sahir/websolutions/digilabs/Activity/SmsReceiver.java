package com.sahir.websolutions.digilabs.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

/**
 * Created by Bhavya on 15-09-2016.
 */
public class SmsReceiver extends BroadcastReceiver {

    private static SmsListener listener;

    @Override
    public void onReceive(Context context, Intent intent) {


        final Bundle bundle = intent.getExtras();
        try {
            if (bundle != null) {
                final Object[] pdusObj = (Object[]) bundle.get("pdus");
                for (int i = 0; i < pdusObj.length; i++) {
                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();
                    String senderNum = phoneNumber;
                    String message = currentMessage.getDisplayMessageBody();
                    try {
                        String messageBody = currentMessage.getMessageBody();
                        listener.messageReceived(messageBody);
                    } catch (Exception e) {
                    }

                }
            }

        } catch (Exception e) {

        }
    }


    public static void bindListener(SmsListener smsListener) {
        listener = smsListener;
    }
}
