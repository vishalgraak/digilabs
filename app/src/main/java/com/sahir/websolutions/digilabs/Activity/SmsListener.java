package com.sahir.websolutions.digilabs.Activity;

/**
 * Created by Bhavya on 15-09-2016.
 */
public interface SmsListener {

    public void messageReceived(String messageText);
}
