package com.sahir.websolutions.digilabs.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sahir.websolutions.digilabs.Adapter.BookTestGridAdapter;
import com.sahir.websolutions.digilabs.HelperClass.Helper;
import com.sahir.websolutions.digilabs.R;
import com.sahir.websolutions.digilabs.objects.TestModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class BookTestActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    GridView gvTests;
    BookTestGridAdapter bookTest;
    ListView lvCustomTestList;
    AutoCompleteTextView etSearch;
    TextView tvDivider, tvSelect;
    ArrayList<TestModel> sub_cat = new ArrayList<>();
    LinearLayout llTestSelections;
    private View v;
    ArrayList<String> testList = new ArrayList<>();
    ArrayList<String> testadded = new ArrayList<>();
    ArrayList<String> idadded = new ArrayList<>();
    Toolbar toolbar;
    ArrayList<TestModel> testModelArrayList = new ArrayList<>();
    HorizontalScrollView horizontalScrollView;
    SharedPreferences prefs;
    String labId, lab_type, user_id;
    ArrayList<String> testsName = new ArrayList<>();
    ArrayList<String> testsId = new ArrayList<>();
    ProgressDialog dialog;
    ArrayList<String> testsSearched = new ArrayList<>();
    ArrayList<String> idsSearched = new ArrayList<>();
    ListView lvSearch;
    Activity sActivity;
    String all_ids = "";
    ArrayList<String> all_ads1 = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.book_test_main);
        sActivity = BookTestActivity.this;
        init();

        setToolbar();

        setNavDrawer();

        onClicks();

        if (!Utils.isNetworkConnected(BookTestActivity.this)) {
            Utils.conDialog(BookTestActivity.this);
        } else {
            if (getIntent().getStringExtra("type").equals("lab")) {
                labId = getIntent().getStringExtra("lab_id");
                lab_type = "lab";
                String url = "labs/getLabTest/" + labId;
//                getCategories(url);
                getSelectedTest(url);
            } else {
                lab_type = "test";
                String url = "tests/getAllTest2";
                getCategories(url);
            }
        }
    }

    private void init() {
        ActivityStack.activity.add(BookTestActivity.this);
        dialog = new ProgressDialog(BookTestActivity.this);
        dialog.setMessage("Fetching Tests....");
        dialog.show();
        gvTests = (GridView) findViewById(R.id.gvTests);
        lvCustomTestList = (ListView) findViewById(R.id.lvCustomTestList);
        etSearch = (AutoCompleteTextView) findViewById(R.id.etSearch);
        llTestSelections = (LinearLayout) findViewById(R.id.llTestSelections);
        tvDivider = (TextView) findViewById(R.id.tvDivider);
        tvSelect = (TextView) findViewById(R.id.tvSelect);
        tvDivider.setTypeface(Utils.setTypeface(this));
        tvSelect.setTypeface(Utils.setTypeface(this));
        etSearch.setTypeface(Utils.setTypeface(this));
        horizontalScrollView = (HorizontalScrollView) findViewById(R.id.horizontalScrollView);
        prefs = getSharedPreferences(getString(R.string.user_details), MODE_PRIVATE);
        lvSearch = (ListView) findViewById(R.id.lvSearch);
        user_id = prefs.getString(getString(R.string.user_id), null);
    }

    private void onClicks() {
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String query = s.toString();
                if (query.length() > 1) {
                    testsSearched.clear();
                    idsSearched.clear();
                    for (int i = 0; i < testsName.size(); i++) {
                        if (testsName.get(i).toLowerCase().trim().contains(query.toLowerCase().trim())) {
                            idsSearched.add(testsId.get(i));
                            testsSearched.add(testsName.get(i));
                        }
                    }
                    lvSearch.setVisibility(View.VISIBLE);
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(BookTestActivity.this,
                            android.R.layout.simple_list_item_1, testsSearched);
                    lvSearch.setAdapter(adapter);
                    if (testsSearched.size() > 0) {
                        adapter.notifyDataSetChanged();
                    }
                } else {
                    lvSearch.setVisibility(View.GONE);
                }
            }
        });


        lvSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                etSearch.setText("");
                lvSearch.setVisibility(View.GONE);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                if (idadded.contains(idsSearched.get(position))) {
                    Toast.makeText(getApplicationContext(), testsSearched.get(position) + " already added", Toast.LENGTH_SHORT).show();
                } else {
                    for (int i = 0; i < testModelArrayList.size(); i++) {
                        TestModel testModel = testModelArrayList.get(i);
                        if (testModel.name.equals(testsSearched.get(position))) {
                            sub_cat = testModel.sub_cat;
                            for (int j = 0; j < sub_cat.size(); j++) {
                                TestModel testModel1 = sub_cat.get(j);
                                testModel1.checkSwitch = true;
                                if (!idadded.contains(testModel1.cat_id)) {
                                    testadded.add(testModel1.cat_name);
                                    idadded.add(testModel1.cat_id);
                                }
                            }

                            String[] array = new String[sub_cat.size()];
                            boolean[] selected = new boolean[array.length];
                            for (int k = 0; k < sub_cat.size(); k++) {
                                TestModel testModel1 = sub_cat.get(k);
                                array[k] = testModel1.cat_name;
                                if (testModel1.checkSwitch) {
                                    selected[k] = true;
                                } else {
                                    selected[k] = false;
                                }
                            }
                            showSubCat("Select" + testModel.cat_name + " Tests", array, selected, sub_cat);

                            if (testadded.size() > 0) {
                                llTestSelections.removeAllViews();
                                for (int j = 0; j < testadded.size(); j++) {
                                    addTestToView(idadded.get(j), testadded.get(j), llTestSelections);
                                }
                            }
                            break;
                        } else {
                            testadded.add(testsSearched.get(position));
                            idadded.add(idsSearched.get(position));
                            addTestToView(idsSearched.get(position), testsSearched.get(position), llTestSelections);
                            break;
                        } /*else if (testModel.sub_cat.get(i).cat_id.equals(idsSearched.get(position))) {
                            sub_cat = testModel.sub_cat;
                            for (int j = 0; j < sub_cat.size(); j++) {
                                TestModel testModel1 = sub_cat.get(j);
                                if (testModel1.cat_id.equals(idsSearched.get(position))) {
                                    testModel1.checkSwitch = true;
                                    testadded.add(testsSearched.get(position));
                                    idadded.add(idsSearched.get(position));
                                    addTestToView(testsSearched.get(position), llTestSelections);
                                }
                            }
                            bookTest.notifyDataSetChanged();
                        }*/
                    }
                }
            }
        });

        gvTests.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                sub_cat = testModelArrayList.get(position).sub_cat;
                if (sub_cat.size() == 0) {
                    if (idadded.contains(testModelArrayList.get(position).cat_id)) {
                        Toast.makeText(BookTestActivity.this, testModelArrayList.get(position).name
                                + " already added", Toast.LENGTH_SHORT).show();
                    } else {
                        testadded.add(testModelArrayList.get(position).name);
                        idadded.add(testModelArrayList.get(position).cat_id);
                        testModelArrayList.get(position).checkSwitch = true;
                        addTestToView(testModelArrayList.get(position).cat_id, testModelArrayList.
                                get(position).name, llTestSelections);
                        bookTest.notifyDataSetChanged();
                    }
                } else {
                    for (int i = 0; i < sub_cat.size(); i++) {
                        TestModel testModel = sub_cat.get(i);
                        if (idadded.contains(testModel.cat_id)) {
                            testModel.checkSwitch = true;
                        }
                    }
                    String[] array = new String[sub_cat.size()];
                    boolean[] selected = new boolean[array.length];
                    for (int i = 0; i < sub_cat.size(); i++) {
                        TestModel testModel = sub_cat.get(i);
                        array[i] = testModel.cat_name;
                        if (testModel.checkSwitch) {
                            selected[i] = true;
                        } else {
                            selected[i] = false;
                        }
                    }
                    showSubCat("Select Tests", array, selected, testModelArrayList.get(position).sub_cat);
                }
            }
        });
    }

    public void showSubCat(String title, final String[] items, final boolean[] checked, final ArrayList<TestModel> list) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle(title)
                .setMultiChoiceItems(items, checked, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int indexSelected, boolean isChecked) {
                        list.get(indexSelected).checkSwitch = isChecked;
                        if (!isChecked) {
                            testadded.remove(list.get(indexSelected).cat_name);
                            idadded.remove(list.get(indexSelected).cat_id);
                        }
                    }
                }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        for (int i = 0; i < checked.length; i++) {
                            boolean check = checked[i];
                            if (check) {
                                if (!idadded.contains(list.get(i).cat_id)) {
                                    testadded.add(list.get(i).cat_name);
                                    idadded.add(list.get(i).cat_id);
                                }
                            }
                        }
                        llTestSelections.removeAllViews();
                        if (testadded.size() > 0) {
                            for (int j = 0; j < testadded.size(); j++) {
                                addTestToView(idadded.get(j), testadded.get(j), llTestSelections);
                            }
                        } else {
                            horizontalScrollView.setVisibility(View.GONE);
                        }
                        bookTest.notifyDataSetChanged();
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // dialog.dismiss();
                    }
                }).create();
        dialog.show();
    }

    public void getCategories(String url) {
        FetchData.getInstance(BookTestActivity.this).getRequestQueue().add(new StringRequest
                (Request.Method.GET, Urls.URL_TO_WEB + url,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                Log.d("valRes", response);
                                dialog.dismiss();
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONArray data = jsonObject.getJSONArray("data");
                                    testsName.clear();
                                    testsId.clear();
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject jobj = data.getJSONObject(i);
                                        if (jobj.has("id")) {
                                            TestModel testModel = new TestModel();
                                            testModel.cat_id = jobj.optString("id");
                                            testModel.cat_name = jobj.optString("name");
                                            testModel.name = jobj.optString("name");
//                                        testModel.name = jobj.optString("name") + "\n"
//                                                + "[" + 1 + " Test]";
                                            testModel.cat_icon = jobj.optString("icon");
                                            testsId.add(testModel.cat_id);
                                            testsName.add(testModel.name);
                                            JSONArray sub_array = jobj.optJSONArray("children");
//                                        int count = 0;
                                            if (sub_array.length() != 0) {
                                                for (int j = 0; j < sub_array.length(); j++) {
                                                    JSONObject sub = sub_array.getJSONObject(j);
                                                    TestModel testModel1 = new TestModel();
                                                    testModel1.checkSwitch = false;
                                                    JSONArray child_arr = sub.optJSONArray("children");
                                                    if (child_arr.length() != 0) {
//                                                    count = count + child_arr.length();
                                                        testModel1.cat_id = sub.optString("id");
                                                        testModel1.cat_name = sub.optString("name");
                                                        testsId.add(testModel1.cat_id);
//                                                    testModel1.cat_name = sub.optString("name") + "\n" + "[" + child_arr.length() + " Tests]";
                                                        testsName.add(testModel1.cat_name);
                                                        for (int k = 0; k < child_arr.length(); k++) {
                                                            JSONObject sub_obj = child_arr.getJSONObject(k);
                                                            TestModel testModel2 = new TestModel();
                                                            testModel2.cat_id = sub_obj.optString("id");
                                                            testModel2.cat_name = sub_obj.optString("name");
//                                                        testModel2.cat_name = sub_obj.optString("name") + "\n"
//                                                                + "[" + 1 + " Test]";
                                                            testModel2.checkSwitch = false;
                                                            testModel.sub_tests.add(testModel2);
                                                            testsId.add(testModel2.cat_id);
                                                            testsName.add(sub_obj.optString("name"));
//                                                        testsName.add(sub_obj.optString("name") + "\n"
//                                                                + "[" + 1 + " Test]");
                                                        }
                                                    } else {
                                                        testModel1.cat_id = sub.optString("id");
                                                        testModel1.cat_name = sub.optString("name");
                                                        testsId.add(testModel1.cat_id);
                                                        testsName.add(testModel1.cat_name);
//                                                    testsName.add(testModel1.cat_name + "\n"
//                                                            + "[" + 1 + " Test]");
                                                    }
                                                    testModel.sub_cat.add(testModel1);
                                                }
                                            }
                                        /*if (count > 0) {
                                            testsId.add(testModel.cat_id);
                                            testModel.name = jobj.optString("name") + "\n"
                                                    + "[" + count + " Tests]";
                                            testsName.add(testModel.name);
                                        } else {
                                            testsId.add(testModel.cat_id);
                                            testsName.add(testModel.name + "\n"
                                                    + "[" + 1 + " Test]");
                                        }*/
                                            testModelArrayList.add(testModel);
                                        }
                                    }
                                    bookTest = new BookTestGridAdapter(BookTestActivity.this, testModelArrayList);
                                    gvTests.setAdapter(bookTest);
                                } catch (Exception e) {
                                    Log.e("CAT", "" + e.getMessage());
                                }
                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                    }
                }) {
        });
    }

    public void getSelectedTest(String url) {
        FetchData.getInstance(BookTestActivity.this).getRequestQueue().add(new StringRequest
                (Request.Method.GET, Urls.URL_TO_WEB + url,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                Log.d("valRes", response);
                                dialog.dismiss();
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONArray data = jsonObject.getJSONArray("data");
                                    testsName.clear();
                                    testsId.clear();
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject jobj = data.getJSONObject(i);
                                        if (jobj.has("id")) {
                                            TestModel testModel = new TestModel();
                                            testModel.cat_id = jobj.optString("id");
                                            testModel.cat_name = jobj.optString("name");//blood
                                            testModel.name = jobj.optString("name");
                                            testModel.cat_icon = jobj.optString("icon");
                                            testsId.add(testModel.cat_id);
                                            testsName.add(testModel.name);
                                            if (jobj.has("children")) {
                                                JSONArray sub_array = jobj.optJSONArray("children");
                                                if (sub_array.length() != 0) {
                                                    for (int j = 0; j < sub_array.length(); j++) {
                                                        JSONObject sub = sub_array.getJSONObject(j);
                                                        TestModel testModel1 = new TestModel();
                                                        testModel1.cat_id = sub.optString("id");
                                                        testModel1.cat_name = sub.optString("name");//cbc
                                                        all_ids = sub.optString("childvalue");
                                                        all_ads1.add(all_ids);
                                                        testModel1.checkSwitch = false;

                                                        if (sub.has("children")) {
                                                            JSONArray child_arr = sub.optJSONArray("children");
                                                            if (child_arr.length() != 0) {
                                                                testModel1.cat_id = sub.optString("id");
                                                                testModel1.cat_name = sub.optString("name");
                                                                testsId.add(testModel1.cat_id);
                                                                testsName.add(testModel1.cat_name);
                                                                for (int k = 0; k < child_arr.length(); k++) {
                                                                    JSONObject sub_obj = child_arr.getJSONObject(k);
                                                                    TestModel testModel2 = new TestModel();
                                                                    testModel2.cat_id = sub_obj.optString("id");
                                                                    testModel2.cat_name = sub_obj.optString("name");
                                                                    testModel2.checkSwitch = false;
                                                                    testModel.sub_tests.add(testModel2);
                                                                    testsId.add(testModel2.cat_id);
                                                                    testsName.add(sub_obj.optString("name"));
                                                                }
                                                            } else {
                                                                testModel1.cat_id = sub.optString("id");
                                                                testModel1.cat_name = sub.optString("name");
                                                                testsId.add(testModel1.cat_id);
                                                                testsName.add(testModel1.cat_name);
                                                            }
                                                        }
                                                        testModel.sub_cat.add(testModel1);
                                                    }
                                                }
                                            }
                                            testModelArrayList.add(testModel);
                                        }
                                    }
                                    bookTest = new BookTestGridAdapter(BookTestActivity.this, testModelArrayList);
                                    gvTests.setAdapter(bookTest);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                    }
                }) {
        });
    }

    public void addTestToView(String id, final String test, LinearLayout parent) {
        if (testadded.size() == 0) {
            horizontalScrollView.setVisibility(View.GONE);
        } else {
            horizontalScrollView.setVisibility(View.VISIBLE);
            v = getLayoutInflater().inflate(R.layout.selected_test_list, null);
            final TextView tv = (TextView) v.findViewById(R.id.tvTestName);
            ImageView iv = (ImageView) v.findViewById(R.id.ivDeselectTest);
            tv.setTextColor(getResources().getColor(R.color.GreyText));
            tv.setTypeface(Utils.setTypeface(BookTestActivity.this));
            tv.setText(test);
            tv.setId(Integer.parseInt(id));
            parent.addView(v);
            horizontalScrollView.postDelayed(new Runnable() {
                public void run() {
                    horizontalScrollView.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
                }
            }, 100L);
            iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    llTestSelections.removeView((View) v.getParent());
                    testList.remove(tv.getText());
                    testadded.remove(tv.getText());
                    idadded.remove(String.valueOf(tv.getId()));
                    for (int j = 0; j < testModelArrayList.size(); j++) {
                        TestModel testModel1 = testModelArrayList.get(j);
                        if (testadded.contains(testModel1.cat_name)) {
                            testModel1.checkSwitch = true;
                        } else {
                            testModel1.checkSwitch = false;
                        }
                        for (int i = 0; i < testModelArrayList.get(j).sub_cat.size(); i++) {
                            TestModel testModel = testModelArrayList.get(j).sub_cat.get(i);
                            if (testadded.contains(testModel.cat_name)) {
                                testModel.checkSwitch = true;
                            } else {
                                testModel.checkSwitch = false;
                            }
                        }
                    }
                    bookTest.notifyDataSetChanged();
                    Log.d("testList", String.valueOf(testList));
                    if (testadded.size() == 0) {
                        horizontalScrollView.setVisibility(View.GONE);
                    }
                }
            });
        }

    }

    public void setToolbar() {
        setTitle("");
        toolbar = (Toolbar) findViewById(R.id.toolbar_image);
        setSupportActionBar(toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        ImageView ivImage = (ImageView) toolbar.findViewById(R.id.ivImage);
        ivImage.setImageResource(R.drawable.ic_book_test_white);
        mTitle.setText("Book a Test");
        mTitle.setTypeface(Utils.setTypeface(this));
    }

    public void setNavDrawer() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        TextView tvCopyright = (TextView) findViewById(R.id.tvCopyright);
        tvCopyright.setTypeface(Utils.setTypeface(this));

        Menu m = navigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);
        }
        if (Helper.getCheck(sActivity).equals("doctor")){
            navigationView.getMenu().setGroupVisible(R.id.group_item5, false);
        }else {
            navigationView.getMenu().setGroupVisible(R.id.group_item5, true);
        }
    }

    private void applyFontToMenuItem(MenuItem mi) {
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", Utils.setTypeface(this)), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.skip, menu);

        MenuItem register = menu.findItem(R.id.skip);
        register.setVisible(false);

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.next:
                if (testadded.size() != 0) {
                    String test = android.text.TextUtils.join(",", idadded);
                    String test_names = android.text.TextUtils.join(",", testadded);
                    Log.i("BookTest", "Test Selected" + test);
                    Intent intent = new Intent(BookTestActivity.this, LabListActivity.class);
                    intent.putExtra("tests", test);
                    intent.putExtra("lab_id1",labId);
                    intent.putExtra("test_names", test_names);
                    intent.putExtra("lab_type", lab_type);
                    startActivity(intent);
                }
                return true;
        }
        return true;
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            Intent intent = new Intent(BookTestActivity.this, PreviewActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_my_favourites) {
            Intent intent = new Intent(BookTestActivity.this, BookedTestsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_Profile) {
            if (prefs.getString("role", null).equals("3")) {
                startActivity(new Intent(BookTestActivity.this, RegisterDoctor.class)
                        .putExtra("intent", "update"));
            } else {
                startActivity(new Intent(BookTestActivity.this, RegisterActivity.class)
                        .putExtra("intent", "update"));
            }
        } else if (id == R.id.nav_share_app) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    "Hey check out my app at: http://bit.ly/2xv0Zi8");
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        } else if (id == R.id.nav_register_lab) {
            startActivity(new Intent(BookTestActivity.this, RegisterUserToDoctor.class)
                    .putExtra("user_id", user_id));
//            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.elvektechnologies.com/Digilabs/"));
//            startActivity(browserIntent);
        } else if (id == R.id.nav_rate_app) {
            startActivity(new Intent(BookTestActivity.this, FeedbackActivity.class));
        } else if (id == R.id.nav_logout_app) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.clear();
            editor.commit();
            for (int i = 0; i < ActivityStack.activity
                    .size(); i++) {
                (ActivityStack.activity.elementAt(i)).finish();
                Log.e("Activity size:" + ActivityStack.activity.size(), "finished...");
            }
            startActivity(new Intent(BookTestActivity.this, RegisterActivity.class).putExtra("intent", "register"));
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
