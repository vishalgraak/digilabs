package com.sahir.websolutions.digilabs.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sahir.websolutions.digilabs.HelperClass.Helper;
import com.sahir.websolutions.digilabs.R;
import com.sahir.websolutions.digilabs.objects.MemberModel;
import com.sahir.websolutions.digilabs.objects.Report;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

/**
 * Created by SWSPC8 on 5/31/2017.
 */


public class ViewReportActivity extends AppCompatActivity {
    int offline;
    String reportName, date, url, id, type, data, report_type,star_rating, user_id,LabId;
    ImageView img_download, img_share, img_more;
    LinearLayout ll_layout, ll_btn, lin_img;
    WebView webView;
    TouchImageView tivImage;
    Report report;
    Toolbar toolbar;
    ProgressBar progress_bar;
    private String mReportType = "";
    Button btn;
    ArrayList<MemberModel> member_lists;

    ArrayList<String> images_list = new ArrayList<>();
    Activity sActivity;
    Context context;
Dialog mDialog;
    SharedPreferences prefs;
    ProgressDialog progressDialog;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pdfviewer_lib);
        member_lists = new ArrayList<MemberModel>();
        sActivity = ViewReportActivity.this;
        context = getApplicationContext();
        init();

        setData();

        setToolbar();

        onClicks();
    }

    private void setData() {

        if (getIntent().getParcelableArrayListExtra("members_list") != null) {
            member_lists = getIntent().getParcelableArrayListExtra("members_list");
        }

        try {
            mReportType = getIntent().getStringExtra("reportType");
        } catch (NullPointerException e) {
            System.err.println("Failed to get the report type ERROR :" + e.getMessage());
        }

        Intent intent = getIntent();
        report = (Report) intent.getSerializableExtra("Report");
        reportName = report.getName();
        url = report.getPath();
        id = String.valueOf(report.getId());  //report id
        type = report.getType();  //pdf,camera,gallery
        report_type = report.report_type;
        offline = intent.getIntExtra("offline", 2);
        LabId = intent.getStringExtra("lab_id");  // //0--> online , 1--> offline
        date = report.getDate();
        images_list = report.image_list;

        if (report.image_list.size() > 1) {
            img_more.setVisibility(View.VISIBLE);
            lin_img.setVisibility(View.VISIBLE);
        }

        if (report_type.equals("patients")) {
            btn.setVisibility(View.GONE);
            ll_btn.setVisibility(View.GONE);

            if (report.report_type.equals("patients")) {
                try
                {
                    JSONArray array = new JSONArray(report.getReportsList());

                    for (int i =0 ; i<array.length();i++) {
                        String tmp =   array.getJSONObject(i).getString("labfile");

                        if (tmp.endsWith(".pdf"))
                        {
                            url=tmp;
                        }
                        else
                        {
                            images_list.add(tmp);
                        }
                    }
                }
                catch (JSONException e){}
            }
        }

        if (report.member_name != null) {
            if (report.member_name.equals("my")) {
                btn.setText("Folder: " + prefs.getString("title", null) + prefs.getString("fname", null));
            } else {
                btn.setText("Folder: " + report.member_name);
            }
        }

        if (member_lists.size() == 0) {
            ll_btn.setVisibility(View.GONE);
        } else {
            ll_btn.setVisibility(View.VISIBLE);
        }




        if (offline == 0) {



            if (url != null) {
                if (url.contains(".pdf")) {
                    progress_bar.setVisibility(View.VISIBLE);
                    final Handler handler = new Handler();
                    new Thread(new Runnable() {
                        public void run() {
                            try {
                                Thread.sleep(3500);
                            } catch (Exception e) {
                            }
                            handler.post(new Runnable() {
                                public void run() {
                                    progress_bar.setVisibility(View.GONE);
                                }
                            });
                        }
                    }).start();
                    webView.setVisibility(View.VISIBLE);
                    tivImage.setVisibility(View.GONE);
                    webView.getSettings().setJavaScriptEnabled(true);
                    webView.getSettings().setBuiltInZoomControls(true);
                    webView.setWebViewClient(new WebViewClient());
                    webView.loadUrl("http://docs.google.com/viewerng/viewer?embedded=true&url=" + url);
                    data = "pdf";
                } else {
                    webView.setVisibility(View.GONE);
                    tivImage.setVisibility(View.VISIBLE);
                    if (!url.equals("")) {
                        progress_bar.setVisibility(View.VISIBLE);
                        Picasso.with(ViewReportActivity.this).load(url).into(tivImage, new Callback() {
                            @Override
                            public void onSuccess() {
                                progress_bar.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                progress_bar.setVisibility(View.GONE);
                            }
                        });
                    }
                    data = "image";
                }
            }
        }
    }

    private void init() {
        prefs = getSharedPreferences(getString(R.string.user_details), MODE_PRIVATE);
       user_id = prefs.getString(getString(R.string.user_id), null);
        progressDialog = new ProgressDialog(ViewReportActivity.this);
        progressDialog.setMessage("Sending Feedback to server...");
        lin_img = (LinearLayout) findViewById(R.id.lin_img);
        img_download = (ImageView) findViewById(R.id.img_download);
        img_share = (ImageView) findViewById(R.id.img_share);
        img_more = (ImageView) findViewById(R.id.img_more);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        btn = (Button) findViewById(R.id.btn);
        ll_layout = (LinearLayout) findViewById(R.id.ll_layout);
        ll_btn = (LinearLayout) findViewById(R.id.ll_btn);
        webView = (WebView) findViewById(R.id.webView);
        tivImage = (TouchImageView) findViewById(R.id.tivImage);
        prefs = getSharedPreferences(getString(R.string.user_details), Context.MODE_PRIVATE);
        if (Helper.getPatient(sActivity).equals("patient")) {
            ll_layout.setVisibility(View.GONE);
        } else {
            ll_layout.setVisibility(View.VISIBLE);
        }
    }

    private void onClicks() {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (member_lists != null) {
                    ArrayList<String> name = new ArrayList<>();
                    ArrayList<String> ids = new ArrayList<>();
                    name.add(prefs.getString("title", null) + prefs.getString("fname", null));
                    ids.add("null");
                    for (MemberModel model : member_lists) {
                        name.add(model.name);
                        ids.add(model.id);
                    }
                    String[] arr = new String[name.size()];
                    arr = name.toArray(arr);
                    String[] arrId = new String[ids.size()];
                    arrId = ids.toArray(arrId);
                    optionDialog(arr, arrId);
                }
            }
        });

        img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewReportActivity.this, ShareWithDoctor.class);
                intent.putExtra("Report", report);
                intent.putExtra("offline", offline);
                startActivity(intent);
            }
        });


        img_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.isNetworkConnected(ViewReportActivity.this)) {
                    Utils.conDialog(ViewReportActivity.this);
                } else {
                    saveToFolder();
                }
            }
        });

        lin_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ViewReportActivity.this, ViewMoreReports.class)
                        .putExtra("Report", report)
                        .putExtra("offline", offline)
                        .putParcelableArrayListExtra("members_list", member_lists));
            }
        });
    }


    protected void optionDialog(final String[] fav_arr2, final String[] arrId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ViewReportActivity.this);
        builder.setTitle("");
        builder.setItems(fav_arr2, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                if (item == 0) {
                    dialog.dismiss();
                    if (!Utils.isNetworkConnected(ViewReportActivity.this)) {
                        Utils.conDialog(ViewReportActivity.this);
                    } else {
                        btn.setText("Folder: " + fav_arr2[0]);
                        progress_bar.setVisibility(View.VISIBLE);
                        updateReport(arrId[0], id);
                    }
                } else if (item == 1) {
                    dialog.dismiss();
                    if (!Utils.isNetworkConnected(ViewReportActivity.this)) {
                        Utils.conDialog(ViewReportActivity.this);
                    } else {
                        btn.setText("Folder: " + fav_arr2[1]);
                        progress_bar.setVisibility(View.VISIBLE);
                        updateReport(arrId[1], id);
                    }
                } else if (item == 2) {
                    dialog.dismiss();
                    if (!Utils.isNetworkConnected(ViewReportActivity.this)) {
                        Utils.conDialog(ViewReportActivity.this);
                    } else {
                        btn.setText("Folder: " + fav_arr2[2]);
                        progress_bar.setVisibility(View.VISIBLE);
                        updateReport(arrId[2], id);
                    }
                } else if (item == 3) {
                    dialog.dismiss();
                    if (!Utils.isNetworkConnected(ViewReportActivity.this)) {
                        Utils.conDialog(ViewReportActivity.this);
                    } else {
                        btn.setText("Folder: " + fav_arr2[3]);
                        progress_bar.setVisibility(View.VISIBLE);
                        updateReport(arrId[3], id);
                    }
                }
            }
        });
        builder.show();
    }

    private void updateReport(final String member_id, final String reportId) {

        if (mReportType.equals("my")) {
            FetchData.getInstance(ViewReportActivity.this).getRequestQueue().add(new StringRequest
                    (Request.Method.POST, Urls.URL_TO_WEB + "myreports/editApi/" + reportId,
                            new Response.Listener<String>() {
                                public void onResponse(String response) {
                                    progress_bar.setVisibility(View.GONE);
                                    Log.i("valRes", "" + response);
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        String status = jsonObject.optString("status");
                                        if (status.equals("1")) {
                                            Toast.makeText(getApplicationContext(), "Report updated successfully", Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                        public void onErrorResponse(VolleyError error) {
                            Log.e("CAT", error.toString());
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new Hashtable<String, String>();
                    params.put("userfamilies_id", member_id);
                    return params;
                }
            });
        } else if (mReportType.equals("lab")) {

            FetchData.getInstance(ViewReportActivity.this).getRequestQueue().add(new StringRequest
                    (Request.Method.GET, Urls.URL_TO_WEB + "booktests/editApi/" + reportId+"?userfamilies_id="+member_id,
                            new Response.Listener<String>() {
                                public void onResponse(String response) {
                                    progress_bar.setVisibility(View.GONE);
                                    Log.i("valRes", "" + response);
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        String status = jsonObject.optString("status");
                                        if (status.equals("1")) {
                                            Toast.makeText(getApplicationContext(), "Report updated successfully", Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                        public void onErrorResponse(VolleyError error) {
                            Log.e("CAT", error.toString());
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new Hashtable<String, String>();
                    params.put("userfamilies_id", member_id);
                    return params;
                }
            });
        }


    }

    public void setToolbar() {
        setTitle("");
        toolbar = (Toolbar) findViewById(R.id.toolbar_image);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(reportName + " - " + date);
        mTitle.setTypeface(Utils.setTypeface(this));
    }


    public void saveToFolder() {
        DownloadFile dlfile = new DownloadFile();
        dlfile.execute();
    }


    public class DownloadFile extends AsyncTask<String, Void, String> {

        public DownloadFile() {

        }

        @Override
        protected String doInBackground(String... params) {
            URL urlTemp = null;
            try {
                for (int i = 0; i < images_list.size(); i++) {
                    if (images_list.get(i).endsWith("png")) {
                        urlTemp = new URL(images_list.get(i));
                        Bitmap bitmapT = getBitmapFromUrl(urlTemp);
                        saveToInternalStorage(bitmapT, "ReportImg" + i + ".png");
                    } else if (images_list.get(i).endsWith("jpg")) {
                        urlTemp = new URL(images_list.get(i));
                        Bitmap bitmapT = getBitmapFromUrl(urlTemp);
                        saveToInternalStorage(bitmapT, "ReportImg" + i + ".jpg");
                    } else if (images_list.get(i).endsWith("gif")) {
                        urlTemp = new URL(images_list.get(i));
                        Bitmap bitmapT = getBitmapFromUrl(urlTemp);
                        saveToInternalStorage(bitmapT, "ReportImg" + i + ".gif");
                    } else if (images_list.get(i).endsWith("pdf")) {
                        saveToInternalStoragePdf(images_list.get(i).replace(" ", "%20"));
                    } else {
                        saveToInternalStorageVideo(images_list.get(i).replace(" ", "%20"));
                    }
                }
                Snackbar.make(ll_btn, "File downloaded successfully!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public Bitmap getBitmapFromUrl(URL url) {
        try {
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    private void saveToInternalStorage(Bitmap bitmapImage, String nameTemp) {
        File directory = new File(Environment.getExternalStorageDirectory() + File.separator + "DigiLabs");
        if (!directory.exists()) {
            directory.mkdirs();
        }
//        String nameTemp = reportName + "_" + date + ".jpg";
        File pictureFile = new File(directory, nameTemp);
        try {
            pictureFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            FileOutputStream out = new FileOutputStream(pictureFile);
            bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.close();
            Snackbar.make(ll_btn, "File downloaded successfully!", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveToInternalStoragePdf(String url) {
        File directory = new File(Environment.getExternalStorageDirectory() + File.separator + "DigiLabs");
        if (!directory.exists()) {
            directory.mkdirs();
        }
        String nameReportTemp = reportName + "_" + date + ".pdf";
        File file = new File(directory, nameReportTemp);
        try {
            file.createNewFile();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        getPdfFromServer(url, file);
    }


    public void getPdfFromServer(String fileURL, File directory) {
        try {
            FileOutputStream f = new FileOutputStream(directory);
            URL u = new URL(fileURL);
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();

            InputStream in = c.getInputStream();

            byte[] buffer = new byte[1024];
            int len1;
            while ((len1 = in.read(buffer)) > 0) {
                f.write(buffer, 0, len1);
            }
            f.close();
            Snackbar.make(ll_btn, "File downloaded  successfully!", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveToInternalStorageVideo(String url) {
        File directory = new File(Environment.getExternalStorageDirectory() + File.separator + "DigiLabs");
        if (!directory.exists()) {
            directory.mkdirs();
        }
        String nameReportTemp = reportName + "_" + date + ".mp4";
        File file = new File(directory, nameReportTemp);
        try {
            file.createNewFile();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        getVideoFromServer(url, file);
    }


    public void getVideoFromServer(String fileURL, File directory) {
        try {
            FileOutputStream f = new FileOutputStream(directory);
            URL u = new URL(fileURL);
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();

            InputStream in = c.getInputStream();

            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ((len1 = in.read(buffer)) > 0) {
                f.write(buffer, 0, len1);
            }
            f.close();
            Snackbar.make(ll_btn, "File downloaded  successfully!", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (mReportType.equals("lab")) {
            showRatingDialog();
        }else{
            finish();
        }
    }
   private void showRatingDialog(){
       DisplayMetrics displayMetrics = new DisplayMetrics();
       getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
       int height = displayMetrics.heightPixels;
       int width = displayMetrics.widthPixels;
       mDialog = new Dialog(this);
       mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

       mDialog.setContentView(R.layout.activity_rate_lab);
       RatingBar mRatingBar=(RatingBar)mDialog.findViewById(R.id.rbRating);
       final TextView txtRating = (TextView) mDialog.findViewById(R.id.txt_rating);
       TextView txtRateAPP = (TextView) mDialog.findViewById(R.id.txt_rate);
       txtRateAPP.setText("Rate This Lab");
       final EditText edtLabFeed = (EditText) mDialog.findViewById(R.id.edt_feedback);
       Button btnSubmitRating= (Button) mDialog.findViewById(R.id.btn_submit);
       txtRating.setTypeface(Utils.setTypeface(this));
       edtLabFeed.setTypeface(Utils.setTypeface(this));

       mRatingBar.setRating(0.0f);
       star_rating = "0";
       mRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
           @Override
           public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
               if (rating == 0.0) {
                   txtRating.setText("");
                   star_rating = "0";
               } else
               if (rating <= 1.0 && rating > 0.0) {
                   txtRating.setText("Terrible");
                   star_rating = "1";
               } else if (rating <= 2.0 && rating > 1.0) {
                   txtRating.setText("Poor");
                   star_rating = "2";
               } else if (rating <= 3.0 && rating > 2.0) {
                   txtRating.setText("Average");
                   star_rating = "3";
               } else if (rating <= 4.0 && rating > 3.0) {
                   txtRating.setText("Good");
                   star_rating = "4";
               } else if (rating > 4.0) {
                   txtRating.setText("Awesome");
                   star_rating = "5";
               }
           }
       });
       btnSubmitRating.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
                String strFeedback=edtLabFeed.getText().toString();
                progressDialog.show();
               sendFeedback(user_id,star_rating,strFeedback);
           }
       });
       WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
       lp.copyFrom(mDialog.getWindow().getAttributes());
       lp.width = WindowManager.LayoutParams.WRAP_CONTENT;

       mDialog.getWindow().setAttributes(lp);
       mDialog.show();

       mDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
           @Override
           public void onCancel(DialogInterface dialog) {
               finish();
           }
       });

   }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (mReportType.equals("lab")) {
                    showRatingDialog();
                }else{
                    finish();
                }
        }
        return true;
    }
    public void sendFeedback(final String userId, final String rating, final String comments) {
        FetchData.getInstance(ViewReportActivity.this).getRequestQueue().add(new StringRequest
                (Request.Method.POST, Urls.URL_TO_WEB + "labs/post_rating",
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                progressDialog.dismiss();
                                Log.i("Feedback Response: ", response);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String status = jsonObject.optString("status");
                                    if (status.equals("1")) {
                                        new AlertDialog.Builder(ViewReportActivity.this)
                                                .setMessage("Thank you for your feedback.")
                                                .setCancelable(false)
                                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        mDialog.cancel();
                                                        finish();

                                                    }
                                                })
                                                .show();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    progressDialog.dismiss();
                                }

                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                        progressDialog.dismiss();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", userId);
                params.put("star", rating);
                params.put("lab_id", LabId);
                params.put("pricerating", "");
                params.put("comments", comments);
                return params;
            }
        });
    }
}


