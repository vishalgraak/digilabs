package com.sahir.websolutions.digilabs.Activity;

import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.sahir.websolutions.digilabs.PaymentPage.PaymentActivity;
import com.sahir.websolutions.digilabs.R;
import com.sahir.websolutions.digilabs.database.AddressDatabase;
import com.sahir.websolutions.digilabs.database.AddressDao;
import com.sahir.websolutions.digilabs.database.AddressModel;
import com.sahir.websolutions.digilabs.objects.LabModel;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class UserAddressActivity extends AppCompatActivity {

    private EditText editTextPinCode, editTextName, editTextEmail, editTextMobile, editTextCity, editTextState, editTextAddressAddress;
    private Button btnAddressSave;
    ImageView ivCancel;
    // TextView tvCountry,tvBillingCountry;
    private List<AddressModel> mAddressList;
    CheckBox checkBoxBillingAddress;
    EditText etBillingAddress, etBillingPincode, etBillingName, etBillingEmail, etBillingMobile, etBillingCity, etBillingState;
    LinearLayout linearLayoutBillingAddress, linearLayoutCheckBox;
    private String mTestId, mLabId, mTestTime, mTestDate, mHomeCollection;
    public String address, email, mobile, name, city, state, pincode, type, country,hcType;
    public String billingAddress, billingName, billingEmail, billingMobile, billingCity, billingState, billingPincode, billingType, billingCountry;
    int id = 0;
    String hcBoxChecked;
    private String labLatitude, labLongitude, mFixCharges, homeCollAvailability;
    private String hcCharges = "", testPrice, convenience, conven_type;
    String calcuHcPrice,hcDistance;
    String collectedDistance = "";
    double shippingLongitude;
    double shippingLatitude;
    /* DBHelper dbHelper;
     UserAddressBaseClass userAddress;*/
    //   SharedPreferences prefCountryRet;
    final List<String> arrayListCountry = new ArrayList<String>();
    boolean value = false;
    private AddressDatabase mDb;
    private AddressDao mAddressDao;
    private ArrayList<? extends LabModel> mLabHcList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.address_editable_form);
        try {
            // Get an Instance of Database class
            mDb = AddressDatabase.getDatabase(UserAddressActivity.this);
            mAddressDao = mDb.addressDao();

            mLabHcList = new ArrayList();
            mAddressList = new ArrayList<>();
        /*get data to get thehome collection according to distance*/
            homeCollAvailability = getIntent().getStringExtra("homeCollAvail");
            hcType = getIntent().getStringExtra("hc_type");
            labLatitude = getIntent().getStringExtra("labLatitude");
            labLongitude = getIntent().getStringExtra("labLongitude");
            mFixCharges = getIntent().getStringExtra("hc_fix_charges");
            hcBoxChecked = getIntent().getStringExtra("hcBox");
            mLabHcList = getIntent().getParcelableArrayListExtra("hc_list");
            testPrice = getIntent().getStringExtra("test_price");
            Log.e("price==", "" + testPrice);
            mTestId = getIntent().getStringExtra("test_id");
            mLabId = getIntent().getStringExtra("lab_id");
            mTestDate = getIntent().getStringExtra("test_date");
            mTestTime = getIntent().getStringExtra("test_time");
            convenience = getIntent().getStringExtra("convenience");
            conven_type = getIntent().getStringExtra("convenience_type");
            mHomeCollection = getIntent().getStringExtra("home_collection");
            if (homeCollAvailability.equals("no")) {
                    Intent mIntent = new Intent(UserAddressActivity.this, PaymentActivity.class);
                    mIntent.putExtra("test_price", testPrice);
                    mIntent.putExtra("hc_charges", "");
                    mIntent.putExtra("hc_distance", "");
                    mIntent.putExtra("lab_id", mLabId);
                    mIntent.putExtra("test_time", mTestTime);
                    mIntent.putExtra("convenience", convenience);
                    mIntent.putExtra("hcBox", hcBoxChecked);
                    mIntent.putExtra("convenience_type", conven_type);
                    mIntent.putExtra("home_collection", mHomeCollection);
                    mIntent.putExtra("test_date", mTestDate);
                    mIntent.putExtra("test_id", mTestId);
                    startActivity(mIntent);
                    finish();
                }
            initView();
            setAddress();
            onClick();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initView() {
        //shipping
        editTextPinCode = (EditText) findViewById(R.id.editTextPinCode);
        editTextName = (EditText) findViewById(R.id.editTextName);
        editTextCity = (EditText) findViewById(R.id.editTextCity);
        editTextState = (EditText) findViewById(R.id.editTextState);
        editTextAddressAddress = (EditText) findViewById(R.id.editTextAddressAddress);
        editTextMobile = (EditText) findViewById(R.id.editTextMobile);
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        ivCancel = (ImageView) findViewById(R.id.ivCancel);
        btnAddressSave = (Button) findViewById(R.id.btnAddressSave);
        checkBoxBillingAddress = (CheckBox) findViewById(R.id.checkBoxBillingAddress);
        linearLayoutBillingAddress = (LinearLayout) findViewById(R.id.linearLayoutBillingAddress);
        linearLayoutCheckBox = (LinearLayout) findViewById(R.id.linearLayoutCheckBox);
        //billing
        etBillingAddress = (EditText) findViewById(R.id.etBillingAddress);
        etBillingPincode = (EditText) findViewById(R.id.etBillingPincode);
        etBillingName = (EditText) findViewById(R.id.etBillingName);
        etBillingEmail = (EditText) findViewById(R.id.etBillingEmail);
        etBillingMobile = (EditText) findViewById(R.id.etBillingMobile);
        etBillingCity = (EditText) findViewById(R.id.etBillingCity);
        etBillingState = (EditText) findViewById(R.id.etBillingState);
        //tvBillingCountry = (TextView) findViewById(R.id.tvBillingCountry);
        btnAddressSave = (Button) findViewById(R.id.btnAddressSave);
    }

    private void onClick() {
        linearLayoutCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkBoxBillingAddress.performClick();

            }
        });
        ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        checkBoxBillingAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkBoxBillingAddress.isChecked()) {
                    etBillingAddress.setText(editTextAddressAddress.getText().toString());
                    etBillingPincode.setText(editTextPinCode.getText().toString());
                    etBillingName.setText(editTextName.getText().toString());
                    etBillingEmail.setText(editTextEmail.getText().toString());
                    etBillingMobile.setText(editTextMobile.getText().toString());
                    etBillingCity.setText(editTextCity.getText().toString());
                    etBillingState.setText(editTextState.getText().toString());
                    //   tvBillingCountry.setText(tvCountry.getText().toString());
                }
            }
        });

        btnAddressSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveAddress();
            }
        });
    }

    private void setAddress() {
        mAddressList = mAddressDao.getAll();
        Log.e("address lengthn lilst", "" + mAddressList.size());
        if (!mAddressList.isEmpty()) {
            editTextName.setText(mAddressList.get(0).mShippingName);
            editTextEmail.setText(mAddressList.get(0).mShippingEmail);
            editTextMobile.setText(mAddressList.get(0).mShippingMobile);
            editTextAddressAddress.setText(mAddressList.get(0).mShippingAddress);
            editTextCity.setText(mAddressList.get(0).mShippingCity);
            editTextPinCode.setText(mAddressList.get(0).mShippingPinCode);
            editTextState.setText(mAddressList.get(0).mShippingState);

            etBillingAddress.setText(mAddressList.get(0).mBillingAddress);
            etBillingPincode.setText(mAddressList.get(0).mBillingPinCode);
            etBillingName.setText(mAddressList.get(0).mBillingName);
            etBillingEmail.setText(mAddressList.get(0).mBillingEmail);
            etBillingMobile.setText(mAddressList.get(0).mBillingMobile);
            etBillingCity.setText(mAddressList.get(0).mBillingCity);
            etBillingState.setText(mAddressList.get(0).mBillingState);
        }
    }

    public void saveAddress() {
        pincode = editTextPinCode.getText().toString();
        name = editTextName.getText().toString();
        email = editTextEmail.getText().toString();
        mobile = editTextMobile.getText().toString();

        city = editTextCity.getText().toString();
        state = editTextState.getText().toString();
        address = editTextAddressAddress.getText().toString();

        type = "1";
        String flag = "";

        if (!checkBoxBillingAddress.isChecked()) {
            //get data of billing
            billingAddress = etBillingAddress.getText().toString();
            billingName = etBillingName.getText().toString();
            billingEmail = etBillingEmail.getText().toString();
            billingMobile = etBillingMobile.getText().toString();
            billingCity = etBillingCity.getText().toString();
            billingState = etBillingState.getText().toString();
            billingPincode = etBillingPincode.getText().toString();
            //   billingCountry=tvBillingCountry.getText().toString();
            flag = "1";//there exists different billing address
        } else {
            billingAddress = address;
            billingEmail = email;
            billingName = name;
            billingMobile = mobile;
            billingCity = city;
            billingState = state;
            billingPincode = pincode;
            billingCountry = country;
            flag = "0";
        }

        if (pincode.equals("") || email.equals("") || name.equals("") || mobile.equals("") || city.equals("") || state.equals("") || address.equals("")
                || ((billingAddress.equals("") || billingName.equals("") || billingMobile.equals("") || billingEmail.equals("") || billingCity.equals("")                               //billing
                || billingState.equals("") || billingPincode.equals(""))) && !checkBoxBillingAddress.isChecked()) {
            if (pincode.equals("")) {
                editTextPinCode.setError("Pincode can not be left empty!");
                editTextPinCode.requestFocus();
            }
            if (name.equals("")) {
                editTextName.setError("Name can not be left empty!");
                editTextName.requestFocus();
            }
            if (email.equals("")) {
                editTextEmail.setError("Email can not be left empty!");
                editTextEmail.requestFocus();
            }
            if (!email.contains("@") && !email.contains(".")) {
                editTextEmail.setError("Please enter valid email!");
                editTextEmail.requestFocus();
            }
            if (mobile.equals("")) {
                editTextMobile.setError("Mobile can not be left empty!");
                editTextMobile.requestFocus();
            }

            if (city.equals("")) {
                editTextCity.setError("City can not be left empty!");
                editTextCity.requestFocus();
            }
            if (state.equals("")) {
                editTextState.setError("State can not be left empty!");
                editTextState.requestFocus();
            }
            if (address.equals("")) {
                editTextAddressAddress.setError("Address can not be left empty!");
                editTextAddressAddress.requestFocus();
            }
            if (billingAddress.equals("")) {
                etBillingAddress.setError("Address can not be left empty!");
                etBillingAddress.requestFocus();
            }
            if (billingName.equals("")) {
                etBillingName.setError("Name can not be left empty!");
                etBillingName.requestFocus();
            }
            if (billingEmail.equals("")) {
                etBillingEmail.setError("Email can not be left empty!");
                etBillingEmail.requestFocus();
            }
            if (!billingEmail.contains("@") && !billingEmail.contains(".")) {
                etBillingEmail.setError("Please enter valid email!");
                etBillingEmail.requestFocus();
            }
            if (billingMobile.equals("")) {
                etBillingMobile.setError("Mobile can not be left empty!");
                etBillingMobile.requestFocus();
            }
            if (billingCity.equals("")) {
                etBillingCity.setError("City can not be left empty!");
                etBillingCity.requestFocus();
            }
            if (billingState.equals("")) {
                etBillingState.setError("State can not be left empty!");
                etBillingState.requestFocus();
            }
            if (billingPincode.equals("")) {
                etBillingPincode.setError("Pincode can not be left empty!");
                etBillingPincode.requestFocus();
            }


        } else {

           /* if(homeCollAvailability.equals("yes")){
                if (hcType.equals("1")){
                    new LoadAllgroups().execute();
                }else{

                }

            }*/

            AddressModel mAddress = new AddressModel();
             /*Shipping address save*/
            mAddress.mShippingName = name;
            mAddress.mShippingEmail = email;
            mAddress.mShippingMobile = mobile;
            mAddress.mShippingAddress = address;
            mAddress.mShippingCity = city;
            mAddress.mShippingState = state;
            mAddress.mShippingPinCode = pincode;
            /*Billing address save*/
            mAddress.mBillingName = billingName;
            mAddress.mBillingEmail = billingEmail;
            mAddress.mBillingMobile = billingMobile;
            mAddress.mBillingAddress = billingAddress;
            mAddress.mBillingCity = billingCity;
            mAddress.mBillingState = billingState;
            mAddress.mBillingPinCode = billingPincode;
            if (mAddressList.size() > 0) {
                mAddressDao.delete(mAddressList.get(0));
            }

            mAddressDao.insertAll(mAddress);


                    //According to kms
                    new LoadAllgroups().execute();


        }
    }

    public JSONObject getLocationInfo(String address) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            Log.e("address=", "Longitude=" + address);
            address = address.replaceAll(" ", "%20");
            HttpPost httppost = new HttpPost("http://maps.google.com/maps/api/geocode/json?address=" + address+  "&sensor=false");
            DefaultHttpClient client = new DefaultHttpClient();
            HttpResponse response;
            stringBuilder = new StringBuilder();
            response = client.execute(httppost);
            HttpEntity entity = response.getEntity();
            InputStream stream = entity.getContent();
            int b;
            while ((b = stream.read()) != -1) {
                stringBuilder.append((char) b);
            }
        } catch (ClientProtocolException e) {
        } catch (IOException e) {
        }
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject(stringBuilder.toString());
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }

    public boolean getLatLong(JSONObject jsonObject) {

        try {
            shippingLongitude = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                    .getJSONObject("geometry").getJSONObject("location")
                    .getDouble("lng");
            shippingLatitude = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                    .getJSONObject("geometry").getJSONObject("location")
                    .getDouble("lat");

            Log.e("lattitude=" + shippingLatitude, "Longitude=" + shippingLongitude);

        } catch (JSONException e) {
            return false;
        }
        return true;
    }
    class GetDistance extends AsyncTask<String, String, String> {
        ProgressDialog mDialog = new ProgressDialog(UserAddressActivity.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        protected String doInBackground(String... args) {
            hcDistance= getDistanceOnRoad(Double.valueOf(labLatitude),Double.valueOf(labLongitude)
                    ,Double.valueOf(shippingLatitude),Double.valueOf(shippingLongitude));
            Log.e("hc Distance",""+hcDistance);
            return "";
        }

        protected void onPostExecute(String distance) {
            mDialog.dismiss();
            try {
                if (homeCollAvailability.equals("yes")) {
                    String deliveryCharges = getHcPrice(mLabHcList, Double.valueOf(hcDistance));
                    if (hcType.equals("1")) {
                        Intent mIntent = new Intent(UserAddressActivity.this, PaymentActivity.class);
                        mIntent.putExtra("test_price", testPrice);
                        mIntent.putExtra("hc_charges", deliveryCharges);
                        mIntent.putExtra("hc_distance", String.valueOf(hcDistance));
                        mIntent.putExtra("lab_id", mLabId);
                        mIntent.putExtra("test_time", mTestTime);
                        mIntent.putExtra("convenience", convenience);
                        mIntent.putExtra("hcBox", hcBoxChecked);
                        mIntent.putExtra("convenience_type", conven_type);
                        mIntent.putExtra("home_collection", mHomeCollection);
                        mIntent.putExtra("test_date", mTestDate);
                        mIntent.putExtra("test_id", mTestId);
                        startActivity(mIntent);
                        finish();
                    } else {
                        Intent mIntent = new Intent(UserAddressActivity.this, PaymentActivity.class);
                        mIntent.putExtra("test_price", testPrice);
                        mIntent.putExtra("hc_charges", mFixCharges);
                        mIntent.putExtra("hc_distance", String.valueOf(hcDistance));
                        mIntent.putExtra("lab_id", mLabId);
                        mIntent.putExtra("test_time", mTestTime);
                        mIntent.putExtra("hcBox", hcBoxChecked);
                        mIntent.putExtra("convenience", convenience);
                        mIntent.putExtra("convenience_type", conven_type);
                        mIntent.putExtra("home_collection", mHomeCollection);
                        mIntent.putExtra("test_date", mTestDate);
                        mIntent.putExtra("test_id", mTestId);
                        startActivity(mIntent);
                        finish();
                    }
                }
            }catch (Exception e){
                Toast.makeText(UserAddressActivity.this,"Please Try again!",Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }
    class LoadAllgroups extends AsyncTask<String, String, String> {
        ProgressDialog mDialog = new ProgressDialog(UserAddressActivity.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mDialog.setMessage("Please wait...");
            mDialog.setCancelable(false);
            mDialog.show();
        }

        protected String doInBackground(String... args) {
            getLatLong(getLocationInfo(address + " " + city + " " + state + " " + pincode));
            return "";
        }

        protected void onPostExecute(String distance) {
       mDialog.dismiss();
          new GetDistance().execute();
           /* if (homeCollAvailability.equals("yes")) {
                if (hcType==1) {
                    String deliveryCharges =calculateDistance();
                    Intent mIntent = new Intent(UserAddressActivity.this, PaymentActivity.class);
                    mIntent.putExtra("test_price", testPrice);
                    mIntent.putExtra("hc_charges", deliveryCharges);
                    mIntent.putExtra("hc_distance", collectedDistance);
                    mIntent.putExtra("lab_id", mLabId);
                    mIntent.putExtra("test_time", mTestTime);
                    mIntent.putExtra("convenience", convenience);
                    mIntent.putExtra("hcBox", hcBoxChecked);
                    mIntent.putExtra("convenience_type", conven_type);
                    mIntent.putExtra("home_collection", mHomeCollection);
                    mIntent.putExtra("test_date", mTestDate);
                    mIntent.putExtra("test_id", mTestId);
                    startActivity(mIntent);
                    finish();
                } else {
                    Intent mIntent = new Intent(UserAddressActivity.this, PaymentActivity.class);
                    mIntent.putExtra("test_price", testPrice);
                    mIntent.putExtra("hc_charges", mFixCharges);
                    mIntent.putExtra("hc_distance", collectedDistance);
                    mIntent.putExtra("lab_id", mLabId);
                    mIntent.putExtra("test_time", mTestTime);
                    mIntent.putExtra("hcBox", hcBoxChecked);
                    mIntent.putExtra("convenience", convenience);
                    mIntent.putExtra("convenience_type", conven_type);
                    mIntent.putExtra("home_collection", mHomeCollection);
                    mIntent.putExtra("test_date", mTestDate);
                    mIntent.putExtra("test_id", mTestId);
                    startActivity(mIntent);
                    finish();
                }
            }*/
        }
    }
    private String getDistanceOnRoad(double latitude, double longitude,
                                     double prelatitute, double prelongitude) {
        String result_in_kms = "";
        String url = "http://maps.google.com/maps/api/directions/xml?origin="
                + latitude + "," + longitude + "&destination=" + prelatitute
                + "," + prelongitude + "&sensor=false&units=metric";
        String tag[] = { "text" };
        HttpResponse response = null;
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();
            HttpPost httpPost = new HttpPost(url);
            response = httpClient.execute(httpPost, localContext);
            InputStream is = response.getEntity().getContent();
            DocumentBuilder builder = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder();
            Document doc = builder.parse(is);
            if (doc != null) {
                NodeList nl;
                ArrayList args = new ArrayList();
                for (String s : tag) {
                    nl = doc.getElementsByTagName(s);
                    if (nl.getLength() > 0) {
                        Node node = nl.item(nl.getLength() - 1);
                        args.add(node.getTextContent());

                    } else {
                        args.add(" - ");
                    }
                }
                Log.e("check Km",""+args.get(0));

                result_in_kms = String.format("%s", args.get(0));
                if(result_in_kms.contains("km")){
                    result_in_kms=result_in_kms.split(" ")[0];
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result_in_kms;
    }
    public String calculateDistance() {
        try {
            Location labLoc = new Location("locationA");
            labLoc.setLatitude(Double.valueOf(labLatitude));
            labLoc.setLongitude(Double.valueOf(labLongitude));
            Location shippingLoc = new Location("locationB");
            shippingLoc.setLatitude(shippingLatitude);
            shippingLoc.setLongitude(shippingLongitude);
            double distance = labLoc.distanceTo(shippingLoc);
            double collDistance = (distance / 1000);
            collectedDistance = String.valueOf(collDistance);
            Log.e("getHc=== Price", mLabHcList.size() + ",," + collDistance);
            calcuHcPrice = getHcPrice(mLabHcList, collDistance);
            Log.e("getHc Price", "" + calcuHcPrice);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return calcuHcPrice;
    }

    private String getHcPrice(ArrayList<? extends LabModel> hcList, Double distance) {
        if (!hcList.isEmpty() && distance != null) {
            for (int i = 0; i < hcList.size(); i++) {
                LabModel mModel = hcList.get(i);
                String[] distArray = mModel.hcDistance.split("-");
                Double startKm = Double.valueOf(distArray[0]);
                Double endKm = Double.valueOf(distArray[1]);
                Log.e("Check the Distance", startKm + "," + distance + "," + endKm);
                if (distance.doubleValue() >= startKm.doubleValue() && distance.doubleValue() <= endKm.doubleValue()) {
                 /*  for (int a=i;a>=0;a--) {*/
                    hcCharges = mModel.hcCharges;
                   /*}*/
                    break;
                }
            }
           /* if(hcCharges.equals("")){
                hcCharges=hcList.get(hcList.size()-1).hcCharges;
            }*/
        }

        return hcCharges;
    }
}