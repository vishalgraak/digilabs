package com.sahir.websolutions.digilabs.Adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sahir.websolutions.digilabs.Activity.LabListActivity;
import com.sahir.websolutions.digilabs.R;
import com.sahir.websolutions.digilabs.Activity.Utils;
import com.sahir.websolutions.digilabs.objects.HealthModel;
import com.sahir.websolutions.digilabs.objects.TestModel;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Sys9 on 7/15/2017.
 */

public class HealthListAdapter extends ArrayAdapter<HealthModel> {

    Context context;
    ArrayList<HealthModel> data;
    ImageView ivLabImage;
    TextView txt_pkg_name, txt_tests, txt_count, txt;
    LinearLayout llLab, ll_view, ll_next;
    ArrayList<String> tests = new ArrayList<>();

    public HealthListAdapter(Context context, ArrayList<HealthModel> data) {
        super(context, R.layout.health_list_row, data);
        this.context = context;
        this.data = data;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final HealthModel model = data.get(position);
        int type = getItemViewType(position);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.health_list_row, parent, false);
        }

        ivLabImage = (ImageView) convertView.findViewById(R.id.ivLabImage);
        txt_pkg_name = (TextView) convertView.findViewById(R.id.txt_pkg_name);
        txt_tests = (TextView) convertView.findViewById(R.id.txt_tests);
        txt_count = (TextView) convertView.findViewById(R.id.txt_count);
        txt = (TextView) convertView.findViewById(R.id.txt);
        llLab = (LinearLayout) convertView.findViewById(R.id.llLab);
        ll_view = (LinearLayout) convertView.findViewById(R.id.ll_view);
        ll_next = (LinearLayout) convertView.findViewById(R.id.ll_next);

        txt_pkg_name.setText(model.package_name);
        if (model.desc == "null") {
            txt_tests.setVisibility(View.GONE);
        } else if (model.desc == null) {
            txt_tests.setVisibility(View.GONE);
        }else if (model.desc.equals("")) {
            txt_tests.setVisibility(View.GONE);
        }  else {
            txt_tests.setVisibility(View.VISIBLE);
            txt_tests.setText("* "+model.desc+"   ");
            txt_tests.setSelected(true);
        }

        if (!model.package_icon.equals("")) {
            Picasso.with(context).load(model.package_icon).resize(100, 100).placeholder(R.drawable.digilabs).
                    into(ivLabImage, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {

                        }
                    });
        } else {
            ivLabImage.setImageResource(R.drawable.digilabs);
        }

        tests.clear();
        for (int i = 0; i < model.tests.size(); i++) {
            TestModel testModel = model.tests.get(i);
            tests.add(testModel.cat_name);
        }
        final String test_names = android.text.TextUtils.join(",", tests);
        //  txt_tests.setText (test_names);

        txt_count.setText(String.valueOf(model.tests.size()) + " ");

        ll_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTests(test_names.replace(",", "\n"));
            }
        });

        llLab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTests(test_names.replace(",", "\n"));
            }
        });

        ll_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, LabListActivity.class);
                intent.putExtra("type", "health");
                intent.putExtra("package_id", model.package_id);
                intent.putExtra("package_name", model.package_name);
                intent.putExtra("package_tests", test_names);
                context.startActivity(intent);
            }
        });

        setTvTypeface();

        return convertView;
    }

    public void setTvTypeface() {
        txt_pkg_name.setTypeface(Utils.setTypeface(context));
        txt_tests.setTypeface(Utils.setTypeface(context));
        txt_count.setTypeface(Utils.setTypeface(context));
        txt.setTypeface(Utils.setTypeface(context));
    }

    public void openTests(String tests) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_list_row);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);
        dialog.show();

        TextView tvTests = (TextView) dialog.findViewById(R.id.tvTests);
        tvTests.setTypeface(Utils.setTypeface(context));
        tvTests.setText(tests);
    }

    protected void showTests(final String[] fav_arr2) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("");
        builder.setItems(fav_arr2, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
            }
        });
        builder.show();
    }
}
