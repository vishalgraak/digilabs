package com.sahir.websolutions.digilabs.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sahir.websolutions.digilabs.R;

import org.json.JSONObject;

/**
 * Created by safal bhatia on 6/29/2017.
 */

public class TermsActivity extends AppCompatActivity {
    Toolbar toolbar;
    WebView txt_terms;
    ProgressBar progress_bar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);

        txt_terms = (WebView) findViewById(R.id.txt_terms);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);

        setToolbar();


    }

    public void setToolbar() {
        setTitle("");
        toolbar = (Toolbar) findViewById(R.id.toolbar_image);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        if (getIntent().getStringExtra("type").equals("terms")) {
            mTitle.setText("Terms and Conditions");
        } else {
            mTitle.setText("Privacy Policy");
        }
        mTitle.setTypeface(Utils.setTypeface(this));
        if (!Utils.isNetworkConnected(TermsActivity.this)) {
            Utils.conDialog(TermsActivity.this);
        } else {//safterm
            if (getIntent().getStringExtra("type").equals("terms")) {
                getTerms("  ");
            } else {
                getTerms("http://path.digilabsonline.com/cmspages/viewApi/1");
            }

        }
    }

    public void getTerms(String url1) {
       // String url = "http://www.elvektechnologies.com/Digilabs/wp-content/themes/dt-the7/webservices/webservices.php?get=terms";
        FetchData.getInstance(TermsActivity.this).getRequestQueue().add(new StringRequest
                (Request.Method.GET, url1,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                progress_bar.setVisibility(View.GONE);
                                Log.d("valRes", response);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    int status = jsonObject.optInt("status");
                                    if (status == 1) {
                                       // JSONObject data = jsonObject.optJSONObject("data");
                                        String term = jsonObject.optString("data");
                                          //  txt_terms.setText(term);
                                        txt_terms.loadData(term, "text/html; charset=utf-8", "UTF-8");
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                    }
                }) {
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return true;
    }
}
