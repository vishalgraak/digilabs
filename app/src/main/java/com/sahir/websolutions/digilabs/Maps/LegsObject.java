package com.sahir.websolutions.digilabs.Maps;

import java.util.List;

public class LegsObject {
 private List<StepsObject> steps;
 private DistanceObject distance;
 public LegsObject( DistanceObject distance, List<StepsObject> steps) {
 this.distance = distance;
 this.steps = steps;
 }
 public List<StepsObject> getSteps() {
 return steps;
 }
 public DistanceObject getDistance() {
 return distance;
 }
}