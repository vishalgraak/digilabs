package com.sahir.websolutions.digilabs.Adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.sahir.websolutions.digilabs.HelperClass.Helper;
import com.sahir.websolutions.digilabs.Activity.LabDetailsActivity;
import com.sahir.websolutions.digilabs.R;
import com.sahir.websolutions.digilabs.Activity.Utils;
import com.sahir.websolutions.digilabs.objects.LabModel;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by SWS-PC10 on 5/30/2017.
 */

public class LabListAdapter extends ArrayAdapter<LabModel> {
    String hcCharges = "";
    Context context;
    ArrayList<LabModel> data;
    ArrayList<Helper> img_array;
    ImageView ivLabImage, ivLabCall, img_dis, img_line, star1, star2, star3, star4, star5, rupee1, rupee2, rupee3, rupee4, rupee5;
    TextView tvLabName, tvLabTime, tvLabDistance, tvLabPrice, tvLabHomeCollection, tvLab_discount_price, tvLab_percentage;
    LinearLayout llLab, ll_price, lin_lab, hcChargesLinear;
    public int per;
    public String img = "";
    public String currentActivty;

    public LabListAdapter(Context context, ArrayList<LabModel> data, ArrayList<Helper> img_array, String getActivity) {

        super(context, R.layout.dummy, data);
        this.context = context;
        this.data = data;
        this.img_array = img_array;
        currentActivty = getActivity;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final LabModel lab = data.get(position);
//        if (BookedTestsActivity.title.equals("Booked Tests")){
//
//        }else {
//
//        }
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.dummy, parent, false);
        }
        lin_lab = (LinearLayout) convertView.findViewById(R.id.lin_lab);
        img_line = (ImageView) convertView.findViewById(R.id.img_line);
        img_dis = (ImageView) convertView.findViewById(R.id.img_dis);
        star1 = (ImageView) convertView.findViewById(R.id.star1);
        star2 = (ImageView) convertView.findViewById(R.id.star2);
        star3 = (ImageView) convertView.findViewById(R.id.star3);
        star4 = (ImageView) convertView.findViewById(R.id.star4);
        star5 = (ImageView) convertView.findViewById(R.id.star5);
        rupee1 = (ImageView) convertView.findViewById(R.id.rupee1);
        rupee2 = (ImageView) convertView.findViewById(R.id.rupee2);
        rupee3 = (ImageView) convertView.findViewById(R.id.rupee3);
        rupee4 = (ImageView) convertView.findViewById(R.id.rupee4);
        rupee5 = (ImageView) convertView.findViewById(R.id.rupee5);
        ivLabImage = (ImageView) convertView.findViewById(R.id.ivLabImage);
        ivLabCall = (ImageView) convertView.findViewById(R.id.ivLabCall);
        tvLabName = (TextView) convertView.findViewById(R.id.tvLabName);
        tvLabTime = (TextView) convertView.findViewById(R.id.tvLabTiming);
        tvLabTime.setSelected(true);

        tvLabDistance = (TextView) convertView.findViewById(R.id.tvLabDistance);
        tvLabHomeCollection = (TextView) convertView.findViewById(R.id.tvLabHomeCollection);
        tvLabPrice = (TextView) convertView.findViewById(R.id.tvLabPrice);
        llLab = (LinearLayout) convertView.findViewById(R.id.llLab);
        ll_price = (LinearLayout) convertView.findViewById(R.id.ll_price);
        hcChargesLinear = (LinearLayout) convertView.findViewById(R.id.hc_charges_linear);
        tvLab_discount_price = (TextView) convertView.findViewById(R.id.tvLab_discount_price);
        tvLab_percentage = (TextView) convertView.findViewById(R.id.tvLab_percentage);
        if (lab.price_value1 == null) {
            tvLab_discount_price.setVisibility(View.GONE);
            tvLab_percentage.setVisibility(View.GONE);
            img_dis.setVisibility(View.GONE);
            img_line.setVisibility(View.GONE);
            tvLab_percentage.setText(lab.price);

        } else if (lab.price_value1.equals("0")) {
            tvLab_discount_price.setVisibility(View.GONE);
            tvLab_percentage.setVisibility(View.GONE);
            img_dis.setVisibility(View.GONE);
            //  tvLabTime.setText(lab.date+" "+lab.timing+" "+lab.time);
            img_line.setVisibility(View.GONE);
            tvLab_percentage.setText(lab.price);
        } else {
            int amount = Integer.parseInt(lab.price);
            per = Integer.valueOf(lab.price_value1);
            int cal_per = (amount * per) / 100;
            int total_amt = amount - cal_per;
            tvLab_discount_price.setText(String.valueOf(total_amt));
            tvLab_discount_price.setVisibility(View.VISIBLE);
            tvLab_percentage.setText(String.valueOf(per));
            tvLab_percentage.setVisibility(View.VISIBLE);
            img_dis.setVisibility(View.VISIBLE);
            if (null != lab.date) {
                String labTotalTime = "Time: " + lab.date + " at " + lab.time;
                String TmeArray[] = {labTotalTime, labTotalTime, labTotalTime};


                String test_name = android.text.TextUtils.join(" ", TmeArray);

                tvLabTime.setText(test_name);
            }
        }
        if (lab.price_type1 == null) {

        } else if (lab.price_type1.equals("percent")) {
            tvLab_percentage.setText(String.valueOf(per + "% off"));
        } else {

        }
        img = img_array.get(position).getImg();
        if (img == null) {
            ivLabImage.setImageResource(R.drawable.digilabs);
        } else if (img.equals("null")) {
            ivLabImage.setImageResource(R.drawable.digilabs);
        } else if (img.equals("")) {
            ivLabImage.setImageResource(R.drawable.digilabs);
        } else {
            Picasso.with(context)
                    .load("http://path.digilabsonline.com//images/Labs/photo/" + img)
                    .into(ivLabImage);
        }
        tvLabName.setEllipsize(TextUtils.TruncateAt.END);
        tvLabName.setMaxLines(2);
        tvLabName.setText(lab.labname);
        if (null != lab.date) {

            tvLabTime.setText("Time: " + lab.date + " at " + lab.time);
        }
        if (lab.star_rating.equals("5")) {
            star1.setImageResource(R.drawable.ic_star_filled);
            star2.setImageResource(R.drawable.ic_star_filled);
            star3.setImageResource(R.drawable.ic_star_filled);
            star4.setImageResource(R.drawable.ic_star_filled);
            star5.setImageResource(R.drawable.ic_star_filled);
        } else if (lab.star_rating.equals("4")) {
            star1.setImageResource(R.drawable.ic_star_filled);
            star2.setImageResource(R.drawable.ic_star_filled);
            star3.setImageResource(R.drawable.ic_star_filled);
            star4.setImageResource(R.drawable.ic_star_filled);
            star5.setImageResource(R.drawable.ic_star);
        } else if (lab.star_rating.equals("3")) {
            star1.setImageResource(R.drawable.ic_star_filled);
            star2.setImageResource(R.drawable.ic_star_filled);
            star3.setImageResource(R.drawable.ic_star_filled);
            star4.setImageResource(R.drawable.ic_star);
            star5.setImageResource(R.drawable.ic_star);
        } else if (lab.star_rating.equals("2")) {
            star1.setImageResource(R.drawable.ic_star_filled);
            star2.setImageResource(R.drawable.ic_star_filled);
            star3.setImageResource(R.drawable.ic_star);
            star4.setImageResource(R.drawable.ic_star);
            star5.setImageResource(R.drawable.ic_star);
        } else if (lab.star_rating.equals("1")) {
            star1.setImageResource(R.drawable.ic_star_filled);
            star2.setImageResource(R.drawable.ic_star);
            star3.setImageResource(R.drawable.ic_star);
            star4.setImageResource(R.drawable.ic_star);
            star5.setImageResource(R.drawable.ic_star);
        }

        if (lab.price_rating.equals("5")) {
            rupee1.setImageResource(R.drawable.ic_rupee);
            rupee2.setImageResource(R.drawable.ic_rupee);
            rupee3.setImageResource(R.drawable.ic_rupee);
        } else if (lab.price_rating.equals("4")) {
            rupee1.setImageResource(R.drawable.ic_rupee);
            rupee2.setImageResource(R.drawable.ic_rupee);
            rupee3.setImageResource(R.drawable.ic_rupee);
        } else if (lab.price_rating.equals("3")) {
            rupee1.setImageResource(R.drawable.ic_rupee);
            rupee2.setImageResource(R.drawable.ic_rupee);
            rupee3.setImageResource(R.drawable.ic_rupee);
        } else if (lab.price_rating.equals("2")) {
            rupee2.setImageResource(R.drawable.ic_rupee);
            rupee3.setImageResource(R.drawable.ic_rupee);
        } else if (lab.price_rating.equals("1")) {
            rupee3.setImageResource(R.drawable.ic_rupee);
        }

        if (!lab.price.equals("")) {
            tvLabPrice.setText("Rs " + lab.price);
            tvLabDistance.setText(String.format("%.2f", lab.distance) + " Km");

            tvLabHomeCollection.setText("Rs " + lab.home_collection_price);
        } else {
            ll_price.setVisibility(View.GONE);
            tvLabDistance.setText(String.format("%.2f", lab.distance) + " Km");

            tvLabHomeCollection.setText("Rs " + lab.home_collection_price);
        }
        if (currentActivty.equals("LabList")) {

            if (lab.homecollection.equals("yes")) {


                if (lab.hc_type.equals("1")) {

                    hcChargesLinear.setVisibility(View.VISIBLE);

                    hcCharges = getHcPrice(lab.hcList, lab.getDistance());
                    Log.e("log hc type true",""+hcCharges);
                    tvLabHomeCollection.setText(hcCharges);
                } else {
                    Log.e("log hc type false",""+lab.hc_type);
                    hcChargesLinear.setVisibility(View.VISIBLE);
                    hcCharges = lab.home_collection_price;
                    tvLabHomeCollection.setText("Rs " + lab.home_collection_price);
                }
            }else{
                hcChargesLinear.setVisibility(View.GONE);
            }
        } else {
            if (lab.isHomecollection.equals("yes")) {
                if (!lab.collection_distance.equals("")) {
                    tvLabDistance.setText(lab.collection_distance + " Km");
                } else {
                    tvLabDistance.setText(lab.collection_distance + " Km");
                }
                tvLabHomeCollection.setText("Rs " + lab.collection_charges);
                hcChargesLinear.setVisibility(View.VISIBLE);
            }else{
                tvLabHomeCollection.setText("N.A");
                tvLabDistance.setText(String.format("%.2f", lab.distance) + " Km");
            }
        }
        llLab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, LabDetailsActivity.class);
                intent.putExtra("image_url", img_array.get(position).getImg());
                intent.putExtra("lab_id", lab.id);
                intent.putExtra("book_id", lab.book_id);
                intent.putExtra("user_id", lab.user_id);
                intent.putExtra("test_name", lab.test_name);
                intent.putExtra("test_price", lab.test_price);
                intent.putExtra("lab_image", lab.lab_image);
                intent.putExtra("lab_name", lab.labname);
                intent.putExtra("opentime", lab.opentime);
                intent.putExtra("closetime", lab.closetime);
                intent.putExtra("working_days", lab.working_days);
                intent.putExtra("latitude", lab.latitude);
                intent.putExtra("longitude", lab.longtitude);
                intent.putExtra("lab_distance", lab.distance);
                if (currentActivty.equals("LabList")) {
                    intent.putExtra("home_collection_price", getHomeCollection(lab));
                    intent.putExtra("homecollection", lab.homecollection);
                    intent.putExtra("hc_type", lab.hc_type);
                    intent.putExtra("convenience", lab.convenience);
                    intent.putExtra("convenience_type", lab.conven_type);
                    intent.putParcelableArrayListExtra("hc_list", lab.hcList);
                }else{
                    intent.putExtra("collection_distance", lab.collection_distance);
                    intent.putExtra("collection_charges", lab.collection_charges);
                }
                intent.putExtra("fix_del_charges", lab.home_collection_price);
                intent.putExtra("lab_price", lab.price);
                intent.putExtra("lab_email", lab.email);
                intent.putExtra("lab_phone", lab.phone);
                intent.putExtra("timing", lab.timing);
                intent.putExtra("date", lab.date);
                intent.putExtra("time", lab.time);
                intent.putStringArrayListExtra("tests", lab.tests);
                intent.putParcelableArrayListExtra("timeArray", lab.timeList);
                intent.putExtra("package", lab.test_type);
                intent.putExtra("type", lab.type);
                intent.putExtra("discount", lab.price_value1);
                context.startActivity(intent);
            }
        });

        ivLabImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, LabDetailsActivity.class);
                intent.putExtra("image_url", img);
                intent.putExtra("lab_id", lab.id);
                intent.putExtra("book_id", lab.book_id);
                intent.putExtra("user_id", lab.user_id);
                intent.putExtra("test_name", lab.test_name);
                intent.putExtra("test_price", lab.test_price);
                intent.putExtra("lab_image", lab.lab_image);
                intent.putExtra("lab_name", lab.labname);
                intent.putExtra("opentime", lab.opentime);
                intent.putExtra("closetime", lab.closetime);
                intent.putExtra("working_days", lab.working_days);
                intent.putExtra("latitude", lab.latitude);
                intent.putExtra("timing", lab.timing);
                intent.putExtra("longitude", lab.longtitude);
                intent.putExtra("lab_distance", lab.distance);
                if (currentActivty.equals("LabList")) {
                    intent.putExtra("home_collection_price", getHomeCollection(lab));
                    intent.putExtra("homecollection", lab.homecollection);
                    intent.putExtra("hc_type", lab.hc_type);
                    intent.putExtra("convenience", lab.convenience);
                    intent.putExtra("convenience_type", lab.conven_type);
                    intent.putParcelableArrayListExtra("hc_list", lab.hcList);
                }else{
                    intent.putExtra("collection_distance", lab.collection_distance);
                    intent.putExtra("collection_charges", lab.collection_charges);
                }
                intent.putExtra("fix_del_charges", lab.home_collection_price);
                intent.putExtra("lab_price", lab.price);
                intent.putExtra("lab_email", lab.email);
                intent.putExtra("lab_phone", lab.phone);
                intent.putExtra("date", lab.date);
                intent.putExtra("time", lab.time);
                intent.putStringArrayListExtra("tests", lab.tests);
                intent.putParcelableArrayListExtra("timeArray", lab.timeList);
                intent.putExtra("package", lab.test_type);
                intent.putExtra("type", lab.type);
                context.startActivity(intent);
            }
        });
        setTvTypeface();
        return convertView;
    }

    public void setTvTypeface() {
        tvLabTime.setTypeface(Utils.setTypeface(context));
        tvLabName.setTypeface(Utils.setTypeface(context));
        tvLabDistance.setTypeface(Utils.setTypeface(context));
        tvLabPrice.setTypeface(Utils.setTypeface(context));
        tvLabHomeCollection.setTypeface(Utils.setTypeface(context));
        tvLab_percentage.setTypeface(Utils.setTypeface(context));
        tvLab_discount_price.setTypeface(Utils.setTypeface(context));
    }

    private String getHomeCollection(LabModel lab) {
        if (!lab.homecollection.equals("yes")) {
            this.hcCharges = "";
        } else if (lab.hc_type.equals("1")) {
            this.hcCharges = getHcPrice(lab.hcList, lab.getDistance());
        } else {
            this.hcCharges = lab.home_collection_price;
        }
        return this.hcCharges;
    }

    private String getHcPrice(ArrayList<LabModel> hcList, Float distance) {
        if (!hcList.isEmpty() && distance != null) {
            Iterator it = hcList.iterator();
            while (it.hasNext()) {
                LabModel mModel = (LabModel) it.next();
                String[] distArray = mModel.hcDistance.split("-");
                Float startKm = Float.valueOf(distArray[0]);
                Float endKm = Float.valueOf(distArray[1]);
                Log.e("Check the Distance", startKm + "," + distance + "," + endKm);
                if (distance.floatValue() >= startKm.floatValue() && distance.floatValue() <= endKm.floatValue()) {
                    hcCharges = mModel.hcCharges;
                    break;
                }
            }
        }
        return hcCharges;
    }
}
