package com.sahir.websolutions.digilabs.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.Manifest;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sahir.websolutions.digilabs.Maps.DirectionObject;
import com.sahir.websolutions.digilabs.Maps.GsonRequest;
import com.sahir.websolutions.digilabs.Maps.Helper;
import com.sahir.websolutions.digilabs.Maps.LegsObject;
import com.sahir.websolutions.digilabs.Maps.PolylineObject;
import com.sahir.websolutions.digilabs.Maps.RouteObject;
import com.sahir.websolutions.digilabs.Maps.StepsObject;
import com.sahir.websolutions.digilabs.Maps.VolleySingleton;
import com.sahir.websolutions.digilabs.R;
import com.sahir.websolutions.digilabs.objects.GpsTracker;
import com.sahir.websolutions.digilabs.objects.LabModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.media.CamcorderProfile.get;
import static com.sahir.websolutions.digilabs.R.id.map;

/**
 * Created by Sys9 on 7/4/2017.
 */

public class SearchLabActivity extends AppCompatActivity implements OnMapReadyCallback, NavigationView.OnNavigationItemSelectedListener {
    private GoogleMap mMap;
    Toolbar toolbar;
    SharedPreferences prefs;
    public List<Marker> markerList = new ArrayList<>();
    ArrayList<LabModel> labModelArrayList = new ArrayList<>();
    Marker markers;
    LatLng mylatlng;
    GpsTracker gpsTracker;
    Button btn_book_test;
    AutoCompleteTextView etSearchLab;
    String routeDistance, lab_id,user_id;
    ArrayList<String> searchArray, latArray, longArray, idArray;
    Activity sActivity;
    JSONObject jobj;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.searchlab_main);
        sActivity = SearchLabActivity.this;
        init();

        setToolbar();

        setNavDrawer();

        onClicks();

        if (Build.VERSION.SDK_INT >= 23) {
            AllowPermissions();
        } else {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(map);
            mapFragment.getMapAsync(this);

            getAllLabs();
        }
    }

    private void onClicks() {
        etSearchLab.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 1) {
                    String searchChar = etSearchLab.getText().toString().trim();
                    searchLabsList(searchChar);
                }
            }
        });

        etSearchLab.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etSearchLab.getWindowToken(), 0);
                refreshMap();
                LatLng dest = new LatLng(Double.parseDouble(latArray.get(position))
                        , Double.parseDouble(longArray.get(position)));
                String directionApiPath = Helper.getUrl(String.valueOf(mylatlng.latitude), String.valueOf(mylatlng.longitude),
                        String.valueOf(dest.latitude), String.valueOf(dest.longitude));
                Log.d("SearchLab", "Path " + directionApiPath);

                for (int i = 0; i < markerList.size(); i++) {
                    LatLng latLng = new LatLng(Double.parseDouble(latArray.get(position)),
                            Double.parseDouble(longArray.get(position)));
                    if (markerList.get(i).getPosition().equals(latLng)) {
                        Marker marker = markerList.get(i);
                        getDirectionFromDirectionApiServer(directionApiPath, marker, searchArray.get(position));
                    }
                }

                etSearchLab.setText("");
                btn_book_test.setText("Book a test with " + searchArray.get(position));
                lab_id = idArray.get(position);
            }
        });

        btn_book_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btn_book_test.getText().toString().equals("Select a Lab to Book Test")) {
                    Snackbar.make(btn_book_test, "Please select a lab to book test", BaseTransientBottomBar.LENGTH_SHORT).show();
                } else {
                    for (int i = 0; i < labModelArrayList.size(); i++) {
                        LabModel model = labModelArrayList.get(i);
                        if (model.id.equals(lab_id)) {
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putString("is_prefered", "yes");
                            editor.putString("lab_name", model.labname);
                            editor.putString("test_name", model.test_name);
                            editor.putString("lab_id", model.id);
                            editor.putString("price", model.price);
                            editor.putString("email", model.email);
                            editor.putString("address", model.address);
                            editor.putString("city", model.city);
                            editor.putString("state", model.state);
                            editor.putString("coll_charges", model.home_price_fix);
                            editor.putString("distance", routeDistance);
                            editor.putString("phone", model.phone);
                            editor.putString("hc_price", model.home_collection_price);
                            editor.putString("homecollection", model.homecollection);
                            editor.putString("star_rating", model.star_rating);
                            editor.putString("price_rating", model.price_rating);
                            editor.putString("lat", model.latitude);
                            editor.putString("lng", model.longtitude);
                            editor.putString("image", model.lab_image);
                            editor.putString("working_days", model.working_days);
                            editor.putString("timing", model.timing);
                            editor.putString("opentime", model.opentime);
                            editor.putString("closetime", model.closetime);
                            editor.putString("type", "test");
                            editor.commit();
                        }
                    }
                    startActivity(new Intent(SearchLabActivity.this, BookTestActivity.class)
                            .putExtra("type", "lab")
                            .putExtra("lab_id", lab_id));
                }
            }
        });
    }

    private void init() {
        prefs = getSharedPreferences(getString(R.string.user_details), MODE_PRIVATE);
        btn_book_test = (Button) findViewById(R.id.btn_book_test);
        etSearchLab = (AutoCompleteTextView) findViewById(R.id.etSearchLab);
        user_id = prefs.getString(getString(R.string.user_id), null);
    }

    public void searchLabsList(String searchChar) {
        searchArray = new ArrayList<>();
        latArray = new ArrayList<>();
        longArray = new ArrayList<>();
        idArray = new ArrayList<>();
        for (int i = 0; i < labModelArrayList.size(); i++) {
            if (labModelArrayList.get(i).labname.toLowerCase().contains(searchChar.toLowerCase())) {
                searchArray.add(labModelArrayList.get(i).labname);
                latArray.add(labModelArrayList.get(i).latitude);
                longArray.add(labModelArrayList.get(i).longtitude);
                idArray.add(labModelArrayList.get(i).id);
            }
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(SearchLabActivity.this,
                android.R.layout.simple_list_item_1, searchArray);
        etSearchLab.setAdapter(adapter);
    }

    public void refreshMap() {
        markerList.clear();
        mMap.clear();
        if (gpsTracker.canGetLocation()) {
            mylatlng = new LatLng(gpsTracker.latitude, gpsTracker.longitude);
            if (mMap != null) {
                mMap.addMarker(new MarkerOptions().position(mylatlng).zIndex(1)
                        .icon(BitmapDescriptorFactory.fromBitmap(setMarker("My Location", R.drawable.ic_loc))));
                for (int i = 0; i < labModelArrayList.size(); i++) {
                    LabModel labModel = labModelArrayList.get(i);
                    LatLng MyLocation = new LatLng(Double.parseDouble
                            (labModel.latitude), Double.parseDouble(labModel.longtitude));
                    markers = mMap.addMarker(new MarkerOptions().position(MyLocation)
                            .icon(BitmapDescriptorFactory.fromBitmap(setMarker(labModel.labname, R.drawable.ic_map))));
                    markerList.add(markers);
                }
            }
        } else {
            showGPSDisabledAlertToUser();
        }
    }

    public void getAllLabs() {
        FetchData.getInstance(SearchLabActivity.this).getRequestQueue().add(new StringRequest
                (Request.Method.GET, Urls.URL_TO_WEB + "labs/alllabs",
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                final String response2 = response;
                                Log.i("valRes", "" + response2);
                                try {
                                    JSONObject json = new JSONObject(response2);
                                    int status = json.getInt("status");
                                    String filepath = json.optString("message");
                                    if (status == 1) {
                                        JSONArray data = json.optJSONArray("data");
                                        for (int i = 0; i < data.length(); i++) {
                                             jobj = data.optJSONObject(i);
                                            final LabModel labModel = new LabModel();
                                            labModel.id = String.valueOf(jobj.optInt("id"));
                                            labModel.user_id = String.valueOf(jobj.optString("user_id"));
                                            labModel.labname = jobj.optString("labname");
                                            labModel.email = jobj.optString("email");
                                            labModel.address = jobj.optString("address");
                                            labModel.city = jobj.optString("city");
                                            labModel.state = jobj.optString("state_id");
                                            labModel.phone = jobj.optString("phone");
                                            labModel.price = jobj.optString("price");

                                            labModel.home_price_fix=jobj.optString("coll_charges");
                                            labModel.homecollection = jobj.optString("homecollection");
                                            labModel.home_collection_price = jobj.optString("hcprice");
                                            labModel.star_rating = jobj.optString("ratings");
                                            labModel.price_rating = jobj.optString("priceRating");
                                            labModel.latitude = jobj.optString("latitude");
                                            labModel.longtitude = jobj.optString("longitude");
//                                                labModel.lab_image = jobj1.optString("filepath") + jobj1.optString("filename");
                                            labModel.timing = jobj.optString("timing");
                                            labModel.lab_image = jobj.optString("filename");
                                            labModel.opentime = jobj.optString("opentime");
                                            labModel.closetime = jobj.optString("closetime");
                                            labModel.working_days = jobj.optString("workingdays");
                                            labModelArrayList.add(labModel);
                                        }
                                        if (mMap != null) {
                                            markerList.clear();
                                            mMap.clear();
                                            setMarker();
                                            for (int i = 0; i < labModelArrayList.size(); i++) {
                                                LabModel labModel = labModelArrayList.get(i);
                                                LatLng MyLocation = new LatLng(Double.parseDouble
                                                        (labModel.latitude), Double.parseDouble(labModel.longtitude));
                                                markers = mMap.addMarker(new MarkerOptions().position(MyLocation)
                                                        .icon(BitmapDescriptorFactory.fromBitmap(setMarker(labModel.labname, R.drawable.ic_map))));
//                                                markers = mMap.addMarker(new MarkerOptions().position(MyLocation)
//                                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map)));
                                                markerList.add(markers);
                                            }

                                            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                                                @Override
                                                public boolean onMarkerClick(Marker marker) {

                                                    try {
                                                        refreshMap();
                                                        for (int i = 0; i < labModelArrayList.size(); i++) {
                                                            LabModel list = labModelArrayList.get(i);
                                                            if (list.latitude.contains(String.valueOf(marker.getPosition().latitude))
                                                                    && list.longtitude.contains(String.valueOf(marker.getPosition().longitude))) {
                                                                btn_book_test.setText("Book a test with " + list.labname);
                                                                lab_id = list.id;
                                                                LatLng dest = new LatLng(marker.
                                                                        getPosition().latitude, marker.getPosition().longitude);
                                                               String aa =  labModelArrayList.get(i).lab_image;
                                                                com.sahir.websolutions.digilabs.HelperClass.Helper.setImage(aa,sActivity);
                                                                String directionApiPath = Helper.getUrl(String.valueOf(mylatlng.latitude), String.valueOf(mylatlng.longitude),
                                                                        String.valueOf(dest.latitude), String.valueOf(dest.longitude));
                                                                Log.d("SearchLab", "Path " + directionApiPath);
                                                                getDirectionFromDirectionApiServer(directionApiPath, markerList.get(i), list.labname);
                                                            }
                                                        }
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                    return false;
                                                }
                                            });
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                    }
                }) {
        });
    }


    private void getDirectionFromDirectionApiServer(String url, Marker marker, String title) {
        GsonRequest<DirectionObject> serverRequest = new GsonRequest<DirectionObject>(
                Request.Method.GET,
                url,
                DirectionObject.class,
                createRequestSuccessListener(marker, title),
                createRequestErrorListener());
        serverRequest.setRetryPolicy(new DefaultRetryPolicy(
                Helper.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(serverRequest);
    }

    private Response.Listener<DirectionObject> createRequestSuccessListener(final Marker marker, final String title) {
        return new Response.Listener<DirectionObject>() {
            @Override
            public void onResponse(DirectionObject response) {
                try {
                    Log.d("JSON Response", response.toString());
                    if (response.getStatus().equals("OK")) {
                        List<LatLng> mDirections = getDirectionPolylines(response.getRoutes(), marker, title);
                        drawRouteOnMap(mMap, mDirections);
                    } else {
                        Toast.makeText(SearchLabActivity.this, "Server Error!", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private List<LatLng> getDirectionPolylines(List<RouteObject> routes, Marker marker, String title) {
        List<LatLng> directionList = new ArrayList<LatLng>();
        for (RouteObject route : routes) {
            List<LegsObject> legs = route.getLegs();
            for (LegsObject leg : legs) {
                routeDistance = leg.getDistance().getText();
//                marker.setTitle(routeDistance + " away");
//                marker.showInfoWindow();
                List<StepsObject> steps = leg.getSteps();
                for (StepsObject step : steps) {
                    PolylineObject polyline = step.getPolyline();
                    String points = polyline.getPoints();
                    List<LatLng> singlePolyline = decodePoly(points);
                    for (LatLng direction : singlePolyline) {
                        directionList.add(direction);
                    }
                }
            }
        }
        return directionList;
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        };
    }

    private void drawRouteOnMap(GoogleMap map, List<LatLng> positions) {
        PolylineOptions options = new PolylineOptions().width(5).color(Color.RED).geodesic(true);
        options.addAll(positions);
        Polyline polyline = map.addPolyline(options);
    }

    /**
     * Method to decode polyline points
     * Courtesy : http://jeffreysambells.com/2010/05/27/decoding-polylines-from-google-maps-direction-api-with-java
     */
    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;
        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;
            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;
            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }
        return poly;
    }

    public Bitmap setMarker(String title, int res) {
        LinearLayout tv = (LinearLayout) getLayoutInflater().inflate(R.layout.map_marker, null, false);
        ImageView img = (ImageView) tv.findViewById(R.id.img);
        img.setImageResource(res);
        TextView text = (TextView) tv.findViewById(R.id.txt);
        text.setText(title);
        tv.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        tv.layout(0, 0, tv.getMeasuredWidth(), tv.getMeasuredHeight());

        tv.setDrawingCacheEnabled(true);
        tv.buildDrawingCache();
        return tv.getDrawingCache();
    }


    public void setToolbar() {
        setTitle("");
        toolbar = (Toolbar) findViewById(R.id.toolbar_image);
        setSupportActionBar(toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        ImageView ivImage = (ImageView) toolbar.findViewById(R.id.ivImage);
        ivImage.setImageResource(R.drawable.ic_book_test_white);
        mTitle.setText("Search a Lab");
        mTitle.setTypeface(Utils.setTypeface(this));
    }

    public void setNavDrawer() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        TextView tvCopyright = (TextView) findViewById(R.id.tvCopyright);
        tvCopyright.setTypeface(Utils.setTypeface(this));

        Menu m = navigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);
        }
        if (com.sahir.websolutions.digilabs.HelperClass.Helper.getCheck(sActivity).equals("doctor")){
            navigationView.getMenu().setGroupVisible(R.id.group_item5, false);
        }else {
            navigationView.getMenu().setGroupVisible(R.id.group_item5, true);
        }
    }

    private void applyFontToMenuItem(MenuItem mi) {
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", Utils.setTypeface(this)), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            Intent intent = new Intent(SearchLabActivity.this, PreviewActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_my_favourites) {
            Intent intent = new Intent(SearchLabActivity.this, BookedTestsActivity.class);
            startActivity(intent);
            com.sahir.websolutions.digilabs.HelperClass.Helper.setAddClass("addNo",sActivity);
        } else if (id == R.id.nav_Profile) {
            if (prefs.getString("role", null).equals("3")) {
                startActivity(new Intent(SearchLabActivity.this, RegisterDoctor.class)
                        .putExtra("intent", "update"));
            } else {
                startActivity(new Intent(SearchLabActivity.this, RegisterActivity.class)
                        .putExtra("intent", "update"));
            }
        } else if (id == R.id.nav_share_app) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    "Hey check out my app at: http://bit.ly/2xv0Zi8");
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        } else if (id == R.id.nav_register_lab) {
            startActivity(new Intent(SearchLabActivity.this, RegisterUserToDoctor.class)
                    .putExtra("user_id", user_id));
//            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.elvektechnologies.com/Digilabs/"));
//            startActivity(browserIntent);
        } else if (id == R.id.nav_rate_app) {
            startActivity(new Intent(SearchLabActivity.this, FeedbackActivity.class));
        } else if (id == R.id.nav_logout_app) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.clear();
            editor.commit();
            for (int i = 0; i < ActivityStack.activity
                    .size(); i++) {
                (ActivityStack.activity.elementAt(i))
                        .finish();
                Log.e("Activity size:" + ActivityStack.activity.size(), "finished...");
            }
            startActivity(new Intent(SearchLabActivity.this, RegisterActivity.class).putExtra("intent", "register"));
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setMapToolbarEnabled(false);
    }

    public void setMarker() {
        gpsTracker = new GpsTracker(SearchLabActivity.this);
        if (gpsTracker.canGetLocation()) {
            mylatlng = new LatLng(gpsTracker.latitude, gpsTracker.longitude);
            if (mMap != null) {
                mMap.addMarker(new MarkerOptions().position(mylatlng).zIndex(1)
                        .icon(BitmapDescriptorFactory.fromBitmap(setMarker("My Location", R.drawable.ic_loc))));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mylatlng,
                        11));
            }
        } else {
            showGPSDisabledAlertToUser();
        }
    }

    //-------------------------Check GPS LOCATION is enable or not
    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SearchLabActivity.this);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Go to Settings Page To Enable GPS",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                                finish();
                            }
                        });
        alertDialogBuilder.setNegativeButton(android.R.string.cancel,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private void AllowPermissions() {
        int hasLocationPermission = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION);
        int hasSMSPermission = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> permissions = new ArrayList<String>();
        if (hasLocationPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }

        if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }

        if (!permissions.isEmpty()) {
            ActivityCompat.requestPermissions(SearchLabActivity.this, permissions.toArray(new String[permissions.size()]), 101);
        } else {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(map);
            mapFragment.getMapAsync(this);

            getAllLabs();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 101: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                                .findFragmentById(map);
                        mapFragment.getMapAsync(this);

                        getAllLabs();
                        return;
                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i]);
//                        finish();
                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

}
