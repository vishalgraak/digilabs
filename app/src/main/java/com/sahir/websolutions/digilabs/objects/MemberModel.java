package com.sahir.websolutions.digilabs.objects;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Sys9 on 7/7/2017.
 */

public class MemberModel implements Parcelable {
    public String name;
    public String id;
    public String user_id;

    public MemberModel() {

    }

    protected MemberModel(Parcel in) {
        name = in.readString();
        id = in.readString();
        user_id = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(user_id);
    }

    public static final Creator<MemberModel> CREATOR = new Creator<MemberModel>() {
        @Override
        public MemberModel createFromParcel(Parcel in) {
            return new MemberModel(in);
        }

        @Override
        public MemberModel[] newArray(int size) {
            return new MemberModel[size];
        }
    };
}
