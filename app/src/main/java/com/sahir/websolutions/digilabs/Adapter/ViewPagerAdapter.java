package com.sahir.websolutions.digilabs.Adapter;

import android.content.Context;
import android.media.MediaPlayer;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.sahir.websolutions.digilabs.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ViewPagerAdapter extends PagerAdapter {
    // Declare Variables
    Context context;
    LayoutInflater inflater;
    ArrayList<String> images;


    public ViewPagerAdapter(Context context, ArrayList<String> images2) {
        this.context = context;
        this.images = images2;
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        // Declare Variables
        View itemView;
        ImageView imageView;
        VideoView video_player_view;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        itemView = inflater.inflate(R.layout.viewpager_item, container,
                false);
        imageView = (ImageView) itemView.findViewById(R.id.image);
        video_player_view = (VideoView) itemView.findViewById(R.id.video_player_view);

        if (images.get(position).endsWith("jpg") || images.get(position).endsWith("png")) {
            imageView.setVisibility(View.VISIBLE);
            video_player_view.setVisibility(View.GONE);
            Picasso.with(context).load(images.get(position)).rotate(0f, 0f, 0f).skipMemoryCache()
                    .into(imageView, new Callback() {

                        @Override
                        public void onSuccess() {
                            // TODO Auto-generated method stub
                        }

                        @Override
                        public void onError() {
                            // TODO Auto-generated method stub

                        }
                    });
        } else if (images.get(position).endsWith("gif")) {
            imageView.setVisibility(View.VISIBLE);
            video_player_view.setVisibility(View.GONE);
            Glide.with(context)
                    .load(images.get(position))
                    .asGif()
                    .into(imageView);
        } else {
            video_player_view.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.GONE);
            //Creating MediaController
            MediaController mediaController= new MediaController(context);
            mediaController.setAnchorView(video_player_view);
            video_player_view.setMediaController(mediaController);
            video_player_view.setVideoPath(images.get(position));
            video_player_view.start();
            video_player_view.setOnErrorListener(new MediaPlayer.OnErrorListener() {

                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    Log.d("video", "setOnErrorListener ");
                    return true;
                }
            });
        }

        // Add viewpager_item.xml to ViewPager
        ((ViewPager) container).addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        ((ViewPager) container).removeView((RelativeLayout) object);

    }
}