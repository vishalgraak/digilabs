package com.sahir.websolutions.digilabs.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sahir.websolutions.digilabs.HelperClass.Helper;
import com.sahir.websolutions.digilabs.IntroSlider.MyPagerAdapter;
import com.sahir.websolutions.digilabs.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;


public class PreviewActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, EasyPermissions.PermissionCallbacks {
    Toolbar toolbar;
    public static int PAGES = 4;
    public static int FIRST_PAGE = 1;
    public MyPagerAdapter adapter;
    public ViewPager pager;
    static ImageView ivThumb1, ivThumb2, ivThumb3, ivThumb4, ivThumb5, ivLeftArrow, ivRightArrow;
    LinearLayout llThumb1, llThumb2, llThumb3, llThumb4, llThumb5;
    static TextView tvThumb1, tvThumb2, tvThumb3, tvThumb4, tvThumb5;
    TextView tvScrollInfo;
    SharedPreferences prefs;
    public static String role;
    NavigationView navigationView;
    Activity sActivity;
    private static final String TAG = "MainActivity";
    private static final int RC_CAMERA_PERM = 123;
    private static final int RC_LOCATION_CONTACTS_PERM = 124;
    private static final int RC_SETTINGS_SCREEN = 125;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preview_main);
        sActivity = PreviewActivity.this;
        locationAndContactsTask();
        prefs = getSharedPreferences(getString(R.string.user_details), Context.MODE_PRIVATE);

        pager = (ViewPager) findViewById(R.id.myviewpager);
        ivThumb1 = (ImageView) findViewById(R.id.ivThumb1);//local report
        ivThumb2 = (ImageView) findViewById(R.id.ivThumb2);//my report
        ivThumb3 = (ImageView) findViewById(R.id.ivThumb3);//book test
        ivThumb4 = (ImageView) findViewById(R.id.ivThumb4);//health
        ivThumb5 = (ImageView) findViewById(R.id.ivThumb5);//patients

        llThumb1 = (LinearLayout) findViewById(R.id.llThumb1);
        llThumb2 = (LinearLayout) findViewById(R.id.llThumb2);
        llThumb3 = (LinearLayout) findViewById(R.id.llThumb3);
        llThumb4 = (LinearLayout) findViewById(R.id.llThumb4);
        llThumb5 = (LinearLayout) findViewById(R.id.llThumb5);

        ivLeftArrow = (ImageView) findViewById(R.id.ivLeftArrow);
        ivRightArrow = (ImageView) findViewById(R.id.ivRightArrow);

        tvThumb1 = (TextView) findViewById(R.id.tvThumb1);
        tvThumb2 = (TextView) findViewById(R.id.tvThumb2);
        tvThumb3 = (TextView) findViewById(R.id.tvThumb3);
        tvThumb4 = (TextView) findViewById(R.id.tvThumb4);
        tvThumb5 = (TextView) findViewById(R.id.tvThumb5);
        tvScrollInfo = (TextView) findViewById(R.id.tvScrollInfo);

        setToolbar();

        setNavDrawer();

        if (getIntent().getStringExtra("home") != null) {
            if (!getIntent().getStringExtra("home").equals("yes")) {
                ActivityStack.activity.add(PreviewActivity.this);
            } else {
                //dfsfs
            }
        } else {
            ActivityStack.activity.add(PreviewActivity.this);
        }


        if (prefs.getString("role", null) != null) {

            role = prefs.getString("role", null);
        } else {
            role = "4";
        }

        //check patient or Doctor
        if (role != null) {
            if (role.equals("3")) {
                llThumb5.setVisibility(View.VISIBLE);
                PAGES = 5;
                FIRST_PAGE = 2;
            } else {
                llThumb5.setVisibility(View.GONE);
                PAGES = 4;
                FIRST_PAGE = 1;
            }
        }

        adapter = new MyPagerAdapter(PreviewActivity.this, this.getSupportFragmentManager());
        pager.setAdapter(adapter);
        pager.setPageTransformer(false, adapter);

        // Set current item to the middle page so we can fling to both
        // directions left and right
        pager.setCurrentItem(FIRST_PAGE);

        // Necessary or the pager will only have one extra page to show
        // make this at least however many pages you can see
        pager.setOffscreenPageLimit(3);
        //pager.setPadding(10,10,10,10);

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                //Toast.makeText(PreviewActivity.this, "scrolled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageSelected(int position) {
                //Toast.makeText(PreviewActivity.this, "selected", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

                //Toast.makeText(PreviewActivity.this, "state change", Toast.LENGTH_SHORT).show();
            }
        });

        double a = getResources().getDisplayMetrics().widthPixels / (4);

        pager.setPageMargin(-(int) a);

        if (prefs.getString("role", null) != null) {
            if (prefs.getString("role", null).equals("3")) {
                llThumb1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pager.setCurrentItem(2);
                    }
                });
                llThumb2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pager.setCurrentItem(3);
                    }
                });
                llThumb3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pager.setCurrentItem(1);
                    }
                });
                llThumb4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pager.setCurrentItem(4);
                    }
                });
                llThumb5.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pager.setCurrentItem(0);
                    }
                });
            } else {
                llThumb1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pager.setCurrentItem(1);
                    }
                });
                llThumb2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pager.setCurrentItem(2);
                    }
                });
                llThumb3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pager.setCurrentItem(0);
                    }
                });
                llThumb4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pager.setCurrentItem(3);
                    }
                });
            }
        } else {
            llThumb1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pager.setCurrentItem(1);
                }
            });
            llThumb2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pager.setCurrentItem(2);
                }
            });
            llThumb3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pager.setCurrentItem(0);
                }
            });
            llThumb4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pager.setCurrentItem(3);
                }
            });
        }

        ivLeftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int i = pager.getCurrentItem() - 1;
                if (i >= 0)
                    pager.setCurrentItem(i);
            }
        });
        ivRightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int i = pager.getCurrentItem() + 1;
                if (i <= 2)
                    pager.setCurrentItem(i);
            }
        });

        clearstack();
        setTvTypeface();

    }

    public void clearstack() {
        for (int i = 0; i < ActivityStack.activity
                .size(); i++) {
            (ActivityStack.activity.elementAt(i))
                    .finish();
            Log.e("Activity size:" + ActivityStack.activity.size(), "finished...");
        }
    }

    public void setToolbar() {
        setTitle("");
        toolbar = (Toolbar) findViewById(R.id.toolbar_image);
        setSupportActionBar(toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        ImageView ivImage = (ImageView) toolbar.findViewById(R.id.ivImage);
        ivImage.setImageResource(R.drawable.ic_digilabs);
        mTitle.setText("DIGILABS");
        mTitle.setTypeface(Utils.setTypeface(this));
    }

    public void setNavDrawer() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        TextView tvCopyright = (TextView) findViewById(R.id.tvCopyright);
        tvCopyright.setTypeface(Utils.setTypeface(this));

        Menu m = navigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);
        }
        if (Helper.getCheck(sActivity).equals("doctor")) {
            navigationView.getMenu().setGroupVisible(R.id.group_item5, false);
        } else {
            navigationView.getMenu().setGroupVisible(R.id.group_item5, true);
        }
        Menu menu = navigationView.getMenu();
        MenuItem nav_camara = menu.findItem(R.id.nav_logout_app);
        if (Helper.getSkip(sActivity).equals("login")) {
            nav_camara.setTitle("Login");
            Helper.setSkip("",sActivity);
        } else {
            nav_camara.setTitle("Logout");
            Helper.setSkip("",sActivity);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!Helper.getCheck(sActivity).equals("doctor")) {
            if (prefs.getString("fname", null) != null || prefs.getString("lname", null) != null) {
                tvScrollInfo.setText("Welcome " +
                        //prefs.getString("title", null) + " " +
                        prefs.getString("fname", null) + " " + prefs.getString("lname", null));
                if (prefs.getString("title", null) != null) {
                    tvScrollInfo.setText("Welcome Mr." +
                            //prefs.getString("title", null) + " " +
                            prefs.getString("fname", null) + " " + prefs.getString("lname", null));
                }
            }
        } else {
            if (prefs.getString("fname", null) == null) {
                tvScrollInfo.setText("DigiLabs - Your portal to Online Reportig");
            } else {
                tvScrollInfo.setText("Welcome " + "Dr. " +
                        //prefs.getString("title", null) + " " +
                        prefs.getString("fname", null) + " " + prefs.getString("lname", null));
            }
        }
        SharedPreferences pref = getApplicationContext().getSharedPreferences("Token", MODE_PRIVATE);
        //Update Device Token
//        if (prefs.getBoolean("firstrun", true)) {
        String userId = prefs.getString(getString(R.string.user_id), null);
        if (prefs.getString("role", null) != null) {
            /*if (prefs.getString("role", null).equals("3")) {
                String url = "users/editDoctorapi/" + userId + "?";
                sendToServer(url, pref.getString("regId", null));
            } else {*/
            String url = "users/editapi/" + userId;
            sendToServer(url, pref.getString("regId", null));
//            }
        }
//            prefs.edit().putBoolean("firstrun", false).commit();
//        }
//        Menu menu = navigationView.getMenu();
//        MenuItem nav_camara = menu.findItem(R.id.nav_logout_app);
//        if (Helper.getSkip(sActivity).equals("login")) {
//            nav_camara.setTitle("Register/Login");
//            Helper.setSkip("", sActivity);
//        } else {
//            nav_camara.setTitle("Logout");
//            Helper.setSkip("", sActivity);
//        }
    }

    public void sendToServer(String url, final String device_token) {
        FetchData.getInstance(PreviewActivity.this).getRequestQueue().add(new StringRequest
                (Request.Method.POST, Urls.URL_TO_WEB + url,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                final String response2 = response;
                                Log.i("User Update", "" + response2);
                                try {
                                    JSONObject json = new JSONObject(response2);
                                    int status = json.getInt("status");
                                    if (status == 1) {
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
              /*  if (prefs.getString("role", null).equals("3")) {
                    params.put("clinicid", prefs.getString("clinic_id", null));
                }*/
                params.put("devicetoken", device_token);
                return params;
            }
        });
    }


    private void applyFontToMenuItem(MenuItem mi) {
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", Utils.setTypeface(this)), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
//            Intent intent = new Intent(BookTestActivity.this, PreviewActivity.class);
//            startActivity(intent);
//            finish();
        } else if (id == R.id.nav_my_favourites) {
            if (prefs.getString(getString(R.string.user_id), null) == null) {
                startActivity(new Intent(PreviewActivity.this, RegisterActivity.class).putExtra("intent", "register"));
                Helper.setAddClass("addNo", sActivity);
                Helper.setAddClass("addNo", sActivity);
                finish();
            } else {
                Intent intent = new Intent(PreviewActivity.this, BookedTestsActivity.class);
                startActivity(intent);
            }
        } else if (id == R.id.nav_Profile) {
            if (prefs.getString(getString(R.string.user_id), null) == null) {
                startActivity(new Intent(PreviewActivity.this, RegisterActivity.class).putExtra("intent", "register"));
                finish();
            } else {
                if (prefs.getString("role", null).equals("3")) {
                    startActivity(new Intent(PreviewActivity.this, RegisterDoctor.class)
                            .putExtra("intent", "update"));
                } else {
                    startActivity(new Intent(PreviewActivity.this, RegisterActivity.class)
                            .putExtra("intent", "update"));
                }
            }
        } else if (id == R.id.nav_share_app) {
            if (prefs.getString(getString(R.string.user_id), null) == null) {
                startActivity(new Intent(PreviewActivity.this, RegisterActivity.class).putExtra("intent", "register"));
                finish();
            } else {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "Hey check out my app at: http://bit.ly/2xv0Zi8");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        } else if (id == R.id.nav_register_lab) {
            if (prefs.getString(getString(R.string.user_id), null) == null) {
                startActivity(new Intent(PreviewActivity.this, RegisterActivity.class).putExtra("intent", "register"));
                finish();
            } else {
                startActivity(new Intent(PreviewActivity.this, RegisterUserToDoctor.class)
                        .putExtra("user_id", prefs.getString(getString(R.string.user_id), null)));
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.elvektechnologies.com/Digilabs/"));
//                startActivity(browserIntent);
            }
        } else if (id == R.id.nav_rate_app) {
            if (prefs.getString(getString(R.string.user_id), null) == null) {
                startActivity(new Intent(PreviewActivity.this, RegisterActivity.class).putExtra("intent", "register"));
                finish();
            } else {
                startActivity(new Intent(PreviewActivity.this, FeedbackActivity.class));
            }
        } else if (id == R.id.nav_logout_app) {
            if (prefs.getString(getString(R.string.user_id), null) == null) {
                startActivity(new Intent(PreviewActivity.this, RegisterActivity.class).putExtra("intent", "register"));
                finish();
            } else {
                SharedPreferences.Editor editor = prefs.edit();
                editor.clear();
                editor.commit();
                for (int i = 0; i < ActivityStack.activity
                        .size(); i++) {
                    (ActivityStack.activity.elementAt(i))
                            .finish();
                    Log.e("Activity size:" + ActivityStack.activity.size(), "finished...");
                }
                Intent intent = new Intent(PreviewActivity.this, RegisterActivity.class);
                intent.putExtra("intent", "register");
                startActivity(intent);
                finish();
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public static void setThumbBackground(int pos) {
        ivThumb1.setImageResource(R.drawable.ic_local_report_grey);
        ivThumb2.setImageResource(R.drawable.ic_my_report_grey);
        ivThumb3.setImageResource(R.drawable.ic_book_test_grey);
        ivThumb4.setImageResource(R.drawable.ic_health_grey);
        ivThumb5.setImageResource(R.drawable.ic_patients_grey);

        // tvThumb1.setTextColor(R.color.colorPrimary);
        tvThumb1.setTextColor(Color.parseColor("#757575"));
        tvThumb2.setTextColor(Color.parseColor("#757575"));
        tvThumb3.setTextColor(Color.parseColor("#757575"));
        tvThumb4.setTextColor(Color.parseColor("#757575"));
        tvThumb5.setTextColor(Color.parseColor("#757575"));

        if (role.equals("3")) {
            switch (pos) {
                case 0:
                    ivThumb5.setImageResource(R.drawable.ic_patients);
                    tvThumb5.setTextColor(Color.parseColor("#05959F"));
                    break;
                case 1:
                    ivThumb3.setImageResource(R.drawable.ic_book_test);
                    tvThumb3.setTextColor(Color.parseColor("#05959F"));
                    break;
                case 2:
                    ivThumb1.setImageResource(R.drawable.ic_local_report);
                    tvThumb1.setTextColor(Color.parseColor("#05959F"));
                    break;
                case 3:
                    ivThumb2.setImageResource(R.drawable.ic_my_report);
                    tvThumb2.setTextColor(Color.parseColor("#05959F"));
                    break;
                case 4:
                    ivThumb4.setImageResource(R.drawable.ic_health);
                    tvThumb4.setTextColor(Color.parseColor("#05959F"));
                    break;
            }
        } else {
            switch (pos) {
                case 0:
                    ivThumb3.setImageResource(R.drawable.ic_book_test);
                    tvThumb3.setTextColor(Color.parseColor("#05959F"));
                    break;
                case 1:
                    ivThumb1.setImageResource(R.drawable.ic_local_report);
                    tvThumb1.setTextColor(Color.parseColor("#05959F"));
                    break;
                case 2:
                    ivThumb2.setImageResource(R.drawable.ic_my_report);
                    tvThumb2.setTextColor(Color.parseColor("#05959F"));
                    break;
                case 3:
                    ivThumb4.setImageResource(R.drawable.ic_health);
                    tvThumb4.setTextColor(Color.parseColor("#05959F"));
                    break;
            }
        }
    }

    public void setTvTypeface() {
        tvThumb1.setTypeface(Utils.setTypeface(this));
        tvThumb2.setTypeface(Utils.setTypeface(this));
        tvThumb3.setTypeface(Utils.setTypeface(this));
        tvThumb4.setTypeface(Utils.setTypeface(this));
        tvThumb5.setTypeface(Utils.setTypeface(this));
        tvScrollInfo.setTypeface(Utils.setTypeface(this));
    }

    //    @Override
//    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            new AlertDialog.Builder(this)
//                    .setMessage("Are you sure you want to exit from app?")
//                    .setCancelable(false)
//                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int id) {
//                            finish();
//                            finish();
//
//                        }
//                    })
//                    .setNegativeButton("No", null)
//                    .show();
//        }
//    }
    public void onBackPressed() {
        if (doubleClick) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        this.doubleClick = true;
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to exit from app?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finishAffinity();
                    }
                })
                .setNegativeButton("No", null)
                .show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleClick = false;
            }
        }, 2000);
    }

    private boolean doubleClick = false;

    public void locationAndContactsTask() {
        String[] perms = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_CONTACTS,
                Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_SMS,
                Manifest.permission.SEND_SMS, Manifest.permission.VIBRATE,
                Manifest.permission.CALL_PHONE, Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

        if (EasyPermissions.hasPermissions(this, perms)) {
        } else {
            EasyPermissions.requestPermissions(this, "This app needs access to your location and contacts to know where and who you are.",
                    RC_LOCATION_CONTACTS_PERM, perms);
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        Log.d(TAG, "onPermissionsGranted:" + requestCode + ":" + perms.size());
    }

    @Override
    //permission
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Log.d(TAG, "onPermissionsDenied:" + requestCode + ":" + perms.size());
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this, "This app may not work correctly without the requested permissions. Open the app settings screen to modify app permissions.")
                    .setTitle("Permissions Required")
                    .setPositiveButton("Settings")
                    .setNegativeButton("Cancel", null)
                    .setRequestCode(RC_SETTINGS_SCREEN)
                    .build()
                    .show();
        }
    }
}
