package com.sahir.websolutions.digilabs.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Vishal on 09/05/2018.
 */
@Entity(tableName = "user_address")
public class AddressModel {

    @PrimaryKey(autoGenerate = true)
    public int id;
    public String mShippingEmail;
    public String mShippingName;
    public String mShippingAddress;
    public String mShippingState;
    public String mShippingCity;
    public String mShippingPinCode;
    public String mShippingMobile;

    public String mBillingEmail;
    public String mBillingName;
    public String mBillingAddress;
    public String mBillingState;
    public String mBillingCity;
    public String mBillingPinCode;
    public String mBillingMobile;
}

