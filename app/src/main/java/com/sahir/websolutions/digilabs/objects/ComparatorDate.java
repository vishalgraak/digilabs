package com.sahir.websolutions.digilabs.objects;

import java.util.Comparator;

/**
 * Created by Sys9 on 6/27/2017.
 */

public class ComparatorDate implements Comparator<Report> {
    @Override
    public int compare(Report o1, Report o2) {

        String date1 = o1.getDate();
        String date2 = o2.getDate();

        return date1.compareTo(date2);
    }
}
