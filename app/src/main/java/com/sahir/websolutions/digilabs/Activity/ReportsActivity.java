package com.sahir.websolutions.digilabs.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sahir.websolutions.digilabs.HelperClass.Helper;
import com.sahir.websolutions.digilabs.R;
import com.sahir.websolutions.digilabs.objects.MemberModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

public class ReportsActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    Toolbar toolbar;
    SharedPreferences prefs;
    String user_id, user_pin;
    Button btn_reports;
    TextView txt1, txt2, txt3, txt;
    LinearLayout ll_add1, ll_add2, ll_add3, ll_add_members;
    ImageView img1, img2, img3;
    String[] arr = new String[2];
    ArrayList<MemberModel> members_list = new ArrayList<>();
    ProgressBar progress_bar;
    ArrayList<String> members = new ArrayList<>();
    ProgressDialog progressDialog;
    Activity sActivity;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_report_main);
        sActivity = ReportsActivity.this;
        context = getApplicationContext();
        init();

        setToolbar();

        setNavDrawer();

        setFonts();

        OnClicks();

        OnLongPress();

        if (!Utils.isNetworkConnected(ReportsActivity.this)) {
            Utils.conDialog(ReportsActivity.this);
        } else {
            getMembers("userfamilies/getApi/" + user_id);
        }

    }

    private void init() {
        ActivityStack.activity.add(ReportsActivity.this);
        progressDialog = new ProgressDialog(ReportsActivity.this);
        progressDialog.setMessage("Loading....");
        progressDialog.show();
        btn_reports = (Button) findViewById(R.id.btn_reports);
        img1 = (ImageView) findViewById(R.id.img1);
        img2 = (ImageView) findViewById(R.id.img2);
        img3 = (ImageView) findViewById(R.id.img3);
        txt = (TextView) findViewById(R.id.txt);
        txt1 = (TextView) findViewById(R.id.txt1);
        txt2 = (TextView) findViewById(R.id.txt2);
        txt3 = (TextView) findViewById(R.id.txt3);
        ll_add1 = (LinearLayout) findViewById(R.id.ll_add1);
        ll_add2 = (LinearLayout) findViewById(R.id.ll_add2);
        ll_add3 = (LinearLayout) findViewById(R.id.ll_add3);
        ll_add_members = (LinearLayout) findViewById(R.id.ll_add_members);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        //progress_bar.setVisibility(View.VISIBLE);
        txt1.setTypeface(Utils.setTypeface(this));
        txt2.setTypeface(Utils.setTypeface(this));
        txt3.setTypeface(Utils.setTypeface(this));
        prefs = getSharedPreferences(getString(R.string.user_details), MODE_PRIVATE);
        user_id = prefs.getString(getString(R.string.user_id), null);
        user_pin = prefs.getString("pin", null);
        arr[0] = "Edit";
        arr[1] = "Delete";

        members.add("My");
    }

    private void setFonts() {
        txt.setTypeface(Utils.setTypeface(this));
        txt1.setTypeface(Utils.setTypeface(this));
        txt2.setTypeface(Utils.setTypeface(this));
        txt3.setTypeface(Utils.setTypeface(this));
        btn_reports.setTypeface(Utils.setTypeface(this));
    }

    private void OnLongPress() {
        ll_add1.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (members_list.size() > 0)
                    optionDialog(arr, ll_add1, "ll_add1", txt1.getText().toString());
                return true;
            }
        });

        ll_add2.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (members_list.size() > 0)
                    optionDialog(arr, ll_add2, "ll_add2", txt2.getText().toString());
                return true;
            }
        });

        ll_add3.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (members_list.size() > 0)
                    optionDialog(arr, ll_add3, "ll_add3", txt3.getText().toString());
                return true;
            }
        });
    }

    private void OnClicks() {

        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });

        ll_add1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txt1.getText().toString().length() == 0) {
                    openDialog(null, 0);
                } else {
                    startActivity(new Intent(ReportsActivity.this, LocalReportActivity.class)
                            .putExtra("member", members_list.get(0).name)
                            .putExtra("member_id", members_list.get(0).id)
                            .putParcelableArrayListExtra("members_list",members_list)
                            .putStringArrayListExtra("member_list", members));
                }
            }
        });

        ll_add2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txt2.getText().toString().length() == 0) {
                    openDialog(null, 0);
                } else {
                    startActivity(new Intent(ReportsActivity.this, LocalReportActivity.class)
                            .putExtra("member", members_list.get(1).name)
                            .putExtra("member_id", members_list.get(1).id)
                            .putParcelableArrayListExtra("members_list",members_list)
                            .putStringArrayListExtra("member_list", members));
                }
            }
        });

        ll_add3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txt3.getText().toString().length() == 0) {
                    openDialog(null, 0);
                } else {
                    startActivity(new Intent(ReportsActivity.this, LocalReportActivity.class)
                            .putExtra("member", members_list.get(2).name)
                            .putExtra("member_id", members_list.get(2).id)
                            .putParcelableArrayListExtra("members_list",members_list)
                            .putStringArrayListExtra("member_list", members));
                }
            }
        });

        btn_reports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ReportsActivity.this, LocalReportActivity.class)
                        .putExtra("member", "my").putStringArrayListExtra("member_list", members)
                        .putParcelableArrayListExtra("members_list",members_list));
            }
        });
    }

    public void openDialog(final String text, final int member_id) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_report_name);
        dialog.show();
        final EditText etName = (EditText) dialog.findViewById(R.id.etName);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        txt_title.setText("Enter Family Member's Name");
        txt_title.setTypeface(Utils.setTypeface(this));
        LinearLayout ll_belong = (LinearLayout)dialog.findViewById(R.id.ll_belong);
        ll_belong.setVisibility(View.GONE);
        TextView tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);
        final TextView tvNext = (TextView) dialog.findViewById(R.id.tvNext);
        tvCancel.setTypeface(Utils.setTypeface(this));
        tvNext.setTypeface(Utils.setTypeface(this));

        if (text != null) {
            etName.setText(text);
        }

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = etName.getText().toString().trim();
                if (etName.length() == 0) {
                    Snackbar.make(tvNext, "Member's name can not be blank.", BaseTransientBottomBar.LENGTH_SHORT).show();
                } else {
                    if (text != null) {
                        String url = "userfamilies/editApi/" + member_id;
                        addMember(url, name, true);
                        progress_bar.setVisibility(View.VISIBLE);
                    } else {
                        String url = "userfamilies/addApi";
                        addMember(url, name, false);
                        progress_bar.setVisibility(View.VISIBLE);
                    }
                    dialog.dismiss();
                }
            }
        });

    }

    protected void optionDialog(final String[] fav_arr2, final View view, final String ll, final String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ReportsActivity.this);
        builder.setTitle("");
        builder.setItems(fav_arr2, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                if (item == 0) {
                    openDialog(text, view.getId());
                    dialog.dismiss();
                } else if (item == 1) {
                    delete(view.getId(), view, ll);
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void delete(final int pos, final View v, final String layout) {
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to delete this member?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        for (int i = 0; i < members_list.size(); i++) {
                            MemberModel list = members_list.get(i);
                            if (list.id.equals(String.valueOf(pos))) {
                                members_list.remove(i);
                                if (layout.equals("ll_add1")) {
                                    int ll_id = R.id.ll_add1;
                                    v.setId(ll_id);
                                } else if (layout.equals("ll_add2")) {
                                    int ll_id = R.id.ll_add2;
                                    v.setId(ll_id);
                                } else if (layout.equals("ll_add3")) {
                                    int ll_id = R.id.ll_add3;
                                    v.setId(ll_id);
                                }
                                String delete_url = "userfamilies/deleteApi/" + pos;
                                deleteMember(delete_url);
                                deleteMember();
                                return;
                            }
                        }
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    public void addMember(String url, final String name, final boolean isUpdate) {
        FetchData.getInstance(ReportsActivity.this).getRequestQueue().add(new StringRequest
                (Request.Method.POST, Urls.URL_TO_WEB + url,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                progress_bar.setVisibility(View.GONE);
                                Log.d("valRes", response);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String status = jsonObject.optString("status");
                                    if (status.equals("1")) {
                                        if (isUpdate) {
                                            JSONObject jobj = jsonObject.optJSONObject("data");
                                            for (int i = 0; i < members_list.size(); i++) {
                                                if (members_list.get(i).id.equals(jobj.optString("id"))) {
                                                    MemberModel memberModel = new MemberModel();
                                                    memberModel.id = jobj.optString("id");
                                                    memberModel.user_id = jobj.optString("user_id");
                                                    memberModel.name = jobj.optString("familymember");
                                                    members_list.set(i, memberModel);
                                                    if (i == 0) {
                                                        updateMember(Integer.parseInt(members_list.get(0).id), members_list.get(0).name, ll_add1, txt1, img1);
                                                    } else if (i == 1) {
                                                        updateMember(Integer.parseInt(members_list.get(1).id), members_list.get(1).name, ll_add2, txt2, img2);
                                                    } else if (i == 2) {
                                                        updateMember(Integer.parseInt(members_list.get(2).id), members_list.get(2).name, ll_add3, txt3, img3);
                                                    }
                                                }
                                            }
                                        } else {
                                            JSONObject jobj = jsonObject.optJSONObject("data");
                                            MemberModel memberModel = new MemberModel();
                                            memberModel.id = jobj.optString("id");
                                            memberModel.user_id = jobj.optString("user_id");
                                            memberModel.name = jobj.optString("familymember");
                                            members_list.add(memberModel);
                                            if (members_list.size() == 0) {
                                                setMember(0, null, ll_add1, txt1, img1);
                                            } else if (members_list.size() == 1) {
                                                setMember(Integer.parseInt(members_list.get(0).id), members_list.get(0).name, ll_add1, txt1, img1);
                                            } else if (members_list.size() == 2) {
                                                setMember(Integer.parseInt(members_list.get(1).id), members_list.get(1).name, ll_add2, txt2, img2);
                                            } else if (members_list.size() == 3) {
                                                setMember(Integer.parseInt(members_list.get(2).id), members_list.get(2).name, ll_add3, txt3, img3);
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", user_id);
                params.put("familymember", name);
                return params;
            }
        });
    }

    public void deleteMember(String url) {
        FetchData.getInstance(ReportsActivity.this).getRequestQueue().add(new StringRequest
                (Request.Method.POST, Urls.URL_TO_WEB + url,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                Log.d("valRes", response);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String status = jsonObject.optString("status");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                    }
                }) {
        });
    }

    public void getMembers(String url) {
        FetchData.getInstance(ReportsActivity.this).getRequestQueue().add(new StringRequest
                (Request.Method.GET, Urls.URL_TO_WEB + url,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                               // progress_bar.setVisibility(View.GONE);
                                progressDialog.dismiss();
                                Log.d("valRes", response);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String status = jsonObject.optString("status");
                                    if (status.equals("1")) {
                                        JSONArray data = jsonObject.optJSONArray("data");
                                        for (int i = 0; i < data.length(); i++) {
                                            JSONObject jobj = data.optJSONObject(i);
                                            MemberModel memberModel = new MemberModel();
                                            memberModel.id = jobj.optString("id");
                                            memberModel.user_id = jobj.optString("user_id");
                                            memberModel.name = jobj.optString("familymember");
                                            members.add(jobj.optString("familymember"));
                                            members_list.add(memberModel);
                                        }
                                        if (members_list.size() == 0) {
                                            setMember(0, null, ll_add1, txt1, img1);
                                        } else {
                                            for (int i = 0; i < members_list.size(); i++) {
                                                if (i == 0) {
                                                    setMember(Integer.parseInt(members_list.get(0).id), members_list.get(0).name, ll_add1, txt1, img1);
                                                } else if (i == 1) {
                                                    setMember(Integer.parseInt(members_list.get(1).id), members_list.get(1).name, ll_add2, txt2, img2);
                                                } else if (i == 2) {
                                                    setMember(Integer.parseInt(members_list.get(2).id), members_list.get(2).name, ll_add3, txt3, img3);
                                                }
                                            }
                                        }
                                    } else {
                                        setMember(0, null, ll_add1, txt1, img1);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                    }
                }) {
        });
    }

    public void setMember(int id, final String name, LinearLayout ll_view, final TextView text, final ImageView img) {
        ll_view.setVisibility(View.VISIBLE);

        if (id != 0) {
            if (ll_view.getId() == R.id.ll_add1) {
                ll_add2.setVisibility(View.VISIBLE);
                ll_add3.setVisibility(View.INVISIBLE);
                img2.setImageResource(R.drawable.ic_add);
                txt2.setText("");
            } else if (ll_view.getId() == R.id.ll_add2) {
                ll_add3.setVisibility(View.VISIBLE);
                img3.setImageResource(R.drawable.ic_add);
                txt3.setText("");
            } else {

            }
            ll_view.setId(id);
        }

        if (name != null) {
            text.setText(name);
            img.setImageResource(R.drawable.ic_members);
        }
    }

    public void updateMember(int id, final String name, LinearLayout ll_view, final TextView text, final ImageView img) {
        if (name != null) {
            text.setText(name);
            img.setImageResource(R.drawable.ic_members);
            ll_view.setId(id);
        }
    }

    public void deleteMember() {
        ll_add1.setVisibility(View.INVISIBLE);
        ll_add2.setVisibility(View.GONE);
        ll_add3.setVisibility(View.GONE);
        if (members_list.size() == 0) {
            ll_add1.setVisibility(View.VISIBLE);
            img1.setImageResource(R.drawable.ic_add);
            txt1.setText("");
        } else {
            for (int i = 0; i < members_list.size(); i++) {
                if (i == 0) {
                    ll_add1.setVisibility(View.VISIBLE);
                    ll_add2.setVisibility(View.VISIBLE);
                    ll_add3.setVisibility(View.INVISIBLE);
                    img1.setImageResource(R.drawable.ic_members);
                    img2.setImageResource(R.drawable.ic_add);
                    txt1.setText(members_list.get(0).name);
                    txt2.setText("");
                    ll_add1.setId(Integer.parseInt(members_list.get(0).id));
                } else if (i == 1) {
                    ll_add2.setVisibility(View.VISIBLE);
                    ll_add3.setVisibility(View.VISIBLE);
                    img2.setImageResource(R.drawable.ic_members);
                    img3.setImageResource(R.drawable.ic_add);
                    txt2.setText(members_list.get(1).name);
                    txt3.setText("");
                    ll_add2.setId(Integer.parseInt(members_list.get(1).id));
                } else {
                    ll_add3.setVisibility(View.VISIBLE);
                    img3.setImageResource(R.drawable.ic_add);
                    txt3.setText("");
                }
            }
        }
    }

    public void setToolbar() {
        setTitle("");
        toolbar = (Toolbar) findViewById(R.id.toolbar_image);
        setSupportActionBar(toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        ImageView ivImage = (ImageView) toolbar.findViewById(R.id.ivImage);
        ivImage.setImageResource(R.drawable.ic_my_report_white);
        mTitle.setText("My Reports");
        mTitle.setTypeface(Utils.setTypeface(this));
    }

    public void setNavDrawer() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        TextView tvCopyright = (TextView) findViewById(R.id.tvCopyright);
        tvCopyright.setTypeface(Utils.setTypeface(this));

        Menu m = navigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);
        }
        if (Helper.getCheck(sActivity).equals("doctor")){
            navigationView.getMenu().setGroupVisible(R.id.group_item5, false);
        }else {
            navigationView.getMenu().setGroupVisible(R.id.group_item5, true);
        }
    }

    private void applyFontToMenuItem(MenuItem mi) {
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", Utils.setTypeface(this)), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            Intent intent = new Intent(ReportsActivity.this, PreviewActivity.class);
            intent.putExtra("home", "yes");
            startActivity(intent);
        } else if (id == R.id.nav_my_favourites) {
            Intent intent = new Intent(ReportsActivity.this, BookedTestsActivity.class);
            startActivity(intent);
            Helper.setAddClass("addNo",sActivity);
        } else if (id == R.id.nav_Profile) {
            if (prefs.getString("role", null).equals("3")) {
                startActivity(new Intent(ReportsActivity.this, RegisterDoctor.class)
                        .putExtra("intent", "update"));
            } else {
                startActivity(new Intent(ReportsActivity.this, RegisterActivity.class)
                        .putExtra("intent", "update"));
            }
        } else if (id == R.id.nav_share_app) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    "Hey check out my app at: http://bit.ly/2xv0Zi8");
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        } else if (id == R.id.nav_register_lab) {
            startActivity(new Intent(ReportsActivity.this, RegisterUserToDoctor.class)
                    .putExtra("user_id", user_id));
//            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.elvektechnologies.com/Digilabs/"));
//            startActivity(browserIntent);
        } else if (id == R.id.nav_rate_app) {
            startActivity(new Intent(ReportsActivity.this,FeedbackActivity.class));
        } else if (id == R.id.nav_logout_app) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.clear();
            editor.commit();
            for (int i = 0; i < ActivityStack.activity
                    .size(); i++) {
                (ActivityStack.activity.elementAt(i))
                        .finish();
                Log.e("Activity size:" + ActivityStack.activity.size(), "finished...");
            }
            startActivity(new Intent(ReportsActivity.this, RegisterActivity.class).putExtra("intent", "register"));
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

