package com.sahir.websolutions.digilabs.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sahir.websolutions.digilabs.Adapter.LabListAdapter;
import com.sahir.websolutions.digilabs.HelperClass.Helper;
import com.sahir.websolutions.digilabs.R;
import com.sahir.websolutions.digilabs.objects.ComparatorDistance;
import com.sahir.websolutions.digilabs.objects.ComparatorPrice;
import com.sahir.websolutions.digilabs.objects.ComparatorRating;
import com.sahir.websolutions.digilabs.objects.LabModel;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class LabListActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, NavigationView.OnNavigationItemSelectedListener {
    TextView tvTestName, tvSort, tvShare, txt_preferred, txt_others;
    LinearLayout llLabSort, llLabShare, ll_list, ll_labs;
    ListView lvLab, lvLabPrefered;
    ArrayList<LabModel> arrayList = new ArrayList<>();
    ArrayList<LabModel> preferredList = new ArrayList<>();
    ArrayList<Helper> img_string;
    LabListAdapter labListAdapter, preferredListAdapter;
    Toolbar toolbar;
    GoogleApiClient mGoogleApiClient = null;
    String img1, img;
    Location mLastLocation, LResult = null;
    String testName = "Blood Test", star_rating, price_rating, prefered_lab_price, user_id;
    public static String test_id;
    TextView txt_no_reports, tvLabName, tvLabDistance, tvLabPrice, tvLabHomeCollection;
    LinearLayout ll_reports, ll_preferedLab;
    SharedPreferences prefs;
    ImageView ivLabImage, star1, star2, star3, star4, star5, rupee1, rupee2, rupee3, rupee4, rupee5, ivLabCall;
    ProgressBar progress_bar;
    ProgressDialog progressDialog;
    boolean isPreferred = false;
    ArrayList<String> tests_arr = new ArrayList<>();
    ArrayList<String> price_arr = new ArrayList<>();
    ArrayList<LabModel> hcChargList = new ArrayList();
    int sort = 0;
    String phone;
    Activity sActivity;

    ArrayList<String> arr_price = new ArrayList<String>();
    ArrayList<String> arr_dis = new ArrayList<String>();
    ArrayList<LabModel> timeList = new ArrayList();
    LocationManager locationManager;
    LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            LResult = location;
            calculateDistance();
            Log.e("Permission ", "No>>>" + LResult.getAltitude());
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lab_list);
        sActivity = LabListActivity.this;
        img_string = new ArrayList<Helper>();
        ll_preferedLab = (LinearLayout) findViewById(R.id.ll_preferedLab);
        init();

        setToolbar();

        setNavDrawer();

        setTvTypeface();

        getLocation();

        setData();

        onClicks();


    }

    private void setData() {
        if (getIntent().getStringExtra("type") != null) {
            txt_others.setText("LABS OFFERING THIS PACKAGE  ");
            getPackagesList(getIntent().getStringExtra("package_id"));
            tvTestName.setText(getIntent().getStringExtra("package_name"));
            testName = getIntent().getStringExtra("package_tests");
            test_id = getIntent().getStringExtra("package_id");
        } else {
            test_id = getIntent().getStringExtra("tests");
            Log.i("BookTest", "Test Selected" + test_id);
            testName = getIntent().getStringExtra("test_names");
            tvTestName.setText(testName);

            if (!Utils.isNetworkConnected(LabListActivity.this)) {
                Utils.conDialog(LabListActivity.this);
            } else {
                if (getIntent().getStringExtra("lab_type").equals("lab")) {
                    isPreferred = false;
                //    setPreferedLab();
                    txt_others.setText("My Preferred Lab  ");
                }
                getLabsList(test_id);//book12
            }
        }
    }

    private void init() {
        ActivityStack.activity.add(LabListActivity.this);
        progressDialog = new ProgressDialog(LabListActivity.this);
        progressDialog.setMessage("Fetching Labs....");
        progressDialog.show();
        tvTestName = (TextView) findViewById(R.id.tvTestName);
        tvTestName.setSelected(true);
        tvSort = (TextView) findViewById(R.id.tvSort);
        tvShare = (TextView) findViewById(R.id.tvShare);
        tvLabName = (TextView) findViewById(R.id.tvLabName);
        tvLabDistance = (TextView) findViewById(R.id.tvLabDistance);
        tvLabHomeCollection = (TextView) findViewById(R.id.tvLabHomeCollection);
        tvLabPrice = (TextView) findViewById(R.id.tvLabPrice);
        txt_preferred = (TextView) findViewById(R.id.txt_preferred);
        txt_others = (TextView) findViewById(R.id.txt_others);
        llLabSort = (LinearLayout) findViewById(R.id.llLabSort);
        llLabShare = (LinearLayout) findViewById(R.id.llLabShare);
        lvLab = (ListView) findViewById(R.id.lvLab);
        lvLabPrefered = (ListView) findViewById(R.id.lvLabPrefered);
        txt_no_reports = (TextView) findViewById(R.id.txt_no_reports);
        ll_reports = (LinearLayout) findViewById(R.id.ll_reports);
        ll_labs = (LinearLayout) findViewById(R.id.ll_labs);
        ll_list = (LinearLayout) findViewById(R.id.ll_list);
        ivLabImage = (ImageView) findViewById(R.id.ivLabImage);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        star1 = (ImageView) findViewById(R.id.star1);
        star2 = (ImageView) findViewById(R.id.star2);
        star3 = (ImageView) findViewById(R.id.star3);
        star4 = (ImageView) findViewById(R.id.star4);
        star5 = (ImageView) findViewById(R.id.star5);
        rupee1 = (ImageView) findViewById(R.id.rupee1);
        rupee2 = (ImageView) findViewById(R.id.rupee2);
        rupee3 = (ImageView) findViewById(R.id.rupee3);
        rupee4 = (ImageView) findViewById(R.id.rupee4);
        rupee5 = (ImageView) findViewById(R.id.rupee5);
        ivLabCall = (ImageView) findViewById(R.id.ivLabCall);

        prefs = getSharedPreferences(getString(R.string.user_details), MODE_PRIVATE);

        user_id = prefs.getString(getString(R.string.user_id), null);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        criteria.setAccuracy(2);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            locationManager.requestSingleUpdate(criteria, this.locationListener, null);
            locationManager.requestLocationUpdates("network", 0, 0.0f, this.locationListener);
            LResult = this.locationManager.getLastKnownLocation("network");
            Log.e("Permission ", "No>>>" + this.LResult.getAltitude());
            return;
        }

        getLstLocation();
    }

    private void onClicks() {
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });

        llLabSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSortDialog();
            }
        });
        llLabShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openShareIntent();
            }
        });
        lvLab.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                LabModel labModel = arrayList.get(position);
                phone = labModel.phone;
                ImageView call = (ImageView) view.findViewById(R.id.ivLabCall);
                call.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Build.VERSION.SDK_INT >= 23) {
                            AllowPermissions();
                        } else {
                            startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone)));
                        }
                    }
                });
            }
        });

        tvTestName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (getIntent().getStringExtra("type") != null) {
                        String[] testArr = testName.split(",");
                        showTests(testArr);
                    } else {
                        String[] testArr = testName.split(",");
                        String[] testIdArr = test_id.split(",");
                        boolean[] testSel = new boolean[testArr.length];
                        for (int i = 0; i < testArr.length; i++) {
                            testSel[i] = true;
                        }
                        editTests(testArr, testIdArr, testSel);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        ll_preferedLab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LabListActivity.this, LabDetailsActivity.class);
                intent.putExtra("lab_id", prefs.getString("lab_id", null));
                intent.putExtra("user_id", prefs.getString("user_id", null));
                intent.putExtra("image_url", Helper.getImage(sActivity));
                intent.putExtra("test_name", testName);
                intent.putExtra("test_price", prefered_lab_price);
                intent.putExtra("lab_image", prefs.getString("image", null));
                intent.putExtra("lab_name", prefs.getString("lab_name", null));
                intent.putExtra("opentime", prefs.getString("opentime", null));
                intent.putExtra("closetime", prefs.getString("closetime", null));
                intent.putExtra("working_days", prefs.getString("working_days", null));
                intent.putExtra("latitude", prefs.getString("lat", null));
                intent.putExtra("longitude", prefs.getString("lng", null));
                intent.putExtra("distance", prefs.getString("distance", null));
                intent.putExtra("homecollection", prefs.getString("homecollection", null));
                intent.putExtra("home_collection_price", prefs.getString("coll_charges", null));
                intent.putExtra("coll_charges", prefs.getString("hc_fix_price", null));
                intent.putExtra("lab_price", prefered_lab_price);
                intent.putExtra("lab_email", prefs.getString("email", null));
                intent.putExtra("lab_phone", prefs.getString("phone", null));
                intent.putExtra("timing", prefs.getString("timing", null));
                intent.putExtra("type", prefs.getString("type", null));
                startActivity(intent);
            }
        });

        ivLabCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.checkPermissionCall(LabListActivity.this)) {
                    String url = "tel:" + prefs.getString("phone", null);
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(url));
                    try {
                        if (ActivityCompat.checkSelfPermission(LabListActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;

                        }
                        startActivity(intent);
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(LabListActivity.this, "Activity not found exception!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    protected void showTests(final String[] fav_arr2) {
        AlertDialog.Builder builder = new AlertDialog.Builder(LabListActivity.this);
        builder.setTitle("");
        builder.setItems(fav_arr2, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void editTests(String[] tests, String[] testsId, final boolean[] checkedTests) {
        // Build an AlertDialog
        AlertDialog.Builder builder = new AlertDialog.Builder(LabListActivity.this);
        final ArrayList<String> testSelected = new ArrayList<>();
        final ArrayList<String> testId = new ArrayList<>();
        // Convert the color array to list
        final List<String> colorsList = Arrays.asList(tests);
        final List<String> idList = Arrays.asList(testsId);
        builder.setMultiChoiceItems(tests, checkedTests, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                // Update the current focused item's checked status
                checkedTests[which] = isChecked;
                // Get the current focused item
                String currentItem = colorsList.get(which);
                String currentId = idList.get(which);
//                // Notify the current action
//                Toast.makeText(getApplicationContext(),
//                        currentItem + " " + currentId, Toast.LENGTH_SHORT).show();
            }
        });

        // Specify the dialog is not cancelable
        builder.setCancelable(false);

        // Set a title for alert dialog
        builder.setTitle("Selected Tests");

        // Set the positive/yes button click listener
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button
                for (int i = 0; i < checkedTests.length; i++) {
                    boolean checked = checkedTests[i];
                    if (checked) {
                        testSelected.add(colorsList.get(i));
                        testId.add(idList.get(i));
                    }
                }
                if (testSelected.size() > 0) {
                    testName = android.text.TextUtils.join(",", testSelected);
                    tvTestName.setText(testName);
//                    progress_bar.setVisibility(View.VISIBLE);
                    progressDialog.setMessage("Updating Labs....");
                    progressDialog.show();
                    test_id = android.text.TextUtils.join(",", testId);
                    getLabsList(android.text.TextUtils.join(",", testId));

                } else {
                    Toast.makeText(getApplicationContext(), "Please select at least one test.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // Set the neutral/cancel button click listener
        builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the neutral button
            }
        });

        AlertDialog dialog = builder.create();
        // Display the alert dialog on interface
        dialog.show();
    }

    private void setPreferedLab() {
        if (prefs.getString("is_prefered", null) != null) {
            if (prefs.getString("is_prefered", null).equals("yes")) {
                ll_preferedLab.setVisibility(View.VISIBLE);
                lvLabPrefered.setVisibility(View.GONE);
                txt_preferred.setText("My Selected Lab");
                llLabSort.setVisibility(View.GONE);
                img = Helper.getImage(sActivity);//search image
                if (img == null) {
                    ivLabImage.setImageResource(R.drawable.digilabs);
                } else if (img.equals("null")) {
                    ivLabImage.setImageResource(R.drawable.digilabs);
                } else if (img.equals("")) {
                    ivLabImage.setImageResource(R.drawable.digilabs);
                } else {
                    Picasso.with(sActivity)
                            .load("http://path.digilabsonline.com//images/Labs/photo/" + img)
                            .into(ivLabImage);
                }

                tvLabName.setText(prefs.getString("lab_name", null));
                star_rating = prefs.getString("star_rating", null);
                price_rating = prefs.getString("price_rating", null);

                if (star_rating.equals("5")) {
                    star1.setImageResource(R.drawable.ic_star_filled);
                    star2.setImageResource(R.drawable.ic_star_filled);
                    star3.setImageResource(R.drawable.ic_star_filled);
                    star4.setImageResource(R.drawable.ic_star_filled);
                    star5.setImageResource(R.drawable.ic_star_filled);
                } else if (star_rating.equals("4")) {
                    star1.setImageResource(R.drawable.ic_star_filled);
                    star2.setImageResource(R.drawable.ic_star_filled);
                    star3.setImageResource(R.drawable.ic_star_filled);
                    star4.setImageResource(R.drawable.ic_star_filled);
                    star5.setImageResource(R.drawable.ic_star);
                } else if (star_rating.equals("3")) {
                    star1.setImageResource(R.drawable.ic_star_filled);
                    star2.setImageResource(R.drawable.ic_star_filled);
                    star3.setImageResource(R.drawable.ic_star_filled);
                    star4.setImageResource(R.drawable.ic_star);
                    star5.setImageResource(R.drawable.ic_star);
                } else if (star_rating.equals("2")) {
                    star1.setImageResource(R.drawable.ic_star_filled);
                    star2.setImageResource(R.drawable.ic_star_filled);
                    star3.setImageResource(R.drawable.ic_star);
                    star4.setImageResource(R.drawable.ic_star);
                    star5.setImageResource(R.drawable.ic_star);
                } else if (star_rating.equals("1")) {
                    star1.setImageResource(R.drawable.ic_star_filled);
                    star2.setImageResource(R.drawable.ic_star);
                    star3.setImageResource(R.drawable.ic_star);
                    star4.setImageResource(R.drawable.ic_star);
                    star5.setImageResource(R.drawable.ic_star);
                }

                if (price_rating.equals("5")) {
                    rupee1.setImageResource(R.drawable.ic_rupee);
                    rupee2.setImageResource(R.drawable.ic_rupee);
                    rupee3.setImageResource(R.drawable.ic_rupee);
                } else if (price_rating.equals("4")) {
                    rupee1.setImageResource(R.drawable.ic_rupee);
                    rupee2.setImageResource(R.drawable.ic_rupee);
                    rupee3.setImageResource(R.drawable.ic_rupee);
                } else if (price_rating.equals("3")) {
                    rupee1.setImageResource(R.drawable.ic_rupee);
                    rupee2.setImageResource(R.drawable.ic_rupee);
                    rupee3.setImageResource(R.drawable.ic_rupee);
                } else if (price_rating.equals("2")) {
                    rupee2.setImageResource(R.drawable.ic_rupee);
                    rupee3.setImageResource(R.drawable.ic_rupee);
                } else if (price_rating.equals("1")) {
                    rupee3.setImageResource(R.drawable.ic_rupee);
                }

                tvLabDistance.setText(prefs.getString("distance", null));
                tvLabHomeCollection.setText("Rs " + prefs.getString("hc_price", null));
            }
        }
    }

    public void setTvTypeface() {
        tvTestName.setTypeface(Utils.setTypeface(this));
        tvSort.setTypeface(Utils.setTypeface(this));
        tvShare.setTypeface(Utils.setTypeface(this));
        tvLabName.setTypeface(Utils.setTypeface(this));
        tvLabPrice.setTypeface(Utils.setTypeface(this));
        tvLabDistance.setTypeface(Utils.setTypeface(this));
        tvLabHomeCollection.setTypeface(Utils.setTypeface(this));
        txt_preferred.setTypeface(Utils.setTypeface(this));
        txt_others.setTypeface(Utils.setTypeface(this));
    }

    public void setToolbar() {
        setTitle("");
        toolbar = (Toolbar) findViewById(R.id.toolbar_image);
        setSupportActionBar(toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("Labs List");
        mTitle.setTypeface(Utils.setTypeface(this));
    }


    public void setNavDrawer() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        TextView tvCopyright = (TextView) findViewById(R.id.tvCopyright);
        tvCopyright.setTypeface(Utils.setTypeface(this));

        Menu m = navigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            @SuppressLint({"NewApi", "LocalSuppress"}) MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    @SuppressLint({"NewApi", "LocalSuppress"}) MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);
        }
        if (Helper.getCheck(sActivity).equals("doctor")) {
            navigationView.getMenu().setGroupVisible(R.id.group_item5, false);
        } else {
            navigationView.getMenu().setGroupVisible(R.id.group_item5, true);
        }
    }

    private void applyFontToMenuItem(MenuItem mi) {
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", Utils.setTypeface(this)), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void openShareIntent() {
        String text = "List of Diagnostic Labs" + "\n";
        if (prefs.getString("is_prefered", null) != null) {
            if (prefs.getString("is_prefered", null).equals("yes")) {
                text = text + "Lab Name: " + prefs.getString("lab_name", null) + "\n"
                        + "Phone no: " + prefs.getString("phone", null) + "\n"
                        + "Price: " + " Rs " + tvLabPrice.getText().toString() + "\n"
                        + "Rating: " + prefs.getString("star_rating", null) + "/5" + "\n"
                        + "Home Collection Charges: " + prefs.getString("hc_price", null) + "\n"
                        + "\n" + "Shared through Digi Labs";
            }
        } else {
            for (LabModel l : arrayList) {
                text = text + "Lab Name: " + l.labname + "\n"
                        + "Phone no: " + l.phone + "\n"
                        + "Price: " + " Rs" + "\n"
                        + "Rating: " + l.star_rating + "/5" + "\n"
                        + "Home Collection Charges: " + l.home_collection_price + "\n"
                        + "\n" + "Shared through Digi Labs";
            }
        }
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, text);
        intent.setType("text/plain");
        startActivity(Intent.createChooser(intent, "Share list via.."));

    }

    public void calculateDistance() {
        try {
            if (!isPreferred) {
                if (arrayList.size() > 0) {
                    for (int i = 0; i < arrayList.size(); i++) {
                        if (LResult == null) {
                            arrayList.get(i).setDistance(Float.valueOf(0.0f));
                        } else {
                            Location loc = new Location("locationA");
                            loc.setLatitude(Double.valueOf(arrayList.get(i).latitude));
                            loc.setLongitude(Double.valueOf(arrayList.get(i).longtitude));
                            float distance = LResult.distanceTo(loc);
                            arrayList.get(i).setDistance(distance / 1000);
                        }
                    }
                    Collections.sort(this.arrayList, new Comparator<LabModel>() {
                        public int compare(LabModel c1, LabModel c2) {
                            return c1.getDistance().compareTo(c1.getDistance());
                        }
                    });
                    labListAdapter.notifyDataSetChanged();
                }
                if (preferredList.size() > 0) {
                    for (int i = 0; i < preferredList.size(); i++) {
                        if (LResult == null) {
                            preferredList.get(i).setDistance(Float.valueOf(0.0f));
                        } else {
                            Location loc = new Location("locationA");
                            loc.setLatitude(Double.valueOf(preferredList.get(i).latitude));
                            loc.setLongitude(Double.valueOf(preferredList.get(i).longtitude));
                            float distance = LResult.distanceTo(loc);
                            preferredList.get(i).setDistance(distance / 1000);
                        }
                    }
                    Collections.sort(this.arrayList, new Comparator<LabModel>() {
                        public int compare(LabModel c1, LabModel c2) {
                            return c1.getDistance().compareTo(c1.getDistance());
                        }
                    });
                    preferredListAdapter.notifyDataSetChanged();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getLabsList(String tests) {
        if (Helper.getAddClass(sActivity).equals("addNo")) {

        } else {
            //asdf
            tests = tests + "&lab_id=" + getIntent().getStringExtra("lab_id1");
            Log.e("LabList UYrl",""+tests);
        }
        FetchData.getInstance(LabListActivity.this).getRequestQueue().add(new StringRequest
                (Request.Method.GET, Urls.URL_TO_WEB + "tests/getLabTest/?test_id=" + tests,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                Log.d("valRes", response);
                                progressDialog.dismiss();
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    Log.d("valRes", jsonObject.toString());
                                    String status = jsonObject.optString("status");
                                    JSONObject message = jsonObject.optJSONObject("message");
                                    String filepath = message.optString("filepath");
                                    if (status.equals("1")) {
                                        JSONArray data = jsonObject.getJSONArray("data");
                                        arrayList.clear();
                                        preferredList.clear();
                                        for (int i = 0; i < data.length(); i++) {
                                            JSONObject jobj = data.getJSONObject(i);
                                            JSONObject jobj1 = null;
                                            LabModel labModel = new LabModel();
                                            labModel.id = jobj.optString("lab_id");
                                            JSONArray tests = jobj.optJSONArray("labtest");
                                            tests_arr.clear();
                                            price_arr.clear();
                                            for (int j = 0; j < tests.length(); j++) {
                                                JSONArray jsonArray = tests.optJSONArray(j);
                                                for (int k = 0; k < jsonArray.length(); k++) {
                                                    JSONObject jsonObject1 = jsonArray.optJSONObject(k);
                                                    String price = jsonObject1.optString("price");
                                                    prefered_lab_price = price;
                                                    tvLabPrice.setText("Rs " + prefered_lab_price);//pri
                                                    JSONObject test = jsonObject1.optJSONObject("test");
                                                    String name = test.optString("name");
                                                    tests_arr.add(name);
                                                    price_arr.add("Rs " + price);
                                                    arr_price.add(price);
                                                }
                                            }
                                            labModel.test_name = android.text.TextUtils.join(",", tests_arr);
                                            labModel.test_price = android.text.TextUtils.join(",", price_arr);
                                            labModel.price = jobj.optString("price");
                                            if (prefs.getString("is_prefered", null) != null) {
                                                if (prefs.getString("lab_id", null).equals(jobj.optString("lab_id"))) {
                                                    prefered_lab_price = jobj.optString("price");
                                                    tvLabPrice.setText("Rs " + prefered_lab_price);//pri
                                                } else {
                                                    prefered_lab_price = jobj.optString("price");
                                                    tvLabPrice.setText("Rs " + prefered_lab_price);
                                                }
                                            }
                                            JSONObject lab = jobj.optJSONObject("lab");
                                            JSONArray jsonArray = lab.optJSONArray("discounts");
                                            for (int j = 0; j < jsonArray.length(); j++) {
                                                JSONObject jsonObject1 = jsonArray.getJSONObject(j);
                                                String value = jsonObject1.optString("value");
                                                String type = jsonObject1.optString("type");
                                                labModel.price_type1 = type;
                                                labModel.price_value1 = value;

                                            }
                                            JSONArray mHcCharges = lab.getJSONArray("hc_charges");
                                            for (int ia = 0; ia < mHcCharges.length(); ia++) {
                                                JSONObject mChargesObject = mHcCharges.getJSONObject(ia);
                                              LabModel  mLabModel = new LabModel();
                                                mLabModel.hcCharges=mChargesObject.optString("charges");
                                                mLabModel.hcDistance=mChargesObject.optString("distance");
                                                labModel.hcList.add(mLabModel);
                                                hcChargList.add(mLabModel);
                                            }
                                            JSONObject lab1 = lab.optJSONObject("0");
                                            //  for (int j = 0; j < lab.length(); j++) {
                                            // jobj1 = lab.optJSONObject(String.valueOf(j));
                                            labModel.user_id = lab1.optString("user_id");
                                            labModel.labname = lab1.optString("labname");
                                            labModel.email = lab1.optString("email");
                                            labModel.address = lab1.optString("address");
                                            labModel.city = lab1.optString("city");
                                            labModel.state = lab1.optString("state");
                                            labModel.phone = lab1.optString("phone");
                                            labModel.homecollection = lab1.optString("homecollection");
                                            labModel.hc_type= lab1.optString("hc_type");
                                            labModel.home_collection_price = lab1.optString("hcprice");
                                            labModel.star_rating = lab1.optString("starrating");
                                            labModel.timing = lab1.optString("timing");
                                            Log.d("<<<<<<<<TIminmg>>>>>>>>.", "" + labModel.timing);
                                            labModel.price_rating = lab1.optString("pricerating");
//                                          labModel.lab_image = jobj1.optString("filepath") + jobj1.optString("filename");
                                            labModel.lab_image = filepath + lab1.optString("filename");
                                            img1 = lab1.optString("filename");
                                            Helper helper = new Helper(img1);
                                            img_string.add(helper);
                                            labModel.latitude = lab1.optString("latitude");
                                            labModel.longtitude = lab1.optString("longitude");
                                            labModel.lab_image = lab1.optString("image");
                                            labModel.working_days = lab1.optString("workingdays");
                                            labModel.convenience = lab1.optString("convenience");
                                            labModel.conven_type = lab1.optString("convenience_type");
                                            labModel.isprefered = lab1.optString("isprefered");
                                            labModel.opentime = lab1.optString("opentime");

                                            labModel.closetime = lab1.optString("closetime");
                                            try {
                                            JSONArray mTimeArray = lab1.getJSONArray("time");

                                                for (int ia1 = 0; ia1 < mTimeArray.length(); ia1++) {
                                                    JSONObject mTimeObject = mTimeArray.getJSONObject(ia1);
                                                    LabModel mLabModel = new LabModel();
                                                    mLabModel.working_days = mTimeObject.optString("day");
                                                    mLabModel.opentime = mTimeObject.optString("open");
                                                    mLabModel.closetime = mTimeObject.optString("close");
                                                    labModel.timeList.add(mLabModel);
                                                    timeList.add(mLabModel);
                                                }
                                            }catch (Exception e){
                                                e.printStackTrace();
                                            }
                                            labModel.type = "test";
                                            //}
                                            if (lab1.optString("isprefered") != null) {
                                                if (lab1.optString("isprefered").equals("yes")) {
                                                    preferredList.add(labModel);
                                                } else {
                                                    arrayList.add(labModel);
                                                }
                                            } else {
                                                arrayList.add(labModel);
                                            }
                                        }
                                        if (!isPreferred) {
                                            if (preferredList.size() > 0) {
                                                ll_preferedLab.setVisibility(View.VISIBLE);
                                                lvLabPrefered.setVisibility(View.VISIBLE);
                                                ll_list.setVisibility(View.GONE);
                                                txt_no_reports.setVisibility(View.GONE);
                                                preferredListAdapter = new LabListAdapter(LabListActivity.this, preferredList, img_string,"LabList");
                                                lvLabPrefered.setAdapter(preferredListAdapter);
                                            } else {
                                                if (prefs.getString("is_prefered", null) != null) {
                                                    if (prefs.getString("is_prefered", null).equals("yes")) {
                                                        ll_preferedLab.setVisibility(View.VISIBLE);
                                                        lvLabPrefered.setVisibility(View.GONE);//asdfg
                                                        check();
                                                    }
                                                } else {
                                                    ll_labs.setVisibility(View.GONE);
                                                    ll_preferedLab.setVisibility(View.GONE);
                                                    txt_no_reports.setVisibility(View.VISIBLE);
                                                }
                                            }
                                            if (arrayList.size() > 0) {
                                                ll_labs.setVisibility(View.VISIBLE);
                                                txt_no_reports.setVisibility(View.GONE);
                                                labListAdapter = new LabListAdapter(LabListActivity.this, arrayList, img_string,"LabList");
                                                lvLab.setAdapter(labListAdapter);
                                                lvLabPrefered.setVisibility(View.GONE);
                                                ll_preferedLab.setVisibility(View.GONE);
                                                setListViewHeightBasedOnChildren(lvLab);
                                            } else {
                                                if (prefs.getString("is_prefered", null) != null) {
                                                    if (prefs.getString("is_prefered", "").equals("yes")) {
                                                        ll_preferedLab.setVisibility(View.VISIBLE);
                                                        lvLabPrefered.setVisibility(View.GONE);
                                                    }
                                                } else {
                                                    ll_labs.setVisibility(View.GONE);
                                                    ll_preferedLab.setVisibility(View.GONE);
                                                    txt_no_reports.setVisibility(View.VISIBLE);
                                                }
                                            }
                                            calculateDistance();
                                        }
                                    } else {
                                        ll_labs.setVisibility(View.GONE);
                                        ll_preferedLab.setVisibility(View.GONE);
                                        txt_no_reports.setVisibility(View.VISIBLE);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener()

                {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                    }
                }) {
        });
    }

    public void getPackagesList(final String package_id) {
        Log.e("HeAlth Package url",""+Urls.URL_TO_WEB + "packagetolab/getAllLabByPackages/" + package_id);
        FetchData.getInstance(LabListActivity.this).getRequestQueue().add(new StringRequest
                (Request.Method.GET, Urls.URL_TO_WEB + "packagetolab/getAllLabByPackages/" + package_id,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                Log.d("valRes", response);
                                progressDialog.dismiss();
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String status = jsonObject.optString("status");
                                    JSONObject message = jsonObject.optJSONObject("message");
                                    String filepath = message.optString("filepath");
                                    if (status.equals("1")) {
                                        JSONArray data = jsonObject.getJSONArray("data");
                                        arrayList.clear();
                                        preferredList.clear();
                                        for (int i = 0; i < data.length(); i++) {
                                            JSONObject jobj = data.getJSONObject(i);
                                            JSONObject jobj1 = null;
                                            LabModel labModel = new LabModel();
                                            labModel.test_name = testName;
                                            labModel.id = jobj.optString("lab_id");
                                            labModel.price = jobj.optString("cost");
                                            String pri = jobj.optString("cost");
                                            arr_price.add(pri);
                                            jobj1 = jobj.optJSONObject("lab");
                                            JSONArray jsonArray = jobj1.optJSONArray("discounts");
                                            for (int j = 0; j < jsonArray.length(); j++) {
                                                JSONObject jsonObject1 = jsonArray.getJSONObject(j);
                                                String value = jsonObject1.optString("value");
                                                String type = jsonObject1.optString("type");
                                                labModel.price_type1 = type;
                                                labModel.price_value1 = value;

                                            }
//                                            JSONArray lab = jobj.optJSONArray("lab");
//                                            for (int j = 0; j < lab.length(); j++) {
//                                                jobj1 = lab.optJSONObject(j);
                                            labModel.user_id = jobj1.optString("user_id");
                                            labModel.labname = jobj1.optString("labname");
                                            labModel.email = jobj1.optString("email");
                                            labModel.address = jobj1.optString("address");
                                            labModel.city = jobj1.optString("city");
                                            labModel.state = jobj1.optString("state");
                                            labModel.phone = jobj1.optString("phone");
                                            labModel.timing = jobj1.optString("timing");
                                            labModel.homecollection = jobj1.optString("homecollection");
                                            labModel.home_price_fix = jobj1.optString("coll_charges");
                                            labModel.home_collection_price = jobj1.optString("hcprice");

                                            labModel.hc_type=jobj1.optString("hc_type");
                                            labModel.star_rating = jobj1.optString("starrating");
                                            labModel.price_rating = jobj1.optString("pricerating");
                                            String img1 = jobj1.optString("filename");
                                            Helper helper = new Helper(img1);
                                            img_string.add(helper);
//                                                labModel.lab_image = jobj1.optString("filepath") + jobj1.optString("filename");
                                            labModel.lab_image = filepath + jobj1.optString("filename");
                                            labModel.latitude = jobj1.optString("latitude");
                                            labModel.longtitude = jobj1.optString("longitude");
                                            labModel.lab_image = jobj1.optString("image");
                                            labModel.working_days = jobj1.optString("workingdays");
                                            labModel.isprefered = jobj1.optString("isprefered");
                                            labModel.convenience = jobj1.optString("convenience");
                                            labModel.conven_type = jobj1.optString("convenience_type");
                                            labModel.opentime = jobj1.optString("opentime");
                                            labModel.closetime = jobj1.optString("closetime");
                                            labModel.type = "package";
//                                            }
                                            JSONArray mHcCharges = jobj.getJSONArray("hc_charges");
                                            for (int ia = 0; ia < mHcCharges.length(); ia++) {
                                                JSONObject mChargesObject = mHcCharges.getJSONObject(ia);
                                                LabModel  mLabModel = new LabModel();
                                                mLabModel.hcCharges=mChargesObject.optString("charges");
                                                mLabModel.hcDistance=mChargesObject.optString("distance");
                                                labModel.hcList.add(mLabModel);
                                                hcChargList.add(mLabModel);
                                            }
                                            if (jobj1.optString("isprefered") != null) {
                                                if (jobj1.optString("isprefered").equals("yes")) {
                                                    preferredList.add(labModel);
                                                } else {
                                                    arrayList.add(labModel);
                                                }
                                            } else {
                                                arrayList.add(labModel);
                                            }
                                        }
                                        if (preferredList.size() > 0) {
                                            ll_preferedLab.setVisibility(View.VISIBLE);
                                            lvLabPrefered.setVisibility(View.VISIBLE);
                                            ll_list.setVisibility(View.GONE);
                                            txt_no_reports.setVisibility(View.GONE);
                                            preferredListAdapter = new LabListAdapter(LabListActivity.this, preferredList, img_string,"LabList");
                                            lvLabPrefered.setAdapter(preferredListAdapter);
                                        }
                                        if (arrayList.size() > 0) {
                                            ll_labs.setVisibility(View.VISIBLE);
                                            txt_no_reports.setVisibility(View.GONE);
                                            labListAdapter = new LabListAdapter(LabListActivity.this, arrayList, img_string,"LabList");
                                            lvLab.setAdapter(labListAdapter);
                                            setListViewHeightBasedOnChildren(lvLab);
                                        } else {
                                            ll_labs.setVisibility(View.GONE);
                                            lvLab.setVisibility(View.GONE);
                                            txt_no_reports.setVisibility(View.VISIBLE);
                                        }
                                        calculateDistance();
                                    } else {
                                        ll_labs.setVisibility(View.GONE);
                                        lvLab.setVisibility(View.GONE);
                                        txt_no_reports.setVisibility(View.VISIBLE);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener()

                {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                    }
                })

        {
        });
    }


    public void openSortDialog() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_lab_sort);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        if (height >= 800) {
            lp.height = height / 2;
        } else if (height >= 1280) {
            lp.height = height / 2;
        } else if (height >= 1920) {
            lp.height = height / 2;
        } else if (height >= 2560) {
            lp.height = height / 2;
        }
        dialog.getWindow().setAttributes(lp);

        dialog.show();

        TextView tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);
        TextView tvNext = (TextView) dialog.findViewById(R.id.tvNext);
        final RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.radioGroup);

        if (sort == 0) {
            ((RadioButton) radioGroup.getChildAt(0)).setChecked(true);
        } else if (sort == 1) {
            ((RadioButton) radioGroup.getChildAt(1)).setChecked(true);
        } else {
            ((RadioButton) radioGroup.getChildAt(2)).setChecked(true);
        }
        sortData(dialog);
        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int radioButtonId = radioGroup.getCheckedRadioButtonId();
                RadioButton radioButton = (RadioButton) dialog.findViewById(radioButtonId);
                String name = radioButton.getText().toString();
                switch (name) {
                    case "Rating (High to Low)":
                        if (arrayList.size() > 0) {
                            Collections.sort(arrayList, new ComparatorRating());
                            Collections.reverse(arrayList);
                            labListAdapter.notifyDataSetChanged();
                            sort = 0;

                        }
                        break;
                    case "Price (Low to High)":
                        if (arrayList.size() > 0) {
                            Collections.sort(arrayList, new ComparatorPrice());
//                        Collections.reverse(arrayList);
                            labListAdapter.notifyDataSetChanged();
                            sort = 1;
                        }
                        break;
                    case "Distance (Lesser to Greater)":
                        if (arrayList.size() > 0) {
                            Collections.sort(arrayList, new ComparatorDistance());
//                            Collections.reverse(arrayList);
                            labListAdapter.notifyDataSetChanged();
                            sort = 2;

                        }
                        break;
                }
                dialog.dismiss();
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    public void sortData(Dialog dialog) {
        RadioButton rbRating = (RadioButton) dialog.findViewById(R.id.rbRating);
        String s = "Rating (High to Low)";
        SpannableString ss1 = new SpannableString(s);
        ss1.setSpan(new RelativeSizeSpan(1.1f), 0, 6, 0); // set size
        ss1.setSpan(new RelativeSizeSpan(0.7f), 7, 20, 0);
        rbRating.setText(ss1);

        RadioButton rbPrice = (RadioButton) dialog.findViewById(R.id.rbPrice);
        String s1 = "Price (Low to High)";
        SpannableString ss11 = new SpannableString(s1);
        ss11.setSpan(new RelativeSizeSpan(1.1f), 0, 6, 0); // set size
        ss11.setSpan(new RelativeSizeSpan(0.7f), 6, 19, 0);
        rbPrice.setText(ss11);

        RadioButton rbDistance = (RadioButton) dialog.findViewById(R.id.rbDistance);
        String s11 = "Distance (Lesser to Greater)";
        SpannableString ss111 = new SpannableString(s11);
        ss111.setSpan(new RelativeSizeSpan(1.1f), 0, 8, 0); // set size
        ss111.setSpan(new RelativeSizeSpan(0.7f), 8, 28, 0);
        rbDistance.setText(ss111);
    }

    public void getLocation() {
        if (Utils.checkPermissionLocation(this)) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        } else {
            LResult = null;
            calculateDistance();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {

        LocationRequest locationRequest = LocationRequest.create()
                .setNumUpdates(1)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(0);


        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Please Wait..");
        pd.show();
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, new LocationCallback() {

            @Override
            public void onLocationResult(LocationResult result) {
                pd.dismiss();
                Log.d("result123", result.toString());

                LResult = result.getLastLocation();
                calculateDistance();

//                Toast.makeText(LabListActivity.this, result.getLastLocation().getLatitude() + "", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLocationAvailability(LocationAvailability locationAvailability) {

                //Log.d("available123",locationAvailability.toString());
                //Toast.makeText(LabListActivity.this, String.valueOf(locationAvailability.isLocationAvailable()), Toast.LENGTH_SHORT).show();
                pd.dismiss();

                if (!locationAvailability.isLocationAvailable()) {

                    if (ActivityCompat.checkSelfPermission(LabListActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(LabListActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                    if (mLastLocation != null) {

                        LResult = mLastLocation;
                        calculateDistance();
//                        Toast.makeText(LabListActivity.this, "Retrieved Last location", Toast.LENGTH_SHORT).show();

                    } else {
                        LResult = null;
                        calculateDistance();
                        Snackbar.make(lvLab, "Turn on GPS to calculate distance", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    }
                }
            }
        }, null);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
        super.onStop();
    }

    private void AllowPermissions() {
        int hasSMSPermission = ActivityCompat.checkSelfPermission(LabListActivity.this, android.Manifest.permission.CALL_PHONE);

        List<String> permissions = new ArrayList<String>();

        if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(android.Manifest.permission.CALL_PHONE);
        }

        if (!permissions.isEmpty()) {
            ActivityCompat.requestPermissions(LabListActivity.this, permissions.toArray(new String[permissions.size()]), 101);
        } else {
            startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone)));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utils.MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocation();
                } else {
                    //code for deny
                }
                break;
            }
            case 101: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone)));
                        return;
                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i]);
//                        finish();
                    }
                }
            }

        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            Intent intent = new Intent(LabListActivity.this, PreviewActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_my_favourites) {
            Intent intent = new Intent(LabListActivity.this, BookedTestsActivity.class);
            startActivity(intent);
            Helper.setAddClass("addNo", sActivity);
        } else if (id == R.id.nav_Profile) {
            if (prefs.getString("role", null).equals("3")) {
                startActivity(new Intent(LabListActivity.this, RegisterDoctor.class)
                        .putExtra("intent", "update"));
            } else {
                startActivity(new Intent(LabListActivity.this, RegisterActivity.class)
                        .putExtra("intent", "update"));
            }
        } else if (id == R.id.nav_share_app) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    "Hey check out my app at: http://bit.ly/2xv0Zi8");
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        } else if (id == R.id.nav_register_lab) {
            startActivity(new Intent(LabListActivity.this, RegisterUserToDoctor.class)
                    .putExtra("user_id", user_id));
//            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.elvektechnologies.com/Digilabs/"));
//            startActivity(browserIntent);
        } else if (id == R.id.nav_rate_app) {
            startActivity(new Intent(LabListActivity.this, FeedbackActivity.class));
        } else if (id == R.id.nav_logout_app) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.clear();
            editor.commit();
            for (int i = 0; i < ActivityStack.activity
                    .size(); i++) {
                (ActivityStack.activity.elementAt(i))
                        .finish();
                Log.e("Activity size:" + ActivityStack.activity.size(), "finished...");
            }
            startActivity(new Intent(LabListActivity.this, RegisterActivity.class).putExtra("intent", "register"));
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LinearLayout.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    void check() {
        if (Helper.getAddClass(sActivity).equals("addNo")) {
            ll_preferedLab.setVisibility(View.GONE);
        } else {
            ll_preferedLab.setVisibility(View.VISIBLE);
        }
    }
    private void getLstLocation() {
        String mgr = this.locationManager.getBestProvider(new Criteria(), true);
        if (ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION") == 0 || ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            this.LResult = this.locationManager.getLastKnownLocation("network");
            calculateDistance();
        }
    }
}