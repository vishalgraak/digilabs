package com.sahir.websolutions.digilabs.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sahir.websolutions.digilabs.Adapter.HealthListAdapter;
import com.sahir.websolutions.digilabs.Adapter.HealthTipsAdapter;
import com.sahir.websolutions.digilabs.HelperClass.Helper;
import com.sahir.websolutions.digilabs.R;
import com.sahir.websolutions.digilabs.objects.HealthModel;
import com.sahir.websolutions.digilabs.objects.TestModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by safal bhatia on 7/12/2017.
 */

public class HealthPackagesActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    Toolbar toolbar;
    TextView txt_no_package,txt_health;
    EditText etSearch;
    ListView lvPackages;
    ArrayList<HealthModel> healthModelArrayList = new ArrayList<>();
    HealthListAdapter healthListAdapter;
    ProgressDialog progressDialog;
    LinearLayout ll_package;
    SharedPreferences prefs;
    String user_id;
    RecyclerView tips_recycler_view;
    ArrayList<String> tips_list = new ArrayList<>();
    HealthTipsAdapter healthTipsAdapter;
    ImageView img_left, img_right;
    LinearLayoutManager layoutManager;
    Activity sActivity;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_heatlh_packges);
        sActivity = HealthPackagesActivity.this;
        init();

        setToolbar();

        setNavDrawer();

        onClicks();

        if (!Utils.isNetworkConnected(HealthPackagesActivity.this)) {
            Utils.conDialog(HealthPackagesActivity.this);
        } else {
            getHealthTips();
            getHealthPackages();
        }

    }

    private void onClicks() {
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (etSearch.getText().toString().equals("")) {
                    displayList();
                } else
                    searchList();
            }
        });

        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });

        img_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutManager.scrollToPosition(layoutManager.findFirstVisibleItemPosition() - 1);
            }
        });

        img_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutManager.scrollToPosition(layoutManager.findLastVisibleItemPosition() + 1);
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tips_recycler_view.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (layoutManager.findFirstVisibleItemPosition() == 0) {
                        img_left.setVisibility(View.GONE);
                    } else if (layoutManager.findLastVisibleItemPosition() == tips_list.size() - 1) {
                        img_right.setVisibility(View.GONE);
                    } else {
                        img_left.setVisibility(View.VISIBLE);
                        img_right.setVisibility(View.VISIBLE);
                    }
                }
            });
        }
    }

    public void searchList() {
        ArrayList<HealthModel> searchArray = new ArrayList<>();
        String searchChar = etSearch.getText().toString().trim();
        for (int i = 0; i < healthModelArrayList.size(); i++) {
            if (healthModelArrayList.get(i).package_name.toLowerCase().contains(searchChar.toLowerCase())) {
                searchArray.add(healthModelArrayList.get(i));
            }
        }
        healthListAdapter = new HealthListAdapter(this, searchArray);
        lvPackages.setAdapter(healthListAdapter);
    }

    private void getHealthPackages() {
        FetchData.getInstance(HealthPackagesActivity.this).getRequestQueue().add(new StringRequest
                (Request.Method.GET, Urls.URL_TO_WEB + "packages/getAllPackage",
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                progressDialog.dismiss();
                                final String response2 = response;
                                Log.i("valRes", "" + response2);
                                try {
                                    JSONObject json = new JSONObject(response2);
                                    int status = json.getInt("status");
                                    if (status == 1) {
                                        healthModelArrayList.clear();
                                        JSONArray data = json.optJSONArray("data");
                                        if (data != null) {
                                            for (int i = 0; i < data.length(); i++) {
                                                JSONObject jobj = data.optJSONObject(i);
                                                HealthModel healthModel = new HealthModel();
                                                healthModel.package_id = jobj.optString("package_id");
                                                healthModel.package_name = jobj.optString("package");
                                                healthModel.package_icon = jobj.optString("icon");
                                                healthModel.desc = jobj.optString("description");
                                                JSONArray tests = jobj.optJSONArray("test");
                                                if (tests != null) {
                                                    for (int j = 0; j < tests.length(); j++) {
                                                        JSONObject test = tests.optJSONObject(j);
                                                        TestModel testModel = new TestModel();
                                                        testModel.cat_id = test.optString("test_id");
                                                        testModel.cat_name = test.optString("name");
                                                        healthModel.tests.add(testModel);
                                                    }
                                                }
                                                healthModelArrayList.add(healthModel);
                                            }
                                            displayList();
                                        }
                                    } else {
                                        txt_no_package.setVisibility(View.VISIBLE);
                                        ll_package.setVisibility(View.GONE);
                                    }
                                } catch (JSONException e) {
                                    progressDialog.dismiss();
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("CAT", error.toString());
                    }
                }) {
        });
    }

    private void getHealthTips() {
        FetchData.getInstance(HealthPackagesActivity.this).getRequestQueue().add(new StringRequest
                (Request.Method.GET, Urls.URL_TO_WEB + "healthtips/viewapi",
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                final String response2 = response;
                                Log.i("valRes", "" + response2);
                                try {
                                    JSONObject json = new JSONObject(response2);
                                    int status = json.getInt("status");
                                    if (status == 1) {
                                        JSONArray data = json.optJSONArray("data");
                                        for (int i = 0; i < data.length(); i++) {
                                            JSONObject jsonObject = data.getJSONObject(i);
                                            String id = jsonObject.optString("id");
                                            String title = jsonObject.optString("title");
                                            String description = jsonObject.optString("description");
                                            tips_list.add(description);
                                        }
                                        if (tips_list.size() == 1) {
                                            img_left.setVisibility(View.GONE);
                                            img_right.setVisibility(View.GONE);
                                        }
                                        healthTipsAdapter = new HealthTipsAdapter(HealthPackagesActivity.this, tips_list);
                                        tips_recycler_view.setAdapter(healthTipsAdapter);
                                    } else {
                                        tips_list.add("Regular Exercise can help you reach or maintain a healthy weight. Exercise also cuts stress, control blood pressure, cholestrol and triglyceride level. Get at least 30 mins a day of aerobics exercise 5 days a week.");
                                        healthTipsAdapter = new HealthTipsAdapter(HealthPackagesActivity.this, tips_list);
                                        tips_recycler_view.setAdapter(healthTipsAdapter);
                                        img_left.setVisibility(View.GONE);
                                        img_right.setVisibility(View.GONE);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                    }
                }) {
        });
    }

    public void displayList() {
        txt_no_package.setVisibility(View.GONE);
        ll_package.setVisibility(View.VISIBLE);
        healthListAdapter = new HealthListAdapter(HealthPackagesActivity.this, healthModelArrayList);
        lvPackages.setAdapter(healthListAdapter);
    }

    private void init() {
        prefs = getSharedPreferences(getString(R.string.user_details), Context.MODE_PRIVATE);
        txt_no_package = (TextView) findViewById(R.id.txt_no_package);
        txt_health = (TextView) findViewById(R.id.txt_health);
        txt_no_package.setTypeface(Utils.setTypeface(this));
        txt_health.setTypeface(Utils.setTypeface(this));
        etSearch = (EditText) findViewById(R.id.etSearch);
        lvPackages = (ListView) findViewById(R.id.lvPackages);
        ll_package = (LinearLayout) findViewById(R.id.ll_package);
        user_id = prefs.getString(getString(R.string.user_id), null);
        tips_recycler_view = (RecyclerView) findViewById(R.id.tips_recycler_view);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        tips_recycler_view.setLayoutManager(layoutManager);
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(tips_recycler_view);
        img_left = (ImageView) findViewById(R.id.img_left);
        img_right = (ImageView) findViewById(R.id.img_right);
        img_left.setRotation(img_left.getRotation() + 180);
    }

    public void setToolbar() {
        prefs = getSharedPreferences(getString(R.string.user_details), Context.MODE_PRIVATE);
        progressDialog = new ProgressDialog(HealthPackagesActivity.this);
        progressDialog.setMessage("Fetching Packages...");
        progressDialog.show();
        setTitle("");
        toolbar = (Toolbar) findViewById(R.id.toolbar_image);
        setSupportActionBar(toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        ImageView ivImage = (ImageView) toolbar.findViewById(R.id.ivImage);
        ivImage.setImageResource(R.drawable.ic_health_white);
        mTitle.setText("Health Packages");
    }

    public void setNavDrawer() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        if (Helper.getCheck(sActivity).equals("doctor")){
            navigationView.getMenu().setGroupVisible(R.id.group_item5, false);
        }else {
            navigationView.getMenu().setGroupVisible(R.id.group_item5, true);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            Intent intent = new Intent(HealthPackagesActivity.this, PreviewActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_my_favourites) {
            Intent intent = new Intent(HealthPackagesActivity.this, BookedTestsActivity.class);
            startActivity(intent);
            Helper.setAddClass("addNo",sActivity);

        } else if (id == R.id.nav_Profile) {
            if (prefs.getString("role", null).equals("3")) {
                startActivity(new Intent(HealthPackagesActivity.this, RegisterDoctor.class)
                        .putExtra("intent", "update"));
            } else {
                startActivity(new Intent(HealthPackagesActivity.this, RegisterActivity.class)
                        .putExtra("intent", "update"));
            }
        } else if (id == R.id.nav_share_app) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    "Hey check out my app at: http://bit.ly/2xv0Zi8");
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        } else if (id == R.id.nav_register_lab) {
            startActivity(new Intent(HealthPackagesActivity.this, RegisterUserToDoctor.class)
                    .putExtra("user_id", user_id));
//            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.elvektechnologies.com/Digilabs/"));
//            startActivity(browserIntent);
        } else if (id == R.id.nav_rate_app) {
            startActivity(new Intent(HealthPackagesActivity.this, FeedbackActivity.class));
        }else if (id == R.id.nav_logout_app) {
            if (prefs.getString(getString(R.string.user_id), null) == null) {
                startActivity(new Intent(HealthPackagesActivity.this, RegisterActivity.class).putExtra("intent", "register"));
                finish();
            } else {
                SharedPreferences.Editor editor = prefs.edit();
                editor.clear();
                editor.commit();
                for (int i = 0; i < ActivityStack.activity
                        .size(); i++) {
                    (ActivityStack.activity.elementAt(i))
                            .finish();
                    Log.e("Activity size:" + ActivityStack.activity.size(), "finished...");
                }
                Intent intent = new Intent(HealthPackagesActivity.this, RegisterActivity.class);
                intent.putExtra("intent", "register");
                startActivity(intent);
                finish();
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
