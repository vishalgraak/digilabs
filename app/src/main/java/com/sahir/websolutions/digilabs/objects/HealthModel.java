package com.sahir.websolutions.digilabs.objects;

import java.util.ArrayList;

/**
 * Created by Sys9 on 7/15/2017.
 */

public class HealthModel {
    public String package_id;
    public String package_name;
    public String package_icon;
    public String desc;
    public ArrayList<TestModel> tests = new ArrayList<>();
}
