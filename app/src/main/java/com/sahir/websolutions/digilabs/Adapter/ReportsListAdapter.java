package com.sahir.websolutions.digilabs.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sahir.websolutions.digilabs.Activity.AddReportActivity;
import com.sahir.websolutions.digilabs.Activity.FetchData;
import com.sahir.websolutions.digilabs.Activity.ViewReportActivity;
import com.sahir.websolutions.digilabs.R;
import com.sahir.websolutions.digilabs.Activity.ShareWithDoctor;
import com.sahir.websolutions.digilabs.Activity.Urls;
import com.sahir.websolutions.digilabs.Activity.Utils;
import com.sahir.websolutions.digilabs.objects.ComparatorDate;
import com.sahir.websolutions.digilabs.objects.Report;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Map;

import static com.sahir.websolutions.digilabs.Activity.LocalReportActivity.intentType;
import static com.sahir.websolutions.digilabs.Activity.LocalReportActivity.member_id;
import static com.sahir.websolutions.digilabs.Activity.LocalReportActivity.member_list;
import static com.sahir.websolutions.digilabs.Activity.LocalReportActivity.members_list;

/**
 * Created by SWS-PC10 on 5/25/2017.
 */

public class ReportsListAdapter extends ArrayAdapter<Report> {
    Context context;
    ArrayList<Report> arrayListReport;
    LinearLayout ll_list;
    ImageView ivStar, ivShare;
    Report report;
    TextView tvName, tvDate;
    int check;      //adapter for local report-1 or myReport-0;
    boolean starList;       //true - to show stared list
    String report_id, star,check_lab_or_mytest,my_report;
    String[] arr = new String[3];
    int pos;
    ProgressDialog progressDialog;
    int a1=2;
    public ReportsListAdapter(final Context context, ArrayList<Report> data, int check, boolean starList) {
        super(context, R.layout.report_list_row, data);

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Processing...");
        this.context = context;
        this.arrayListReport = data;
        this.check = check;
        this.starList = starList;
        arr[0] = "Rename";
        arr[1] = "Edit";
        arr[2] = "Delete";

        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                ((Activity) context).finish();
            }
        });
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        report = arrayListReport.get(position);
        check_lab_or_mytest  = report.getLab_report();

        try{
        if(a1==2){
            String a[] = check_lab_or_mytest.split(",");
            String lab_or_my_id = a[0];
            String diff = a[1];
            if (diff.equals("a")){
                my_report = "lab";
            }else {
                my_report = "my";
            }
            a1 = 22;
        }}catch(Exception e)
        {}

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.report_list_row, parent, false);
        }
        ll_list = (LinearLayout) convertView.findViewById(R.id.ll_list);
        ivStar = (ImageView) convertView.findViewById(R.id.ivStar);
        ivShare = (ImageView) convertView.findViewById(R.id.ivShare);
        tvName = (TextView) convertView.findViewById(R.id.tvName);
        tvName.setSelected(true);
        tvDate = (TextView) convertView.findViewById(R.id.tvDate);

        if (report.report_type.equals("patients")) {
            ivStar.setVisibility(View.GONE);
            ivShare.setVisibility(View.GONE);
        } else {
            if (report.getIsstar().equals("yes")) {
                ivStar.setImageResource(R.drawable.ic_star_filled);
            } else {
                ivStar.setImageResource(R.drawable.ic_star);
            }
            ll_list.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    pos = position;
                    report = arrayListReport.get(position);
                    if (report.getIsLocal().equals("yes")) {
                        optionDialog(arr, report.getName(), report.getId(), report.getUser_id());
                    }
                    return true;
                }
            });
        }

        tvName.setText(report.getName());

        tvDate.setText(report.getDate());


        ll_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Helper.setPatient("patient",context);
                report = arrayListReport.get(position);
//                Intent intent = new Intent(context, ViewReportActivity.class);
//                intent.putExtra("Report", report);
//                intent.putExtra("offline", check);
////                intent.putExtra("reportType",report.report_type);
//                intent.putParcelableArrayListExtra("members_list", members_list);
//                context.startActivity(intent);


//                if(report.report_type.equals("patients"))
//                {
//                    report = arrayListReport.get(position);
//                    Intent intent = new Intent(context, ViewPatientsReportActivity.class);
//                    intent.putExtra("files", report.getReportsList());
//                    //intent.putExtra("offline", check);
////                intent.putExtra("reportType",report.report_type);
//                   // intent.putParcelableArrayListExtra("members_list", members_list);
//                    context.startActivity(intent);
//                }
//                else
//                {
                    report = arrayListReport.get(position);
                    Intent intent = new Intent(context, ViewReportActivity.class);
                    intent.putExtra("Report", report);
                    intent.putExtra("offline", check);
                    intent.putExtra("lab_id", report.getLab_id());
                    intent.putExtra("reportType",report.report_type);
                    intent.putParcelableArrayListExtra("members_list", members_list);
                    context.startActivity(intent);
               // }

            }
        });

        ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //call share intent
                report = arrayListReport.get(position);
                Intent intent = new Intent(context, ShareWithDoctor.class);
                intent.putExtra("Report", report);
                intent.putExtra("offline", check);
                intent.putExtra("reportType", report.report_type);
                context.startActivity(intent);
            }
        });

        ivStar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                report = arrayListReport.get(position);
                report_id = report.getId();
                if (report.getIsstar().equals("yes")) {
                    //set empty image
                    if (report_id != null) {
                        star = "no";
                        report.setIsstar(star);
                        progressDialog.show();


                        if (report.report_type.equals("lab")){
                            updateReport1(report.getUser_id(), report_id, "isstar", star);
                        }else {
                            updateReport(report.getUser_id(), report_id, "isstar", star);
                        }

                    }
                } else {
                    if (report_id != null) {
                        star = "yes";
                        report.setIsstar(star);
                        progressDialog.show();
                       // updateReport(report.getUser_id(), report_id, "isstar", star);
                        if (report.report_type.equals("lab")){
                            updateReport1(report.getUser_id(), report_id, "isstar", star);
                        }else {
                            updateReport(report.getUser_id(), report_id, "isstar", star);
                        }
                    }
                }
            }
        });

        setTvTypeface();

        return convertView;
    }

    public void setTvTypeface() {
        tvName.setTypeface(Utils.setTypeface(context));
        tvDate.setTypeface(Utils.setTypeface(context));
    }

    protected void optionDialog(final String[] fav_arr2, final String report_name,
                                final String report_id, final String user_id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("");
        builder.setItems(fav_arr2, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                if (item == 0) {
                    openDialog(report_name, report_id, user_id);
                    dialog.dismiss();
                } else if (item == 1) {
                    Intent intent = new Intent(context, AddReportActivity.class);
                    intent.putExtra("type", "update");
                    intent.putExtra("report_id", report_id);
                    context.startActivity(intent);
                    dialog.dismiss();
                } else if (item == 2) {
                    delete(report_id);
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void openDialog(final String text, final String reportId, final String userId) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_report_name);
        dialog.show();
        final EditText etName = (EditText) dialog.findViewById(R.id.etName);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        txt_title.setText("Edit Report Name");
        txt_title.setTypeface(Utils.setTypeface(context));
        LinearLayout ll_belong = (LinearLayout) dialog.findViewById(R.id.ll_belong);
        ll_belong.setVisibility(View.GONE);
        TextView tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);
        final TextView tvNext = (TextView) dialog.findViewById(R.id.tvNext);
        tvCancel.setTypeface(Utils.setTypeface(context));
        tvNext.setTypeface(Utils.setTypeface(context));

        if (text != null) {
            etName.setText(text);
        }

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etName.length() == 0) {
                    Snackbar.make(tvNext, "Report name can not be blank", BaseTransientBottomBar.LENGTH_SHORT).show();
                } else {
                    String name = etName.getText().toString().trim();
                    progressDialog.show();
                   // updateReport(userId, reportId, "name", name);
                    if (my_report.equals("lab")){
                        updateReport1(userId, reportId, "name", name);
                    }else {
                        updateReport(userId, reportId, "name", name);
                    }
                    dialog.dismiss();
                }
            }
        });

    }

    public void delete(final String reportId) {
        new AlertDialog.Builder(context)
                .setMessage("Are you sure you want to delete this report?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        arrayListReport.remove(pos);
                        progressDialog.show();
                        deleteReport(reportId);
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }


    private void updateReport(final String user_id, final String reportId, final String key, final String like) {
        String url = Urls.URL_TO_WEB + "myreports/editApi/" + reportId;
        FetchData.getInstance(context).getRequestQueue().add(new StringRequest
                (Request.Method.POST, url,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                Log.i("valRes", "" + response);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String status = jsonObject.optString("status");
                                    if (status.equals("1")) {
                                        Toast.makeText(context, "Report updated successfully", Toast.LENGTH_SHORT).show();
                                        if (member_id.equals("0")) {
                                            String url = Urls.URL_TO_WEB + "myreports/getReportsApi/" + user_id;
                                            getMyReports(url);
                                        } else {
                                            String url = Urls.URL_TO_WEB + "myreports/getReportsApiByFamily/" + user_id + "?userfamilies_id=" + member_id;
                                            getMyReports(url);
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", user_id);
                params.put(key, like);
                return params;
            }
        });
    }
    private void updateReport1(final String user_id, final String reportId, final String key, final String like) {
        String url = Urls.URL_TO_WEB + "booktests/editApi/" + reportId+"?isstar="+like;
        FetchData.getInstance(context).getRequestQueue().add(new StringRequest
                (Request.Method.GET, url,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                Log.i("valRes", "" + response);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String status = jsonObject.optString("status");
                                    if (status.equals("1")) {
                                        Toast.makeText(context, "Report updated successfully", Toast.LENGTH_SHORT).show();
                                        if (member_id.equals("0")) {
                                            String url = Urls.URL_TO_WEB + "myreports/getReportsApi/" + user_id;
                                            getMyReports(url);
                                        } else {
                                            String url = Urls.URL_TO_WEB + "myreports/getReportsApiByFamily/" + user_id + "?userfamilies_id=" + member_id;
                                            getMyReports(url);
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                    }
                }) {

        });
    }

    private void deleteReport(final String reportId) {
        FetchData.getInstance(context).getRequestQueue().add(new StringRequest
                (Request.Method.GET, Urls.URL_TO_WEB + "myreports/deleteApi/" + reportId,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                progressDialog.dismiss();
                                Log.i("valRes", "" + response);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String status = jsonObject.optString("status");
                                    if (status.equals("1")) {
                                        Toast.makeText(context, "Report deleted successfully", Toast.LENGTH_SHORT).show();
                                        notifyDataSetChanged();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                    }
                }) {
        });
    }

    private void getMyReports(String url) {
        FetchData.getInstance(context).getRequestQueue().add(new StringRequest
                (Request.Method.POST, url,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                Log.d("valRes", response);
                                progressDialog.dismiss();
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String status = jsonObject.optString("status");
                                    if (status.equals("1")) {
                                        arrayListReport.clear();
                                        JSONObject data = jsonObject.optJSONObject("data");
                                        JSONArray myreports = data.optJSONArray("myreports");
                                        if (myreports != null) {
                                            for (int i = 0; i < myreports.length(); i++) {
                                                JSONObject jobj = myreports.getJSONObject(i);
                                                Report report = new Report();
                                                report.setId(jobj.optString("id"));
                                                report.setUser_id(jobj.optString("user_id"));
                                                report.setName(jobj.optString("name"));
                                                report.setPath(jobj.optString("photo"));
                                                if (jobj.optString("photo").endsWith("pdf")) {
                                                    report.setType("pdf");
                                                } else {
                                                    report.setType("image");
                                                }
                                                report.setIsstar(jobj.optString("isstar"));
                                                report.setIsLocal(jobj.optString("islocal"));
                                                report.setDate(jobj.optString("created"));
                                                report.member_name = intentType;
                                                report.member_list = member_list;
                                                report.report_type = "my";
                                                arrayListReport.add(report);
                                            }
                                        }
                                        JSONArray Labreports = data.optJSONArray("Labreports");
                                        if (Labreports != null) {
                                            for (int i = 0; i < Labreports.length(); i++) {
                                                JSONObject jobj = Labreports.getJSONObject(i);
                                                Report report = new Report();
                                                report.setId(jobj.optString("id"));
                                                report.setUser_id(jobj.optString("user_id"));
                                                report.setName(jobj.optString("name"));
                                                report.setIsstar(jobj.optString("isstar"));
                                                report.setIsLocal("no");
                                                report.setDate(jobj.optString("created"));
                                                report.member_name = intentType;
                                                report.member_list = member_list;
                                                report.report_type = "lab";
                                                JSONArray file = jobj.optJSONArray("file");
                                                if (file != null) {
                                                    for (int j = 0; j < file.length(); j++) {
                                                        JSONObject obj = file.optJSONObject(j);
                                                        report.setPath(obj.optString("filename"));
                                                        if (obj.optString("filename").endsWith("pdf")) {
                                                            report.setType("pdf");
                                                        } else {
                                                            report.setType("image");
                                                        }
                                                    }
                                                }
                                                arrayListReport.add(report);
                                            }
                                        }
                                        Collections.sort(arrayListReport, new ComparatorDate());
                                        Collections.reverse(arrayListReport);
                                        notifyDataSetChanged();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                    }
                }) {
        });
    }
}
