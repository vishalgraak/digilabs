package com.sahir.websolutions.digilabs.objects;

import java.util.HashMap;

/**
 * Created by SWSPC8 on 7/10/2017.
 */

public class Dialogclass {
    private boolean checkSwitch;
    private HashMap<String, String> sub_cat;
    private HashMap<String, String> sub_tests;

    public Dialogclass(boolean checkSwitch, HashMap<String, String> cat, HashMap<String, String> list) {
        this.checkSwitch = checkSwitch;
        this.sub_tests = list;
        this.sub_cat = cat;
    }

    public boolean getCheckSwitch() {
        return checkSwitch;
    }

    public void setCheckSwitch(boolean checkSwitch) {
        this.checkSwitch = checkSwitch;
    }

}
