package com.sahir.websolutions.digilabs.Activity;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Bhavya on 25-09-2016.
 */
public class FetchData {

    private static FetchData classObject;
    private static Context context;
    private RequestQueue requestQueue;
    String jsonString=null;

  //  private ImageLoader mImageLoader;

    private FetchData(Context context){
        this.context=context;
        requestQueue=getRequestQueue();
    }

    public static synchronized FetchData getInstance(Context context) {
        if (classObject== null) {
            classObject = new FetchData(context);
        }
        return classObject;
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return requestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }


    public static final String url = "https://elvektechnologies.com/sabonclick/index_high.php?interests=1";


    public String showJSON(){

        return jsonString;
    }







}
