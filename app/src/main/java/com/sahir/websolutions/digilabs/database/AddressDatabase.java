package com.sahir.websolutions.digilabs.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

/**
 * Created by Vishal on 09/05/2018.
 */


@Database(entities = {AddressModel.class}, version = 1)
public abstract class AddressDatabase  extends RoomDatabase {
    public abstract AddressDao addressDao();

    public static AddressDatabase INSTANCE;


    public static AddressDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AddressDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AddressDatabase.class, "address_database").allowMainThreadQueries()
                            .build();

                }
            }
        }
        return INSTANCE;
    }

}
