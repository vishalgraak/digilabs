package com.sahir.websolutions.digilabs.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.sahir.websolutions.digilabs.HelperClass.Helper;
import com.sahir.websolutions.digilabs.R;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by sandeepsingh on 23/11/17.
 */

public class ViewPatientsReportActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private JSONArray mArray;
    Activity sActivity;
    Context sActivity1;
    Toolbar toolbar;
    public static String user_id;

    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_patients_report);

        sActivity = ViewPatientsReportActivity.this;
        sActivity1 = getApplicationContext();

        mPager = (ViewPager) findViewById(R.id.viewPager);

       /* prefs = getSharedPreferences(getString(R.string.user_details), Context.MODE_PRIVATE);

        if (prefs.getString("isFirstP", null) != null) {
            if (prefs.getString("isFirstP", null).equals("yes")) {
           //     showDialog(prefs.getString("role", ""));
                editor = prefs.edit();


            }
        }

        user_id = prefs.getString(getString(R.string.user_id), null);

        setToolbar();

        setNavDrawer();
        */

        String files = getIntent().getStringExtra("files");

        try {
            mArray = new JSONArray(files);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);


    }


    public void setToolbar() {
        setTitle("");
        toolbar = (Toolbar) findViewById(R.id.toolbar_image);
        setSupportActionBar(toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        ImageView ivImage = (ImageView) toolbar.findViewById(R.id.ivImage);
        ivImage.setImageResource(R.drawable.ic_patients_white);
        mTitle.setText("Patient's Reports");
        Helper.setPatient("patient",sActivity1);
        mTitle.setTypeface(Utils.setTypeface(this));
    }

    public void setNavDrawer() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        TextView tvCopyright = (TextView) findViewById(R.id.tvCopyright);
        tvCopyright.setTypeface(Utils.setTypeface(this));

        Menu m = navigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);
        }
        if (Helper.getCheck(sActivity).equals("doctor")){
            navigationView.getMenu().setGroupVisible(R.id.group_item5, false);
        }else {
            navigationView.getMenu().setGroupVisible(R.id.group_item5, true);
        }
    }

    private void applyFontToMenuItem(MenuItem mi) {
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", Utils.setTypeface(this)), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            Fragment fragment = new ScreenSlidePageFragment();

            Bundle bundle = new Bundle();

            try {
                bundle.putString("url", mArray.getJSONObject(position).getString("labfile"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            fragment.setArguments(bundle);

            return fragment;
        }

        @Override
        public int getCount() {
            return mArray.length();
        }
    }

    public static class ScreenSlidePageFragment extends Fragment {


        private String url;
        private WebView webView;
        private  TouchImageView touchImageView;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            ViewGroup rootView = (ViewGroup) inflater.inflate(
                    R.layout.fragment_screen_slide_page, container, false);

            webView = (WebView) rootView.findViewById(R.id.webView);
            touchImageView = (TouchImageView) rootView.findViewById(R.id.tivImage);

            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setBuiltInZoomControls(true);
            webView.setWebViewClient(new WebViewClient());


            url = getArguments().getString("url");


            String fileName = url.substring(url.lastIndexOf('/') + 1, url.length());
            String extn = fileName.substring(fileName.lastIndexOf('.'));


            if (extn.equalsIgnoreCase(".pdf")) {
                webView.setVisibility(View.VISIBLE);
                touchImageView.setVisibility(View.GONE);

                webView.loadUrl("http://docs.google.com/viewerng/viewer?embedded=true&url=" + url);
            }

            return rootView;
        }

    }

    @Override
    public boolean onNavigationItemSelected( MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            Intent intent = new Intent(ViewPatientsReportActivity.this, PreviewActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_my_favourites) {
            Intent intent = new Intent(ViewPatientsReportActivity.this, BookedTestsActivity.class);
            startActivity(intent);
            Helper.setAddClass("addNo",sActivity);
        } else if (id == R.id.nav_Profile) {
            if (prefs.getString("role", null).equals("3")) {
                startActivity(new Intent(ViewPatientsReportActivity.this, RegisterDoctor.class)
                        .putExtra("intent", "update"));
            } else {
                startActivity(new Intent(ViewPatientsReportActivity.this, RegisterActivity.class)
                        .putExtra("intent", "update"));
            }
        } else if (id == R.id.nav_share_app) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    "Hey check out my app at: http://bit.ly/2xv0Zi8");
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        } else if (id == R.id.nav_register_lab) {
            startActivity(new Intent(ViewPatientsReportActivity.this, RegisterUserToDoctor.class)
                    .putExtra("user_id", user_id));
//            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.elvektechnologies.com/Digilabs/"));
//            startActivity(browserIntent);
        } else if (id == R.id.nav_rate_app) {
            startActivity(new Intent(ViewPatientsReportActivity.this, FeedbackActivity.class));
        } else if (id == R.id.nav_logout_app) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.clear();
            editor.commit();
            for (int i = 0; i < ActivityStack.activity
                    .size(); i++) {
                (ActivityStack.activity.elementAt(i))
                        .finish();
                Log.e("Activity size:" + ActivityStack.activity.size(), "finished...");
            }
            startActivity(new Intent(ViewPatientsReportActivity.this, RegisterActivity.class).putExtra("intent", "register"));
            finish();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}
