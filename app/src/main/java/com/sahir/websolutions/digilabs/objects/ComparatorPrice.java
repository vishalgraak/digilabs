package com.sahir.websolutions.digilabs.objects;

import java.util.Comparator;

/**
 * Created by Sys9 on 6/30/2017.
 */

public class ComparatorPrice implements Comparator<LabModel> {
    @Override
    public int compare(LabModel o1, LabModel o2) {
        return (o1.price).compareTo(o2.price);
    }
}
