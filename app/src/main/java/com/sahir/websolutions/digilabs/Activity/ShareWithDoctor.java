package com.sahir.websolutions.digilabs.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sahir.websolutions.digilabs.R;
import com.sahir.websolutions.digilabs.objects.Report;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ShareWithDoctor extends AppCompatActivity {
    Toolbar toolbar;
    Button btn_doctor, btn_social;
    String reportName, url, id, type, time, user_id, date, report_type = "none",lab_report;
    String nameReportTemp;
    Report report;
    int offline;
    Dialog dialog;
    URL urlTemp;
    SharedPreferences prefs;
    ProgressDialog progressDialog;
    ArrayList<String> images_list = new ArrayList<>();
    ArrayList<String> path_list = new ArrayList<>();
    private int BUFFER = 1024;
    String path_arr[];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_with_doctor);


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        init();

        setToolbar();

        setTvTypeface();

        onClicks();

    }

    private void init() {
        ActivityStack.activity.add(ShareWithDoctor.this);
        prefs = getSharedPreferences(getString(R.string.user_details), Context.MODE_PRIVATE);

        Intent intent = getIntent();
        report = (Report) intent.getSerializableExtra("Report");


        lab_report = report.getLab_report();
        reportName = report.getName();
        url = report.getPath();
        id = report.getId();
        user_id = report.getUser_id();
        type = report.getType();
        offline = intent.getIntExtra("offline", 2);   //offline-1, online -0
        images_list = report.image_list;

        if (images_list.size() > 1) {
            report_type = "more";
        }

        /*if (intent.getStringExtra("type") != null) {
            report_type = intent.getStringExtra("type");
        }*/

        if (prefs.getString("zipFile", null) != null) {
            File file = new File(prefs.getString("zipFile", null));
            file.delete();
        }

        progressDialog = new ProgressDialog(ShareWithDoctor.this);
        progressDialog.setMessage("Sharing Report...");

        btn_doctor = (Button) findViewById(R.id.btn_doctor);
        btn_social = (Button) findViewById(R.id.btn_social);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm a");
        time = sdf.format(c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        date = df.format(c.getTime());

        if (offline == 1) {
        } else if (offline == 0) {
            btn_doctor.setVisibility(View.VISIBLE);
            btn_social.setVisibility(View.VISIBLE);
        }
    }

    private void onClicks() {
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });

        btn_social.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                socialnetwork();
            }
        });

        btn_doctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ShareWithDoctor.this, ViewShareDoctor.class)
                        .putExtra("Report", report));
            }
        });
    }

    public void setTvTypeface() {
        btn_doctor.setTypeface(Utils.setTypeface(this));
        btn_social.setTypeface(Utils.setTypeface(this));
    }

    public void setToolbar() {
        setTitle("");
        toolbar = (Toolbar) findViewById(R.id.toolbar_image);
        setSupportActionBar(toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("Share Report");
        mTitle.setTypeface(Utils.setTypeface(this));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    public void socialnetwork() {
        if (offline == 0) {
            if (type != null) {
                if (report_type.equals("more")) {
                    if (!Utils.isNetworkConnected(ShareWithDoctor.this)) {
                        Utils.conDialog(ShareWithDoctor.this);
                    } else {
                        DownloadFile dlfile = new DownloadFile("more");
                        dlfile.execute();
                    }
                } else {
                    if (type.equals("pdf")) {
                        if (!Utils.isNetworkConnected(ShareWithDoctor.this)) {
                            Utils.conDialog(ShareWithDoctor.this);
                        } else {
                            DownloadFile dlfile = new DownloadFile("one");
                            dlfile.execute();
                        }
                    } else {
                        //for online image    --data may be gallery or camera
                        String shareBody = "Hi, Please find attached my medical test report.";
                        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                        sharingIntent.setType("image/*");
                        urlTemp = null;
                        try {
                            urlTemp = new URL(url);
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                        Bitmap b = getBitmapFromUrl(urlTemp);
                        sharingIntent.putExtra(Intent.EXTRA_STREAM, getImageUri(this, b));
                        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "" + "Medical Test Report");
                        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                        startActivity(Intent.createChooser(sharingIntent, "Share With"));
                    }
                }
            } else {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "Hey check out my app at: http://www.elvektechnologies.com/Digilabs/");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        }
        else {

        }
    }

    public class DownloadFile extends AsyncTask<String, Void, String> {
        String val;

        public DownloadFile(String str) {
            this.val = str;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Compressing Files....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (val.equals("one")) {
                urlTemp = null;
                try {
                    urlTemp = new URL(url);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                if (type.equals("pdf")) {
                    saveToInternalStoragePdf(url, "yes");
                } else if (type.equals("gallery") || type.equals("camera")) {
                    Bitmap bitmapT = getBitmapFromUrl(urlTemp);
                    String temp = "file:///" + Environment.getExternalStorageDirectory() + File.separator + "DigiLabs";
                    String nameReportTemp1 = reportName + "_" + date + ".jpg";
                    File file = new File(temp, nameReportTemp1);
                    Log.d("filePath", file.toString());
                    saveToInternalStorage(bitmapT, nameReportTemp1);
                    String shareBody = "Hi, Please find attached my medical test report.";
                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
                    sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "" + "Medical Test Report");
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                    startActivity(Intent.createChooser(sharingIntent, "Share With"));
                }
            } else {
                try {
                     urlTemp = null;
                    for (int i = 0; i < images_list.size(); i++) {
                        if (images_list.get(i).endsWith("png")) {
                            urlTemp = new URL(images_list.get(i));
                            Bitmap bitmapT = getBitmapFromUrl(urlTemp);
                            saveToInternalStorage(bitmapT, "ReportImg" + i + ".png");
                        } else if (images_list.get(i).endsWith("jpg")) {
                            urlTemp = new URL(images_list.get(i));
                            Bitmap bitmapT = getBitmapFromUrl(urlTemp);
                            saveToInternalStorage(bitmapT, "ReportImg" + i + ".jpg");
                        } else if (images_list.get(i).endsWith("gif")) {
                            urlTemp = new URL(images_list.get(i));
                            Bitmap bitmapT = getBitmapFromUrl(urlTemp);
                            saveToInternalStorage(bitmapT, "ReportImg" + i + ".gif");
                        } else if (images_list.get(i).endsWith("pdf")) {
                            saveToInternalStoragePdf(images_list.get(i).replace(" ", "%20"), "no");
                        } else if (images_list.get(i).endsWith("mp4")) {
                            saveToInternalStorageVideo(images_list.get(i).replace(" ", "%20"));
                        }
                    }
                    path_arr = new String[path_list.size()];
                    path_arr = path_list.toArray(path_arr);

                    zip(path_arr);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }

    public void saveToInternalStorageVideo(String url) {
        File directory = new File(Environment.getExternalStorageDirectory() + File.separator + "DigiLabs");
        if (!directory.exists()) {
            directory.mkdirs();
        }
        String nameReportTemp = reportName.trim() + "_" + date + ".mp4";
        File file = new File(directory, nameReportTemp);
        path_list.add(file.getPath());
        try {
            if (file.exists()) {
                file.delete();
                file.createNewFile();
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        getVideoFromServer(url, file);
    }


    public void getVideoFromServer(String fileURL, File directory) {
        try {
            URL u = new URL(fileURL);
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();

            InputStream in = c.getInputStream();
            BufferedInputStream inStream = new BufferedInputStream(in, 1024 * 5);
            FileOutputStream f = new FileOutputStream(directory);

            byte[] buffer = new byte[5 * 1024];
            int len1;
            while ((len1 = inStream.read(buffer)) > 0) {
                f.write(buffer, 0, len1);
            }
            f.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveToInternalStoragePdf(String url, String isOpen) {
        File directory = new File(Environment.getExternalStorageDirectory() + File.separator + "DigiLabs");
        if (!directory.exists()) {
            directory.mkdirs();
        }
        nameReportTemp = reportName + "_" + date + ".pdf";
        File file = new File(directory, nameReportTemp);
        Log.d("filePath", file.toString());
        path_list.add(file.getPath());
        try {
            if (file.exists()) {
                file.delete();
                file.createNewFile();
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        getPdfFromServer(url, file, isOpen);
    }

    public void getPdfFromServer(String fileURL, File directory, String isOpen) {
        try {
            Log.d("filePath", directory.toString());
            FileOutputStream f = new FileOutputStream(directory);
            URL u = new URL(fileURL);
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();

            InputStream in = c.getInputStream();

            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ((len1 = in.read(buffer)) > 0) {
                f.write(buffer, 0, len1);
            }
            f.close();
            if (isOpen.equals("yes")) {
                String shareBody = "Hi, Please find attached my medical test report.";
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/pdf");

                sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + directory));
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "" + "Medical Test Report");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody+ ": - "+urlTemp);
                startActivity(Intent.createChooser(sharingIntent, "Share With"));
                progressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Bitmap getBitmapFromUrl(URL url) {
        try {
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
//            ((ImageView)findViewById(R.id.ivImage)).setImageBitmap(myBitmap);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            Log.d("IOException", e.toString());
            return null;
        }
    }

    private void saveToInternalStorage(Bitmap bitmapImage, String reportName) {
        File directory = new File(Environment.getExternalStorageDirectory() + File.separator + "DigiLabs");
        if (!directory.exists()) {
            directory.mkdirs();
        }
//        String nameTemp = reportName + "." + ext;
        File pictureFile = new File(directory, reportName);
        path_list.add(pictureFile.getPath());
        try {
            if (pictureFile.exists()) {
                pictureFile.delete();
                pictureFile.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            FileOutputStream out = new FileOutputStream(pictureFile);
            bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        //String path = (Environment.getExternalStorageDirectory() + File.separator + "DigiLabs" +nameReportTemp);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    public void zip(String[] _files) {
        try {
            BufferedInputStream origin = null;
            FileOutputStream dest = new FileOutputStream(Environment.getExternalStorageDirectory()
                    + File.separator + "DigiLabs" + File.separator + "myfiles.zip");

            SharedPreferences.Editor edit = prefs.edit();
            edit.putString("zipFile", Environment.getExternalStorageDirectory()
                    + File.separator + "DigiLabs" + File.separator + "myfiles.zip");
            edit.commit();

            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(
                    dest));
            byte data[] = new byte[BUFFER];

            for (int i = 0; i < _files.length; i++) {
                Log.v("Compress", "Adding: " + _files[i]);
                FileInputStream fi = new FileInputStream(_files[i]);
                origin = new BufferedInputStream(fi, BUFFER);

                ZipEntry entry = new ZipEntry(_files[i].substring(_files[i].lastIndexOf("/") + 1));
                out.putNextEntry(entry);
                int count;

                while ((count = origin.read(data, 0, BUFFER)) != -1) {
                    out.write(data, 0, count);
                }
                origin.close();
            }

            out.close();
            progressDialog.dismiss();
            String shareBody = "Hi, Please find attached my medical test report.";
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.setType("application/zip");
            sendIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + Environment.getExternalStorageDirectory()
                    + File.separator + "DigiLabs" + File.separator + "myfiles.zip"));
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, "" + "Medical Test Report");
            sendIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sendIntent, "Share With"));

            for (int i = 0; i < path_list.size(); i++) {
                File file = new File(path_list.get(i));
                file.delete();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return true;
    }
}
