package com.sahir.websolutions.digilabs.IntroSlider;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;


import com.sahir.websolutions.digilabs.Activity.BookTestActivity;
import com.sahir.websolutions.digilabs.Activity.HealthPackagesActivity;
import com.sahir.websolutions.digilabs.HelperClass.Helper;
import com.sahir.websolutions.digilabs.Activity.PatientsReportActivity;
import com.sahir.websolutions.digilabs.Activity.PreviewActivity;
import com.sahir.websolutions.digilabs.R;
import com.sahir.websolutions.digilabs.Activity.RegisterActivity;
import com.sahir.websolutions.digilabs.Activity.ReportsActivity;
import com.sahir.websolutions.digilabs.Activity.SearchLabActivity;

import static android.content.Context.MODE_PRIVATE;

public class MyFragment extends Fragment {

    static Context mcontext;
    static SharedPreferences prefs;
    Activity sActivity;
    int[] layouts;

    public static Fragment newInstance(PreviewActivity context, int pos, float scale) {
        Bundle b = new Bundle();
        b.putInt("pos", pos);
        b.putFloat("scale", scale);
        mcontext = context;

        prefs = context.getSharedPreferences(context.getString(R.string.user_details), MODE_PRIVATE);
        return Fragment.instantiate(context, MyFragment.class.getName(), b);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        sActivity = getActivity();
        if (container == null) {
            return null;
        }
        final int pos = this.getArguments().getInt("pos"); //range 0-3 //size 4

        if (prefs.getString("role", null) != null) {
            if (prefs.getString("role", null).equals("3")) {
                layouts = new int[]{
                        R.layout.intro_slider5,
                        R.layout.intro_slider3,
                        R.layout.intro_slider4,
                        R.layout.intro_slider2,
                        R.layout.intro_slider1};
            } else {
                layouts = new int[]{
                        R.layout.intro_slider3,
                        R.layout.intro_slider4,
                        R.layout.intro_slider2,
                        R.layout.intro_slider1,
                R.layout.intro_slider5};
            }
        } else {
            layouts = new int[]{
                    R.layout.intro_slider3,
                    R.layout.intro_slider4,
                    R.layout.intro_slider2,
                    R.layout.intro_slider1,
                    R.layout.intro_slider5,
            };
        }

        LinearLayout l;
        try {
            l = (LinearLayout)
                    inflater.inflate(layouts[pos], container, false);
        } catch (Exception e) {
            l = (LinearLayout)
                    inflater.inflate(layouts[pos], container, false);
        }

        l.setId(pos);

        MyLinearLayout root = (MyLinearLayout) l.findViewById(R.id.root);
        RelativeLayout llBackground = (RelativeLayout) root.findViewById(R.id.llBackground);

        llBackground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (prefs.getString(mcontext.getString(R.string.user_id), null) != null) {
                    if (pos == MyPagerAdapter.mid_view_id) {
                        Intent intent;
                        if (prefs.getString("role", null).equals("3")) {
                            switch (pos) {
                                case 0:
                                    intent = new Intent(mcontext, PatientsReportActivity.class);
                                    startActivity(intent);

                                    break;
                                case 1:
                                    intent = new Intent(mcontext, BookTestActivity.class);
                                    intent.putExtra("type", "book");
                                    startActivity(intent);
                                    Helper.setAddClass("addNo",sActivity);
                                    break;
                                case 2:
                                    intent = new Intent(mcontext, ReportsActivity.class);
                                    startActivity(intent);
                                    Helper.setAddClass("addNo",sActivity);
                                    break;
                                case 3:
                                    intent = new Intent(mcontext, SearchLabActivity.class);
                                    startActivity(intent);
                                    Helper.setAddClass("addYes",sActivity);
                                    break;
                                case 4:
                                    intent = new Intent(mcontext, HealthPackagesActivity.class);
                                    startActivity(intent);
                                    Helper.setAddClass("addNo",sActivity);
                                    break;
                            }
                        } else {
                            switch (pos) {
                                case 0:
                                    intent = new Intent(mcontext, BookTestActivity.class);
                                    intent.putExtra("type", "book");
                                    Helper.setAddClass("addNo",sActivity);
                                    startActivity(intent);
                                    break;
                                case 1:
                                    intent = new Intent(mcontext, ReportsActivity.class);
                                    startActivity(intent);
                                    Helper.setAddClass("addNo",sActivity);
                                    break;
                                case 2:
                                    intent = new Intent(mcontext, SearchLabActivity.class);
                                    startActivity(intent);
                                    Helper.setAddClass("addYes",sActivity);
                                    break;
                                case 3:
                                    intent = new Intent(mcontext, HealthPackagesActivity.class);
                                    startActivity(intent);
                                    Helper.setAddClass("addNo",sActivity);
                                    break;
                            }
                        }
                    }
                } else {
                    startActivity(new Intent(mcontext, RegisterActivity.class).putExtra("intent", "register"));
                    getActivity().finish();
                }
            }
        });
        float scale = this.getArguments().getFloat("scale");
        root.setScaleBoth(scale);
        return l;
    }
}
