package com.sahir.websolutions.digilabs.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sahir.websolutions.digilabs.HelperClass.Helper;
import com.sahir.websolutions.digilabs.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Safal Bhatia on 6/28/2017.
 */

public class LoginActivity extends AppCompatActivity {
    Toolbar toolbar;
    EditText editTextPhone;
    Button buttonLogin;
    String phone, user_id;
    TextView tvBubble;
    ProgressDialog progressDialog;
    SharedPreferences pref;
    Activity sActivity;
    String title;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sActivity = LoginActivity.this;
        init();

        setToolbar();

        onClicks();

    }

    private void init() {
        ActivityStack.activity.add(LoginActivity.this);

        pref = getSharedPreferences(getString(R.string.user_details), MODE_PRIVATE);
        editTextPhone = (EditText) findViewById(R.id.editTextPhone);
        buttonLogin = (Button) findViewById(R.id.buttonLogin);
        editTextPhone.setTypeface(Utils.setTypeface(this));
        buttonLogin.setTypeface(Utils.setTypeface(this));
        tvBubble = (TextView) findViewById(R.id.tvBubble);
        tvBubble.setTypeface(Utils.setTypeface(this));
    }

    private void onClicks() {
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextPhone.length() == 0) {
                    editTextPhone.setError("Phone number empty!");
                    editTextPhone.requestFocus();
                } else if (editTextPhone.length() != 10) {
                    editTextPhone.setError("Phone number should be of 10 digits!");
                    editTextPhone.requestFocus();
                } else {
                    if (!Utils.isNetworkConnected(LoginActivity.this)) {
                        Utils.conDialog(LoginActivity.this);
                    } else {
                        progressDialog.show();
                        phone = editTextPhone.getText().toString();
                        checkUser(phone);
                    }
                }
            }
        });
    }

    private void setToolbar() {
        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage("Processing...");
        //Toolbar code
        setTitle("");
        toolbar = (Toolbar) findViewById(R.id.toolbar_image);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("LOGIN");
        mTitle.setTypeface(Utils.setTypeface(this));
    }

    public void checkUser(final String phone) {
        FetchData.getInstance(LoginActivity.this).getRequestQueue().add(new StringRequest
                (Request.Method.GET, Urls.URL_TO_WEB + "users/loginapi?phone=" + phone,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                final String response2 = response;
                                Log.d("valRes", response2);
                                try {
                                    JSONObject json = new JSONObject(response2);
                                    int status = json.getInt("status");
                                    if (status == 1) {
                                        JSONObject data = json.optJSONObject("data");
                                        JSONObject jobj = data.optJSONObject("0");
                                        user_id = jobj.optString("id");
                                        title = jobj.optString("title");
                                        String firstname = jobj.optString("firstname");
                                        String lastname = jobj.optString("lastname");
                                        String email = jobj.optString("email");
                                        String role = jobj.optString("role");
                                        if(role.equals("3")){
                                            Helper.setCheck("doctor",sActivity);
                                        }else {
                                            Helper.setCheck("normal",sActivity);
                                        }
                                        String is_active = jobj.optString("is_active");
                                        String phone = jobj.optString("phone");
                                        String gender = jobj.optString("gender");
                                        String dob = jobj.optString("dob");
                                        String pin = jobj.optString("pin");
                                        String address = jobj.optString("address");
                                        String city = jobj.optString("city");
                                        String clinic_id = "", clinic = "";
                                        if (data.has("doctor")) {
                                            JSONArray doctor = data.optJSONArray("doctor");
                                            if (doctor != null) {
                                                for (int i = 0; i < doctor.length(); i++) {
                                                    JSONObject obj = doctor.optJSONObject(i);
                                                    clinic_id = obj.optString("id");
                                                    clinic = obj.optString("clinicname");
                                                    email = obj.optString("email");
                                                }
                                            }
                                        }
                                        String state_id = "", state_name = "";
                                        if (data.has("state")) {
                                            JSONArray state = data.optJSONArray("state");
                                            if (state != null) {
                                                for (int i = 0; i < state.length(); i++) {
                                                    JSONObject obj = state.optJSONObject(i);
                                                    state_id = obj.optString("id");
                                                    state_name = obj.optString("name");
                                                }
                                            }
                                        }
                                        saveUserDataSF(title, firstname, lastname, email, phone, pin,
                                                gender, dob, user_id, role, is_active, city, state_id,
                                                state_name, address, clinic_id, clinic);
                                        progressDialog.dismiss();
                                        startActivity(new Intent(LoginActivity.this, PreviewActivity.class)
                                                .putExtra("role", role)
                                                .putExtra("home", "yes"));

                                        /** Otp send* */
                                        startActivity(new Intent(LoginActivity.this, OTPActivity.class)
                                                .putExtra("phone", phone)
                                                .putExtra("user_id", user_id)
                                                .putExtra("login", "yes"));
                                        /***************/
                                    } else {
                                        progressDialog.dismiss();
                                        editTextPhone.setError("Phone number incorrect!");
                                        editTextPhone.requestFocus();
                                        Snackbar.make(editTextPhone, "Please enter correct phone number",
                                                BaseTransientBottomBar.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                    }
                }) {
        });
    }

    public void saveUserDataSF(String title, String fname, String lname, String email, String phone, String pin,
                               String gender, String dob, String userId, String role, String isActive,
                               String city, String state_id, String state, String address, String clinic_id, String clinicName) {
        SharedPreferences.Editor pInfo = pref.edit();
        pInfo.putString("title", title);
        pInfo.putString("fname", fname);
        pInfo.putString("lname", lname);
        pInfo.putString("email", email);
        pInfo.putString("phone", phone);
        pInfo.putString("pin", pin);
        pInfo.putString("gender", gender);
        pInfo.putString("dob", dob);
        pInfo.putString(getString(R.string.user_id), userId);
        pInfo.putString("role", role);
        pInfo.putString("isActive", isActive);
        pInfo.putString("isFirst", "yes");
        pInfo.putString("city", city);
        pInfo.putString("state_id", state_id);
        pInfo.putString("state", state);
        pInfo.putString("address", address);
        pInfo.putString("clinic_id", clinic_id);
        pInfo.putString("clinic", clinicName);
        pInfo.apply();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return true;
    }
}
