package com.sahir.websolutions.digilabs.Activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sahir.websolutions.digilabs.Adapter.LabListAdapter;
import com.sahir.websolutions.digilabs.HelperClass.Helper;
import com.sahir.websolutions.digilabs.R;
import com.sahir.websolutions.digilabs.objects.LabModel;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class BookedTestsActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    ListView lvLab;
    LabListAdapter labListAdapter;
    ArrayList<Helper> img_string;
    Toolbar toolbar;
    String fav_labs;
    GoogleApiClient mGoogleApiClient = null;
    Location mLastLocation, LResult = null;
    ArrayList<LabModel> arrayListFavLabs = new ArrayList<>();
    SharedPreferences prefs;
    TextView txt_no_reports;
    LinearLayout ll_reports;
    ProgressDialog progressDialog;
    public static String title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav__favourite_lab);
        img_string = new ArrayList<Helper>();
        init();

        setToolbar();

        getLocation();

        if (!Utils.isNetworkConnected(BookedTestsActivity.this)) {
            Utils.conDialog(BookedTestsActivity.this);
        } else {
            getbookedTestsList(prefs.getString(getString(R.string.user_id), null));
        }

        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });

    }

    private void getbookedTestsList(final String user_id) {
        FetchData.getInstance(BookedTestsActivity.this).getRequestQueue().add(new StringRequest
                (Request.Method.GET, Urls.URL_TO_WEB + "booktests/mybookedtest/" + user_id,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                progressDialog.dismiss();
                                final String response2 = response;
                                Log.i("valRes", "" + response2);
                                try {
                                    JSONObject json = new JSONObject(response2);
                                    int status = json.getInt("status");
                                    if (status == 1) {
                                        arrayListFavLabs.clear();
                                        JSONArray data = json.optJSONArray("data");
                                        for (int i = 0; i < data.length(); i++) {
                                            JSONObject jobj = data.optJSONObject(i);
                                            final LabModel labModel = new LabModel();
                                            labModel.id = String.valueOf(jobj.optInt("lab_id"));
                                            labModel.user_id = user_id;
                                            labModel.phone = jobj.optString("user_id");
                                            labModel.collection_charges = jobj.optString("collection_charges");
                                            labModel.isHomecollection = jobj.optString("ishomecollection");
                                            labModel.collection_distance = jobj.optString("collection_distance");
                                            labModel.price = jobj.optString("bookingcost");
                                            labModel.date = jobj.optString("testdate");
                                            labModel.book_id = jobj.optString("bookingId");
                                            labModel.time = jobj.optString("timeslot");
                                            JSONArray booktestlines = jobj.optJSONArray("booktestlines");
                                            if (booktestlines != null) {
                                                for (int j = 0; j < booktestlines.length(); j++) {
                                                    JSONObject jobj1 = booktestlines.optJSONObject(j);
                                                    labModel.labname = jobj1.optString("labname");
                                                    labModel.test_name = jobj1.optString("booktest_id");
                                                    labModel.home_collection_price = jobj1.optString("hcprice");
                                                    labModel.home_price_fix=jobj1.optString("coll_charges");
                                                    labModel.star_rating = jobj1.optString("starrating");
                                                    labModel.price_rating = jobj1.optString("pricerating");
                                                    labModel.latitude = jobj1.optString("latitude");
                                                    labModel.longtitude = jobj1.optString("longitude");
                                                    labModel.lab_image = jobj.optString("file_name");
                                                    String img1 = jobj1.optString("filename");
                                                    Helper helper = new Helper(img1);
                                                    img_string.add(helper);
                                                    labModel.opentime = jobj1.optString("opentime");
                                                    labModel.closetime = jobj1.optString("closetime");
                                                    labModel.working_days = jobj1.optString("workingdays");
                                                    labModel.test_type = jobj1.optString("booktype");
                                                    if (jobj1.optString("booktype").equals("package")) {
                                                        labModel.tests.add(jobj1.optString("package"));
                                                    } else {
                                                        labModel.tests.add(jobj1.optString("booktest_id"));
                                                    }
                                                    labModel.type = "test_booked";
                                                }
                                                arrayListFavLabs.add(labModel);
                                            }
                                        }
                                        if (arrayListFavLabs.size() > 0) {
                                            ll_reports.setVisibility(View.VISIBLE);
                                            txt_no_reports.setVisibility(View.GONE);
                                            calculateDistance();
                                        } else {
                                            ll_reports.setVisibility(View.GONE);
                                            txt_no_reports.setVisibility(View.VISIBLE);
                                        }
                                    }
                                } catch (JSONException e) {
                                    progressDialog.dismiss();
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("CAT", error.toString());
                    }
                }) {
        });
    }

    private void calculateDistance() {
        for (int i = 0; i < arrayListFavLabs.size(); i++) {
            if (LResult == null) {
                arrayListFavLabs.get(i).distance = 0;
            } else {
                Location loc = new Location("locationA");
                loc.setLatitude(Double.valueOf(arrayListFavLabs.get(i).latitude));
                loc.setLongitude(Double.valueOf(arrayListFavLabs.get(i).longtitude));
                float distance = LResult.distanceTo(loc);
                arrayListFavLabs.get(i).distance = (distance / 1000);
            }
        }
        labListAdapter = new LabListAdapter(BookedTestsActivity.this, arrayListFavLabs, img_string,"Booked");
        lvLab.setAdapter(labListAdapter);
    }

    public void init() {
        ActivityStack.activity.add(BookedTestsActivity.this);

        prefs = getSharedPreferences(getString(R.string.user_details), Context.MODE_PRIVATE);
        fav_labs = prefs.getString(getString(R.string.favourite_labs), "");
        progressDialog = new ProgressDialog(BookedTestsActivity.this);
        progressDialog.setMessage("Fetching Labs....");
        progressDialog.show();
        lvLab = (ListView) findViewById(R.id.lvLab);
        txt_no_reports = (TextView) findViewById(R.id.txt_no_reports);
        ll_reports = (LinearLayout) findViewById(R.id.ll_reports);
    }

    public void getLocation() {
        if (Utils.checkPermissionLocation(this)) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        } else {
            LResult = null;
            calculateDistance();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {

        LocationRequest locationRequest = LocationRequest.create()
                .setNumUpdates(1)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(0);


        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Please Wait..");
        pd.show();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, new LocationCallback() {

            @Override
            public void onLocationResult(LocationResult result) {
                pd.dismiss();
                Log.d("result123", result.toString());

                LResult = result.getLastLocation();
                calculateDistance();

//                Toast.makeText(LabListActivity.this, result.getLastLocation().getLatitude() + "", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLocationAvailability(LocationAvailability locationAvailability) {

                //Log.d("available123",locationAvailability.toString());
                //Toast.makeText(LabListActivity.this, String.valueOf(locationAvailability.isLocationAvailable()), Toast.LENGTH_SHORT).show();
                pd.dismiss();

                if (!locationAvailability.isLocationAvailable()) {

                    if (ActivityCompat.checkSelfPermission(BookedTestsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(BookedTestsActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                    if (mLastLocation != null) {

                        LResult = mLastLocation;
                        calculateDistance();
//                        Toast.makeText(LabListActivity.this, "Retrieved Last location", Toast.LENGTH_SHORT).show();

                    } else {
                        LResult = null;
                        calculateDistance();
                        Snackbar.make(lvLab, "Turn on GPS to calculate distance", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    }
                }
            }
        }, null);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utils.MY_PERMISSIONS_REQUEST_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocation();
                } else {
                }
                break;
        }
    }


    public void setToolbar() {
        setTitle("");
        toolbar = (Toolbar) findViewById(R.id.toolbar_image);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("Booked Tests");
        title = mTitle.getText().toString();
        mTitle.setTypeface(Utils.setTypeface(this));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return true;
    }
}
