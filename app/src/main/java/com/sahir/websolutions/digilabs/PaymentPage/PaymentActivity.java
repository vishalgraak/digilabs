package com.sahir.websolutions.digilabs.PaymentPage;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.vision.text.Text;
import com.google.gson.Gson;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.sahir.websolutions.digilabs.Activity.FetchData;
import com.sahir.websolutions.digilabs.Activity.LabDetailsActivity;
import com.sahir.websolutions.digilabs.Activity.LoginActivity;
import com.sahir.websolutions.digilabs.Activity.OTPActivity;
import com.sahir.websolutions.digilabs.Activity.PreviewActivity;
import com.sahir.websolutions.digilabs.Activity.RegisterDoctor;
import com.sahir.websolutions.digilabs.Activity.Urls;
import com.sahir.websolutions.digilabs.Activity.UserAddressActivity;
import com.sahir.websolutions.digilabs.R;
import com.sahir.websolutions.digilabs.database.AddressDao;
import com.sahir.websolutions.digilabs.database.AddressDatabase;
import com.sahir.websolutions.digilabs.database.AddressModel;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class PaymentActivity extends AppCompatActivity implements PaytmPaymentTransactionCallback {
    private ImageView mBackImg;
    private RadioGroup radioGroup;
    private RadioButton mCodRadio, mPaytmRadio;
    private TextView mTestAmountTxt, mDeliveryTxt, mTotalAmountTxt, mConvenTxt;
    SharedPreferences mPrefs;
    private Button mProceedBtn;
    private AddressDatabase mDb;
    private AddressDao mAddressDao;
    private String mTestId, mLabId, mTestTime, mTestDate, mHomeCollection, mUserId, mHcDistance, coll_avail;
    private String mHcCharges, mTestPrice, mTotalAmount, convenience, convenType, calculatedConven;
    ProgressDialog progressDialog;
    private String hcBoxChecked;
    private List<AddressModel> mAddressList;


@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        // Get an Instance of Database class
        try {
            mDb = AddressDatabase.getDatabase(PaymentActivity.this);
            mAddressDao = mDb.addressDao();
            mAddressList = new ArrayList<>();
            mAddressList = mAddressDao.getAll();
            mHcCharges = getIntent().getStringExtra("hc_charges");
            mHcDistance = getIntent().getStringExtra("hc_distance");
            mTestPrice = getIntent().getStringExtra("test_price");
            mTestId = getIntent().getStringExtra("test_id");
            mLabId = getIntent().getStringExtra("lab_id");
            mTestDate = getIntent().getStringExtra("test_date");
            mTestTime = getIntent().getStringExtra("test_time");
            convenience = getIntent().getStringExtra("convenience");
            convenType = getIntent().getStringExtra("convenience_type");
            hcBoxChecked = getIntent().getStringExtra("hcBox");
            mHomeCollection = getIntent().getStringExtra("home_collection");
            mPrefs = getSharedPreferences(getString(R.string.user_details), Context.MODE_PRIVATE);
            mUserId = mPrefs.getString(getString(R.string.user_id), "");
            initView();
            onClick();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initView() {
        mBackImg = (ImageView) findViewById(R.id.ivCancel);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        mCodRadio = (RadioButton) findViewById(R.id.cod_radio);
        mPaytmRadio = (RadioButton) findViewById(R.id.paytm_radio);
        mTestAmountTxt = (TextView) findViewById(R.id.test_amount_txt);
        mTotalAmountTxt = (TextView) findViewById(R.id.total_amount_txt);
        mDeliveryTxt = (TextView) findViewById(R.id.del_amount_txt);
        mConvenTxt = (TextView) findViewById(R.id.conven_charges_txt);
        mProceedBtn = (Button) findViewById(R.id.proceed_btn);
        try {
            mTestAmountTxt.setText("Test Amount: Rs " + mTestPrice);
            mDeliveryTxt.setText("Home Collection: Rs " + mHcCharges);
            calculateConven();
            if (mHomeCollection.equals("yes") && hcBoxChecked.equals("yes")) {
                coll_avail="yes";
                mDeliveryTxt.setVisibility(View.VISIBLE);
                Log.e("failed", "" + mTestPrice);
                Double mInteger = Double.valueOf(mTestPrice) + Double.valueOf(calculatedConven) + Double.valueOf(mHcCharges);
                mTotalAmount = String.valueOf(mInteger);
                if (mTotalAmount.contains(".")) {
                    String convenArray[] = mTotalAmount.split("\\.");
                    mTotalAmount = convenArray[0];
                }
                mTotalAmountTxt.setText("Total Amount: Rs " + mTotalAmount);
            } else {
                coll_avail="no";
                mDeliveryTxt.setVisibility(View.GONE);
                Integer mInteger = Integer.valueOf(mTestPrice) + Integer.valueOf(calculatedConven);
                mTotalAmount = String.valueOf(mInteger);
                if (mTotalAmount.contains(".")) {
                    String convenArray[] = mTotalAmount.split("\\.");
                    mTotalAmount = convenArray[0];
                }
                mTotalAmountTxt.setText("Total Amount: Rs " + mTotalAmount);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void calculateConven() {
        if (convenType.equals("fix")) {
            mConvenTxt.setText("Convenience Charges: Rs " + convenience);
            calculatedConven = convenience;
        } else {
            double convenInt = Double.parseDouble(convenience);
            double mAmtInteger = Double.valueOf(mTestPrice);
            double convenPercent = (mAmtInteger / 100.0f) * convenInt;

            calculatedConven = String.valueOf(convenPercent);
            if (calculatedConven.contains(".")) {
                String convenArray[] = calculatedConven.split("\\.");
                calculatedConven = convenArray[0];
            }
            mConvenTxt.setText("Convenience Charges: Rs " + calculatedConven);
        }
    }

    private void onClick() {
        mProceedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("click two times","");
                sendToServer();
            }
        });
        mBackImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    private void payWithPaytm(String checkSum, String orderId) {
        //getting paytm service
        PaytmPGService Service = PaytmPGService.getStagingService();
        //use this when using for production
        //PaytmPGService Service = PaytmPGService.getProductionService();
        HashMap<String, String> paramMap = new HashMap<String, String>();
        paramMap.put("MID", "furodi29838479620981");
        paramMap.put("ORDER_ID", orderId);
        paramMap.put("CUST_ID", mUserId);
        paramMap.put("INDUSTRY_TYPE_ID", "Retail");
        paramMap.put("CHANNEL_ID", "WAP");
        paramMap.put("TXN_AMOUNT", mTotalAmount);
        paramMap.put("WEBSITE", "APPSTAGING");
        paramMap.put("CALLBACK_URL", "https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp");
        paramMap.put("CHECKSUMHASH", checkSum);
        PaytmOrder Order = new PaytmOrder(paramMap);
        //PaytmPGService.initialize(Order,null);
        //intializing the paytm service
        Service.initialize(Order, null);

        //finally starting the payment transaction
        Service.startPaymentTransaction(this, true, true, this);
    }

    @Override
    public void onTransactionResponse(Bundle bundle) {
        Log.e("rtersponse", "" + bundle.toString());

        String status= bundle.getString("STATUS");
        if(status.equals("TXN_SUCCESS")){
            String txn_id = bundle.getString("TXNID");
            String txn_amount = bundle.getString("TXNAMOUNT");
            String order_id = bundle.getString("ORDERID");
            responseCheckSum(order_id, txn_amount, txn_id);
            Log.e("CAT", txn_id + "," + txn_amount + "," + order_id);
            // confirmOrder(orderId,checkSum,txn_id);
        }else{
            Toast.makeText(PaymentActivity.this,"Transaction Failed. Please Try Again.",Toast.LENGTH_SHORT).show();
        }

        // Toast.makeText(this, bundle.toString(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void networkNotAvailable() {
        Toast.makeText(this, "Network error", Toast.LENGTH_LONG).show();

    }

    @Override
    public void clientAuthenticationFailed(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
        Log.e("failed", "" + s.toString());
    }

    @Override
    public void someUIErrorOccurred(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
        Log.e("ui error", "" + s.toString());
    }

    @Override
    public void onErrorLoadingWebPage(int i, String s, String s1) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
        Log.e("error page", "" + s.toString());
    }

    @Override
    public void onBackPressedCancelTransaction() {
        Toast.makeText(this, "Back Pressed", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onTransactionCancel(String s, Bundle bundle) {
        Toast.makeText(this, s + bundle.toString(), Toast.LENGTH_LONG).show();
        Log.e("back cancel", "" + s.toString());
    }

    public void sendToServer() {
        progressDialog = new ProgressDialog(PaymentActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        Log.e("hit web two times","");
        FetchData.getInstance(PaymentActivity.this).getRequestQueue().add(new StringRequest
                (Request.Method.POST, "\n" +
                        Urls.URL_TO_WEB+ "booktests/make_booking",
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                progressDialog.cancel();
                                final String response2 = response;
                                Log.i("Register Doctor", "" + response2);
                                try {

                                        JSONObject json = new JSONObject(response2);
                                        String status = json.getString("status");
                                        String order_id = json.getString("booking_id");
                                        String check_sum = json.getString("checksum");
                                        Log.e("Get Response", status + "," + order_id + ",,," + check_sum);
                                        if (status.equals("1")) {

                                            //Paytm
                                            payWithPaytm(check_sum, order_id);

                                        } else {
                                            Toast.makeText(PaymentActivity.this, "Server error! Please try again!", Toast.LENGTH_SHORT).show();
                                        }

                                    // Log.i("valuesAre", "" + userId);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                        progressDialog.cancel();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                Log.e("send map two times",""+params);
                params.put("lab_id", mLabId);
                params.put("test_date", mTestDate);
                params.put("fname", mAddressList.get(0).mShippingName);
                params.put("lname", "");
                params.put("email", mAddressList.get(0).mShippingEmail);
                params.put("phone", mAddressList.get(0).mShippingMobile);
                params.put("address", mAddressList.get(0).mShippingAddress);
                params.put("city", mAddressList.get(0).mShippingCity);
                params.put("state", mAddressList.get(0).mShippingState);
                params.put("zip", mAddressList.get(0).mShippingPinCode);
                params.put("time", mTestTime);
                params.put("age", "");
                params.put("referred", "");
                params.put("gender", "");
                params.put("distance", mHcDistance);
                params.put("hc_charges", mHcCharges);
                params.put("conv_charges", calculatedConven);
                params.put("user_id", mUserId);
                params.put("tests", mTestId);
                params.put("price", mTestPrice);
                params.put("home_collection", coll_avail);
                params.put("source", "android");
                Log.e("check details", "" + params.toString());
                return params;
            }
        });
    }

    public void confirmOrder(final String order_id, final String txn_amt, final String txn_id) {
        progressDialog = new ProgressDialog(PaymentActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        FetchData.getInstance(PaymentActivity.this).getRequestQueue().add(new StringRequest
                (Request.Method.POST, "\n" +
                        Urls.URL_TO_WEB+"booktests/update_booking",
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                progressDialog.cancel();
                                final String response2 = response;
                                Log.i("Register Doctor", "" + response2);
                                try {
                                    JSONObject json = new JSONObject(response2);
                                    String status = json.getString("status");
                                    if (status.equals("1")) {
                                        LabDetailsActivity.orderConfirmed = true;
                                        LabDetailsActivity.book_id = order_id;
                                        Log.e("Order Confirmed", ":");
                                        finish();
                                    } else {
                                        Toast.makeText(PaymentActivity.this, "Server error! Please try again!", Toast.LENGTH_SHORT).show();
                                    }

                                    // Log.i("valuesAre", "" + userId);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                        progressDialog.cancel();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                params.put("booking_id", order_id);
                params.put("txn_amount", txn_amt);
                params.put("conv_charges", calculatedConven);
                params.put("txn_id", txn_id);
                params.put("collection_charges", mHcCharges);
                params.put("price", mTestPrice);
                return params;
            }
        });
    }
    public void responseCheckSum(final String order_id,final String txn_amt, final String txn_id) {
        progressDialog = new ProgressDialog(PaymentActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        FetchData.getInstance(PaymentActivity.this).getRequestQueue().add(new StringRequest
                (Request.Method.POST, "\n" +
                        Urls.URL_TO_WEB+"booktests/get_checksum",
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                progressDialog.cancel();
                                final String response2 = response;
                                Log.i("Register Doctor", "" + response2);
                                try {
                                    JSONObject json = new JSONObject(response2);
                                    String responseCheckSum = json.getString("check_sum");
                                    newREsponse(order_id, responseCheckSum, txn_id);
                                    // Log.i("valuesAre", "" + userId);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                        progressDialog.cancel();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                params.put("ORDERID", order_id);

                return params;
            }
        });
    }
   /* public void finalResponse(final String orderId, final String checkSum, final String txn_id) {
        progressDialog = new ProgressDialog(PaymentActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        JSONObject mObject = null;
        JSONObject params = new JSONObject();
        String mid = "furodi29838479620981";
        try {
            params.put("MID", mid);

            params.put("ORDERID", orderId);
            params.put("CHECKSUMHASH", checkSum);
            Log.e("jasom", "" + params.toString());
            String urlencode = URLEncoder.encode(params.toString(), "UTF-8");
            mObject = new JSONObject(urlencode);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        FetchData.getInstance(PaymentActivity.this).getRequestQueue().add(new JsonObjectRequest
                (Request.Method.POST, "\n" +
                        "https://securegw-stage.paytm.in/merchant-status/getTxnStatus", mObject,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                progressDialog.cancel();
                                final String response2 = response;
                                Log.i("Final Response", "" + response2);
                                try {
                                    JSONObject json = new JSONObject(response2);
                                    String status = json.getString("status");
                                    if (status.equals("1")) {
                                        LabDetailsActivity.orderConfirmed = true;
                                        LabDetailsActivity.book_id = orderId;
                                        Log.e("Order Confirmed", ":");
                                        finish();
                                    } else {
                                        Toast.makeText(PaymentActivity.this, "Server error! Please try again!", Toast.LENGTH_SHORT).show();
                                    }

                                    // Log.i("valuesAre", "" + userId);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                        progressDialog.cancel();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                JSONObject params = new JSONObject();
                Map<String, String> param = new Hashtable<String, String>();
                try {


                } catch (Exception e) {
                    e.printStackTrace();
                }


                return param;
            }
        });
    }*/
        private void newREsponse(final String orderId, final String checkSum, final String txn_id){
            progressDialog = new ProgressDialog(PaymentActivity.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            JSONObject params = new JSONObject();
            String mid = "furodi29838479620981";
            try {
                params.put("MID", mid);
                params.put("ORDERID", orderId);
                params.put("CHECKSUMHASH", checkSum);
                String str="MID:furodi29838479620981,ORDERID:"+orderId+",CHECKSUMHASH:"+checkSum;
                Log.e("jasom", "" + params.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }

        RequestQueue queue = FetchData.getInstance(PaymentActivity.this).getRequestQueue();
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST,"https://securegw-stage.paytm.in/merchant-status/getTxnStatus?JsonData=",params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println(response);
                    //    hideProgressDialog();
                        progressDialog.cancel();
                        try {
                            String status= response.getString("STATUS");
                            if(status.equals("TXN_SUCCESS")){

                                confirmOrder(orderId,checkSum,txn_id);
                            }else{
                                Toast.makeText(PaymentActivity.this,"Something went wrong",Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                  //      hideProgressDialog();
                    }
                });
        queue.add(jsObjRequest);
    }
    }

