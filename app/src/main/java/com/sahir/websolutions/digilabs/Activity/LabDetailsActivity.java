package com.sahir.websolutions.digilabs.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.identity.intents.model.UserAddress;
import com.sahir.websolutions.digilabs.HelperClass.Helper;
import com.sahir.websolutions.digilabs.R;
import com.sahir.websolutions.digilabs.objects.BooktestModel;
import com.sahir.websolutions.digilabs.objects.LabModel;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Map;

public class LabDetailsActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    String date1;
    public static String book_id;
    public static boolean orderConfirmed = false;
    LinearLayout llCallBack, lin_total, lin_total1, llLabSort, llLabFavourite, llShowRoute, ll_status, mHcLinear;
    ImageView ivLabImage, ivLabOpen, ivFav, img1, img2, img3, img4;
    TextView tvLabName, tvLabDistance, tvLabPrice, tvLabHomeCollection, tvWorkingHours, tvWorkingDays, tvOpenNow, tvTestName,
            tvBookTest, tvDate, tvCallback, tvAddContact, tvAddFav, tvShowRoute, tvStatus, tvTest, tvTime, tvTestPrice;
    String fav_labs, test_name, lab_id, user_id, myFormat = "yyyy-MM-dd", isHome = "yes", test_price,
            time = "9 AM - 12 PM", startDay, endDay;
    Calendar myCalendar = Calendar.getInstance();
    Toolbar toolbar;
    String mFixCharges, mSelectedDate;
    String labLatitude, labLongitude, homeCollAvailability, convenience, conven_type;
    String hcType;
    SharedPreferences prefs;
    ArrayList<BooktestModel> booktestModelArrayList = new ArrayList<>();
    ProgressDialog progressDialog;
    DatePickerDialog datePickerDialog;
    Dialog book_dialog;
    AlertDialog.Builder builder;
    String[] main_arr = {"6 AM - 9 AM", "9 AM - 12 PM", "12 PM - 3 PM", "3 PM - 6 PM",
            "6 PM - 9 PM"};
    int mile = -1;
    CheckBox ch_home;
    String hcBoxChecked = "no";
    int hits = 0, total_amt1;
    double total_amt, amount;
    ArrayList<Integer> daysList = new ArrayList<>();
    String[] days;
    NavigationView navigationView;
    Activity sActivity;
    String aa1, aa2, home_coll_price, coll_yes = "no";
    int coll_pri = 10;
    TextView tvTotalPrice, tvDiscountPrice, tvTotalPrice1, txt_tot, txt_dis, txt_pay;
    public ArrayList<? extends LabModel> mLabTimeList, mLabHcList;
    private String mTiming, mTestOrderTime, mTestOrderDate;
    private String mPreviousActivity;

    public LabDetailsActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lab_details);
        sActivity = LabDetailsActivity.this;

        init();

        setToolbar();

        setNavDrawer();

        setTvTypeface();

        setData();

        OnClicks();
    }

    public void init() {
        ActivityStack.activity.add(LabDetailsActivity.this);
        txt_tot = (TextView) findViewById(R.id.txt_tot);
        txt_dis = (TextView) findViewById(R.id.txt_dis);
        txt_pay = (TextView) findViewById(R.id.txt_pay);
        mLabTimeList = new ArrayList();
        mLabHcList = new ArrayList();
        tvTotalPrice = (TextView) findViewById(R.id.tvTotalPrice);
        tvDiscountPrice = (TextView) findViewById(R.id.tvDiscountPrice);
        tvTotalPrice1 = (TextView) findViewById(R.id.tvTotalPrice1);
        lin_total = (LinearLayout) findViewById(R.id.lin_total);
        lin_total1 = (LinearLayout) findViewById(R.id.lin_total1);
        progressDialog = new ProgressDialog(LabDetailsActivity.this);
        progressDialog.setMessage("Processing Request....");
        llCallBack = (LinearLayout) findViewById(R.id.llCallBack);
        llLabSort = (LinearLayout) findViewById(R.id.llLabSort);
        llLabFavourite = (LinearLayout) findViewById(R.id.llLabFavourite);
        llShowRoute = (LinearLayout) findViewById(R.id.llShowRoute);
        ll_status = (LinearLayout) findViewById(R.id.ll_status);
        mHcLinear = (LinearLayout) findViewById(R.id.hc_charges_linear);
        ivLabImage = (ImageView) findViewById(R.id.ivLabImage);
        ivLabOpen = (ImageView) findViewById(R.id.ivLabOpen);
        ivFav = (ImageView) findViewById(R.id.ivFav);
        img1 = (ImageView) findViewById(R.id.img1);
        img2 = (ImageView) findViewById(R.id.img2);
        img3 = (ImageView) findViewById(R.id.img3);
        img4 = (ImageView) findViewById(R.id.img4);
        tvLabName = (TextView) findViewById(R.id.tvLabName);
        tvLabDistance = (TextView) findViewById(R.id.tvLabDistance);
        tvLabPrice = (TextView) findViewById(R.id.tvLabPrice);
        tvLabHomeCollection = (TextView) findViewById(R.id.tvLabHomeCollection);
        tvWorkingHours = (TextView) findViewById(R.id.tvWorkingHours);
        tvWorkingDays = (TextView) findViewById(R.id.tvWorkingDays);
        tvOpenNow = (TextView) findViewById(R.id.tvOpenNow);
        tvTestName = (TextView) findViewById(R.id.tvTestName);
        tvBookTest = (TextView) findViewById(R.id.tvBookTest);
        tvCallback = (TextView) findViewById(R.id.tvCallback);
        tvAddContact = (TextView) findViewById(R.id.tvAddContact);
        tvAddFav = (TextView) findViewById(R.id.tvAddFav);
        tvShowRoute = (TextView) findViewById(R.id.tvShowRoute);
        tvStatus = (TextView) findViewById(R.id.tvStatus);
        tvTest = (TextView) findViewById(R.id.tvTest);
        tvTestPrice = (TextView) findViewById(R.id.tvTestPrice);
        prefs = getSharedPreferences(getString(R.string.user_details), Context.MODE_PRIVATE);
        fav_labs = prefs.getString(getString(R.string.favourite_labs), "");
    }

    public void setData() {
        try {
            mTiming = getIntent().getStringExtra("timing");
        } catch (NullPointerException e) {
            Log.w("LabDetailsActivity", e.getLocalizedMessage());
        }

        //Set Lab Details
        if (getIntent().getStringExtra("image_url") == null) {
            ivLabImage.setImageResource(R.drawable.digilabs);
        } else if (getIntent().getStringExtra("image_url").equals("")) {
            ivLabImage.setImageResource(R.drawable.digilabs);
        } else {
            Picasso.with(this).load("http://path.digilabsonline.com//images/Labs/photo/" + getIntent().getStringExtra("image_url")).placeholder(R.drawable.digilabs).
                    into(ivLabImage, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        }
        try {
            mPreviousActivity = getIntent().getStringExtra("type");
            if (mPreviousActivity.equals("test_booked")) {
                book_id = getIntent().getStringExtra("book_id");
                tvBookTest.setText("Cancel Test");
                ll_status.setVisibility(View.VISIBLE);
                tvStatus.setText("Booking request has been sent for " + getIntent().getStringExtra("date"));
                if (getIntent().getStringArrayListExtra("tests").size() > 1) {
                    test_name = android.text.TextUtils.join(",", getIntent().getStringArrayListExtra("tests"));
                    tvTestName.setText(test_name);
                } else {
                    test_name = android.text.TextUtils.join(",", getIntent().getStringArrayListExtra("tests"));
                    if (getIntent().getStringExtra("package").equals("package")) {
                        tvTestName.setText(test_name + " Package");
                    } else {
                        tvTestName.setText(test_name);
                    }
                }
                if (!Utils.isNetworkConnected(LabDetailsActivity.this)) {
                    Utils.conDialog(LabDetailsActivity.this);
                } else {
                    progressDialog.setMessage("Updating....");
                    progressDialog.show();
                    mTestOrderDate = getIntent().getStringExtra("date");
                    mTestOrderTime = getIntent().getStringExtra("time");
                    updateStatus(book_id, getIntent().getStringExtra("date"), getIntent().getStringExtra("time"));
                }
                mFixCharges = getIntent().getStringExtra("fix_del_charges");
                String collDistance = getIntent().getStringExtra("collection_distance");
                mFixCharges = getIntent().getStringExtra("collection_charges");
                Log.e("new Coll Charges", collDistance + "," + mFixCharges);
                tvLabDistance.setText(collDistance +" KM");

            } else {
                test_name = getIntent().getStringExtra("test_name").replace("\n", "");
                if (getIntent().getStringExtra("test_price") != null) {
                    test_price = getIntent().getStringExtra("test_price").replace("\n", "");
                    tvTestPrice.setText(test_price.replace(",", "\n"));
                    Log.e("price==", "" + test_price);
//                total_amt = Double.parseDouble(getIntent().getStringExtra("test_price"));
//                tot = String.valueOf(total_amt);
                    if (Helper.getAddClass(sActivity).equals("addYes")) {
                        tvTestPrice.setVisibility(View.GONE);
                    } else {
                        tvTestPrice.setVisibility(View.VISIBLE);
                    }
                }
                tvTestName.setText(test_name.replace(",", "\n"));
                if (getIntent().getStringExtra("type").equals("package")) {
                    tvBookTest.setText("Book Package");
                }
                homeCollAvailability = getIntent().getStringExtra("homecollection");
                hcType = getIntent().getStringExtra("hc_type");
                convenience = getIntent().getStringExtra("convenience");
                conven_type = getIntent().getStringExtra("convenience_type");
                mLabHcList = getIntent().getParcelableArrayListExtra("hc_list");
                mLabTimeList = getIntent().getParcelableArrayListExtra("timeArray");
                if (getIntent().getFloatExtra("lab_distance", 0) == 0) {
                    tvLabDistance.setText(getIntent().getStringExtra("distance"));
                } else {
                    tvLabDistance.setText(String.format("%.2f", getIntent().getFloatExtra("lab_distance", 0)) + " km");
                }
                mFixCharges = getIntent().getStringExtra("fix_del_charges");
            }

            lab_id = getIntent().getStringExtra("lab_id");
            user_id = prefs.getString(getString(R.string.user_id), null);
            isHome = getIntent().getStringExtra("homecollection");
            tvLabName.setText(getIntent().getStringExtra("lab_name"));
            if (getIntent().getStringExtra("discount") == null) {
                total_amt = Double.parseDouble(getIntent().getStringExtra("lab_price"));
                total_amt1 = (int) total_amt;
                lin_total.setVisibility(View.GONE);
                tvTotalPrice.setText("Rs " + String.valueOf(total_amt1));
            } else if (getIntent().getStringExtra("discount").equals("0")) {
                total_amt = Double.parseDouble(getIntent().getStringExtra("lab_price"));
                total_amt1 = (int) total_amt;
                lin_total.setVisibility(View.GONE);
                tvTotalPrice.setText("Rs " + String.valueOf(total_amt1));
            } else {
                try {
                    double aa = Double.parseDouble(getIntent().getStringExtra("lab_price"));
                    double per = Double.valueOf(getIntent().getStringExtra("discount"));
                    double cal_per = (aa * per) / 100;
                    total_amt = aa - cal_per;
                    int aa1 = (int) aa;
                    int per1 = (int) per;
                    total_amt1 = (int) total_amt;
                    tvTotalPrice.setText("Rs " + String.valueOf(aa1));
                    tvDiscountPrice.setText(String.valueOf(per1) + " %");
                    tvTotalPrice1.setText("Rs " + String.valueOf(total_amt1));
                } catch (Exception e) {

                }
            }

            tvLabPrice.setText("Rs " + String.valueOf(total_amt1));

            labLatitude = getIntent().getStringExtra("latitude");
            labLongitude = getIntent().getStringExtra("longitude");
            home_coll_price = getIntent().getStringExtra("home_collection_price");
            // fixCharges= getIntent().getStringExtra("coll_charges");
            /*if (this.home_coll_price.equals("")) {
                tvLabHomeCollection.setText("Not Available!");
            } else {
                tvLabHomeCollection.setText("Rs " + home_coll_price);
            }
*/

            if (mPreviousActivity.equals("test_booked")) {
                //fix Charges
                tvLabHomeCollection.setText("Rs " + mFixCharges);
            } else {

                if (homeCollAvailability.equals("yes")) {

                    if (hcType.equals("1")) {
                        //According to kms
                        //  hide delivery visibility
                        mHcLinear.setVisibility(View.GONE);
                        tvLabHomeCollection.setText("");

                    } else {
                        mHcLinear.setVisibility(View.VISIBLE);
                        //fix Charges
                        tvLabHomeCollection.setText("Rs " + mFixCharges);

                    }
                } else {
                    //in case of free delivery

                    mHcLinear.setVisibility(View.GONE);

                }
            }
            tvWorkingHours.setText("Working Hours: " + getIntent().getStringExtra("opentime") + " - "
                    + getIntent().getStringExtra("closetime"));

            days = getIntent().getStringExtra("working_days").split(",");
            for (int i = 0; i < days.length; i++) {
                tvWorkingDays.setText("Working Days: " + days[0] + " - " + days[days.length - 1]);
            }

            setTime(getIntent().getStringExtra("opentime"), getIntent().getStringExtra("closetime"), true, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setTime1(String open_time, String close_time, boolean flag, Calendar cal) {
        Date date;

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");

        Calendar now = Calendar.getInstance();
        int day = now.get(Calendar.DAY_OF_WEEK);
        int hour = now.get(Calendar.HOUR);
        int minutes = now.get(Calendar.MINUTE);
        int am = now.get(Calendar.AM_PM);

        String a;

        if (am == Calendar.AM) {
            a = "AM";
        } else {
            a = "PM";
        }

        daysList.clear();
        Log.e("", "");
        for (int i = 0; i < mLabTimeList.size(); i++) {
            if (mLabTimeList.get(i).working_days.equals("Monday")) {
                daysList.add(now.MONDAY);
            } else if (mLabTimeList.get(i).working_days.equals("Tuesday")) {
                daysList.add(now.TUESDAY);
            } else if (mLabTimeList.get(i).working_days.equals("Wednesday")) {
                daysList.add(now.WEDNESDAY);
            } else if (mLabTimeList.get(i).working_days.equals("Thursday")) {
                daysList.add(now.THURSDAY);
            } else if (mLabTimeList.get(i).working_days.equals("Friday")) {
                daysList.add(now.FRIDAY);
            } else if (mLabTimeList.get(i).working_days.equals("Saturday")) {
                daysList.add(now.SATURDAY);
            } else if (mLabTimeList.get(i).working_days.equals("Sunday")) {
                daysList.add(now.SUNDAY);
            }
        }

        try {
            String str_date = simpleDateFormat.format(now.getTime());
            date = simpleDateFormat.parse(str_date);
            Date open = simpleDateFormat.parse(open_time);
            Date close = simpleDateFormat.parse(close_time);

            if (daysList.contains(day)) {
                if (date.before(open)) {
                    ivLabOpen.setImageResource(R.drawable.ic_close);
                    tvOpenNow.setText("Closed");
                } else if (date.after(close)) {
                    ivLabOpen.setImageResource(R.drawable.ic_close);
                    tvOpenNow.setText("Closed");
                } else {
                    ivLabOpen.setImageResource(R.drawable.ic_open);
                    tvOpenNow.setText("Open Now");
                }
            } else {
                ivLabOpen.setImageResource(R.drawable.ic_close);
                tvOpenNow.setText("Closed");
            }
        } catch (Exception e) {
            Log.i("Error", "Date parse error" + e);
        }

        try {
            date = simpleDateFormat.parse(simpleDateFormat.format(cal.getTime()));
            Date d1 = simpleDateFormat.parse(open_time);
            Date d01 = simpleDateFormat.parse("8:59 AM");
            Date d2 = simpleDateFormat.parse("11:59 AM");
            Date d3 = simpleDateFormat.parse("02:59 PM");
            Date d4 = simpleDateFormat.parse("05:59 PM");
            Date d5 = simpleDateFormat.parse("06:00 PM");
            Date d6 = simpleDateFormat.parse(close_time);
            Date d11 = simpleDateFormat.parse("09:01 AM");
            Date d22 = simpleDateFormat.parse("12:01 PM");
            Date d33 = simpleDateFormat.parse("03:01 PM");
            Date d44 = simpleDateFormat.parse("06:01 PM");
            Date d55 = simpleDateFormat.parse("09:01 PM");
            Log.e("check all date", "date=" + date + "," + d1 + "," + d2 + "," + d3 + "," + d4 + "," + d5 + "," + d6 + ",");
            if (flag) {
                String[] arr = {"6 AM - 9 AM", "9 AM - 12 PM", "12 PM - 3 PM", "3 PM - 6 PM",
                        "6 PM - 9 PM"};
                main_arr = arr;

            } else {
                if (date.before(d1)) {
                    if (d6.before(d11)) {
                        main_arr = new String[]{"6 AM - 9 AM"};
                    } else if (d6.before(d22)) {
                        main_arr = new String[]{"6 AM - 9 AM", "9 AM - 12 PM"};
                    } else if (d6.before(d33)) {
                        main_arr = new String[]{"6 AM - 9 AM", "9 AM - 12 PM", "12 PM - 3 PM"};
                    } else if (d6.before(d44)) {
                        main_arr = new String[]{"6 AM - 9 AM", "9 AM - 12 PM", "12 PM - 3 PM", "3 PM - 6 PM"};
                    } else if (d6.before(d55)) {
                        main_arr = new String[]{"6 AM - 9 AM", "9 AM - 12 PM", "12 PM - 3 PM", "3 PM - 6 PM", "6 PM - 9 PM"};
                    }
                } else if (date.before(d2)) {
                    if (d6.before(d22)) {
                        main_arr = new String[]{"9 AM - 12 PM"};
                    } else if (d6.before(d33)) {
                        main_arr = new String[]{"9 AM - 12 PM", "12 PM - 3 PM"};
                    } else if (d6.before(d44)) {
                        main_arr = new String[]{"9 AM - 12 PM", "12 PM - 3 PM", "3 PM - 6 PM"};
                    } else if (d6.before(d55)) {
                        main_arr = new String[]{"9 AM - 12 PM", "12 PM - 3 PM", "3 PM - 6 PM", "6 PM - 9 PM"};
                    }
                } else if (date.before(d3)) {
                    if (d6.before(d33)) {
                        main_arr = new String[]{"12 PM - 3 PM"};
                    } else if (d6.before(d44)) {
                        main_arr = new String[]{"12 PM - 3 PM", "3 PM - 6 PM"};
                    } else if (d6.before(d55)) {
                        main_arr = new String[]{"12 PM - 3 PM", "3 PM - 6 PM", "6 PM - 9 PM"};
                    }
                } else if (date.before(d4)) {
                    if (d6.before(d44)) {
                        main_arr = new String[]{"3 PM - 6 PM"};
                    } else if (d6.before(d55)) {
                        main_arr = new String[]{"3 PM - 6 PM", "6 PM - 9 PM"};
                    }
                } else if (date.after(d5) && date.after(d6)) {
                    main_arr = new String[]{"6 PM - 9 PM"};
                }
            }
        } catch (Exception e2) {
        }


    }

    public void setTime(String open_time, String close_time, boolean flag, Calendar cal) {


        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");

        Calendar now = Calendar.getInstance();
        int day = now.get(Calendar.DAY_OF_WEEK);
        int hour = now.get(Calendar.HOUR);
        int minutes = now.get(Calendar.MINUTE);
        int am = now.get(Calendar.AM_PM);

        String a;

        if (am == Calendar.AM) {
            a = "AM";
        } else {
            a = "PM";
        }

        daysList.clear();

        for (int i = 0; i < days.length; i++) {
            if (days[i].equals("MON")) {
                daysList.add(now.MONDAY);
            } else if (days[i].equals("TUE")) {
                daysList.add(now.TUESDAY);
            } else if (days[i].equals("WED")) {
                daysList.add(now.WEDNESDAY);
            } else if (days[i].equals("THU")) {
                daysList.add(now.THURSDAY);
            } else if (days[i].equals("FRI")) {
                daysList.add(now.FRIDAY);
            } else if (days[i].equals("SAT")) {
                daysList.add(now.SATURDAY);
            } else if (days[i].equals("SUN")) {
                daysList.add(now.SUNDAY);
            }
        }

        try {
            String str_date = simpleDateFormat.format(now.getTime());
            Date date = simpleDateFormat.parse(str_date);
            Date open = simpleDateFormat.parse(open_time);
            Date close = simpleDateFormat.parse(close_time);

            if (daysList.contains(day)) {
                if (date.before(open)) {
                    ivLabOpen.setImageResource(R.drawable.ic_close);
                    tvOpenNow.setText("Closed");
                } else if (date.after(close)) {
                    ivLabOpen.setImageResource(R.drawable.ic_close);
                    tvOpenNow.setText("Closed");
                } else {
                    ivLabOpen.setImageResource(R.drawable.ic_open);
                    tvOpenNow.setText("Open Now");
                }
            } else {
                ivLabOpen.setImageResource(R.drawable.ic_close);
                tvOpenNow.setText("Closed");
            }
        } catch (Exception e) {
            Log.i("Error", "Date parse error" + e);
        }

        if (flag) {
            String[] arr = {"6 AM - 9 AM", "9 AM - 12 PM", "12 PM - 3 PM", "3 PM - 6 PM",
                    "6 PM - 9 PM"};
            main_arr = arr;

        } else {

            try {


                String str_date = simpleDateFormat.format(cal.getTime());
                Date date = simpleDateFormat.parse(str_date);

                Date d1 = simpleDateFormat.parse(open_time);
                Date d2 = simpleDateFormat.parse("11:59 AM");
                Date d3 = simpleDateFormat.parse("02:59 PM");
                Date d4 = simpleDateFormat.parse("05:59 PM");
                Date d5 = simpleDateFormat.parse("06:00 PM");
                Date d6 = simpleDateFormat.parse(close_time);

                if (date.before(d1)) {
                    String[] arr = {"6 AM - 9 AM", "9 AM - 12 PM", "12 PM - 3 PM", "3 PM - 6 PM",
                            "6 PM - 9 PM"};
                    main_arr = arr;
                } else if (date.before(d2)) {
                    String[] arr = {"9 AM - 12 PM", "12 PM - 3 PM", "3 PM - 6 PM",
                            "6 PM - 9 PM"};
                    main_arr = arr;
                } else if (date.before(d3)) {
                    String[] arr = {"12 PM - 3 PM", "3 PM - 6 PM",
                            "6 PM - 9 PM"};
                    main_arr = arr;
                } else if (date.before(d4)) {
                    String[] arr = {"3 PM - 6 PM", "6 PM - 9 PM"};
                    main_arr = arr;
                } else if (date.after(d5) && date.after(d6)) {
                    String[] arr = {"6 PM - 9 PM"};
                    main_arr = arr;
                }
            } catch (Exception e) {
            }
        }


    }


    public void OnClicks() {

        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });

        llCallBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Utils.isNetworkConnected(LabDetailsActivity.this)) {
                    Utils.conDialog(LabDetailsActivity.this);
                } else {
                    requestCallback();
                }
            }
        });

        llLabSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveContact();
            }
        });

        llShowRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMeRoute(getIntent().getStringExtra("latitude"), getIntent().getStringExtra("longitude"));
            }
        });

        tvBookTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tvBookTest.getText().toString().equals("Cancel Test")) {
                    new AlertDialog.Builder(LabDetailsActivity.this)
                            .setMessage("Are you sure you want to send a cancellation request to "
                                    + getIntent().getStringExtra("lab_name"))
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    if (!Utils.isNetworkConnected(LabDetailsActivity.this)) {
                                        Utils.conDialog(LabDetailsActivity.this);
                                    } else {
                                        if (book_id != null) {
                                            progressDialog.show();
                                            String url = Urls.URL_TO_WEB + "booktests/editApi/" + book_id + "?status=canceled";
                                            cancelTest(url);
                                        }
                                    }
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            })
                            .show();
                } else {
                    book_dialog = new Dialog(LabDetailsActivity.this);
                    book_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    book_dialog.setContentView(R.layout.dialog_book_test);
                    book_dialog.show();
                    TextView tvTestName = (TextView) book_dialog.findViewById(R.id.tvTestName);
                    //  final TextView tvHomeDelivery = (TextView) book_dialog.findViewById(R.id.ch_home_delivery);
                    TextView tvTest = (TextView) book_dialog.findViewById(R.id.tvTest);
                    final TextView tvTotalPrice = (TextView) book_dialog.findViewById(R.id.tvTotalPrice);
                    tvTime = (TextView) book_dialog.findViewById(R.id.tvTime);
                    ch_home = (CheckBox) book_dialog.findViewById(R.id.ch_home);

/*if(null!=fixCharges) {
    if (fixCharges.equals("fixed")) {
       ch_home.setText("Home Collection (Rs " + home_coll_price + " Extra)");
        tvHomeDelivery.setVisibility(View.GONE);
    } else {
        ch_home.setText("Home Collection");
        tvHomeDelivery.setText(home_coll_price);
    }
}*/
                    if (mPreviousActivity.equals("test_booked")) {
                        //fix Charges
                        tvLabHomeCollection.setText("Rs " + mFixCharges);
                    } else {
                        if (homeCollAvailability.equals("yes")) {
                            ch_home.setVisibility(View.VISIBLE);
                            if (hcType.equals("1")) {
                                //According to kms
                                //  hide delivery visibility
                                mHcLinear.setVisibility(View.GONE);
                                tvLabHomeCollection.setText("Not Available!");
                                ch_home.setText("Home Collection");
                            } else {
                                mHcLinear.setVisibility(View.VISIBLE);
                                //fix Charges
                                tvLabHomeCollection.setText("Rs " + mFixCharges);
                                ch_home.setText("Home Collection (Rs " + mFixCharges + " Extra)");
                            }
                        } else {
                            /*if home collection is no then check box is hidden and hc box will be no*/
                            hcBoxChecked = "no";
                            ch_home.setVisibility(View.GONE);
                            mHcLinear.setVisibility(View.GONE);

                        }
                    }
                    // tvHomeDelivery.setVisibility(View.GONE);
                    LinearLayout ll_date = (LinearLayout) book_dialog.findViewById(R.id.ll_date);
                    tvDate = (TextView) book_dialog.findViewById(R.id.tvDate);
                    final TextView tvNext = (TextView) book_dialog.findViewById(R.id.tvNext);
                    final TextView tvCancel = (TextView) book_dialog.findViewById(R.id.tvCancel);
                    tvNext.setTypeface(Utils.setTypeface(LabDetailsActivity.this));
                    tvCancel.setTypeface(Utils.setTypeface(LabDetailsActivity.this));
                    tvTestName.setTypeface(Utils.setTypeface(LabDetailsActivity.this));
                    tvTest.setTypeface(Utils.setTypeface(LabDetailsActivity.this));
                    tvDate.setTypeface(Utils.setTypeface(LabDetailsActivity.this));
                    tvTime.setTypeface(Utils.setTypeface(LabDetailsActivity.this));
                    test_name = test_name.replace("\n", "");
//                    tvDate.setText("Select a Day between " + days[0] + " - " + days[days.length - 1]);
                    tvTestName.setText(test_name.replace(",", "\n"));
                    tvTotalPrice.setText("Total Amount - Rs " + total_amt);

                    ch_home.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (ch_home.isChecked()) {
                                if (homeCollAvailability.equals("yes")) {
                                    hcBoxChecked = "yes";
                                    if (!hcType.equals("1")) {

                                        double col = Double.parseDouble(mFixCharges);
                                        tvTotalPrice.setText("Total Amount - Rs " + (col + total_amt));
                                    } else {
                                        tvTotalPrice.setText("Total Amount - Rs " + (total_amt));
                                    }
                                }

                                Log.e("price", tvTotalPrice.getText().toString());
                                coll_pri = 12;
                                coll_yes = "yes";
                                //  tvHomeDelivery.setVisibility(View.VISIBLE);
                            } else {
                                hcBoxChecked = "no";
                                //tvHomeDelivery.setVisibility(View.GONE);
                                tvTotalPrice.setText("Total Amount - Rs " + total_amt);
                                coll_pri = 10;
                                coll_yes = "no";
                            }
                           /* if(ch_home.isChecked()){
                                Intent  mIntent=new Intent(LabDetailsActivity.this, UserAddressActivity.class);
                                mIntent.putExtra("labLatitude",labLatitude);
                                mIntent.putExtra("labLongitude",labLongitude);
                                mIntent.putExtra("homeCollAvail",homeCollAvailability);
                                mIntent.putExtra("hc_type",hcType);
                                mIntent.putExtra("test_price",test_price);
                                mIntent.putExtra("hc_fix_charges",mFixCharges);
                                mIntent.putExtra("lab_id",lab_id);
                                mIntent.putExtra("test_time",lab_id);
                                mIntent.putExtra("test_date",lab_id);
                                mIntent.putExtra("test_id",LabListActivity.test_id);
                                mIntent.putParcelableArrayListExtra("hc_list",mLabHcList);
                                startActivity(mIntent);
                            }else{

                            }*/
                        }
                    });
                    ll_date.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            datePickerDialog = new DatePickerDialog(LabDetailsActivity.this, datePicker, myCalendar
                                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                    myCalendar.get(Calendar.DAY_OF_MONTH));
                            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                            datePickerDialog.show();
                        }
                    });

                    tvNext.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
//                            if (tvDate.getText().toString().contains("Select a Date")) {
//                                Snackbar.make(tvNext, "Please select date", BaseTransientBottomBar.LENGTH_SHORT).show();
                            if (tvDate.getText().toString().equals("")) {
                                Toast.makeText(getApplicationContext(), "Please select date and time", Toast.LENGTH_SHORT).show();
                            } else if (tvTime.getText().toString().equals("")) {
                                Toast.makeText(getApplicationContext(), "Please select time", Toast.LENGTH_SHORT).show();
                            } else {
                                tvDate.setText("Select a Day between " + days[0] + " - " + days[days.length - 1]);
                                if (!Utils.isNetworkConnected(LabDetailsActivity.this)) {
                                    Utils.conDialog(LabDetailsActivity.this);
                                } else {
                                    if (!Utils.isNetworkConnected(LabDetailsActivity.this)) {
                                        Utils.conDialog(LabDetailsActivity.this);
                                    } else {

                                        time.replace("%20", " ");
                                        Log.d("Taggg>>>>>>>>>>>>>>>>", time);
                                        if (getIntent().getStringExtra("type").equals("test")) {
                                            String test_url = null;
                                            try {
                                                test_url = Urls.URL_TO_WEB + "booktests/addapi?test_id=" + LabListActivity.test_id +
                                                        "&user_id=" + user_id + "&bookingtype=test" + "&ishome=" + coll_yes + "&lab_id="
                                                        + lab_id + "&testdate=" + date1 + "&timeslot=" + URLEncoder.encode(time, "UTF-8");
                                            } catch (UnsupportedEncodingException e) {

                                            }

                                            Intent mIntent = new Intent(LabDetailsActivity.this, UserAddressActivity.class);
                                            mIntent.putExtra("labLatitude", labLatitude);
                                            mIntent.putExtra("labLongitude", labLongitude);
                                            mIntent.putExtra("homeCollAvail", homeCollAvailability);
                                            mIntent.putExtra("hc_type", hcType);
                                            mIntent.putExtra("convenience", convenience);
                                            mIntent.putExtra("hcBox", hcBoxChecked);
                                            mIntent.putExtra("convenience_type", conven_type);
                                            mIntent.putExtra("test_price", String.valueOf(total_amt1));
                                            mIntent.putExtra("hc_fix_charges", mFixCharges);
                                            mIntent.putExtra("lab_id", lab_id);
                                            if (ch_home.isChecked()) {
                                                mIntent.putExtra("home_collection", "yes");
                                            } else {
                                                mIntent.putExtra("home_collection", "no");
                                            }
                                            mIntent.putExtra("test_time", time);
                                            mIntent.putExtra("test_date", date1);
                                            mIntent.putExtra("test_id", LabListActivity.test_id);
                                            mIntent.putParcelableArrayListExtra("hc_list", mLabHcList);
                                            startActivity(mIntent);
                                            book_dialog.cancel();
                                            // bookTest(test_url);
                                        } else {
                                            time.replace("%20", " ");
                                            String package_url = null;
                                            try {
                                                package_url = Urls.URL_TO_WEB + "booktests/addPackageApi?packages=" + LabListActivity.test_id +
                                                        "&user_id=" + user_id + "&bookingtype=package&ishome=" + isHome + "&lab_id="
                                                        + lab_id + "&testdate=" + date1 + "&timeslot=" + URLEncoder.encode(time, "UTF-8");
                                            } catch (UnsupportedEncodingException e) {
                                            }
                                            //time.replace(" ", "%20")
                                            // bookTest(package_url);
                                            Intent mIntent = new Intent(LabDetailsActivity.this, UserAddressActivity.class);
                                            mIntent.putExtra("labLatitude", labLatitude);
                                            mIntent.putExtra("labLongitude", labLongitude);
                                            mIntent.putExtra("homeCollAvail", homeCollAvailability);
                                            mIntent.putExtra("hc_type", hcType);
                                            mIntent.putExtra("convenience", convenience);
                                            mIntent.putExtra("hcBox", hcBoxChecked);
                                            mIntent.putExtra("convenience_type", conven_type);
                                            mIntent.putExtra("test_price", String.valueOf(total_amt1));
                                            mIntent.putExtra("hc_fix_charges", mFixCharges);
                                            mIntent.putExtra("lab_id", lab_id);
                                            mIntent.putExtra("test_time", lab_id);
                                            if (ch_home.isChecked()) {
                                                mIntent.putExtra("home_collection", "yes");
                                            } else {
                                                mIntent.putExtra("home_collection", "no");
                                            }
                                            mIntent.putExtra("test_date", lab_id);
                                            mIntent.putExtra("test_id", LabListActivity.test_id);
                                            mIntent.putParcelableArrayListExtra("hc_list", mLabHcList);
                                            startActivity(mIntent);
                                        }
                                    }
                                }
                            }
                        }
                    });

                    tvCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            book_dialog.dismiss();
                        }
                    });
                }
            }
        });
    }

    public void setTvTypeface() {
        tvCallback.setTypeface(Utils.setTypeface(this));
        tvAddContact.setTypeface(Utils.setTypeface(this));
        tvAddFav.setTypeface(Utils.setTypeface(this));
        tvLabName.setTypeface(Utils.setTypeface(this));
        tvLabName.setTypeface(Utils.setTypeface(this));
        tvLabDistance.setTypeface(Utils.setTypeface(this));
        tvLabPrice.setTypeface(Utils.setTypeface(this));
        tvLabHomeCollection.setTypeface(Utils.setTypeface(this));
        tvWorkingHours.setTypeface(Utils.setTypeface(this));
        tvWorkingDays.setTypeface(Utils.setTypeface(this));
        tvOpenNow.setTypeface(Utils.setTypeface(this));
        tvTestName.setTypeface(Utils.setTypeface(this));
        tvBookTest.setTypeface(Utils.setTypeface(this));
        tvShowRoute.setTypeface(Utils.setTypeface(this));
        tvStatus.setTypeface(Utils.setTypeface(this));
        tvTest.setTypeface(Utils.setTypeface(this));
        tvTestPrice.setTypeface(Utils.setTypeface(this));
        txt_tot.setTypeface(Utils.setTypeface(this));
        tvTotalPrice.setTypeface(Utils.setTypeface(this));
        txt_dis.setTypeface(Utils.setTypeface(this));
        tvDiscountPrice.setTypeface(Utils.setTypeface(this));
        txt_pay.setTypeface(Utils.setTypeface(this));
        tvTotalPrice1.setTypeface(Utils.setTypeface(this));
    }

    public void setToolbar() {
        setTitle("");
        toolbar = (Toolbar) findViewById(R.id.toolbar_image);
        setSupportActionBar(toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("Lab Details");
        mTitle.setTypeface(Utils.setTypeface(this));
    }


    public void setNavDrawer() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        TextView tvCopyright = (TextView) findViewById(R.id.tvCopyright);
        tvCopyright.setTypeface(Utils.setTypeface(this));

        Menu m = navigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);
        }
        if (Helper.getCheck(sActivity).equals("doctor")) {
            navigationView.getMenu().setGroupVisible(R.id.group_item5, false);
        } else {
            navigationView.getMenu().setGroupVisible(R.id.group_item5, true);
        }
    }

    private void applyFontToMenuItem(MenuItem mi) {
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", Utils.setTypeface(this)), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void showMeRoute(String latitude, String longitude) {
        String uri = "https://www.google.com/maps/dir/?api=1&destination=" + latitude + "," + longitude + "&travelmode=drive";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        startActivity(intent);
    }


    public void saveContact() {
        //save contact to device
        Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
        intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
        intent.putExtra(ContactsContract.Intents.Insert.PHONE, getIntent().getStringExtra("lab_phone"));
        intent.putExtra(ContactsContract.Intents.Insert.NAME, getIntent().getStringExtra("lab_name"));
        startActivity(intent);
    }

    public void requestCallback() {
        progressDialog.show();
        requestCallback(user_id, lab_id, "Request Callback....");
    }

    public void cancelTest(final String url) {
        FetchData.getInstance(LabDetailsActivity.this).getRequestQueue().add(new StringRequest
                (Request.Method.GET, url,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                progressDialog.dismiss();
                                Log.d("valRes", response);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String status = jsonObject.optString("status");
                                    if (status.equals("1")) {
                                        new AlertDialog.Builder(LabDetailsActivity.this)
                                                .setMessage("Cancel Request Sent.")
                                                .setCancelable(false)
                                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        img1.setImageResource(R.drawable.ic_cancel);
                                                        tvStatus.setText("Cancellation Request Sent.");
                                                        tvBookTest.setVisibility(View.GONE);
                                                        ll_status.setVisibility(View.VISIBLE);
                                                    }
                                                })
                                                .show();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    progressDialog.dismiss();
                                }

                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                        progressDialog.dismiss();
                    }
                }) {
        });
    }

    public void updateStatus(final String id, final String date, final String time) {
        FetchData.getInstance(LabDetailsActivity.this).getRequestQueue().add(new StringRequest
                (Request.Method.GET, Urls.URL_TO_WEB + "booktests/getBookingStatusById/" + id,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                progressDialog.dismiss();
                                Log.d("valRes", response);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String status = jsonObject.optString("status");
                                    if (status.equals("1")) {
                                        JSONObject data = jsonObject.optJSONObject("data");
                                        String book_status = data.optString("status");

                                        checkOrderStatus(book_status, date, time);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    progressDialog.dismiss();
                                }

                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                        progressDialog.dismiss();
                    }
                }) {
        });
    }

    public void checkOrderStatus(String book_status, String date, String time) {

        if (book_status.equals("booked")) {
            tvBookTest.setText("Cancel Test");
            ll_status.setVisibility(View.VISIBLE);
            tvStatus.setText("Booking request has been sent for " + date + " at " + time);
        } else if (book_status.equals("confirm")) {
            img2.setImageResource(R.drawable.ic_done);
            tvBookTest.setVisibility(View.GONE);
            ll_status.setVisibility(View.VISIBLE);
            tvStatus.setText("Booking has been confirmed for " + date + " at " + time);
        } else if (book_status.equals("collected")) {
            img2.setImageResource(R.drawable.ic_done);
            img3.setImageResource(R.drawable.ic_done);
            tvBookTest.setVisibility(View.GONE);
            ll_status.setVisibility(View.VISIBLE);
            tvStatus.setText("Sample Collected.");
        } else if (book_status.equals("delivered")) {
            img2.setImageResource(R.drawable.ic_done);
            img3.setImageResource(R.drawable.ic_done);
            img4.setImageResource(R.drawable.ic_done);
            tvBookTest.setVisibility(View.GONE);
            ll_status.setVisibility(View.VISIBLE);
            tvStatus.setText("Report Prepared.");
        } else if (book_status.equals("completed")) {
            img2.setImageResource(R.drawable.ic_done);
            img3.setImageResource(R.drawable.ic_done);
            img4.setImageResource(R.drawable.ic_done);
            tvBookTest.setVisibility(View.GONE);
            ll_status.setVisibility(View.VISIBLE);
            tvStatus.setText("Report Delivered.");
        } else {
            img1.setImageResource(R.drawable.ic_cancel);
            tvStatus.setText("Cancellation Request Sent.");
            tvBookTest.setVisibility(View.GONE);
            ll_status.setVisibility(View.VISIBLE);
        }

    }

    public void requestCallback1(final String user_id, final String lab_id, final String message) {
        FetchData.getInstance(LabDetailsActivity.this).getRequestQueue().add(new StringRequest
                (Request.Method.POST, Urls.URL_TO_WEB + "notifications/addApi",
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                progressDialog.dismiss();
                                Log.d("valRes", response);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String status = jsonObject.optString("status");
                                    if (status.equals("1")) {
                                        new AlertDialog.Builder(LabDetailsActivity.this)
                                                .setMessage("CallBack Request Sent.")
                                                .setCancelable(false)
                                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.dismiss();
                                                    }
                                                })
                                                .show();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    progressDialog.dismiss();
                                }

                            }
                        }
                        , new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                        progressDialog.dismiss();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", user_id);
                params.put("lab_id", lab_id);
                params.put("message", message);
                return params;
            }
        });

    }

    public void requestCallback(final String user_id, final String lab_id, final String message) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.URL_TO_WEB + "notifications/addApi",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.d("valRes", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.optString("status");
                            if (status.equals("1")) {
                                new AlertDialog.Builder(LabDetailsActivity.this)
                                        .setMessage("CallBack Request Sent.")
                                        .setCancelable(false)
                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.dismiss();
                                            }
                                        })
                                        .show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //You can handle error here if you want

                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", user_id);
                params.put("lab_id", lab_id);
                params.put("message", message);
                return params;

            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void bookTest(final String url) {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //If we are getting success from server
                        Log.e("Total Url===", "" + url);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.optString("status");
                            if (status.equals("1")) {
                                JSONArray data = jsonObject.getJSONArray("data");
                                booktestModelArrayList.clear();
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject jobj = data.getJSONObject(i);
                                    BooktestModel booktestModel = new BooktestModel();
                                    booktestModel.booktest_id = jobj.optString("booktest_id");
                                    book_id = jobj.optString("booktest_id");
                                    booktestModel.user_id = jobj.optString("user_id");
                                    booktestModel.lab_id = jobj.optString("lab_id");
                                    booktestModel.created = jobj.optString("created");
                                    booktestModel.modified = jobj.optString("modified");
                                    booktestModelArrayList.add(booktestModel);
                                }
                                new AlertDialog.Builder(LabDetailsActivity.this)
                                        .setMessage("Booking Request Sent for " + tvDate.getText().toString() + " at " + time)
                                        .setCancelable(false)
                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                book_dialog.dismiss();
                                                tvBookTest.setText("Cancel Test");
                                                ll_status.setVisibility(View.VISIBLE);
                                                progressDialog.dismiss();
                                                tvStatus.setText("Booking request has been sent for " + tvDate.getText().toString() + " at " + time);
                                            }
                                        })
                                        .show();

                            } else {
                                progressDialog.dismiss();
                                // Toast.makeText(getApplicationContext(), "Some Error Occurs Please Try Again", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            progressDialog.dismiss();
                            // Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //You can handle error here if you want
                        progressDialog.dismiss();
                        //  Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {


        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    DatePickerDialog.OnDateSetListener datePicker = new DatePickerDialog.OnDateSetListener() {

        @SuppressLint("LongLogTag")
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            int day = myCalendar.get(Calendar.DAY_OF_WEEK);
            Log.e("message of contain or not", day + ",," + daysList.toString());
            if (daysList.contains(day)) {
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                tvDate.setText(sdf.format(myCalendar.getTime()));
                mSelectedDate = sdf.format(myCalendar.getTime());
                date1 = tvDate.getText().toString();

                boolean flag = true;

                if (myCalendar.getTime().after(Calendar.getInstance().getTime())) {

                    flag = true;
                    // String[] arr = {"6 AM - 9 AM", "9 AM - 12 PM", "12 PM - 3 PM", "3 PM - 6 PM",
                    //"6 PM - 9 PM"};
                    //main_arr = arr;
                } else {
                    flag = false;
                }
                //    setTime(getIntent().getStringExtra("opentime"), getIntent().getStringExtra("closetime"));
                //}


                String dayWeek = myCalendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());

                int k = -1;

                try {
                    JSONArray timingsArray = new JSONArray(mTiming);
                    for (int i = 0; i < timingsArray.length(); i++) {
                        JSONObject days = timingsArray.getJSONObject(i);
                        if (dayWeek.equalsIgnoreCase(days.getString("day"))) {
                            setTime(days.getString("open"), days.getString("close"), flag, myCalendar);

                            k = i;

                            break;
                        }
                     /*   if (dayWeek.equalsIgnoreCase(mLabTimeList.get(i).working_days)) {
                            setTime1(mLabTimeList.get(i).opentime, mLabTimeList.get(i).closetime, flag, myCalendar);

                            k = i;

                            break;
                        }*/
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (view.isShown()) {
                    if (k > -1) {
                        MyDialogSingle(main_arr, "Select Time Slot", mile);
                    } else {
                        //Snackbar.make(view, "Not available!", BaseTransientBottomBar.LENGTH_SHORT).show();
                        Toast.makeText(getApplicationContext(), "Not available! Select other date", Toast.LENGTH_LONG).show();
                    }

                }
            } else {
                Toast.makeText(LabDetailsActivity.this, "Not available!", Toast.LENGTH_SHORT).show();
            }
        }
    };


    //***********************************************My Dialog**************************************
    public void MyDialogSingle(final String[] charSequences, String title, final int value) {

        builder = new AlertDialog.Builder(LabDetailsActivity.this);
        builder.setTitle(title);

        builder.setSingleChoiceItems(charSequences, value,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        mile = which;
                        time = charSequences[which];
                        tvTime.setVisibility(View.VISIBLE);
                        tvTime.setText(time);
                        dialog.dismiss();
                    }
                });

        builder.setNegativeButton("CANCEL",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
        builder.show();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
//        SharedPreferences prefs = getSharedPreferences("string1", MODE_PRIVATE);
//        String restoredText = prefs.getString("user_check", null);
//        if(!restoredText.equals("normal")){
//            navigationView.getMenu().setGroupVisible(R.id.group_item5, false);
//        }else {
//            navigationView.getMenu().setGroupVisible(R.id.group_item5, true);
//        }
        if (id == R.id.nav_home) {
            Intent intent = new Intent(LabDetailsActivity.this, PreviewActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_my_favourites) {
            Intent intent = new Intent(LabDetailsActivity.this, BookedTestsActivity.class);
            startActivity(intent);
            Helper.setAddClass("addNo", sActivity);
        } else if (id == R.id.nav_Profile) {
            if (prefs.getString("role", null).equals("3")) {
                startActivity(new Intent(LabDetailsActivity.this, RegisterDoctor.class)
                        .putExtra("intent", "update"));
            } else {
                startActivity(new Intent(LabDetailsActivity.this, RegisterActivity.class)
                        .putExtra("intent", "update"));
            }
        } else if (id == R.id.nav_share_app) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    "Hey check out my app at: http://bit.ly/2xv0Zi8");
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        } else if (id == R.id.nav_register_lab) {
            startActivity(new Intent(LabDetailsActivity.this, RegisterUserToDoctor.class)
                    .putExtra("user_id", user_id));
//            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.elvektechnologies.com/Digilabs/"));
//            startActivity(browserIntent);
        } else if (id == R.id.nav_rate_app) {
            startActivity(new Intent(LabDetailsActivity.this, FeedbackActivity.class));
        } else if (id == R.id.nav_logout_app) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.clear();
            editor.commit();
            for (int i = 0; i < ActivityStack.activity
                    .size(); i++) {
                (ActivityStack.activity.elementAt(i))
                        .finish();
                Log.e("Activity size:" + ActivityStack.activity.size(), "finished...");
            }
            startActivity(new Intent(LabDetailsActivity.this, RegisterActivity.class).putExtra("intent", "register"));
            finish();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.labOrderStatus = true;
        if (orderConfirmed) {
            orderConfirmed = false;
            if (book_dialog.isShowing()) {
                book_dialog.dismiss();
            }
            tvBookTest.setText("Cancel Test");
            ll_status.setVisibility(View.VISIBLE);
            tvStatus.setText("Booking request has been sent for " + mSelectedDate + " at " + time);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        MyApplication.labOrderStatus = false;
    }

    public class LabReciever extends BroadcastReceiver {
        public LabReciever() {
            super();
        }

        @SuppressLint("LongLogTag")
        @Override
        public void onReceive(Context context, Intent intent) {
            String updateStatus = intent.getStringExtra("action");
            Log.d("Message Notification Body: Brodcast Reciever>>>>>>>>>>", updateStatus);
            checkOrderStatus(updateStatus, mTestOrderDate, mTestOrderTime);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (book_dialog.isShowing()) {
                book_dialog.dismiss();
            }
            tvBookTest.setText("Cancel Test");
            ll_status.setVisibility(View.VISIBLE);
            tvStatus.setText("Booking request has been sent for " + tvDate.getText().toString() + " at " + time);
        }
    }
}
