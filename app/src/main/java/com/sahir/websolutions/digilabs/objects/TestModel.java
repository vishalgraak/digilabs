package com.sahir.websolutions.digilabs.objects;

import java.util.ArrayList;

/**
 * Created by Sys9 on 6/23/2017.
 */

public class TestModel {
    public String cat_id;
    public String cat_name;
    public String name;
    public String cat_icon;
    public String all_ids;
    public boolean checkSwitch;
    public ArrayList<TestModel> sub_cat = new ArrayList<>();
    public ArrayList<TestModel> sub_tests = new ArrayList<>();

}
