package com.sahir.websolutions.digilabs.Activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sahir.websolutions.digilabs.Adapter.DoctorListAdapter;
import com.sahir.websolutions.digilabs.R;
import com.sahir.websolutions.digilabs.objects.DoctorModel;
import com.sahir.websolutions.digilabs.objects.GpsTracker;
import com.sahir.websolutions.digilabs.objects.Report;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ViewShareDoctor extends AppCompatActivity {
    Toolbar toolbar;
    AutoCompleteTextView etSearch;
    ProgressBar progress_bar;
    String reportName, id, type, user_id, lat, lng;
    public static String report_id,lab_report;
    public static String reportType;
    Report report;
    Dialog dialog;
    SharedPreferences prefs;
    ProgressDialog progressDialog;
    ArrayList<DoctorModel> doctorModelArrayList = new ArrayList<>();
    ArrayList<String> doctorSearched;
    ArrayList<String> doctorId;
    ArrayList<String> doctorPhoto;
    GpsTracker gpsTracker;
    DoctorListAdapter doctorListAdapter;
    ListView lvDoctors;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_doctors);

        init();

        setToolbar();

        setTvTypeface();

        onClicks();

        if (Build.VERSION.SDK_INT >= 23) {
            AllowPermissions();
        } else {
            gpsTracker = new GpsTracker(ViewShareDoctor.this);
            if (gpsTracker.canGetLocation()) {
                lat = String.valueOf(gpsTracker.latitude);
                lng = String.valueOf(gpsTracker.longitude);
            } else {
                showGPSDisabledAlertToUser();
            }
        }

    }

    private void onClicks() {
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String query = s.toString();
                doctorSearched = new ArrayList<>();
                doctorId = new ArrayList<>();
                doctorPhoto = new ArrayList<>();
                if (query.length() > 1) {
                    getDoctors(query, lat, lng);
                } else {
                    lvDoctors.setVisibility(View.GONE);
                }
            }
        });
    }

    private void init() {
        ActivityStack.activity.add(ViewShareDoctor.this);
        prefs = getSharedPreferences(getString(R.string.user_details), Context.MODE_PRIVATE);
        Intent intent = getIntent();
        report = (Report) intent.getSerializableExtra("Report");
        lab_report = report.getLab_report();
        reportType = report.report_type;
        reportName = report.getName();
        report_id = report.getId();
        user_id = report.getUser_id();
        type = report.getType();

        progressDialog = new ProgressDialog(ViewShareDoctor.this);

        etSearch = (AutoCompleteTextView) findViewById(R.id.etSearch);
        lvDoctors = (ListView) findViewById(R.id.lvDoctors);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
    }

    public void getDoctors(final String query, String lat, String lng) {
        FetchData.getInstance(ViewShareDoctor.this).getRequestQueue().add(new StringRequest
                (Request.Method.GET, Urls.URL_TO_WEB + "users/searchDoctorapi?&str=" + query
                        + "&latitude=" + lat + "&longitude=" + lng,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                Log.i("ViewShareDoctor", "Get doctors: " + response);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String status = jsonObject.optString("status");
                                    String base_url = jsonObject.optString("message");
                                    if (status.equals("1")) {
                                        doctorModelArrayList.clear();
                                        JSONArray jsonArray = jsonObject.optJSONArray("data");
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            JSONObject jobj = jsonArray.optJSONObject(i);
                                            DoctorModel doctorModel = new DoctorModel();
                                            doctorModel.id = jobj.optString("doctorId");
                                            doctorModel.firstname = jobj.optString("firstname");
                                            doctorModel.lastname = jobj.optString("lastname");
                                            if (jobj.optString("lastname").equals("")) {
                                                doctorModel.username = jobj.optString("firstname");
                                            } else {
                                                doctorModel.username = jobj.optString("firstname") + " " + jobj.optString("lastname");
                                            }
                                            doctorModel.pin = jobj.optString("pin");
                                            doctorModel.photo = base_url + jobj.optString("photo");
                                            JSONArray clinic = jobj.optJSONArray("clinic");
                                            for (int j = 0; j < clinic.length(); j++) {
                                                JSONObject jsonObject1 = clinic.optJSONObject(j);
                                                doctorModel.clinicid = jsonObject1.optString("clinicid");
                                                doctorModel.clinicname = jsonObject1.optString("clinicname");
                                                doctorModel.city = jsonObject1.optString("city");
                                            }
                                            doctorModelArrayList.add(doctorModel);
                                        }
                                        lvDoctors.setVisibility(View.VISIBLE);
                                        doctorListAdapter = new DoctorListAdapter(ViewShareDoctor.this, doctorModelArrayList);
                                        lvDoctors.setAdapter(doctorListAdapter);
                                    } else {
                                        lvDoctors.setVisibility(View.GONE);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Log.e("CAT", error.toString());
                    }
                }) {
        });
    }

    public void setTvTypeface() {
        etSearch.setTypeface(Utils.setTypeface(this));
    }

    public void setToolbar() {
        setTitle("");
        toolbar = (Toolbar) findViewById(R.id.toolbar_image);
        setSupportActionBar(toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("Share Report");
        mTitle.setTypeface(Utils.setTypeface(this));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void AllowPermissions() {
        int hasLocationPermission = ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION);
        int hasSMSPermission = ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> permissions = new ArrayList<String>();
        if (hasLocationPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        }

        if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
        }

        if (!permissions.isEmpty()) {
            ActivityCompat.requestPermissions(ViewShareDoctor.this, permissions.toArray(new String[permissions.size()]), 101);
        } else {
            gpsTracker = new GpsTracker(ViewShareDoctor.this);
            if (gpsTracker.canGetLocation()) {
                lat = String.valueOf(gpsTracker.latitude);
                lng = String.valueOf(gpsTracker.longitude);
            } else {
                showGPSDisabledAlertToUser();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 101: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        gpsTracker = new GpsTracker(ViewShareDoctor.this);
                        if (gpsTracker.canGetLocation()) {
                            lat = String.valueOf(gpsTracker.latitude);
                            lng = String.valueOf(gpsTracker.longitude);
                        } else {
                            showGPSDisabledAlertToUser();
                        }
                        return;
                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i]);
//                        finish();
                    }
                }
            }
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    //-------------------------Check GPS LOCATION is enable or not
    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ViewShareDoctor.this);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Go to Settings Page To Enable GPS",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                                finish();
                            }
                        });
        alertDialogBuilder.setNegativeButton(android.R.string.cancel,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return true;
    }
}
