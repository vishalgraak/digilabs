package com.sahir.websolutions.digilabs.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sahir.websolutions.digilabs.Activity.FetchData;
import com.sahir.websolutions.digilabs.R;
import com.sahir.websolutions.digilabs.Activity.Urls;
import com.sahir.websolutions.digilabs.Activity.Utils;
import com.sahir.websolutions.digilabs.Activity.ViewShareDoctor;
import com.sahir.websolutions.digilabs.objects.DoctorModel;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import me.philio.pinentry.PinEntryView;

/**
 * Created by Sys9 on 7/24/2017.
 */

public class DoctorListAdapter extends ArrayAdapter<DoctorModel> {
    Context context;
    ArrayList<DoctorModel> data;
    ImageView ivLabImage;
    TextView tvDocName, tvAddress;
    LinearLayout llLab;
    Dialog dialog;
    ProgressDialog progressDialog;
    String time, date, user_id;
    SharedPreferences prefs;
    String test_id,my_id;
    public DoctorListAdapter(final Context context, ArrayList<DoctorModel> data) {
        super(context, R.layout.doctor_list_row, data);
        this.context = context;
        this.data = data;
        progressDialog = new ProgressDialog(context);
        prefs = context.getSharedPreferences(context.getString(R.string.user_details), Context.MODE_PRIVATE);
        final Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm a");
        time = sdf.format(c.getTime()).replace(" ", "%20");

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        date = df.format(c.getTime());
        user_id = prefs.getString(context.getString(R.string.user_id), null);

        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                ((Activity) context).finish();
            }
        });
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final DoctorModel model = data.get(position);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.doctor_list_row, parent, false);
        }

        ivLabImage = (ImageView) convertView.findViewById(R.id.ivLabImage);
        tvDocName = (TextView) convertView.findViewById(R.id.tvDocName);
        tvAddress = (TextView) convertView.findViewById(R.id.tvAddress);
        llLab = (LinearLayout) convertView.findViewById(R.id.llLab);

        tvDocName.setText(model.username);

        if (model.clinicname.equals("")) {
            tvAddress.setText(model.city);
        } else {
            tvAddress.setText(model.clinicname + ", " + model.city);
        }

        if (!model.photo.equals("")) {
            Picasso.with(context).load(model.photo).resize(100, 100).placeholder(R.drawable.avatar).
                    into(ivLabImage, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {

                        }
                    });
        } else {
            ivLabImage.setImageResource(R.drawable.avatar);
        }

        llLab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(model.id, user_id, model.pin, time);
            }
        });

        setTvTypeface();

        return convertView;
    }

    public void showDialog(final String doctorid, final String user_id, final String doctor_pin, final String time) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_doctor_pin);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        if (height >= 800) {
            lp.height = height / 2;
        } else if (height >= 1280) {
            lp.height = height / 2;
        } else if (height >= 1920) {
            lp.height = height / 2;
        } else if (height >= 2560) {
            lp.height = height / 2;
        }
        dialog.getWindow().setAttributes(lp);
        dialog.show();

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                ((Activity) context).finish();
            }
        });

        final PinEntryView pinEntryView = (PinEntryView) dialog.findViewById(R.id.pin_entry_simple);
        pinEntryView.setOnPinEnteredListener(new PinEntryView.OnPinEnteredListener() {
            @Override
            public void onPinEntered(String pin) {
                if (doctor_pin.equals(pin)) {
                    dialog.dismiss();
                    progressDialog.setMessage("Sharing Report...");
                    progressDialog.show();

                    try {
                        String re = ViewShareDoctor.lab_report;

                    String a[] = re.split(",");
                    String lab_or_my_id = a[0];
                    String diff = a[1];
                    if (diff.equals("a")) {
                        test_id = lab_or_my_id;
                        my_id = "";
                    } else {
                        test_id = "";
                        my_id = lab_or_my_id;
                    }
                }catch(Exception e){}
                    sharetodoctor(doctorid, user_id, my_id, time, test_id);
                  //  sharetodoctor(doctorid, user_id, ViewShareDoctor.report_id, time,);
                } else {
                    pinEntryView.clearText();
                    Snackbar.make(pinEntryView, "Please enter correct pin to view reports", BaseTransientBottomBar.LENGTH_SHORT).show();
                }
            }
        });

        TextView tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                ((Activity) context).finish();
            }
        });

    }

    public void sharetodoctor(final String doctor_id, final String user_id, final String report_id, final String time, final String test_report) {

        String url="";

        if (ViewShareDoctor.reportType.equals("my"))
        {
             url = Urls.URL_TO_WEB + "reportshares/addApi/" + doctor_id + "/?user_id="

                    //labreport_id
                    + user_id + "&myreport_id=" + report_id +"&testreport_id="+test_report+"&sharetime=" + time;
        }
        else if (ViewShareDoctor.reportType.equals("lab")){
          url = Urls.URL_TO_WEB + "reportshares/addApi/" + doctor_id + "/?user_id="

                    //labreport_id
                    + user_id + "&labreport_id=" + report_id +"&testreport_id="+test_report+"&sharetime=" + time;
        }
        FetchData.getInstance(context).getRequestQueue().add(new StringRequest
                (Request.Method.GET, url,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                progressDialog.dismiss();
                                final String response2 = response;
                                Log.i("valRes", "" + response2);
                                try {
                                    JSONObject json = new JSONObject(response2);
                                    int status = json.getInt("status");
                                    if (status == 1) {
                                        new AlertDialog.Builder(context)
                                                .setMessage("Report Shared with doctor.")
                                                .setCancelable(false)
                                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        ((Activity) context).finish();
                                                    }
                                                })
                                                .show();
                                    } else {

                                    }
                                } catch (JSONException e) {
                                    progressDialog.dismiss();
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("CAT", error.toString());
                    }
                }) {
        });
    }

    public void setTvTypeface() {
        tvDocName.setTypeface(Utils.setTypeface(context));
        tvAddress.setTypeface(Utils.setTypeface(context));
    }
}
