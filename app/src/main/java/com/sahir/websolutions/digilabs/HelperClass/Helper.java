package com.sahir.websolutions.digilabs.HelperClass;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Sys9 on 9/6/2017.
 */

public class Helper {
    public static void  setPatient(String Patient,Context sActivity){
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("Patient",Patient);
        editor.commit();
    }
    public static String getPatient(Context sActivity){
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        return pref.getString("Patient","");
    }
    public static void  setCheck(String Check,Activity sActivity){
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("Check",Check);
        editor.commit();
    }
    public static String getCheck(Activity sActivity){
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        return pref.getString("Check","");
    }
    public static void  setImage(String Image,Activity sActivity){
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("Image",Image);
        editor.commit();
    }
    public static String getImage(Activity sActivity){
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        return pref.getString("Image","");
    }
    public static void  setSkip(String Skip,Activity sActivity){
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("Skip",Skip);
        editor.commit();
    }
    public static String getSkip(Activity sActivity){
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        return pref.getString("Skip","");
    }
    public static void  setAddClass(String AddClass,Activity sActivity){
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("AddClass",AddClass);
        editor.commit();
    }
    public static String getAddClass(Activity sActivity){
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        return pref.getString("AddClass","");
    }
    public static void  setFirstName(String FirstName,Activity sActivity){
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("FirstName",FirstName);
        editor.commit();
    }
    public static String getFirstName(Activity sActivity){
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        return pref.getString("FirstName","");
    }
    public static void  setLastName(String LastName,Activity sActivity){
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("LastName",LastName);
        editor.commit();
    }
    public static String getLastName(Activity sActivity){
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        return pref.getString("LastName","");
    }
    public static void  setEmail(String Email,Activity sActivity){
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("Email",Email);
        editor.commit();
    }
    public static String getEmail(Activity sActivity){
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        return pref.getString("Email","");
    }
    public static void  setPhone(String Phone,Activity sActivity){
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("Phone",Phone);
        editor.commit();
    }
    public static String getPhone(Activity sActivity){
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        return pref.getString("Phone","");
    }
    public String img;
    public Helper(String img) {
        this.img = img;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
