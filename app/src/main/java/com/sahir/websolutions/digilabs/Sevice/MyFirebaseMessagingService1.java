package com.sahir.websolutions.digilabs.Sevice;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.sahir.websolutions.digilabs.Activity.BookedTestsActivity;
import com.sahir.websolutions.digilabs.Activity.LabDetailsActivity;
import com.sahir.websolutions.digilabs.Activity.MyApplication;
import com.sahir.websolutions.digilabs.Activity.PatientsReportActivity;
import com.sahir.websolutions.digilabs.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by safal bhatia on 5/31/2017.
 */
public  class MyFirebaseMessagingService1 extends FirebaseMessagingService {
    public MyFirebaseMessagingService1() {

    }
    private static final String TAG = "MyFirebaseMsgService";
    private Intent intent;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            sendNotification(remoteMessage);
        }
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            sendNotification(remoteMessage);
        }
    }

    private void sendNotification(RemoteMessage remoteMessage) {

            String msgBody = remoteMessage.getData().get("body");
            String msgType = remoteMessage.getData().get("type");
            String msgTitle = remoteMessage.getData().get("title");
            if (msgType.equals("booked")) {
                intent = new Intent(this, BookedTestsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            } else if (msgType.equals("confirmed") || msgType.equals("collected")) {
                intent = new Intent(this, LabDetailsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            } else if (msgType.equals("reported")) {
                intent = new Intent(this, PatientsReportActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            }if(MyApplication.labOrderStatus) {
            Log.d(TAG, "Message Notification Body: Brodcast Reciever" +msgType);
            Intent intent = new Intent(getApplicationContext(), LabDetailsActivity.LabReciever.class);
        //    intent.setAction("com.sahir.websolutions.digilabs.BroadcastReciever");
            intent.putExtra("action", msgType);
            sendBroadcast(intent);
        }

            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(msgTitle)
                    .setContentText(msgBody)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationBuilder.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());

       /* }catch(Exception e){
            e.printStackTrace();
        }*/
    }
}