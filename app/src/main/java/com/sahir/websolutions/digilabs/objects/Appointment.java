package com.sahir.websolutions.digilabs.objects;

import java.util.Date;

/**
 * Created by SWS-PC10 on 5/31/2017.
 */

public class Appointment {

    private int id;
    private int rating;
    private Date date;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



}
